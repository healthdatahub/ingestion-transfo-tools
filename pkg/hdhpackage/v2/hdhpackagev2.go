package v2

import (
	"archive/zip"
	"context"
	"encoding/json"
	"fmt"
	"io"

	hdhlog "rdd-pipeline/pkg/log"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"golang.org/x/sync/errgroup"
)

const (
	DataFileName         = "data"
	Version              = "v2"
	CompressionExtension = ".zip"
)

type HDHPackageV2 struct {
	*worker.Blob
	FileConfig *v1alpha1.HashFileConfig
	Data       io.Reader
}

func New(ctx context.Context, r io.ReaderAt, blob *worker.Blob) (*HDHPackageV2, error) {
	log := hdhlog.FromContext(ctx)

	config := new(v1alpha1.HashFileConfig)

	if hashConfig, ok := blob.Metadata()[v1alpha1.MetadataHashConfig]; ok {
		if err := json.Unmarshal([]byte(hashConfig), config); err != nil {
			return nil, fmt.Errorf("can't parse config: %w", err)
		}
	}

	delete(blob.Metadata(), v1alpha1.MetadataOriginalPath)

	hdhfile := &HDHPackageV2{
		Blob:       blob,
		FileConfig: config,
	}

	zipr, err := zip.NewReader(r, blob.Size())
	if err != nil {
		return nil, err
	}

	for _, f := range zipr.File {
		switch f.Name {
		case DataFileName:
			rc, err := f.Open()
			if err != nil {
				return nil, err
			}
			hdhfile.Data = rc
		default:
			log.Info("unused file in archive", "fileName", f.Name)
		}
	}

	return hdhfile, nil
}

func (p *HDHPackageV2) Read(b []byte) (n int, err error) {
	return p.Data.Read(b)
}

func (p *HDHPackageV2) Config() *v1alpha1.HashFileConfig {
	return p.FileConfig
}

func (p *HDHPackageV2) zip(buf io.Writer) (int64, error) {

	w := zip.NewWriter(buf)
	defer w.Close()

	var n int64

	dataf, err := w.Create(DataFileName)
	if err != nil {
		return n, err
	}

	nData, err := io.CopyBuffer(dataf, p.Data, nil)
	n += nData
	if err != nil {
		return n, err
	}

	return n, w.Flush()
}

func (p *HDHPackageV2) Upload(ctx context.Context, cpk *blob.CPKInfo) error {
	jconf, err := json.Marshal(p.Config())
	if err != nil {
		return fmt.Errorf("fail to marshall config: %w", err)
	}

	p.Blob.Metadata()[v1alpha1.MetadataHashConfig] = string(jconf)
	delete(p.Blob.Metadata(), v1alpha1.MetadataOriginalPath)

	rOut, wOut := io.Pipe()

	g, gctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		_, err := p.zip(wOut)
		wOut.Close()
		return err
	})

	g.Go(func() error {
		defer rOut.Close()
		if err := p.Blob.Upload(gctx, rOut, cpk); err != nil {
			return fmt.Errorf("can't upload package: %w", err)
		}
		return nil
	})
	return g.Wait()
}

func (p *HDHPackageV2) Version() string {
	return Version
}

func (p *HDHPackageV2) Type() string {
	return p.Blob.Metadata()[v1alpha1.MetadataBlobType]
}

func (p *HDHPackageV2) Clone(ctx context.Context, container *container.Client, blobName string, r io.Reader) interface{} {
	return &HDHPackageV2{
		Blob:       p.Blob.Clone(ctx, container, blobName),
		FileConfig: p.FileConfig,
		Data:       r,
	}
}
