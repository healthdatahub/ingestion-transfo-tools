package hdhpackage

import (
	"compress/gzip"
	"context"
	"errors"
	"fmt"
	"io"

	v1 "rdd-pipeline/pkg/hdhpackage/v1"
	v2 "rdd-pipeline/pkg/hdhpackage/v2"
	v3 "rdd-pipeline/pkg/hdhpackage/v3"
	"rdd-pipeline/pkg/worker"

	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"github.com/guilhem/chunkreaderat"
)

var ErrNoVersion = errors.New("no package version found")

func NewPackageFromBlob(ctx context.Context, blob *worker.Blob, cpk *blob.CPKInfo) (HDHPackage, error) {

	log := hdhlog.FromContext(ctx)
	var err error
	var chunkreader io.ReaderAt

	version := ""
	if blob.Metadata()[v1alpha1.MetadataVersion] != "" {
		version = blob.Metadata()[v1alpha1.MetadataVersion]
	}
	var p HDHPackage
	switch version {
	case v1.Version:
		log.Info("deserialize package v1", "size", blob.Size(), "meta", blob.Metadata())
		// zip requires a readerAt to decompress the file
		bReadAt := blob.BuildReaderAt(cpk)
		chunkreader, err = chunkreaderat.NewChunkReaderAt(bReadAt, 1024*4096)
		if err != nil {
			return nil, fmt.Errorf("can't create chunk reader: %w", err)
		}
		p, err = v1.New(ctx, chunkreader, blob)
		if err != nil {
			return nil, fmt.Errorf("can't create HDHPackageV1: %w", err)
		}

	case v2.Version:
		log.Info("deserialize package v2", "size", blob.Size(), "meta", blob.Metadata())
		// zip requires a readerAt to decompress the file
		bReadAt := blob.BuildReaderAt(cpk)
		chunkreader, err = chunkreaderat.NewChunkReaderAt(bReadAt, 1024*4096)
		if err != nil {
			return nil, fmt.Errorf("can't create chunk reader: %w", err)
		}
		p, err = v2.New(ctx, chunkreader, blob)
		if err != nil {
			return nil, fmt.Errorf("can't create HDHPackageV2: %w", err)
		}
	case v3.Version:
		fallthrough

	default: // by default we try to deserialize as a V3
		log.Info("deserialize package", "v3", v3.Version, "meta", blob.Metadata())

		var reader io.ReadCloser

		reader, _, err := blob.Download(ctx, cpk)
		if err != nil {
			return nil, fmt.Errorf("can't get reader: %w", err)
		}

		log.Info("create new package v3", "size", blob.Size(), "meta", blob.Metadata())

		blob.Metadata()[v1alpha1.MetadataVersion] = v3.Version

		if blob.Metadata()[v1alpha1.MetadataPlatformCompressionType] != v1alpha1.CompressionTypeNone {
			reader, err = gzip.NewReader(reader)
		}
		if err != nil {
			return nil, err
		}
		p, err = v3.New(ctx, reader, blob)
		if err != nil {
			return nil, fmt.Errorf("can't create HDHPackageV3: %w", err)
		}
	}
	return p, nil
}

type HDHPackage interface {
	io.Reader
	Metadata() map[string]string
	Config() *v1alpha1.HashFileConfig
	Upload(context.Context, *blob.CPKInfo) error
	Delete(context.Context) error
	Version() string
	Type() string
	Name() string
	AcquireLease(ctx context.Context) error
	ReleaseLease(ctx context.Context) error
	Clone(context.Context, *container.Client, string, io.Reader) interface{}
	Size() int64
}
