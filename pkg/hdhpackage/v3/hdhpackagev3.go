package v3

import (
	"compress/gzip"
	"context"
	"encoding/json"
	"fmt"
	"io"

	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"golang.org/x/sync/errgroup"
)

const (
	Version              = "v3"
	CompressionExtension = ".gz"
)

type HDHPackageV3 struct {
	*worker.Blob
	FileConfig *v1alpha1.HashFileConfig
	Data       io.Reader
}

func New(_ context.Context, r io.Reader, blob *worker.Blob) (*HDHPackageV3, error) {
	config := new(v1alpha1.HashFileConfig)
	if hashConfig, ok := blob.Metadata()[v1alpha1.MetadataHashConfig]; ok {
		if err := json.Unmarshal([]byte(hashConfig), config); err != nil {
			return nil, fmt.Errorf("can't parse config: %w", err)
		}
	}
	delete(blob.Metadata(), v1alpha1.MetadataOriginalPath)
	hdhfile := &HDHPackageV3{
		Blob:       blob,
		FileConfig: config,
		Data:       r,
	}
	return hdhfile, nil
}

func (p *HDHPackageV3) Read(b []byte) (n int, err error) {
	return p.Data.Read(b)
}

func (p *HDHPackageV3) Config() *v1alpha1.HashFileConfig {
	return p.FileConfig
}

func (p *HDHPackageV3) Upload(ctx context.Context, cpk *blob.CPKInfo) error {
	jconf, err := json.Marshal(p.Config())
	if err != nil {
		return fmt.Errorf("fail to marshall config: %w", err)
	}

	p.Blob.Metadata()[v1alpha1.MetadataHashConfig] = string(jconf)

	// gzip writer cannot directly work on io.Reader types
	// it is only able to transform a writer into a gziped writer.
	// As we need to convert a reader into gziped reader, we need a pipe.
	// the chain looks like p.data -> wc -> pipeW -> pipeR -> upload
	pipeR, pipeW := io.Pipe()
	defer pipeR.Close()

	g, gctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		if err := p.Blob.Upload(gctx, pipeR, cpk); err != nil {
			return fmt.Errorf("can't upload package: %w", err)
		}
		return nil
	})

	g.Go(func() error {
		defer pipeW.Close() // ends the p.Blob.Upload()

		var wc io.WriteCloser = pipeW
		if p.Blob.Metadata()[v1alpha1.MetadataPlatformCompressionType] != v1alpha1.CompressionTypeNone {
			wc = gzip.NewWriter(pipeW)
		}
		if _, err := io.Copy(wc, p.Data); err != nil {
			return fmt.Errorf("can't copy from blob reader: %w", err)
		}
		defer wc.Close()
		return nil
	})

	return g.Wait()
}

func (p *HDHPackageV3) Version() string {
	return Version
}

func (p *HDHPackageV3) Type() string {
	return p.Blob.Metadata()[v1alpha1.MetadataBlobType]
}

func (p *HDHPackageV3) Clone(ctx context.Context, container *container.Client, blobName string, r io.Reader) interface{} {
	return &HDHPackageV3{
		Blob:       p.Blob.Clone(ctx, container, blobName),
		FileConfig: p.FileConfig,
		Data:       r,
	}
}

func (p *HDHPackageV3) Metadata() map[string]string {
	return p.Blob.Metadata()
}
