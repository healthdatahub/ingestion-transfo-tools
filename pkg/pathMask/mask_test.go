package pathMask_test

import (
	"os"
	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/pathMask"
	"testing"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/stretchr/testify/assert"
)

const hmackey = "a0ad86477f3aca0cdfc8913b20e733e196b46b03bc3f2dc831539f21022b0e8a91900057541e79373d876523c3dd6cbcff163c1ed8b1e3e6785bc4f402d18ac915f0a1a18eb0cb76e53303d36bab59e922425b6b97a98ba1d595c74d88ee895c6ec9f93d87e3dbc156ef4653ac2ee8219a4e468d5c2b4f00165c2bd3e4a31b61fcf682739a85c83a50455d6a5d9298073b3227f65d27ccb0cf4970fa8c9669c5e5de6b3f80e533424dd3fd813236704adf84728e3b6b646046457ce60fb726c450b36491064142046c0677b937704c92f20fbb35b6dddf28805ff7213c61afd1817089945158341ef5bb38b7d482e6a2f328613b720421aedf911d1b3cd20a61"
const aesKey = "4145533235364B65792D33324368617261637465727331323334353637383930"

func TestMaskPath(t *testing.T) {

	tests := []struct {
		name string

		blobname    string
		separator   string
		depthLevels []int

		encryptionType string
		key            string

		ignoreValues []string

		useLocalFile bool

		want                     string
		expectedCT               masquage.CorrespondenceTable
		expectedLocalFileContent string
	}{
		{
			name:        "a blobname without extension",
			blobname:    "patient-martin-dumont", // here "martin" and "dumont" is sensitive information
			separator:   "-",
			depthLevels: []int{2, 3},

			encryptionType: "aes-siv",
			key:            aesKey,

			want: "patient-prefix:11eacbf61c41489bfb8284196ea4967a3805e0d41284-prefix:18fed4ed1b5b1068d45b01f444ce8fc1784a3e114875",
		},
		{
			name:        "a blobname with two extensions",
			blobname:    "patient-martin-dumont.zip.gpg",
			separator:   "-",
			depthLevels: []int{2, 3},

			encryptionType: "aes-siv",
			key:            aesKey,

			want: "patient-prefix:11eacbf61c41489bfb8284196ea4967a3805e0d41284-prefix:18fed4ed1b5b1068d45b01f444ce8fc1784a3e114875.zip.gpg",
		},
		{
			name:        "a blobname without a third extension",
			blobname:    "patient-martin-dumont.csv.zip.gpg",
			separator:   "-",
			depthLevels: []int{2, 3},

			encryptionType: "aes-siv",
			key:            aesKey,

			want: "patient-prefix:11eacbf61c41489bfb8284196ea4967a3805e0d41284-prefix:18fed4ed1b5b1068d45b01f444ce8fc1784a3e114875.csv.zip.gpg",
		},
		{
			name:        "non-existing path levels are ignored",
			blobname:    "patient-martin-dumont.csv.zip.gpg",
			separator:   "-",
			depthLevels: []int{0, 2, 3, 14},

			encryptionType: "aes-siv",
			key:            aesKey,

			want: "patient-prefix:11eacbf61c41489bfb8284196ea4967a3805e0d41284-prefix:18fed4ed1b5b1068d45b01f444ce8fc1784a3e114875.csv.zip.gpg",
		},
		{
			name:        "encryptionType equal to aes-siv with one ignored value",
			blobname:    "patient-UNKNOWN-dumont.zip.gpg",
			separator:   "-",
			depthLevels: []int{2, 3},

			ignoreValues: []string{"UNKNOWN"},

			encryptionType: "aes-siv",
			key:            aesKey,

			want: "patient-UNKNOWN-prefix:18fed4ed1b5b1068d45b01f444ce8fc1784a3e114875.zip.gpg",
		},

		// In some cases, we still use HMAC for hiding data and we need a corresponding Table to store pairs of HASHES/ORIGIN-VALUE
		// This is required to be able to reverse this operation in a later ACI called "pairing"
		{
			name:        "HMAC encryptionType with CT in memory (map[string]string)",
			blobname:    "patient-martin-dumont.zip.gpg",
			separator:   "-",
			depthLevels: []int{2, 3},

			encryptionType: "legacy",
			key:            hmackey,

			expectedCT: masquage.CorrespondenceTable{
				"prefix:13c2a5e7d5d92a48826dbb932950a6577128cbbbbc5588e970a8eba38334be18": "martin",
				"prefix:752c4bd910c2897f20306395eeeaf00741726398d4f063d5424dff830458f956": "dumont",
			},

			want: "patient-prefix:13c2a5e7d5d92a48826dbb932950a6577128cbbbbc5588e970a8eba38334be18-prefix:752c4bd910c2897f20306395eeeaf00741726398d4f063d5424dff830458f956.zip.gpg",
		},
		{
			name:        "HMAC encryptionType with CT in a local file (does not fit in memory)",
			blobname:    "patient-martin-dumont.zip.gpg",
			separator:   "-",
			depthLevels: []int{2, 3},

			encryptionType: "legacy",
			key:            hmackey,

			useLocalFile: true,

			expectedLocalFileContent: `
"prefix:13c2a5e7d5d92a48826dbb932950a6577128cbbbbc5588e970a8eba38334be18":"martin",
"prefix:752c4bd910c2897f20306395eeeaf00741726398d4f063d5424dff830458f956":"dumont",`,

			want: "patient-prefix:13c2a5e7d5d92a48826dbb932950a6577128cbbbbc5588e970a8eba38334be18-prefix:752c4bd910c2897f20306395eeeaf00741726398d4f063d5424dff830458f956.zip.gpg",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			ct := masquage.CorrespondenceTable{}
			ctFileName := ""

			// in some cases, the caller provides a disk file to fill
			if tt.useLocalFile {
				f, _ := os.CreateTemp("", "")
				ctFileName = f.Name()
				defer f.Close()
			}

			got := pathMask.Process(
				hdhlog.Discard(),
				tt.blobname,
				tt.separator,
				tt.depthLevels,
				tt.ignoreValues,
				tt.key,
				"prefix", // unchanged between tests
				ct,
				ctFileName,
				tt.encryptionType,
			)
			if !assert.Equal(t, tt.want, got) {
				t.Errorf("Process() got %v, want %v is not equal", got, tt.want)
			}
			// in case work in memory => ct map is filled
			if tt.expectedCT != nil && !assert.Equal(t, tt.expectedCT, ct) {
				t.Errorf("correspondanceTable are not equal got %v, want %v", ct, tt.expectedCT)
			}
			// in case we work on disk (huge file) => localfile is filled
			if tt.useLocalFile {
				content, _ := os.ReadFile(ctFileName)
				if !assert.Equal(t, tt.expectedLocalFileContent, string(content)) {
					t.Errorf("filecontent is not as expected: got %v, want %v", string(content), tt.expectedLocalFileContent)
				}
			}

		})
	}
}
