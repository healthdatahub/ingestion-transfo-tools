package pathMask

import (
	"os"
	"path/filepath"
	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"strings"

	hdhlog "rdd-pipeline/pkg/log"
)

// Process masks the input string using "separator" as separator, and "depthLevels" as a list of subsets.
// Returns the masked value, as follow: <maskPrefix:maskedValue> and also add the hdh-mask <-> prod-mask if we need to
// bKey: the key used to mask the data
// maskPrefix: will be added before the newly masked value, with the following pattern: <maskPrefix:maskedValue>
// ct is the same ct than for the file content --> not optimal from a performance perspective when we have to reverse it
func Process(log hdhlog.Logger, blobname string, separator string, depthLevels []int, ignoreValues []string, bKey string, maskPrefix string, ct masquage.CorrespondenceTable, ctFileName, encryptionType string) string {

	blobname, fileExt := splitNameAndExtension(blobname)

	split := mask(log, blobname, depthLevels, separator, ignoreValues, encryptionType, bKey, maskPrefix, ctFileName, ct)

	var blobNameOutput string
	for i := range split {
		blobNameOutput += split[i]
	}
	return strings.TrimSuffix(blobNameOutput, separator) + fileExt
}

func mask(log hdhlog.Logger, blobname string, depthLevels []int, separator string, ignoreValues []string, encryptionType string, bKey string, maskPrefix string, ctFileName string, ct masquage.CorrespondenceTable) []string {

	split := strings.SplitAfter(blobname, separator)

	for _, v := range depthLevels {
		if (v-1 < len(split)) && (v-1 >= 0) {

			var oldName = strings.TrimSuffix(split[v-1], separator)

			// if the value has to be ignored, just prefix with the mask and continue
			if ignore(ignoreValues, oldName) {
				continue
			}

			var err error
			if encryptionType == v1alpha1.Mask_aes {
				split[v-1], err = masquage.EncryptAES(oldName, bKey)
				if err != nil {
					log.Error(err, "aes cipher computation: %w")
				}
				split[v-1] = maskPrefix + ":" + split[v-1]
			} else {
				// Legacy hmac
				split[v-1], err = masquage.HMAC(oldName, bKey)
				if err != nil {
					log.Error(err, "hmac computation: %w")
				}
				split[v-1] = maskPrefix + ":" + split[v-1]

				if ctFileName == "" {
					ct[split[v-1]] = oldName
				} else {
					ctFile, err := os.OpenFile(ctFileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
					if err != nil {
						log.Error(err, "opening tmp ct file : %w")
					}
					defer func() {
						if err := ctFile.Close(); err != nil {
							log.Error(err, "close tmp file %w")
						}
					}()
					if _, err = ctFile.Write([]byte("\n" + "\"" + split[v-1] + "\"" + ":" + "\"" + oldName + "\",")); err != nil {
						log.Error(err, "adding the path name part to the local ct: %w")
					}
				}
			}
			if v != len(split) {
				split[v-1] += separator
			}
		} else {
			log.Info("Path index not existing, skipping this index (info: depthlevel starts at 1)", "index", v)
		}
	}

	return split
}

func ignore(ignoreValues []string, s string) bool {
	for _, v := range ignoreValues {
		if strings.TrimSpace(s) == v {
			return true
		}
	}
	return false
}

func splitNameAndExtension(blobname string) (string, string) {
	var finalElement = filepath.Ext(blobname)
	var fileExt string

	// If the last extension is .gpg or .pgp, we have to keep it without masking it
	if finalElement == ".gpg" || finalElement == ".pgp" {
		blobname = blobname[:strings.LastIndex(blobname, ".")]
		fileExt = finalElement
	}
	// Second level of file extension: compression. This also have to be kept
	finalElement = filepath.Ext(blobname)
	if finalElement == ".gzip" || finalElement == ".gz" || finalElement == ".zip" {
		blobname = blobname[:strings.LastIndex(blobname, ".")]
		fileExt = finalElement + fileExt
	}

	// The extension is kept apart until the end of the path masking process
	finalElement = filepath.Ext(blobname)
	if strings.LastIndex(blobname, ".") != -1 {
		// We end up here if there is no third extension, ex: file.zip.gpg
		blobname = blobname[:strings.LastIndex(blobname, ".")]
		fileExt = finalElement + fileExt
	}
	return blobname, fileExt
}
