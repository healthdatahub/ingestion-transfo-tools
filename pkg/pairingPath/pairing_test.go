package pairingPath_test

import (
	"fmt"
	"testing"

	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/pairingPath"
	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"
)

const aesKey = "4145533235364B65792D33324368617261637465727331323334353637383930"

func TestPairingPath(t *testing.T) {
	tests := []struct {
		name string

		blobName       string
		ct             masquage.CorrespondenceTable
		encryptionType string
		key            string
		log            hdhlog.Logger
		separator      string

		wantErr bool
		want    string
	}{
		{
			name: fmt.Sprintf("run with %s encryptionType returns the decrypted value of the blobname", v1alpha1.Mask_aes),

			blobName:       "patient-prefix:11eacbf61c41489bfb8284196ea4967a3805e0d41284-prefix:18fed4ed1b5b1068d45b01f444ce8fc1784a3e114875.zip",
			ct:             nil,
			encryptionType: v1alpha1.Mask_aes,
			key:            aesKey,
			separator:      "-",

			want:    "patient-martin-dumont.zip",
			wantErr: false,
		},
		{
			name: fmt.Sprintf("run with %s encryptionType returns the decrypted value of the blobname using the Corresponding Table", v1alpha1.Mask_legacy),

			blobName: "patient-prefix:13c2a5e7d5d92a48826dbb932950a6577128cbbbbc5588e970a8eba38334be18-prefix:752c4bd910c2897f20306395eeeaf00741726398d4f063d5424dff830458f956.zip.gpg",
			ct: map[string]string{
				"prefix:13c2a5e7d5d92a48826dbb932950a6577128cbbbbc5588e970a8eba38334be18": "martin",
				"prefix:752c4bd910c2897f20306395eeeaf00741726398d4f063d5424dff830458f956": "dumont",
			},
			encryptionType: v1alpha1.Mask_legacy,
			key:            "",
			separator:      "-",

			want:    "patient-martin-dumont.zip.gpg",
			wantErr: false,
		},
		{
			name:           fmt.Sprintf("run with %s encryptionType and with the wrong separator returns an error", v1alpha1.Mask_aes),
			blobName:       "patient-prefix:11eacbf61c41489bfb8284196ea4967a3805e0d41284-prefix:18fed4ed1b5b1068d45b01f444ce8fc1784a3e114875.zip",
			ct:             nil,
			encryptionType: v1alpha1.Mask_aes,
			key:            aesKey,
			separator:      "_",

			want:    "",
			wantErr: true,
		},
		{
			name:           "an unknown encryptionType has no effect",
			blobName:       "patient-prefix:11eacbf61c41489bfb8284196ea4967a3805e0d41284-prefix:18fed4ed1b5b1068d45b01f444ce8fc1784a3e114875.zip",
			ct:             nil,
			encryptionType: "foo",
			key:            aesKey,
			separator:      "-",

			want:    "patient-prefix:11eacbf61c41489bfb8284196ea4967a3805e0d41284-prefix:18fed4ed1b5b1068d45b01f444ce8fc1784a3e114875.zip",
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := pairingPath.Process(
				hdhlog.Discard(),
				tt.blobName,
				tt.ct,
				tt.separator,
				tt.encryptionType,
				tt.key,
			)
			if (err != nil) != tt.wantErr {
				t.Errorf("PairingPath() return err = %v, want %v", err, tt.wantErr)
			}
			if got != tt.want {
				t.Errorf("PairingPath() return blobName = %v, want %v", got, tt.want)
			}
		})
	}
}
