package pairingPath

import (
	"fmt"
	"strings"

	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"
)

func Process(log hdhlog.Logger, blobName string, ct masquage.CorrespondenceTable, separator, encryptionType, key string) (string, error) {
	switch encryptionType {
	case v1alpha1.Mask_legacy:
		// For each CT element: replaceAll in the file name. To be optimized
		for v := range ct {
			blobName = strings.ReplaceAll(blobName, v, ct[v])
		}
	case v1alpha1.Mask_aes:
		// Split the blob name by part according to the "separator" hash field
		// For each of these parts, find the ones with a prefix, and remove the prefix and decrypt the rest of the subpart
		splittedBlobName := strings.Split(blobName, separator)
		if len(splittedBlobName) > 0 {
			for subPart := range splittedBlobName {
				toAppend := ""
				if len(strings.Split(splittedBlobName[subPart], ":")) > 1 {
					subPartPrefixRemoved := strings.Split(splittedBlobName[subPart], ":")[1]
					// split it and keeping the extension apart to append it in the end
					dotIndex := strings.IndexByte(subPartPrefixRemoved, '.')
					var subPartPrefixRemovedNoFileExtension string
					if dotIndex != -1 {
						subPartPrefixRemovedNoFileExtension = subPartPrefixRemoved[:dotIndex]
					} else {
						subPartPrefixRemovedNoFileExtension = subPartPrefixRemoved
					}
					if subPartPrefixRemovedNoFileExtension != subPartPrefixRemoved {
						toAppend = subPartPrefixRemoved[strings.IndexByte(subPartPrefixRemoved, '.'):]
					}

					subPartPaired, err := masquage.DecryptAES(subPartPrefixRemovedNoFileExtension, key)
					if err != nil {
						return subPartPaired, fmt.Errorf("pairing the blob name : %w", err)

					}
					blobName = strings.ReplaceAll(blobName, splittedBlobName[subPart], subPartPaired) + toAppend
				}
			}
		}
	default:
		log.Info("Don't changed the BlobName: %s with pairingPath", blobName)
	}

	return blobName, nil
}
