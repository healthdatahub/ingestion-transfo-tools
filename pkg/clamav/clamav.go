package clamav

import (
	"bufio"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"os/user"
	"path"
	"path/filepath"
	"sync"
	"syscall"
	"text/template"
	"time"

	hdhlog "rdd-pipeline/pkg/log"
)

const MinUpdateFreqInSec = 3600

var errExit *exec.ExitError

type Scanner struct {
	freshclamConfFile string
	clamdConfFile     string
	databaseDir       string
	username          string
	logger            hdhlog.Logger
	updateOnce        *sync.Once
}

func New(log hdhlog.Logger) (*Scanner, error) {
	databaseDir := path.Join(os.TempDir(), "clam-database")
	freshclamConfFile, err := os.CreateTemp(os.TempDir(), "freshclam")
	if err != nil {
		return nil, fmt.Errorf("create temp clam config : %w", err)
	}

	user, err := user.Current()
	if err != nil {
		return nil, fmt.Errorf("read current user info: %w", err)
	}

	var proxy struct {
		Server string
		Port   string
	}

	if httpProxy, ok := os.LookupEnv("HDH_HTTP_PROXY"); ok {
		u, err := url.Parse(httpProxy)
		if err != nil {
			return nil, fmt.Errorf("retrieve HDH_HTTP_PROXY env var: %w", err)
		}
		proxy.Server = u.Hostname()
		proxy.Port = u.Port()
	}

	freshclamConf, err := template.New("freshclam.conf").Parse(`
DatabaseMirror database.clamav.net
{{if .Server -}} HTTPProxyServer {{.Server}} {{- end}}
{{if .Port -}} HTTPProxyPort {{.Port}} {{- end -}}`)
	if err != nil {
		return nil, fmt.Errorf("create template freshclam conf file: %w", err)
	}

	if err := freshclamConf.Execute(freshclamConfFile, proxy); err != nil {
		return nil, fmt.Errorf("process freshclam conf file: %w", err)
	}

	if err := os.MkdirAll(databaseDir, 0o755); err != nil {
		return nil, fmt.Errorf("can't create database directory: %w", err)
	}

	clamdConfFile, err := os.CreateTemp(os.TempDir(), "clamd.conf")
	if err != nil {
		return nil, fmt.Errorf("create temp clamd.conf file : %w", err)
	}

	// Get the absolute path of the temporary file
	clamdConfFilePath, err := filepath.Abs(clamdConfFile.Name())
	if err != nil {
		return nil, fmt.Errorf("get absolute path of clamd.conf file: %w", err)
	}

	clamdConfContent := `DatabaseDirectory /tmp/clam-database
LogFile /var/log/clamav/clamd.log
LogTime yes
PidFile /run/clamav/clamd.pid
LocalSocket /run/clamav/clamd.sock
TCPSocket 3310
User rdduser`

	if _, err := clamdConfFile.WriteString(clamdConfContent); err != nil {
		return nil, fmt.Errorf("write to clamd.conf file: %w", err)
	}

	return &Scanner{
		freshclamConfFile: freshclamConfFile.Name(),
		clamdConfFile:     clamdConfFilePath,
		databaseDir:       databaseDir,
		username:          user.Username,
		logger:            log,
		updateOnce:        &sync.Once{},
	}, nil
}

func (scanner *Scanner) StartClamd() error {

	var err error
	scanner.logger.Info("checking clamav daemon (clamd) version")
	cmd := exec.Command(
		"/usr/sbin/clamd", "--version")
	if err = logStdOutputs(cmd, "clamd (server)", scanner.logger); err != nil {
		return err
	}
	if err = cmd.Run(); err != nil {
		return fmt.Errorf("clamd version execution failed: %w", err)
	}

	scanner.logger.Info("starting clamav daemon (clamd)")
	cmd = exec.Command(
		"/usr/sbin/clamd",
		"--config-file="+scanner.clamdConfFile)
	if err := logStdOutputs(cmd, "clamd (server)", scanner.logger); err != nil {
		return err
	}
	if err = cmd.Run(); err != nil {
		return fmt.Errorf("clamd execution failed: %w", err)
	}
	return nil
}

func (scanner *Scanner) Scan(file string) (int, error) {
	start := time.Now()

	scanner.logger.Info("starting clamdscan")

	cmd := exec.Command(
		"clamdscan",
		"--verbose",
		"--multiscan",
		"--fdpass",
		"--stream",
		"--config-file="+scanner.clamdConfFile,
		file)

	if err := logStdOutputs(cmd, "clamdscan (client)", scanner.logger); err != nil {
		return 0, err
	}

	if err := cmd.Run(); err != nil {
		if errExit, ok := err.(*exec.ExitError); ok {
			return errExit.ExitCode(), fmt.Errorf("clamdscan (client) execution failed: %w", err)
		}
	}

	scanner.logger.Info(fmt.Sprintf("clamdscan terminated in %s", time.Since(start)))

	return cmd.ProcessState.Sys().(syscall.WaitStatus).ExitStatus(), nil
}

func logStdOutputs(cmd *exec.Cmd, processName string, log hdhlog.Logger) error {
	// forward stdout to log
	pr, pw, err := os.Pipe()
	if err != nil {
		return err
	}
	cmd.Stdout = pw
	go func() {
		s := bufio.NewScanner(pr)

		for s.Scan() {
			log.Info(fmt.Sprintf("%s stdout :%s", processName, s.Text()))
		}
		if err := s.Err(); err != nil {
			log.Info("logger failed to read latest stderr token, the program may have ended too quickly", "error", err)
		}
	}()
	// forward stderr to log
	prerr, pwerr, err := os.Pipe()
	if err != nil {
		return err
	}
	cmd.Stderr = pwerr
	go func() {
		s := bufio.NewScanner(prerr)
		for s.Scan() {
			log.Error(fmt.Errorf("%s stderr :%s", processName, s.Text()), "stderr")
		}
		if err := s.Err(); err != nil {
			log.Info("logger failed to read latest stderr token, the program may have ended too quickly", "error", err)
		}
	}()
	return nil
}

func (scanner *Scanner) Update() (int, error) {
	var errCode int
	var err error

	scanner.updateOnce.Do(func() {
		defer func() {
			go func() {
				<-time.After(time.Second * MinUpdateFreqInSec)
				scanner.updateOnce = &sync.Once{}
			}()
		}()

		start := time.Now()

		scanner.logger.Info("Start updating database file", "databaseDir", scanner.databaseDir, "configFile", scanner.freshclamConfFile)

		cmd := exec.Command(
			"freshclam",
			"--verbose",
			"--user="+scanner.username,
			"--foreground",
			"--datadir="+scanner.databaseDir,
			"--config-file="+scanner.freshclamConfFile)

		if err = logStdOutputs(cmd, "freshclam", scanner.logger); err != nil {
			return
		}
		if errS := cmd.Run(); errS != nil {
			if errExit, ok := errS.(*exec.ExitError); ok {
				errCode = errExit.ExitCode()
				err = fmt.Errorf("freshclam execution failed : %w", errS)
				return
			}
		}
		errCode = cmd.ProcessState.Sys().(syscall.WaitStatus).ExitStatus()
		scanner.logger.Info(fmt.Sprintf("Database updated in %s", time.Since(start)))
	})
	return errCode, err
}
