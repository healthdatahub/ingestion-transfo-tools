package jsonMask

import (
	"bytes"
	"encoding/json"
	"io"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"reflect"
	"strings"
	"testing"

	"rdd-pipeline/pkg/masquage"

	hdhlog "rdd-pipeline/pkg/log"
)

type hashReaderTest struct {
	Reader *HashReader
	Writer *io.PipeWriter
}

func TestNewHashReader(t *testing.T) {
	Tags := []string{"$..userId"}

	type args struct {
		log            hdhlog.Logger
		in             io.Reader
		key            string
		ignoreValues   []string
		prefix         string
		tags           []string
		ct             masquage.CorrespondenceTable
		ct_name        string
		encryptionType string
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "json new hash reader legacy",
			args: args{
				log:            hdhlog.Discard(),
				in:             strings.NewReader(""),
				key:            "0123456789abcezu",
				prefix:         "HDH",
				tags:           Tags,
				ct:             masquage.CorrespondenceTable{},
				ct_name:        "ct",
				encryptionType: v1alpha1.Mask_legacy,
			},
		},
	}
	for _, tt := range tests {
		tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			got := NewHashReader(tt.args.log, tt.args.in, tt.args.key, tt.args.prefix, tt.args.tags, tt.args.ignoreValues, tt.args.ct, tt.args.ct_name, tt.args.encryptionType)

			switch v := got.(type) {
			case *HashReader, io.Reader:

			default:
				t.Errorf("NewHashReader() = %v", v)
			}
		})
	}
}

func TestHashReader_process(t *testing.T) {
	jsonInNominal := `[ { "id": "1.2.3.4.5.4732847239432", "userId": "PATIENT_1", "value": { "medicine": ["doliprane","oxycodon","benzo"] ,"characteristics": { "birthdate": "1990-09-14", "age": 18 } } }, { "id": "4.5.7.9.6.969540965406", "userId": "PATIENT_2", "value": { "characteristics": { "birthdate": "1980-03-14", "age": 32 } } }]`
	jsonInSingleObject := `{ "id": "1.2.3.4.5.4732847239432", "userId": "PATIENT_1", "value": { "characteristics": { "birthdate": "1990-09-14", "age": 18 } } }`
	jsonInMultiline := `{
		"id": "1.2.3.4.5.4732847239432",
		"userId": "PATIENT_1",
		"value": {
			"characteristics": {
				"birthdate": "1990-09-14",
				"age": 18
			}
		}
	}`

	keyForAES := "4145533235364B65792D33324368617261637465727331323334353637383930"
	keyForHMAC := "0123456789abcdef"

	tests := []struct {
		name string

		input, key, encryptionType string
		tags, ignoreValues         []string

		wantErr bool
		want    string
		wantCt  masquage.CorrespondenceTable
	}{
		{
			name: "one key to mask with in-depth pattern ($..userId) legacy",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_legacy,
			key:            keyForHMAC,
			tags:           []string{"$..userId"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
			},
		},
		{
			name: "one key to mask with in-depth pattern ($..userId) aes_siv",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$..userId"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:2ccaedcb3061572eb5fc38274f7a0a15fd68acb9d9cd06ba09","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "two keys to mask with indepth pattern ($..userId, $..birthdate) legacy",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_legacy,
			key:            keyForHMAC,
			tags:           []string{"$..userId", "$..birthdate"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0","value":{"characteristics":{"birthdate":"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297","age":32}}}]`,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
				"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297": "1980-03-14",
			},
		},
		{
			name: "two keys to mask with indepth pattern ($..userId, $..birthdate) aes",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$..userId", "$..birthdate"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"HDH:4db280b2581f3a5cb69b776157f362a163a5e6aefb22e3e7bb2b","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:2ccaedcb3061572eb5fc38274f7a0a15fd68acb9d9cd06ba09","value":{"characteristics":{"birthdate":"HDH:4db281b2581f305cb69bf32053f288b09dd279eda930535f68f8","age":32}}}]`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "one key to mask, direct pattern ($.value.characteristics.birthdate) legacy",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_legacy,
			key:            keyForHMAC,
			tags:           []string{"$.value.characteristics.birthdate"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297","age":32}}}]`,
			wantCt: masquage.CorrespondenceTable{
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
				"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297": "1980-03-14",
			},
		},
		{
			name: "one key to mask, direct pattern ($.value.characteristics.birthdate) aes",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$.value.characteristics.birthdate"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"HDH:4db280b2581f3a5cb69b776157f362a163a5e6aefb22e3e7bb2b","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"HDH:4db281b2581f305cb69bf32053f288b09dd279eda930535f68f8","age":32}}}]`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			// In case of non existing key, the JSON object should simply not change
			name: "non existing key ($..iddddddd) legacy",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_legacy,
			key:            keyForHMAC,
			tags:           []string{"$..idddddddd"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			// In case of non existing key, the JSON object should simply not change
			name: "non existing key ($..iddddddd) aes",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$..idddddddd"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "in depth with array index legacy",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_legacy,
			key:            keyForHMAC,
			tags:           []string{"$.value.medicine[0]"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["HDH:f97873b217eb0fb8f99a7b5b97d3c906da9dc3db31234e305eb68ecdbefbae75","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
			wantCt: masquage.CorrespondenceTable{
				"HDH:f97873b217eb0fb8f99a7b5b97d3c906da9dc3db31234e305eb68ecdbefbae75": "doliprane",
			},
		},
		{
			name: "in depth with array index aes",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$.value.medicine[0]"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["HDH:18e4d5eb055d621fe2998cfbd1e23a06d545f6574cc7f3a5d7","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "multiple type keys: ($.value.medicine[*], $.value.characteristics.birthdate, $..idddddddd, $.userId) legacy",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_legacy,
			key:            keyForHMAC,
			tags:           []string{"$.value.medicine[*]", "$.value.characteristics.birthdate", "$..idddddddd", "$.userId"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"medicine":["HDH:f97873b217eb0fb8f99a7b5b97d3c906da9dc3db31234e305eb68ecdbefbae75","HDH:698c715f1d6fdd20dc6046dd2bb1e080b94cf1a13ae92183210f072d77a18d2c","HDH:48b4685e99def91bd550b17ef4c4302377a3a322cfa59abdc2ea56dd5c92961e"],"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0","value":{"characteristics":{"birthdate":"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297","age":32}}}]`,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:48b4685e99def91bd550b17ef4c4302377a3a322cfa59abdc2ea56dd5c92961e": "benzo",
				"HDH:698c715f1d6fdd20dc6046dd2bb1e080b94cf1a13ae92183210f072d77a18d2c": "oxycodon",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
				"HDH:e6c8d8e67fc9e6f88b2c93a9974fc7e5ac1210bfb54fac57b4ca594104ceb297": "1980-03-14",
				"HDH:f97873b217eb0fb8f99a7b5b97d3c906da9dc3db31234e305eb68ecdbefbae75": "doliprane",
			},
		},
		{
			name: "multiple type keys: ($.value.medicine[*], $.value.characteristics.birthdate, $..idddddddd, $.userId) aes",

			input:          jsonInNominal,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$.value.medicine[*]", "$.value.characteristics.birthdate", "$..idddddddd", "$.userId"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"medicine":["HDH:18e4d5eb055d621fe2998cfbd1e23a06d545f6574cc7f3a5d7","HDH:13f3c0e11a4b6c1f4f74b274dddbd98dc6561a670b1ce404","HDH:1eeed7f81aed14031a7c9452d8231daaabf992d84d"],"characteristics":{"birthdate":"HDH:4db280b2581f3a5cb69b776157f362a163a5e6aefb22e3e7bb2b","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:2ccaedcb3061572eb5fc38274f7a0a15fd68acb9d9cd06ba09","value":{"characteristics":{"birthdate":"HDH:4db281b2581f305cb69bf32053f288b09dd279eda930535f68f8","age":32}}}]`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "single json object: ($..userId, $..birthdate) legacy",

			input:          jsonInSingleObject,
			encryptionType: v1alpha1.Mask_legacy,
			key:            keyForHMAC,
			tags:           []string{"$..userId", "$..birthdate"},

			wantErr: false,
			want:    `{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}}`,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
			},
		},
		{
			name: "single json object: ($..userId, $..birthdate) aes",

			input:          jsonInSingleObject,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$..userId", "$..birthdate"},

			wantErr: false,
			want:    `{"id":"1.2.3.4.5.4732847239432","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"characteristics":{"birthdate":"HDH:4db280b2581f3a5cb69b776157f362a163a5e6aefb22e3e7bb2b","age":18}}}`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "single json object: ($..userId, $..birthdate) legacy",

			input:          jsonInMultiline,
			encryptionType: v1alpha1.Mask_legacy,
			key:            keyForHMAC,
			tags:           []string{"$..userId", "$..birthdate"},

			wantErr: false,
			want:    `{"id":"1.2.3.4.5.4732847239432","userId":"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080","value":{"characteristics":{"birthdate":"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13","age":18}}}`,
			wantCt: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:a6283c0866747535d4ca0c35058f1940d78dd7ec7a6d80802933608e1b62da13": "1990-09-14",
			},
		},
		{
			name: "single json multiline object: ($..userId, $..birthdate) aes",

			input:          jsonInMultiline,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$..userId", "$..birthdate"},

			wantErr: false,
			want:    `{"id":"1.2.3.4.5.4732847239432","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"characteristics":{"birthdate":"HDH:4db280b2581f3a5cb69b776157f362a163a5e6aefb22e3e7bb2b","age":18}}}`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "single json multiline object: ($..userId, $..birthdate) aes",

			input:          jsonInMultiline,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$..userId", "$..birthdate"},

			wantErr: false,
			want:    `{"id":"1.2.3.4.5.4732847239432","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"characteristics":{"birthdate":"HDH:4db280b2581f3a5cb69b776157f362a163a5e6aefb22e3e7bb2b","age":18}}}`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "ignoredValues are unmodified legacy",
			// only the "id" value should be masked here

			input: `[
				{ "id": "1.2.3.4.5.4732847239432", "userId": "PATIENT_1", "size": "  " },
				{ "id": "2.3.4.5.6.4732847239432", "userId": "PATIENT_2", "size": "n/a" }]
			`,
			encryptionType: v1alpha1.Mask_legacy,
			key:            keyForHMAC,
			tags:           []string{"$..id", "$..birthdate", "$.size"},
			ignoreValues:   []string{"1.2.3.4.5.4732847239432", "", "n/a"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","size":"  "},{"id":"HDH:acd70b875f04fa5be4d0860010b18d2006415a0e336f6e44a3c8d8c559db65e7","userId":"PATIENT_2","size":"n/a"}]`,
			wantCt: masquage.CorrespondenceTable{
				"HDH:acd70b875f04fa5be4d0860010b18d2006415a0e336f6e44a3c8d8c559db65e7": "2.3.4.5.6.4732847239432",
			},
		},
		{
			name: "ignoredValues are unmodified aes",

			input: `[
				{ "id": "1.2.3.4.5.4732847239432", "userId": "PATIENT_1", "size": "  " },
				{ "id": "2.3.4.5.6.4732847239432", "userId": "PATIENT_2", "size": "n/a" }]
			`,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$..id", "$..birthdate", "$.size"},
			ignoreValues:   []string{"1.2.3.4.5.4732847239432", "", "n/a"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","size":"  "},{"id":"HDH:4ea58aac4101365fb1819999f5a92b343e441205117337b0636cd2f011958119b5ca7a4321de2e","userId":"PATIENT_2","size":"n/a"}]`,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "ignoredValues are trimmed before beeing compared",

			input: `[
				{ "id": "1.2.3.4.5.4732847239432", "userId": "PATIENT_1", "size": "         " },
				{ "id": "2.3.4.5.6.4732847239432", "userId": "PATIENT_2", "size": "     n/a" }]
			`,
			encryptionType: v1alpha1.Mask_aes,
			key:            keyForAES,
			tags:           []string{"$..id", "$..birthdate", "$.size"},
			ignoreValues:   []string{"1.2.3.4.5.4732847239432", "", "n/a"},

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","size":"         "},{"id":"HDH:4ea58aac4101365fb1819999f5a92b343e441205117337b0636cd2f011958119b5ca7a4321de2e","userId":"PATIENT_2","size":"     n/a"}]`,
			wantCt:  masquage.CorrespondenceTable{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt // to avoid race condition due to the routine
			r := newHashReader(tt.input, tt.key, tt.tags, tt.ignoreValues, tt.encryptionType)
			go func() {
				errP := r.Reader.process()
				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()
			got, _ := io.ReadAll(r.Reader)

			gotJSON := new(bytes.Buffer)
			json.Compact(gotJSON, got)
			gotString := string(gotJSON.Bytes())

			if string(gotJSON.Bytes()) != tt.want {
				t.Errorf("got \n%s\n -- want \n%v\n", gotString, tt.want)
			}
			if !reflect.DeepEqual(r.Reader.ct, tt.wantCt) {
				t.Errorf("got \n%s\n -- want  \n%v\n", r.Reader.ct, tt.wantCt)
			}
		})
	}
}

func newHashReader(input string, key string, tags []string, ignoreValues []string, encryptionType string) hashReaderTest {
	rOut, wOut := io.Pipe()
	return hashReaderTest{
		Reader: &HashReader{
			log:            hdhlog.Discard(),
			in:             strings.NewReader(input),
			Key:            key,
			Prefix:         "HDH",
			Tags:           tags,
			ignoreValues:   ignoreValues,
			jsonWriter:     wOut,
			ct:             masquage.CorrespondenceTable{},
			out:            rOut,
			encryptionType: encryptionType,
		},
		Writer: wOut,
	}
}
