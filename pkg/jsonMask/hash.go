package jsonMask

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strings"

	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/PaesslerAG/jsonpath"
)

type HashReader struct {
	in              io.Reader
	Prefix          string
	Tags            []string
	ignoreValues    []string
	Key             string
	currentline     int
	jsonWriter      *io.PipeWriter
	out             *io.PipeReader
	ct              masquage.CorrespondenceTable
	log             hdhlog.Logger
	ctContainerName string
	encryptionType  string
}

func NewHashReader(log hdhlog.Logger, in io.Reader, key string, prefix string, tags []string, ignoreValues []string, ct masquage.CorrespondenceTable, ctContainerName, encryptionType string) io.Reader {
	jsonReader := in
	rOut, wOut := io.Pipe()

	jsonWriter := wOut

	log = log.With("prefix", prefix)
	log = log.With("tags", tags)
	log = log.With("encryptionType", encryptionType)
	log = log.With("ignoreValues", ignoreValues)

	hashReader := &HashReader{
		in:              jsonReader,
		jsonWriter:      jsonWriter,
		out:             rOut,
		Tags:            tags,
		ignoreValues:    ignoreValues,
		Prefix:          prefix,
		Key:             key,
		currentline:     0,
		ct:              ct,
		log:             log,
		ctContainerName: ctContainerName,
		encryptionType:  encryptionType,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := hashReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()
	return hashReader
}

func (r *HashReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *HashReader) process() error {
	// Read char by char as of now. Possible upgrade: read more to reduce the number of call to r.in.Read
	content := make([]byte, 1)
	var currentObjectString string = ""
	var firstBracket bool = false
	var nbOpened int = 0
	var firstItem bool = true
	var isArray bool = false
	var end = false
	for {
		_, err := r.in.Read(content)

		if err == io.EOF {
			end = true
		} else if err != nil {
			return fmt.Errorf("reading char : %w", err)
		}

		currentChar := rune(content[0])

		// if New Json object
		if currentChar == '[' && currentObjectString == "" { //Subcase: json object is surrounded by []
			isArray = true
		}
		if currentChar == '{' && currentObjectString == "" {
			nbOpened = 1
			firstBracket = true
		}

		if nbOpened > 0 {
			if currentChar == '{' && !firstBracket {
				nbOpened++
			} else if currentChar == '}' {
				nbOpened--
				firstBracket = false
			} else {
				firstBracket = false
			}
			currentObjectString += string(currentChar)
		}
		if nbOpened == 0 {
			if currentObjectString != "" {
				// End of the current json object (reached the last '}'), it is now stores in the "currentObjectString" string in memory
				payload := interface{}(nil)

				err := json.Unmarshal([]byte(currentObjectString), &payload)
				if err != nil {
					return fmt.Errorf("fail to unmarshall json object: %w", err)
				}

				if !firstItem {
					currentObjectString = "," + currentObjectString
				}
				// Check JsonPath
				for _, path := range r.Tags {
					outValue := ""
					value, _ := jsonpath.Get(path, payload)
					// No check on the existence of the key, it might not exist in some json object, but still be valid
					if fmt.Sprintf("%T", value) == "string" {

						// The if/else is needed because the type of "value" can varry with this library
						valueString := fmt.Sprintf("%v", value)
						if r.ignoreValue(valueString) {
							continue
						} else if r.encryptionType == v1alpha1.Mask_aes {
							outValue, err = masquage.EncryptAES(valueString, r.Key)
						} else {
							outValue, err = masquage.HMAC(valueString, r.Key)
						}
						if err != nil {
							return fmt.Errorf("hash json %w", err)
						}

						if r.Prefix != "" {
							outValue = fmt.Sprintf("%s:%s", r.Prefix, outValue)
						}

						currentObjectString = strings.ReplaceAll(currentObjectString, valueString, outValue)
						if r.encryptionType == v1alpha1.Mask_legacy { // CT only for legacy encryptionType
							r.ct[outValue] = valueString
						}

					} else if fmt.Sprintf("%T", value) == "[]interface {}" {

						for _, val := range value.([]interface{}) {
							strVal := fmt.Sprintf("%v", val)
							if r.ignoreValue(strVal) {
								continue
							} else if r.encryptionType == v1alpha1.Mask_aes {
								outValue, err = masquage.EncryptAES(strVal, r.Key)
							} else {
								outValue, err = masquage.HMAC(strVal, r.Key)
							}
							if err != nil {
								return fmt.Errorf("hash json %w", err)
							}
							if r.Prefix != "" {
								outValue = fmt.Sprintf("%s:%s", r.Prefix, outValue)
							}
							currentObjectString = strings.ReplaceAll(currentObjectString, strVal, outValue)
							if r.encryptionType == v1alpha1.Mask_legacy { // CT only for legacy encryptionType
								r.ct[outValue] = strVal
							}
						}
					} else {
						continue
					}
				}
				if firstItem {
					if isArray {
						currentObjectString = "[" + currentObjectString
					}
					firstItem = false
				}

				if _, err := io.WriteString(r.jsonWriter, currentObjectString); err != nil {
					log.Fatal(err)
				}

				currentObjectString = ""
			}
		}

		if end {
			if firstItem {
				if _, err := io.WriteString(r.jsonWriter, currentObjectString+"} "); err != nil {
					log.Fatal(err)
				}
			}
			if isArray {
				if _, err := io.WriteString(r.jsonWriter, "] "); err != nil {
					log.Fatal(err)
				}
			}
			break
		}
	}
	return nil
}

func (h HashReader) ignoreValue(s string) bool {
	for _, v := range h.ignoreValues {
		if strings.TrimSpace(s) == v {
			return true
		}
	}
	return false
}
