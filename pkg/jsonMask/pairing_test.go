package jsonMask

import (
	"io"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"strings"
	"testing"

	"rdd-pipeline/pkg/masquage"

	hdhlog "rdd-pipeline/pkg/log"
)

type pairingReaderTest struct {
	Reader *PairingReader
	Writer *io.PipeWriter
}

func newPairingReader(input string, tags, ignoreValues []string, ct masquage.CorrespondenceTable, encryptionType, key string) pairingReaderTest {
	rOut, wOut := io.Pipe()
	return pairingReaderTest{
		Reader: &PairingReader{
			log:            hdhlog.Discard(),
			in:             strings.NewReader(input),
			tags:           tags,
			ignoreValues:   ignoreValues,
			jsonWriter:     wOut,
			ct:             ct,
			out:            rOut,
			encryptionType: encryptionType,
			key:            key,
		},
		Writer: wOut,
	}
}
func TestNewPairingReader(t *testing.T) {
	tags := []string{"userId"}

	type args struct {
		log            hdhlog.Logger
		in             io.Reader
		tags           []string
		ignoreValues   []string
		ct             masquage.CorrespondenceTable
		encryptionType string
		key            string
	}
	tests := []struct {
		name string
		args args
		want io.Reader
	}{
		{
			name: "create",
			args: args{
				log:            hdhlog.Discard(),
				in:             strings.NewReader(""),
				tags:           tags,
				ignoreValues:   []string{},
				ct:             masquage.CorrespondenceTable{},
				encryptionType: v1alpha1.Mask_legacy,
				key:            "",
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			got := NewPairingReader(tt.args.log, tt.args.in, tt.args.tags, tt.args.ignoreValues, tt.args.ct, tt.args.encryptionType, tt.args.key)

			switch v := got.(type) {
			case *PairingReader, io.Reader:

			default:
				t.Errorf("NewPairingReader() = %v", v)
			}
		})
	}
}

func TestPairingReader_process(t *testing.T) {
	jsonInHMAC := `[{ "id": "1.2.3.4.5.4732847239432", "userId": "HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080", "value": { "medicine": ["doliprane","oxycodon","benzo"] ,"characteristics": { "birthdate": "1990-09-14", "age": 18 } } }
	,{ "id": "4.5.7.9.6.969540965406", "userId": "HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0", "value": { "characteristics": { "birthdate": "1980-03-14", "age": 32 } } }
	]`

	jsonInAES := `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:2ccaedcb3061572eb5fc38274f7a0a15fd68acb9d9cd06ba09","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`

	var keyAES = "4145533235364B65792D33324368617261637465727331323334353637383930"

	tests := []struct {
		name string

		input          string
		tags           []string
		ignoreValues   []string
		ct             masquage.CorrespondenceTable
		encryptionType string
		key            string

		wantErr bool
		want    string
	}{
		{
			name: "simple with in-depth jsonPath ($..userId) legacy",

			input: jsonInHMAC,
			tags:  []string{"$..userId"},
			ct: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
			},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want:    `[{ "id": "1.2.3.4.5.4732847239432", "userId": "PATIENT_1", "value": { "medicine": ["doliprane","oxycodon","benzo"] ,"characteristics": { "birthdate": "1990-09-14", "age": 18 } } },{ "id": "4.5.7.9.6.969540965406", "userId": "PATIENT_2", "value": { "characteristics": { "birthdate": "1980-03-14", "age": 32 } } }]`,
		}, {
			name: "Id not existing ($..iiiiiiiid) legacy",

			input: jsonInHMAC,
			tags:  []string{"$..iiiiiiiid"},
			ct: masquage.CorrespondenceTable{
				"HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080": "PATIENT_1",
				"HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0": "PATIENT_2",
			},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want:    `[{ "id": "1.2.3.4.5.4732847239432", "userId": "HDH:070a62c19636e9db013f750b66978cc7e7c4a03d86f773972e906fe134f12080", "value": { "medicine": ["doliprane","oxycodon","benzo"] ,"characteristics": { "birthdate": "1990-09-14", "age": 18 } } },{ "id": "4.5.7.9.6.969540965406", "userId": "HDH:7ffcaeb54589904f0e65a11a3dc87a7f42dd2a63e05447f196de8fe0e11b9de0", "value": { "characteristics": { "birthdate": "1980-03-14", "age": 32 } } }]`,
		},
		{
			name: "simple with in-depth jsonPath ($..userId) aes",

			input:          jsonInAES,
			tags:           []string{"$..userId"},
			encryptionType: v1alpha1.Mask_aes,
			key:            keyAES,

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"PATIENT_1","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
		},
		{
			name: "double keys with in-depth jsonPath ($..userId, $..id) aes",

			input: `[
				{"id":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},
				{"id":"HDH:2ccaedcb3061572eb5fc38274f7a0a15fd68acb9d9cd06ba09","userId":"HDH:2ccaedcb3061572eb5fc38274f7a0a15fd68acb9d9cd06ba09","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}
				]`,
			tags:           []string{"$..userId", "$..id"},
			encryptionType: v1alpha1.Mask_aes,
			key:            keyAES,

			wantErr: false,
			want:    `[{"id":"PATIENT_1","userId":"PATIENT_1","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"PATIENT_2","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
		},

		{
			name: "Id not existing ($.iiiiiiiid) aes",

			input:          jsonInAES,
			tags:           []string{"$..iiiiiiiid"},
			encryptionType: v1alpha1.Mask_aes,
			key:            keyAES,

			wantErr: false,
			want:    `[{"id":"1.2.3.4.5.4732847239432","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"4.5.7.9.6.969540965406","userId":"HDH:2ccaedcb3061572eb5fc38274f7a0a15fd68acb9d9cd06ba09","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
		},
		{
			name: "double keys with a value ignored aes",

			input: `[
				{"id":"n/a","userId":"HDH:2ccaedcb3061572eb60d6cb7b3a3bc15211a7280b0bcd3a255","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},
				{"id":"HDH:2ccaedcb3061572eb5fc38274f7a0a15fd68acb9d9cd06ba09","userId":"HDH:2ccaedcb3061572eb5fc38274f7a0a15fd68acb9d9cd06ba09","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}
				]`,
			tags:           []string{"$..userId", "$..id"},
			encryptionType: v1alpha1.Mask_aes,
			ignoreValues:   []string{"n/a"},
			key:            keyAES,

			wantErr: false,
			want:    `[{"id":"n/a","userId":"PATIENT_1","value":{"medicine":["doliprane","oxycodon","benzo"],"characteristics":{"birthdate":"1990-09-14","age":18}}},{"id":"PATIENT_2","userId":"PATIENT_2","value":{"characteristics":{"birthdate":"1980-03-14","age":32}}}]`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			r := newPairingReader(tt.input, tt.tags, tt.ignoreValues, tt.ct, tt.encryptionType, tt.key)
			go func() {
				err := r.Reader.process()
				r.Writer.CloseWithError(err)
				if (err != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()
			got, _ := io.ReadAll(r.Reader)
			if string(got) != tt.want {
				t.Errorf("got\n%s\nwant\n%v", got, tt.want)
			}
		})
	}
}
