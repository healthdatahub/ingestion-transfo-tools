package jsonMask

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strings"

	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/PaesslerAG/jsonpath"
)

type PairingReader struct {
	in              io.Reader
	tags            []string
	ignoreValues    []string
	currentline     int
	jsonWriter      *io.PipeWriter
	out             *io.PipeReader
	ct              masquage.CorrespondenceTable
	log             hdhlog.Logger
	ctContainerName string
	encryptionType  string
	key             string
}

func NewPairingReader(log hdhlog.Logger, in io.Reader, tags, ignoreValues []string, ct masquage.CorrespondenceTable, encryptionType, key string) io.Reader {
	jsonReader := in
	rOut, wOut := io.Pipe()

	jsonWriter := wOut

	log = log.With("tags", tags)
	log = log.With("encryptionType", encryptionType)
	log = log.With("ignoreValues", ignoreValues)

	pairingReader := &PairingReader{
		in:             jsonReader,
		jsonWriter:     jsonWriter,
		out:            rOut,
		tags:           tags,
		ignoreValues:   ignoreValues,
		currentline:    0,
		ct:             ct,
		log:            log,
		encryptionType: encryptionType,
		key:            key,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := pairingReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()
	return pairingReader
}

func (r *PairingReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *PairingReader) process() error {
	// Read char by char as of now. Possible upgrade: read more to reduce the number of call to r.in.Read
	content := make([]byte, 1)
	var currentObjectString string = ""
	var firstBracket bool = false
	var nbOpened int = 0
	var firstItem bool = true
	var isArray bool = false
	var end = false
	var ok bool

	for {
		_, err := r.in.Read(content)

		if err == io.EOF {
			end = true
		} else if err != nil {
			return fmt.Errorf("reading char : %w", err)
		}

		currentChar := rune(content[0])

		// if New Json object
		if currentChar == '[' && currentObjectString == "" { //Subcase: json object is surrounded by []
			isArray = true
		}
		if currentChar == '{' && currentObjectString == "" {
			nbOpened = 1
			firstBracket = true
		}

		if nbOpened > 0 {
			if currentChar == '{' && !firstBracket {
				nbOpened++
			} else if currentChar == '}' {
				nbOpened--
				firstBracket = false
			} else {
				firstBracket = false
			}
			currentObjectString += string(currentChar)
		}

		if nbOpened == 0 {
			// End of the current json object (reached the last '}'), it is now stores in the "currentObjectString" string in memory
			if currentObjectString != "" {

				payload := interface{}(nil)

				err := json.Unmarshal([]byte(currentObjectString), &payload)
				if err != nil {
					log.Fatal(err)
				}

				if !firstItem {
					currentObjectString = "," + currentObjectString
				}

				for _, path := range r.tags {
					decrypted := ""
					value, _ := jsonpath.Get(path, payload)
					if fmt.Sprintf("%T", value) == "string" {
						decrypted := ""
						// The if/else is needed because the type of "value" can varry with this library
						valueString := fmt.Sprintf("%v", value)
						if r.ignoreValue(valueString) {
							decrypted = valueString
						} else if r.encryptionType == v1alpha1.Mask_aes {
							// Removing the prefix (isn't encrypted)
							idNoPrefix := fmt.Sprintf("%v", valueString)[strings.Index(fmt.Sprintf("%v", valueString), ":")+1:]

							decrypted, err = masquage.DecryptAES(fmt.Sprintf("%v", idNoPrefix), r.key)
							if err != nil {
								return fmt.Errorf("fail to decrypt %v", idNoPrefix)
							}

						} else if r.encryptionType == v1alpha1.Mask_legacy {
							decrypted, ok = r.ct[valueString]
							if !ok {
								return fmt.Errorf("CT key %v could not be found", valueString)
							}
						}
						currentObjectString = strings.ReplaceAll(currentObjectString, fmt.Sprintf("%v", valueString), decrypted)
					} else if fmt.Sprintf("%T", value) == "[]interface {}" {
						for _, val := range value.([]interface{}) {

							if r.ignoreValue(fmt.Sprintf("%v", val)) {
								decrypted = fmt.Sprintf("%v", val)
							} else if r.encryptionType == v1alpha1.Mask_aes {
								idNoPrefix := fmt.Sprintf("%v", val)[strings.Index(fmt.Sprintf("%v", val), ":")+1:]
								decrypted, err = masquage.DecryptAES(fmt.Sprintf("%v", idNoPrefix), r.key)

								if err != nil {
									return fmt.Errorf("fail to decrypt %v", idNoPrefix)
								}

							} else if r.encryptionType == v1alpha1.Mask_legacy {
								decrypted, ok = r.ct[fmt.Sprintf("%v", val)]
								if !ok {
									return fmt.Errorf("CT key %v could not be found", val)
								}
							}
							currentObjectString = strings.ReplaceAll(currentObjectString, fmt.Sprintf("%v", val), decrypted)
						}
					} else {
						continue
					}
				}

				if firstItem {
					if isArray {
						currentObjectString = "[" + currentObjectString
					}
					firstItem = false
				}

				if _, err := io.WriteString(r.jsonWriter, currentObjectString); err != nil {
					log.Fatal(err)
				}
				currentObjectString = ""

			}
		}
		if end {
			if firstItem {
				if _, err := io.WriteString(r.jsonWriter, currentObjectString+"} "); err != nil {
					log.Fatal(err)
				}
			}
			if isArray {
				if _, err := io.WriteString(r.jsonWriter, "]"); err != nil {
					log.Fatal(err)
				}
			}
			break
		}

	}
	return nil
}

func (h PairingReader) ignoreValue(s string) bool {
	for _, v := range h.ignoreValues {
		if strings.TrimSpace(s) == v {
			return true
		}
	}
	return false
}
