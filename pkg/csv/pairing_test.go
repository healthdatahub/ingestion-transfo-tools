package csv

import (
	"encoding/csv"
	"io"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"strings"
	"testing"

	"rdd-pipeline/pkg/masquage"

	hdhlog "rdd-pipeline/pkg/log"
)

type pairingReaderTest struct {
	Reader *PairingReader
	Writer *io.PipeWriter
}

func newPairingReader(input string, skipLines, columns []int, columnNames []string, ignoreValues []string, ct masquage.CorrespondenceTable, encryptionType, key string) pairingReaderTest {

	csvReader := csv.NewReader(
		strings.NewReader(input),
	)
	csvReader.Comma = ';'
	csvReader.LazyQuotes = true

	rOut, wOut := io.Pipe()

	csvWriter := csv.NewWriter(wOut)
	csvWriter.Comma = ';'

	return pairingReaderTest{
		Reader: &PairingReader{
			log:            hdhlog.Discard(),
			in:             csvReader,
			SkipLines:      skipLines,
			ColumnNames:    columnNames,
			ignoreValues:   ignoreValues,
			Columns:        columns,
			csvWriter:      csvWriter,
			ct:             ct,
			out:            rOut,
			EncryptionType: encryptionType,
			key:            key,
		},
		Writer: wOut,
	}
}

func TestPairingReader_process(t *testing.T) {

	csvInHMAC := `id;name;pseudo` + "\n" +
		`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;health-data-hub;hdh` + "\n" +
		`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;france-television;ftv` + "\n"
	ct := masquage.CorrespondenceTable{
		"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
		"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
	}

	csvInAES := `id;name;pseudo` + "\n" +
		`HDH:4d08788dc1d2bb1b69c354ab3ecd0d3658;health-data-hub;hdh` + "\n" +
		`HDH:4e96a6b4a8a36e0335586188aa56ba086e;france-television;ftv` + "\n"

	csvOut := `id;name;pseudo` + "\n" +
		`1;health-data-hub;hdh` + "\n" +
		`2;france-television;ftv` + "\n"

	tests := []struct {
		name string

		input          string
		skipLines      []int
		columns        []int
		columnNames    []string
		ignoreValues   []string
		ct             masquage.CorrespondenceTable
		encryptionType string
		key            string

		wantErr bool
		want    string
	}{
		{
			name: "pairing with col name legacy",

			input:          csvInHMAC,
			skipLines:      []int{1},
			columns:        []int{},
			columnNames:    []string{"id"},
			ct:             ct,
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want:    csvOut,
		},
		{
			name: "pairing with col rank legacy",

			input:          csvInHMAC,
			skipLines:      []int{1},
			columns:        []int{1},
			columnNames:    []string{},
			ct:             ct,
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want:    csvOut,
		},
		{
			name: "pairing with col name too small legacy",

			input:          csvInHMAC,
			skipLines:      []int{},
			columns:        []int{-1},
			columnNames:    []string{},
			ct:             ct,
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: true,
			want:    "",
		},
		{
			name: "pairing with col rank out of bound legacy",

			input:          csvInHMAC,
			skipLines:      []int{},
			columns:        []int{99},
			columnNames:    []string{},
			ct:             ct,
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: true,
			want:    "",
		},
		{
			name: "pairing with ignored values legacy",

			input: `HDH:cc863dde8bdf94120186915b27c6461b82c35a08d62fe7543146acf88de30065;HDH:94c675159ee0920a4e67a34450b7f601e545936936615adbfe328a10537421da;HDH:63c6551e0676338575f016e602b7a05cc6340290cf87e744b090d32f0dfa583d` + "\n" +
				`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;"    ";HDH:fb1360e21653cb4325fdffb8d6a80c71fff584863c8369574448b4c6f3fb82e0` + "\n" +
				`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;france-television;n/a` + "\n" +
				`HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb;HDH:93d1ec3a483fac17a369084888303ea4dee2fc211e712f5e59fed19203204111;` + "\n",
			skipLines:    []int{},
			columns:      []int{1, 2, 3},
			columnNames:  []string{},
			ignoreValues: []string{"france-television", "n/a", ""},
			ct: masquage.CorrespondenceTable{
				"HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb": "3",
				"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
				"HDH:63c6551e0676338575f016e602b7a05cc6340290cf87e744b090d32f0dfa583d": "pseudo",
				"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
				"HDH:93d1ec3a483fac17a369084888303ea4dee2fc211e712f5e59fed19203204111": "rolandgarros",
				"HDH:94c675159ee0920a4e67a34450b7f601e545936936615adbfe328a10537421da": "name",
				"HDH:cc863dde8bdf94120186915b27c6461b82c35a08d62fe7543146acf88de30065": "id",
				"HDH:fb1360e21653cb4325fdffb8d6a80c71fff584863c8369574448b4c6f3fb82e0": "hdh",
			},
			encryptionType: v1alpha1.Mask_legacy,
			wantErr:        false,
			want: `id;name;pseudo` + "\n" +
				`1;"    ";hdh` + "\n" +
				`2;france-television;n/a` + "\n" +
				`3;rolandgarros;` + "\n",
		},
		{
			name: "pairing with col name aes",

			input:          csvInAES,
			skipLines:      []int{1},
			columns:        []int{},
			columnNames:    []string{"id"},
			encryptionType: v1alpha1.Mask_aes,
			key:            "4145533235364B65792D33324368617261637465727331323334353637383930",

			wantErr: false,
			want:    csvOut,
		},
		{
			name: "pairing with col rank aes",

			input:          csvInAES,
			skipLines:      []int{1},
			columns:        []int{1},
			columnNames:    []string{},
			encryptionType: v1alpha1.Mask_aes,
			key:            "4145533235364B65792D33324368617261637465727331323334353637383930",

			wantErr: false,
			want:    csvOut,
		},
		{
			name: "pairing with col rank too small aes",

			input:          csvInAES,
			skipLines:      []int{},
			columns:        []int{-1},
			columnNames:    []string{},
			encryptionType: v1alpha1.Mask_aes,
			key:            "4145533235364B65792D33324368617261637465727331323334353637383930",

			wantErr: true,
			want:    "",
		},
		{
			name: "pairing with col rank out of bound aes",

			input:          csvInAES,
			skipLines:      []int{},
			columns:        []int{99},
			columnNames:    []string{},
			encryptionType: v1alpha1.Mask_aes,
			key:            "4145533235364B65792D33324368617261637465727331323334353637383930",

			wantErr: true,
			want:    "",
		},
		{
			name: "pairing with col rank too small aes",

			input:          csvInAES,
			skipLines:      []int{},
			columns:        []int{-1},
			columnNames:    []string{},
			encryptionType: v1alpha1.Mask_aes,
			key:            "4145533235364B65792D33324368617261637465727331323334353637383930",

			wantErr: true,
			want:    "",
		},
		{
			name: "pairing with col rank too small aes",

			input:          csvInAES,
			skipLines:      []int{},
			columns:        []int{-1},
			columnNames:    []string{},
			encryptionType: v1alpha1.Mask_aes,
			key:            "4145533235364B65792D33324368617261637465727331323334353637383930",

			wantErr: true,
			want:    "",
		},
		{
			name: "pairing with ignored values aes",

			input: `id;name;pseudo` + "\n" +
				`HDH:4d08788dc1d2bb1b69c354ab3ecd0d3658;HDH:14eed8ee01472e15e6dbcc83aeee7163c6194b203f9201061fefc8c6b9cb07;hdh` + "\n" +
				`HDH:4e96a6b4a8a36e0335586188aa56ba086e;france-television;ftv` + "\n" +
				`HDH:4f5d135c7073dd0b012e8d69d9dfd71d83;rolandgarros;` + "\n",
			skipLines:      []int{1},
			columns:        []int{1, 2},
			columnNames:    []string{},
			ignoreValues:   []string{"france-television", "rolandgarros"},
			encryptionType: v1alpha1.Mask_aes,
			key:            "4145533235364B65792D33324368617261637465727331323334353637383930",

			wantErr: false,
			want: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
		},

		{
			name: "pairing with fields containing double quotes aes",

			input: `id;name;pseudo` + "\n" +
				`HDH:4d08788dc1d2bb1b69c354ab3ecd0d3658;\"\";HDH:14efd18654eb7c1ddd9c5e45ee8c8b8e35e6f3` + "\n" +
				`HDH:4e96a6b4a8a36e0335586188aa56ba086e;france-\"television\";HDH:1affcf8607f2a84f7db0c571d0e1ea91280e7a` + "\n" +
				`HDH:4f5d135c7073dd0b012e8d69d9dfd71d83;\"rolandgarros\";HDH:30affb79edafdb19a66e2bbb16817d8d` + "\n",
			skipLines:      []int{1},
			columns:        []int{},
			columnNames:    []string{"id", "pseudo"},
			encryptionType: v1alpha1.Mask_aes,
			key:            "4145533235364B65792D33324368617261637465727331323334353637383930",

			wantErr: false,
			want: `id;name;pseudo` + "\n" +
				`1;"\""\""";hdh` + "\n" +
				`2;"france-\""television\""";ftv` + "\n" +
				`3;"\""rolandgarros\""";` + "\n",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			r := newPairingReader(tt.input, tt.skipLines, tt.columns, tt.columnNames, tt.ignoreValues, tt.ct, tt.encryptionType, tt.key)
			go func() {
				errP := r.Reader.process()
				r.Writer.CloseWithError(errP)
				if (errP != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", errP, tt.wantErr)
				}
			}()
			got, _ := io.ReadAll(r.Reader)
			if string(got) != tt.want {
				t.Errorf("got=\n%s\nwant=\n%v\n", got, tt.want)
			}
		})
	}
}
