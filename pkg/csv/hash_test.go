package csv

import (
	"encoding/csv"
	"io"
	"reflect"
	"strings"
	"testing"

	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"
)

type hashReaderTest struct {
	Reader *HashReader
	Writer *io.PipeWriter
}

func newHashReader(input string, skipLines, columns []int, columnNames []string, ignoreValues []string, encryptionType string) hashReaderTest {
	csvReader := csv.NewReader(strings.NewReader(input))
	csvReader.Comma = rune(";"[0])
	csvReader.LazyQuotes = true

	rOut, wOut := io.Pipe()

	var key string
	if encryptionType == v1alpha1.Mask_legacy {
		key = "0123456789abcdef"
	} else if encryptionType == v1alpha1.Mask_aes {
		key = "4145533235364B65792D33324368617261637465727331323334353637383930"
	}

	csvWriter := csv.NewWriter(wOut)
	csvWriter.Comma = rune(";"[0])

	createCT := true
	ctFileName := ""
	return hashReaderTest{
		Reader: &HashReader{
			log:            hdhlog.Discard(),
			in:             csvReader,
			Key:            key,
			Prefix:         "HDH",
			SkipLines:      skipLines,
			ColumnNames:    columnNames,
			Columns:        columns,
			ignoreValues:   ignoreValues,
			csvWriter:      csvWriter,
			createCT:       createCT,
			ctFileName:     ctFileName,
			ct:             masquage.CorrespondenceTable{},
			out:            rOut,
			EncryptionType: encryptionType,
		},
		Writer: wOut,
	}
}

func TestHashReader_process(t *testing.T) {
	csvOut := `id;name;pseudo` + "\n" +
		`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;health-data-hub;hdh` + "\n" +
		`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;france-television;ftv` + "\n" +
		`HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb;rolandgarros;` + "\n"
	ctOut := masquage.CorrespondenceTable{
		"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
		"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
		"HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb": "3",
	}

	csvOutAesSiv := `id;name;pseudo` + "\n" +
		`HDH:4d08788dc1d2bb1b69c354ab3ecd0d3658;health-data-hub;hdh` + "\n" +
		`HDH:4e96a6b4a8a36e0335586188aa56ba086e;france-television;ftv` + "\n" +
		`HDH:4f5d135c7073dd0b012e8d69d9dfd71d83;rolandgarros;` + "\n"

	tests := []struct {
		name string

		input          string
		skipLines      []int
		columns        []int
		columnNames    []string
		ignoreValues   []string
		encryptionType string

		wantErr bool
		want    string
		wantCt  masquage.CorrespondenceTable
	}{
		{
			name: "mask with col name legacy",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{1},
			columnNames:    []string{"id"},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want:    csvOut,
			wantCt:  ctOut,
		},
		{
			name: "mask with col name aes",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{1},
			columns:        []int{},
			columnNames:    []string{"id"},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_aes,

			wantErr: false,
			want:    csvOutAesSiv,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "mask with non existence col name legacy",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{0},
			columns:        []int{},
			columnNames:    []string{"dummy"},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			wantCt: masquage.CorrespondenceTable{},
		},
		{
			name: "mask with non existence col name aes",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{0},
			columns:        []int{},
			columnNames:    []string{"dummy"},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_aes,

			wantErr: false,
			want: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			wantCt: masquage.CorrespondenceTable{},
		},
		{
			name: "mask with col rank legacy",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{1},
			columns:        []int{1},
			columnNames:    []string{},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want:    csvOut,
			wantCt:  ctOut,
		},
		{
			name: "mask with col rank aes",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{1},
			columns:        []int{1},
			columnNames:    []string{},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_aes,

			wantErr: false,
			want:    csvOutAesSiv,
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "mask with col rank out of bound legacy",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{0},
			columns:        []int{99},
			columnNames:    []string{},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: true,
			want:    "",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "mask with col rank out of bound aes",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{0},
			columns:        []int{99},
			columnNames:    []string{},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_aes,

			wantErr: true,
			want:    "",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "mask with col rank too small legacy",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{},
			columns:        []int{-1},
			columnNames:    []string{},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: true,
			want:    "",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "mask with col rank too small aes",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{},
			columns:        []int{-1},
			columnNames:    []string{},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_aes,

			wantErr: true,
			want:    "",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name: "mask with 2 cols legacy",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{1},
			columns:        []int{},
			columnNames:    []string{"id", "name"},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want: `id;name;pseudo` + "\n" +
				`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;HDH:1c097515e0d8b3b520e145fa60bdeba3b6387f4add2272469087d7ac30642e9a;hdh` + "\n" +
				`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;HDH:92139665a786179214c6039c32e2bb489ba597d432575dccfdefc5de70aa3299;ftv` + "\n" +
				`HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb;HDH:93d1ec3a483fac17a369084888303ea4dee2fc211e712f5e59fed19203204111;` + "\n",
			wantCt: masquage.CorrespondenceTable{
				"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
				"HDH:1c097515e0d8b3b520e145fa60bdeba3b6387f4add2272469087d7ac30642e9a": "health-data-hub",
				"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
				"HDH:92139665a786179214c6039c32e2bb489ba597d432575dccfdefc5de70aa3299": "france-television",
				"HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb": "3",
				"HDH:93d1ec3a483fac17a369084888303ea4dee2fc211e712f5e59fed19203204111": "rolandgarros",
			},
		},
		{
			name: "mask with 2 cols aes",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{1},
			columns:        []int{},
			columnNames:    []string{"id", "name"},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_aes,

			wantErr: false,
			want: `id;name;pseudo` + "\n" +
				`HDH:4d08788dc1d2bb1b69c354ab3ecd0d3658;HDH:14eed8ee01472e15e6dbcc83aeee7163c6194b203f9201061fefc8c6b9cb07;hdh` + "\n" +
				`HDH:4e96a6b4a8a36e0335586188aa56ba086e;HDH:1af9d8ec164a2e05e2c3c8d8afe87a6f67009d5ba3a6ceddbec620ec72e46deca0;ftv` + "\n" +
				`HDH:4f5d135c7073dd0b012e8d69d9dfd71d83;HDH:0ee4d5e31b4b6410f5ddc2ddd94cdd23ad9a9259de7d184959f005af;` + "\n",
			wantCt: masquage.CorrespondenceTable{},
		},
		{
			name: "mask without skipping first line legacy",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{0},
			columns:        []int{1},
			columnNames:    []string{},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want: `HDH:cc863dde8bdf94120186915b27c6461b82c35a08d62fe7543146acf88de30065;name;pseudo` + "\n" +
				`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;health-data-hub;hdh` + "\n" +
				`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;france-television;ftv` + "\n" +
				`HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb;rolandgarros;` + "\n",
			wantCt: masquage.CorrespondenceTable{
				"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
				"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
				"HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb": "3",
				"HDH:cc863dde8bdf94120186915b27c6461b82c35a08d62fe7543146acf88de30065": "id",
			},
		},
		{
			name: "mask without skipping first line aes",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{0},
			columns:        []int{1},
			columnNames:    []string{},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_aes,

			wantErr: false,
			want: `HDH:15ef276b3330f6569e846b6d0bd4734ddce0;name;pseudo` + "\n" +
				`HDH:4d08788dc1d2bb1b69c354ab3ecd0d3658;health-data-hub;hdh` + "\n" +
				`HDH:4e96a6b4a8a36e0335586188aa56ba086e;france-television;ftv` + "\n" +
				`HDH:4f5d135c7073dd0b012e8d69d9dfd71d83;rolandgarros;` + "\n",
			wantCt: masquage.CorrespondenceTable{},
		},
		{
			name: "mask with ignored values legacy",

			input: `id;name;pseudo` + "\n" +
				`1;    ;hdh` + "\n" +
				`2;france-television;n/a` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{0},
			columns:        []int{},
			columnNames:    []string{"id", "name", "pseudo"},
			ignoreValues:   []string{"france-television", "n/a", ""},
			encryptionType: v1alpha1.Mask_legacy,

			wantErr: false,
			want: `HDH:cc863dde8bdf94120186915b27c6461b82c35a08d62fe7543146acf88de30065;HDH:94c675159ee0920a4e67a34450b7f601e545936936615adbfe328a10537421da;HDH:63c6551e0676338575f016e602b7a05cc6340290cf87e744b090d32f0dfa583d` + "\n" +
				`HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671;"    ";HDH:fb1360e21653cb4325fdffb8d6a80c71fff584863c8369574448b4c6f3fb82e0` + "\n" +
				`HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0;france-television;n/a` + "\n" +
				`HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb;HDH:93d1ec3a483fac17a369084888303ea4dee2fc211e712f5e59fed19203204111;` + "\n",
			wantCt: masquage.CorrespondenceTable{
				"HDH:1c8495ac371d51eabff02462f51e3c551a5394c759dc7476240aa199c016fbeb": "3",
				"HDH:5e20255a41357249a020d8a5b2a71e1bb04ee896438e0a5b792d8c8c0760e671": "1",
				"HDH:63c6551e0676338575f016e602b7a05cc6340290cf87e744b090d32f0dfa583d": "pseudo",
				"HDH:8d18ef5ba1eee1cf13929fecfec8d4d654a83ea3a147e3266cbebdf6e3f726f0": "2",
				"HDH:93d1ec3a483fac17a369084888303ea4dee2fc211e712f5e59fed19203204111": "rolandgarros",
				"HDH:94c675159ee0920a4e67a34450b7f601e545936936615adbfe328a10537421da": "name",
				"HDH:cc863dde8bdf94120186915b27c6461b82c35a08d62fe7543146acf88de30065": "id",
				"HDH:fb1360e21653cb4325fdffb8d6a80c71fff584863c8369574448b4c6f3fb82e0": "hdh",
			},
		},
		{
			name: "mask with ignored values aes",

			input: `id;name;pseudo` + "\n" +
				`1;health-data-hub;hdh` + "\n" +
				`2;france-television;ftv` + "\n" +
				`3;rolandgarros;` + "\n",
			skipLines:      []int{0},
			columns:        []int{1, 2},
			columnNames:    []string{},
			ignoreValues:   []string{"france-television", "rolandgarros"},
			encryptionType: v1alpha1.Mask_aes,

			wantErr: false,
			want: `HDH:15ef276b3330f6569e846b6d0bd4734ddce0;HDH:12ead4e7920518e528117133dc40041a064df2db;pseudo` + "\n" +
				`HDH:4d08788dc1d2bb1b69c354ab3ecd0d3658;HDH:14eed8ee01472e15e6dbcc83aeee7163c6194b203f9201061fefc8c6b9cb07;hdh` + "\n" +
				`HDH:4e96a6b4a8a36e0335586188aa56ba086e;france-television;ftv` + "\n" +
				`HDH:4f5d135c7073dd0b012e8d69d9dfd71d83;rolandgarros;` + "\n",
			wantCt: masquage.CorrespondenceTable{},
		},
		{
			name: "mask with fields containing double quotes aes",

			input: `id;name;pseudo` + "\n" +
				`1;\"\";hd"h` + "\n" +
				`2;france-\"television\";\"ftv\"` + "\n" +
				`3;roland"garros;` + "\n",
			skipLines:      []int{1},
			columns:        []int{},
			columnNames:    []string{"id", "name"},
			ignoreValues:   []string{},
			encryptionType: v1alpha1.Mask_aes,

			wantErr: false,
			want: `id;name;pseudo` + "\n" +
				`HDH:4d08788dc1d2bb1b69c354ab3ecd0d3658;HDH:20a9e5a0950d00c3c498007a9523a78ebfe442d2;"hd""h"` + "\n" +
				`HDH:4e96a6b4a8a36e0335586188aa56ba086e;HDH:1af9d8ec164a2e2da5dbc8c2a3ed7a7360194f600722c5f97116e4093e95365e9720303808;"\""ftv\"""` + "\n" +
				`HDH:4f5d135c7073dd0b012e8d69d9dfd71d83;HDH:0ee4d5e31b4b2116e6dddfc1b5984651440d85f487f50097bd0d486937;` + "\n",
			wantCt: masquage.CorrespondenceTable{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			r := newHashReader(tt.input, tt.skipLines, tt.columns, tt.columnNames, tt.ignoreValues, tt.encryptionType)

			go func() {
				errP := r.Reader.process()
				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()
			got, _ := io.ReadAll(r.Reader)
			if string(got) != tt.want {
				t.Errorf("got =\n%s, want=\n%v", got, tt.want)
			}
			if !reflect.DeepEqual(r.Reader.ct, tt.wantCt) {
				t.Errorf("got =\n%s, want=\n%v", r.Reader.ct, tt.wantCt)
			}
		})
	}
}

func Test_convertColNameToColNum(t *testing.T) {
	type args struct {
		cols  []string
		names []string
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		{
			name: "test colnum",
			args: args{
				cols:  []string{"name", "id", "desc"},
				names: []string{"id"},
			},
			want:    []int{2},
			wantErr: false,
		},
		{
			name: "test not found",
			args: args{
				cols:  []string{"name", "id", "desc"},
				names: []string{"my_id"},
			},
			want:    nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			got, err := convertColNameToColNum(tt.args.cols, tt.args.names, hdhlog.Discard())
			if (err != nil) != tt.wantErr {
				t.Errorf("convertColNameToColNumCSV() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("convertColNameToColNumCSV() = %v, want %v", got, tt.want)
			}
		})
	}
}
