package csv

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"sort"
	"strings"

	"rdd-pipeline/pkg/schema/v1alpha1"

	"rdd-pipeline/pkg/masquage"

	hdhlog "rdd-pipeline/pkg/log"
)

var _ io.Reader = (*PairingReader)(nil)

type PairingReader struct {
	in             *csv.Reader
	SkipLines      []int
	ColumnNames    []string
	ignoreValues   []string
	Columns        []int
	currentline    int
	csvWriter      *csv.Writer
	out            io.Reader
	ct             masquage.CorrespondenceTable
	log            hdhlog.Logger
	EncryptionType string
	key            string
}

func NewPairingReader(log hdhlog.Logger, in io.Reader, separator string, skipLines []int, columns []int, columnNames []string,
	ignoreValues []string, ct masquage.CorrespondenceTable, encryptionType, key string) io.Reader {
	csvReader := csv.NewReader(in)
	csvReader.Comma = rune(separator[0])
	csvReader.LazyQuotes = true

	rOut, wOut := io.Pipe()

	csvWriter := csv.NewWriter(wOut)
	csvWriter.Comma = rune(separator[0])

	sort.Ints(skipLines)

	log = log.With("separator", separator)
	log = log.With("skipLines", skipLines)
	log = log.With("columns", columns)
	log = log.With("columnNames", columnNames)
	log = log.With("encryptionType", encryptionType)
	log = log.With("ignoreValues", ignoreValues)

	pairingReader := &PairingReader{
		in:             csvReader,
		csvWriter:      csvWriter,
		out:            rOut,
		SkipLines:      skipLines,
		ColumnNames:    columnNames,
		ignoreValues:   ignoreValues,
		Columns:        columns,
		currentline:    0,
		ct:             ct,
		log:            log,
		EncryptionType: encryptionType,
		key:            key,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := pairingReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error pairing")
		}
	})()

	return pairingReader
}

func (r *PairingReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *PairingReader) process() error {
	defer r.csvWriter.Flush()

	maskCols := r.Columns

	for {
		// 1. Iterate over columns from the input blob
		// 2. Find Correspondence Table Key (K) within each line
		// 3. Replace the key (K) in input CSV by the value (V) from Correspondence Table
		r.currentline++

		if r.currentline%10e5 == 0 {
			r.log.Info("current line CSV", "currentline", r.currentline)
		}

		line, err := r.in.Read()

		if err != nil {
			// end of file
			if errors.Is(err, io.EOF) {
				return nil
			}
			return fmt.Errorf("can't read CSV: %w", err)
		}

		// convert colnames to colnums
		if r.currentline == 1 {
			if len(r.ColumnNames) > 0 {
				r.log.Info("masking columns", "names", r.ColumnNames)
				maskCols, err = convertColNameToColNum(line, r.ColumnNames, r.log)
				if err != nil {
					return fmt.Errorf("converting colname to colnum : %w", err)
				}
			}
			r.log.Info("masking columns", "positions", maskCols, "skiplines", r.SkipLines)
		}

		// Skip line
		if n := sort.SearchInts(r.SkipLines, r.currentline); !(n < len(r.SkipLines) && r.SkipLines[n] == r.currentline) {
			for _, col := range maskCols {
				i := col - 1

				if col > len(line) {
					return fmt.Errorf("column-position too big: %d > %d", col, len(line))
				} else if i < 0 {
					return fmt.Errorf("column-position too small: %d", col)
				}
				id := line[i]
				if r.ignoreValue(id) {
					continue
				}
				var idUnmasked string
				var ok bool
				if r.EncryptionType == v1alpha1.Mask_legacy {
					idUnmasked, ok = r.ct[id]
					if !ok {
						return fmt.Errorf("CT key at line %d/%d could not be found", col, r.currentline)
					}
				} else if r.EncryptionType == v1alpha1.Mask_aes {
					// Removing the prefix (isn't encrypted)
					idNoPrefix := id[strings.Index(id, ":")+1:]
					idUnmasked, err = masquage.DecryptAES(idNoPrefix, r.key)
					if err != nil {
						return fmt.Errorf("decrypting using AES : %w", err)
					}
				}
				line[i] = idUnmasked
			}
		}

		if err := r.csvWriter.Write(line); err != nil {
			return fmt.Errorf("can't write CSV: %w", err)
		}
	}
}

func (r PairingReader) ignoreValue(s string) bool {
	for _, v := range r.ignoreValues {
		if strings.TrimSpace(s) == v {
			return true
		}
	}
	return false
}
