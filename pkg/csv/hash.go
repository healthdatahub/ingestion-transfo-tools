package csv

import (
	"bytes"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"os"
	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/preview"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"regexp"
	"sort"
	"strings"

	hdhlog "rdd-pipeline/pkg/log"
)

var _ io.Reader = (*HashReader)(nil)

type HashReader struct {
	in             *csv.Reader
	SkipLines      []int
	Prefix         string
	ColumnNames    []string
	ignoreValues   []string
	Columns        []int
	Key            string
	currentline    int
	csvWriter      *csv.Writer
	out            *io.PipeReader
	createCT       bool
	ctFileName     string
	ct             masquage.CorrespondenceTable
	log            hdhlog.Logger
	EncryptionType string
}

func NewHashReader(log hdhlog.Logger, in io.Reader, separator string, key string, prefix string, skipLines []int, columns []int, columnNames []string, ignoreValues []string, ct masquage.CorrespondenceTable, createCT bool, ctFileName, encryptionType string) (io.Reader, string) {
	log = log.With("prefix", prefix).
		With("skipLines", skipLines).
		With("columns", columns).
		With("columnNames", columnNames).
		With("createCT", createCT).
		With("encryptionType", encryptionType).
		With("ignoreValues", ignoreValues)

	var csvReader *csv.Reader
	if separator == "" {
		log.Info("No separator was set in the configuration file, we will try to autodetect it")
		// Buffer to store first 50 lines
		var buffer bytes.Buffer
		lineCount := 0

		// Read character by character until 50 lines are reached
		for {
			char := make([]byte, 1)
			_, err := in.Read(char)

			// Break if end of file is reached
			if err == io.EOF {
				break
			}

			if err != nil {
				log.Error(err, "Error while reading the first lines of the file")
				return nil, ""
			}

			buffer.Write(char)

			// Check for line breaks
			if char[0] == '\n' || char[0] == '\r' {
				lineCount++
				if lineCount >= 50 {
					break
				}
			}
		}

		// Autodetect the separator from the first lines
		bufferString := buffer.String()
		re := regexp.MustCompile(`\r\n|\n|\r`)
		stringSlice := re.Split(bufferString, -1)

		// Check if the last element is an empty string and remove it if present
		if len(stringSlice) > 0 && stringSlice[len(stringSlice)-1] == "" {
			stringSlice = stringSlice[:len(stringSlice)-1]
		}
		mostConsistentChar, _, err := preview.GuessDelimiter(stringSlice)
		if err != nil {
			log.Error(err, "No specific separator was found")
			return nil, ""
		} else {
			separator = string(mostConsistentChar)
		}

		// Use io.MultiReader to concatenate the buffer with the remaining file stream
		multiReader := io.MultiReader(&buffer, in)
		// Create the CSV reader from the new combined reader
		csvReader = csv.NewReader(multiReader)
		csvReader.Comma = rune(separator[0])
	} else {
		csvReader = csv.NewReader(in)
		csvReader.Comma = rune(separator[0])
	}
	log = log.With("separator", separator)

	// csv parser follows RFC4180 and fails when double quotes are not enclosed.
	// (see https://www.rfc-editor.org/rfc/rfc4180#section-2)
	// to avoid this error we set LazyQuotes parameter to true.
	csvReader.LazyQuotes = true

	rOut, wOut := io.Pipe()

	csvWriter := csv.NewWriter(wOut)
	csvWriter.Comma = rune(separator[0])

	sort.Ints(skipLines)

	hashReader := &HashReader{
		in:             csvReader,
		csvWriter:      csvWriter,
		out:            rOut,
		SkipLines:      skipLines,
		ColumnNames:    columnNames,
		ignoreValues:   ignoreValues,
		Columns:        columns,
		Prefix:         prefix,
		Key:            key,
		currentline:    0,
		ct:             ct,
		createCT:       createCT,
		ctFileName:     ctFileName,
		log:            log,
		EncryptionType: encryptionType,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := hashReader.process()
		if err := wOut.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()

	return hashReader, separator
}

func (r *HashReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *HashReader) process() error {
	var ctFile *os.File
	var err error
	start := ""
	hash := ""
	idin := ""
	var ok bool
	defer r.csvWriter.Flush()

	if r.createCT {
		// r.ctFileName != "" means that we use a local ct file instead of a map to avoir OOM errors (too much RAM usage)
		if r.ctFileName != "" {
			ctFile, err = os.OpenFile(r.ctFileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
			if err != nil {
				return fmt.Errorf("opening tmp ct file : %w", err)
			}

			defer func() {
				if err := ctFile.Close(); err != nil {
					r.log.Error(err, "close tmp file %w")
				}
			}()
		}
	}

	for {
		r.currentline++
		if r.currentline%10e5 == 0 {
			r.log.Info("current line CSV", "currentline", r.currentline)
		}

		line, err := r.in.Read()
		if err != nil {
			// end of file
			if errors.Is(err, io.EOF) {
				return nil
			}
			return fmt.Errorf("can't read CSV: %w", err)
		}

		// convert colnames to colnums
		if r.currentline == 1 {
			if len(r.ColumnNames) > 0 {
				r.log.Info("masking columns", "names", r.ColumnNames)
				r.Columns, err = convertColNameToColNum(line, r.ColumnNames, r.log)
				if err != nil {
					return fmt.Errorf("converting colname to colnum : %w", err)
				}
			}
			r.log.Info("masking columns", "positions", r.Columns, "skiplines", r.SkipLines)
		}

		// Skip lines
		if n := sort.SearchInts(r.SkipLines, r.currentline); !(n < len(r.SkipLines) && r.SkipLines[n] == r.currentline) {
			// mask colums
			for _, col := range r.Columns {
				iC := col - 1

				if col > len(line) {
					return fmt.Errorf("column-position too big: %d > %d", col, len(line))
				} else if iC < 0 {
					return fmt.Errorf("column-position too small: %d", col)
				}

				id := line[iC]
				if r.ignoreValue(id) {
					continue
				}
				if r.EncryptionType == v1alpha1.Mask_aes {
					hash, err = masquage.EncryptAES(id, r.Key)
				} else {
					// legacy hmac with a ct
					hash, err = masquage.HMAC(id, r.Key)
				}
				if err != nil {
					return fmt.Errorf("hash CSV %w", err)
				}
				if r.Prefix != "" {
					hash = r.Prefix + ":" + hash
				}
				line[iC] = hash
				if r.createCT && r.EncryptionType != v1alpha1.Mask_aes {
					if idin, ok = r.ct[hash]; ok {
						if idin != id {
							return fmt.Errorf("collision on hash: %s", hash)
						}
					} else {

						if r.ctFileName != "" {
							_, err = ctFile.Write([]byte(start + "\n" + "\"" + hash + "\"" + ":" + "\"" + id + "\""))
							start = ","
							if err != nil {
								return fmt.Errorf("adding value in the ct file: %w", err)
							}
						} else {
							if idin, ok = r.ct[hash]; ok {
								if idin != id {
									return fmt.Errorf("collision on hash: %s", hash)
								}
							} else {
								r.ct[hash] = id
							}
						}
					}
				}
			}
		}
		if err := r.csvWriter.Write(line); err != nil {
			return fmt.Errorf("can't write CSV: %w", err)
		}
	}
}
func convertColNameToColNum(cols []string, names []string, log hdhlog.Logger) ([]int, error) {
	colDict := make(map[string]int)
	for colRank, colName := range cols {
		colDict[colName] = colRank + 1
	}

	var colIds []int
	for _, name := range names {
		if colRank, exist := colDict[name]; exist {
			colIds = append(colIds, colRank)
		} else {
			log.Info("[WARNING] column does not exist in col list but has been specified in your masking configuration", "columnName", name)
		}
	}
	return colIds, nil
}

func (r HashReader) ignoreValue(s string) bool {
	for _, v := range r.ignoreValues {
		if strings.TrimSpace(s) == v {
			return true
		}
	}
	return false
}
