package dicomMask

import (
	"io"
	"testing"

	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"
)

type pairingReaderTest struct {
	Reader *PairingReader
	Writer *io.PipeWriter
}

func newPairingReader(t *testing.T, filename string, tags, ignoreValues []string, ct masquage.CorrespondenceTable, encryptionType, key string) pairingReaderTest {

	file, err := testdata.Open(filename)
	if err != nil {
		t.Errorf("Couldn't open file for the test: %s", err)
	}
	rOut, wOut := io.Pipe()

	return pairingReaderTest{
		Reader: &PairingReader{
			in:          file,
			dicomWriter: wOut,
			out:         rOut,

			log:            hdhlog.Discard(),
			tags:           tags,
			ignoreValues:   ignoreValues,
			ct:             ct,
			encryptionType: encryptionType,
			key:            key,
		},
		Writer: wOut,
	}
}

// this function covers the legacy approach where ciphers are stored directly in the tag
// but makes the dicom files unreadable by some tools like orthanc
func TestPairingReaderWithCiphersInTags(t *testing.T) {

	tests := []struct {
		name string

		filename       string
		tags           []string
		ignoreValues   []string
		ct             masquage.CorrespondenceTable
		encryptionType string
		key            string

		wantErr bool
		want    string
	}{
		{
			name: "DICOM pairing simple legacy",

			filename: "testdata/mr_small_pairing_before_custom_tags_legacy.dcm",

			tags: []string{"0010,0020"},
			ct: masquage.CorrespondenceTable{
				"FHDH:41f67ceffa39628cf2201c9334bef9707e3ee84abd0cc5e937fa2d87bc0c5844": "4MR1",
			},
			encryptionType: v1alpha1.Mask_legacy,
			wantErr:        false,
			want:           "testdata/mr_small_pairing_before_custom_tags_legacy_out.dcm",
		},
		{
			name: "DICOM pairing simple aes",

			filename: "testdata/mr_small_pairing_before_custom_tags_aes.dcm",

			tags:           []string{"0010,0020"},
			encryptionType: v1alpha1.Mask_aes,
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",

			wantErr: false,
			want:    "testdata/mr_small_pairing_before_custom_tags_aes_out.dcm",
		},
		{
			name: "DICOM pairing ignore specific values legacy",

			filename: "testdata/mr_small_pairing_before_custom_tags_legacy_ignore_values.dcm",

			tags: []string{
				"0010,0020",
				"0002,0016", /* tag containing "CLUNIE1" */
			},
			ct: masquage.CorrespondenceTable{
				"FHDH:41f67ceffa39628cf2201c9334bef9707e3ee84abd0cc5e937fa2d87bc0c5844": "4MR1",
				"CLUNIE1": "THIS_VALUE_SHOULD_NOT_APPEAR_IN_THE_OUTPUT_FILE",
			},
			encryptionType: v1alpha1.Mask_legacy,
			ignoreValues:   []string{"CLUNIE1"},

			wantErr: false,
			want:    "testdata/mr_small_pairing_before_custom_tags_legacy_ignore_values_out.dcm",
		},
		{
			name: "DICOM pairing ignore specific values aes",

			filename: "testdata/mr_small_pairing_before_custom_tags_aes_ignore_values.dcm",

			tags: []string{
				"0010,0020",
				"0002,0016", /* tag containing "CLUNIE1" */
			},
			encryptionType: v1alpha1.Mask_aes,
			ignoreValues:   []string{"CLUNIE1"},
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",

			wantErr: false,
			want:    "testdata/mr_small_pairing_before_custom_tags_aes_ignore_values_out.dcm",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			r := newPairingReader(t, tt.filename, tt.tags, tt.ignoreValues, tt.ct, tt.encryptionType, tt.key)

			go func() {
				errP := r.Reader.process()
				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("PairingReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()

			got, err := io.ReadAll(r.Reader)
			if err != nil {
				t.Errorf("Couldn't read the output: %+v", err) //nolint
			}
			wantBytes, err := testdata.ReadFile(tt.want)
			if err != nil {
				t.Errorf("Couldn't read the expected result file: %v", err) //nolint
			}

			if !testEq(got, wantBytes) {
				t.Errorf("Dicom output does not match expected file (%s)\n", tt.want)
				// uncomment to debug
				// os.WriteFile("/tmp/dicom_output_test_file", got, fs.ModePerm)
			}
		})
	}
}

func TestPairingReader(t *testing.T) {

	tests := []struct {
		name string

		filename       string
		tags           []string
		ignoreValues   []string
		ct             masquage.CorrespondenceTable
		encryptionType string
		key            string

		wantErr bool
		want    string
	}{
		{
			name: "DICOM pairing simple legacy",

			filename: "testdata/mr_small_pairing_legacy.dcm",

			tags: []string{"0010,0020"},
			ct: masquage.CorrespondenceTable{
				"FHDH:41f67ceffa39628cf2201c9334bef9707e3ee84abd0cc5e937fa2d87bc0c5844": "4MR1",
			},
			encryptionType: v1alpha1.Mask_legacy,
			wantErr:        false,
			want:           "testdata/mr_small_pairing_legacy_out.dcm",
		},
		{
			name: "DICOM pairing simple aes",

			filename: "testdata/mr_small_pairing_aes.dcm",

			tags:           []string{"0010,0020"},
			encryptionType: v1alpha1.Mask_aes,
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",

			wantErr: false,
			want:    "testdata/mr_small_pairing_aes_out.dcm",
		},
		{
			name: "DICOM pairing ignore specific values legacy",

			filename: "testdata/mr_small_pairing_legacy_ignore_values.dcm",

			tags: []string{
				"0010,0020",
				"0002,0016", /* tag containing "CLUNIE1" */
			},
			ct: masquage.CorrespondenceTable{
				"FHDH:41f67ceffa39628cf2201c9334bef9707e3ee84abd0cc5e937fa2d87bc0c5844": "4MR1",
				"CLUNIE1": "THIS_VALUE_SHOULD_NOT_APPEAR_IN_THE_OUTPUT_FILE",
			},
			encryptionType: v1alpha1.Mask_legacy,
			ignoreValues:   []string{"CLUNIE1"},

			wantErr: false,
			want:    "testdata/mr_small_pairing_legacy_ignore_values_out.dcm",
		},
		{
			name: "DICOM pairing ignore specific values aes",

			filename: "testdata/mr_small_pairing_aes_ignore_values.dcm",

			tags: []string{
				"0010,0020",
				"0002,0016", /* tag containing "CLUNIE1" */
			},
			encryptionType: v1alpha1.Mask_aes,
			ignoreValues:   []string{"CLUNIE1"},
			key:            "635266556A586E5A7234753778214125442A472D4B6150645367566B59703373",

			wantErr: false,
			want:    "testdata/mr_small_pairing_aes_ignore_values_out.dcm",
		},
		{
			name: "DICOM pairing on a file masked twice (mac+aes)",

			filename: "testdata/mr_small_pairing_legacy_then_aes.dcm",

			tags:           []string{"0010,0020"},
			encryptionType: v1alpha1.Mask_aes,
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",

			wantErr: false,
			want:    "testdata/mr_small_pairing_legacy_then_aes_out.dcm",
		},
		{
			name: "DICOM pairing legacy with implicit transfert syntax",

			filename: "testdata/implicit_pairing_legacy.dcm",

			tags: []string{"0010,0020", "0010,0021"},
			ct: masquage.CorrespondenceTable{
				"FHDH:6b9fec95a222069d19f9e4370da11386c758733e816edde633e031c5cd6889b2": "Jean Dujardin",
				"FHDH:ee78ccead1c49241a14d14393ccd31acf7c8ea8c88de0226f0340f3690f686b5": "Clinique Jean Vilars",
			},
			encryptionType: v1alpha1.Mask_legacy,
			wantErr:        false,
			want:           "testdata/implicit_pairing_legacy_out.dcm",
		},
		{
			name: "DICOM pairing simple aes with implicit transfert syntax",

			filename: "testdata/implicit_pairing_legacy_then_aes.dcm",

			tags:           []string{"0010,0020", "0010,0021"},
			encryptionType: v1alpha1.Mask_aes,
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",

			wantErr: false,
			want:    "testdata/implicit_pairing_legacy_then_aes_out.dcm",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

			r := newPairingReader(t, tt.filename, tt.tags, tt.ignoreValues, tt.ct, tt.encryptionType, tt.key)

			go func() {
				errP := r.Reader.process()
				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("PairingReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()

			got, err := io.ReadAll(r.Reader)
			if err != nil {
				t.Errorf("Couldn't read the output: %+v", err) //nolint
			}
			wantBytes, err := testdata.ReadFile(tt.want)
			if err != nil {
				t.Errorf("Couldn't read the expected result file: %v", err) //nolint
			}

			if !testEq(got, wantBytes) {
				t.Errorf("Dicom output does not match expected file (%s)\n", tt.want)
				// uncomment to debug
				// os.WriteFile("/tmp/dicom_output_test_file", got, fs.ModePerm)
			}
		})
	}
}
