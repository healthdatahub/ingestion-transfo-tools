// package dicomMask contains the logic to mask and unmask dicom files
// once masked, values are stored in private dicom tag (7777, 0020) while
// (7777, 0010) contains the implementor (Health Data Hub).
package dicomMask

import (
	"fmt"
	hdhlog "rdd-pipeline/pkg/log"
	"strconv"
	"strings"

	"github.com/suyashkumar/dicom"
	"github.com/suyashkumar/dicom/pkg/tag"
)

const (
	HDHOrgRoot     = "1.2.826.0.1.3680043.10.1304" // official HDH org root, do not change.
	hdhImplementor = "Health Data Hub"
)

var (
	hdhPrivateTagImplementor = tag.Tag{Group: 0x7777, Element: 0x0010}
	hdhPrivateTagCipherMap   = tag.Tag{Group: 0x7777, Element: 0x0020}
)

// ignoreValue returns true if the value has to be ignored
func ignoreValue(valuesToIgnore []string, s string) bool {
	for _, v := range valuesToIgnore {
		if strings.TrimSpace(s) == v {
			return true
		}
	}
	return false
}

// convertToDicomTags parse hexa string and create a tag object structures
// It silently ignores unsupported metadata or error to preserve legacy behavior
func convertToDicomTags(log hdhlog.Logger, input []string) ([]tag.Tag, error) {
	tags := make([]tag.Tag, len(input))
	for i, currentTag := range input {
		hexa := strings.Split(currentTag, ",")
		if len(hexa) != 2 {
			log.Info(fmt.Sprintf("expect two parts in tag %q but got %d", currentTag, len(hexa)))
			return nil, fmt.Errorf("expect two parts in tag but got %q", currentTag)
		}
		decimalGroup, err := strconv.ParseInt(strings.TrimSpace(hexa[0]), 16, 64)
		if err != nil {
			log.Info(fmt.Sprintf("parsing hexa value: %s", err))
			return nil, fmt.Errorf("failed to parse hexa value %q", strings.TrimSpace(hexa[0]))
		}
		decimalElement, err := strconv.ParseInt(strings.TrimSpace(hexa[1]), 16, 64)
		if err != nil {
			log.Info(fmt.Sprintf("parsing hexa value: %s", err))
			return nil, fmt.Errorf("failed to parse hexa value %q", strings.TrimSpace(hexa[1]))
		}
		tags[i] = tag.Tag{Group: uint16(decimalGroup), Element: uint16(decimalElement)}
	}
	return tags, nil
}

// implementorTagExists returns true if the tag (7777, 0010) exists
func implementorTagExists(d dicom.Dataset) (bool, error) {
	_, err := d.FindElementByTag(hdhPrivateTagImplementor)
	if err == dicom.ErrorElementNotFound {
		return false, nil
	} else if err != nil {
		return false, err
	}
	return true, nil
}

// cipherFromMap returns the cipher from the cipher map if it exists
func cipherFromMap(ciphers []string, cipher string) string {
	for _, c := range ciphers {
		if strings.HasPrefix(c, cipher) {
			return strings.Split(c, "::")[1]
		}
	}
	return ""
}

// cleanupDataset drops the hdh implementors and cipher map elements
// if no elements starting with HDH org root remains in any tag content
func cleanupDataset(d *dicom.Dataset) {
	for _, e := range d.Elements {
		if e.Value.ValueType() == dicom.Strings && e.Tag != hdhPrivateTagCipherMap {
			for _, v := range e.Value.GetValue().([]string) {
				if strings.HasPrefix(v, HDHOrgRoot) {
					return // at least one element remain, keep everything
				}
			}
		}
	}
	for i := len(d.Elements) - 1; i >= 0; i-- {
		if d.Elements[i].Tag == hdhPrivateTagImplementor || d.Elements[i].Tag == hdhPrivateTagCipherMap {
			d.Elements = append(d.Elements[:i], d.Elements[i+1:]...)
		}
	}
}
