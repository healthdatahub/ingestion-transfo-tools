package dicomMask

import (
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"strings"

	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/suyashkumar/dicom"
	"github.com/suyashkumar/dicom/pkg/tag"
)

var _ io.Reader = (*HashReader)(nil)

type HashReader struct {
	in          io.Reader // incoming data stream
	dicomWriter io.Writer // the dicom lib writes here
	out         io.Reader // the caller reads here

	prefix         string
	tags           []string
	ignoreValues   []string
	key            string
	ct             masquage.CorrespondenceTable
	log            hdhlog.Logger
	encryptionType string
}

func NewHashReader(log hdhlog.Logger, in io.Reader, key string, prefix string, tags []string, ignoreValues []string, ct masquage.CorrespondenceTable, encryptionType string) io.Reader {

	dicomReader := in
	pipeR, pipeW := io.Pipe()

	log = log.With("prefix", prefix)
	log = log.With("tags", tags)
	log = log.With("Encryption Type", encryptionType)
	log = log.With("ignoreValues", ignoreValues)

	hashReader := &HashReader{
		in:             dicomReader,
		dicomWriter:    pipeW,
		out:            pipeR,
		tags:           tags,
		ignoreValues:   ignoreValues,
		prefix:         prefix,
		key:            key,
		ct:             ct,
		log:            log,
		encryptionType: encryptionType,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := hashReader.process()
		if err := pipeW.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()
	return hashReader
}

func (r *HashReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func ensureImplementorTag(d *dicom.Dataset) error {
	_, err := d.FindElementByTag(hdhPrivateTagImplementor)
	if err == nil {
		return nil // already present
	} else if err != dicom.ErrorElementNotFound {
		return fmt.Errorf("finding private tag implementor: %w", err)
	}
	v, err := dicom.NewValue([]string{hdhImplementor})
	if err != nil {
		return fmt.Errorf("creating new tag value: %w", err)
	}
	investor := &dicom.Element{
		Tag:                    hdhPrivateTagImplementor,
		ValueRepresentation:    tag.VRStringList,
		RawValueRepresentation: "LO",
		Value:                  v,
	}
	d.Elements = append(d.Elements, investor)
	return nil
}

func (r *HashReader) process() error {

	r.log.Info("Start processing file masking")

	dataset, err := dicom.ParseUntilEOF(r.in, nil,
		dicom.AllowMismatchPixelDataLength(),
		dicom.SkipProcessingPixelDataValue())
	if err != nil {
		return fmt.Errorf("parsing dicom: %w", err)
	}
	// create the map tag if does not exist
	newCiphers := []string{}
	existingCiphers := []string{}
	cipherMap, err := dataset.FindElementByTag(hdhPrivateTagCipherMap)
	if err == dicom.ErrorElementNotFound {
		v, err := dicom.NewValue(newCiphers)
		if err != nil {
			return fmt.Errorf("creating new tag value: %w", err)
		}
		cipherMap = &dicom.Element{
			Tag:                    hdhPrivateTagCipherMap,
			ValueRepresentation:    tag.VRStringList,
			RawValueRepresentation: "LT",
			Value:                  v,
		}
		dataset.Elements = append(dataset.Elements, cipherMap)
	} else if err != nil {
		return fmt.Errorf("finding element by tag: %w", err)
	}
	switch cipherMap.Value.GetValue().(type) {
	case []uint8:
		// Two character codes of the single-byte character sets invoked have
		// special significance in the DICOM Standard. The character SPACE,
		// represented by bit combination 02/00, shall be used for the padding
		// of Data Element Values that are character strings. This is why we
		// use a trim function below.
		// The Graphic Character represented by "\" (BACKSLASH) shall only
		// be used in character strings with Value Representations of UT, ST and LT.
		// Otherwise the character is used as a separator for multi-valued Data Elements.
		// This is why we split.
		splittedVals := bytes.Split(cipherMap.Value.GetValue().([]uint8), []byte("\\"))
		for _, v := range splittedVals {
			existingCiphers = append(existingCiphers, strings.TrimSpace(string(v)))
		}
	case []string:
		existingCiphers = cipherMap.Value.GetValue().([]string)
	}
	tags, err := convertToDicomTags(r.log, r.tags)
	if err != nil {
		return fmt.Errorf("converting tags: %w", err)
	}
	for _, t := range tags {
		elem, err := dataset.FindElementByTag(t)
		if err != nil {
			r.log.Info("cant find tag in file. Ignoring it.", "tag", t)
			continue
		}
		// we only support string types, as we need to replace it with a hash/cipher
		if elem.Value.ValueType() != dicom.Strings {
			return errors.New("unsupported dicom type, can only work on string types")
		}
		clearText := elem.Value.GetValue().([]string)[0]
		if ignoreValue(r.ignoreValues, clearText) {
			continue
		}
		if strings.HasPrefix(clearText, HDHOrgRoot) {
			r.log.Info("tag %s already masked, mask it again", t)
			clearText = cipherFromMap(existingCiphers, clearText)
		}

		var masked string
		switch r.encryptionType {
		case v1alpha1.Mask_aes:
			masked, err = masquage.EncryptAES(clearText, r.key)
			if err != nil {
				return fmt.Errorf("encrypt value with AES: %w", err)
			}
		case v1alpha1.Mask_legacy:
			masked, err = masquage.HMAC(clearText, r.key)
			if err != nil {
				return fmt.Errorf("hashing value with HMAC: %w", err)
			}
			r.ct[r.prefix+":"+masked] = clearText
		}
		// 1. we generate a random numerical only id and we store the ID
		// Note that even MD5 in known for collision vulnerabilities, we use it
		// to generate a pseudo unique id from source content and not for security purposes.
		hash := md5.Sum([]byte(r.prefix + ":" + masked))
		uid := fmt.Sprintf("%s.%d", HDHOrgRoot, binary.BigEndian.Uint32(hash[:]))

		elem.Value, err = dicom.NewValue([]string{uid})
		if err != nil {
			return fmt.Errorf("creating new tag value: %w", err)
		}
		// 2. we add the cipher to the cipher map
		newCiphers = append(newCiphers, uid+"::"+r.prefix+":"+masked)
	}
	// write ciphers map as well as implementor tag
	cipherMap.Value, err = dicom.NewValue(newCiphers)
	if err != nil {
		return fmt.Errorf("creating new tag value: %w", err)
	}
	if err := ensureImplementorTag(&dataset); err != nil {
		return fmt.Errorf("ensuring implementor tag: %w", err)
	}
	cleanupDataset(&dataset)
	if err := dicom.Write(r.dicomWriter,
		dataset,
		dicom.SkipValueTypeVerification(),
		dicom.SkipVRVerification(),
		dicom.DefaultMissingTransferSyntax(),
	); err != nil {
		return fmt.Errorf("serializing dicom file: %w", err)
	}
	r.log.Info("dicom file processed with success")
	return nil
}
