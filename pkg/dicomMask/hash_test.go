package dicomMask

import (
	"embed"
	"io"
	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"reflect"
	"testing"

	hdhlog "rdd-pipeline/pkg/log"
)

//go:embed testdata/*.dcm
var testdata embed.FS

type hashReaderTest struct {
	Reader *HashReader
	Writer *io.PipeWriter
}

func newHashReader(t *testing.T, filename, prefix, key, encryptionType string, tags, ignoreValues []string) hashReaderTest {

	file, err := testdata.Open(filename)
	if err != nil {
		t.Errorf("Couldn't open file for the test: %s", err)
	}

	rOut, wOut := io.Pipe()
	return hashReaderTest{
		Reader: &HashReader{
			in:          file,
			dicomWriter: wOut,
			out:         rOut,

			log:            hdhlog.Discard(),
			key:            key,
			prefix:         prefix,
			tags:           tags,
			ignoreValues:   ignoreValues,
			ct:             masquage.CorrespondenceTable{},
			encryptionType: encryptionType,
		},
		Writer: wOut,
	}
}

func TestHashReader_process(t *testing.T) {
	tests := []struct {
		name string

		filename, prefix, key, encryptionType string
		tags, ignoreValues                    []string

		wantErr bool
		want    string
		wantCt  masquage.CorrespondenceTable
	}{
		{
			name:           "dicom mask Simple with existing pattern (0010,0020) legacy",
			filename:       "testdata/mr_small.dcm",
			prefix:         "FHDH",
			key:            "48656c6c6f",
			tags:           []string{"0010,0020"},
			encryptionType: v1alpha1.Mask_legacy,
			ignoreValues:   []string{},

			wantErr: false,
			want:    "testdata/mr_small_mask_legacy_out.dcm",
			wantCt: masquage.CorrespondenceTable{
				"FHDH:41f67ceffa39628cf2201c9334bef9707e3ee84abd0cc5e937fa2d87bc0c5844": "4MR1",
			},
		},
		{
			name:           "dicom mask Simple with existing pattern (0010,0020) aes",
			filename:       "testdata/mr_small.dcm",
			prefix:         "pdlr",
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",
			tags:           []string{"0010,0020"},
			encryptionType: v1alpha1.Mask_aes,
			ignoreValues:   []string{},

			wantErr: false,
			want:    "testdata/mr_small_mask_aes_out.dcm",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name:           "dicom mask Simple with nothing to mask (unknown) legacy",
			filename:       "testdata/mr_small.dcm",
			prefix:         "FHDH",
			key:            "48656c6c6f",
			tags:           []string{"0099,0020"},
			encryptionType: v1alpha1.Mask_legacy,
			ignoreValues:   []string{},

			wantErr: false,
			want:    "testdata/mr_small_mask_legacy_nothing_to_mask_out.dcm",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name:           "dicom mask Simple with nothing to mask (unknown) aes",
			filename:       "testdata/mr_small.dcm",
			prefix:         "pdlr",
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",
			tags:           []string{"0099,0020"},
			encryptionType: v1alpha1.Mask_aes,
			ignoreValues:   []string{},

			wantErr: false,
			want:    "testdata/mr_small_mask_aes_nothing_to_mask_out.dcm",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name:     "dicom mask ignore values legacy",
			filename: "testdata/mr_small.dcm",
			prefix:   "FHDH",
			key:      "48656c6c6f",
			tags: []string{
				"0010,0020",
				"0002,0016", /* tag containing "CLUNIE1" */
			},
			encryptionType: v1alpha1.Mask_legacy,
			ignoreValues:   []string{"CLUNIE1"},

			wantErr: false,
			want:    "testdata/mr_small_mask_legacy_ignore_values_out.dcm",
			wantCt: masquage.CorrespondenceTable{
				"FHDH:41f67ceffa39628cf2201c9334bef9707e3ee84abd0cc5e937fa2d87bc0c5844": "4MR1",
			},
		},
		{
			name:     "dicom mask ignore values aes",
			filename: "testdata/mr_small.dcm",
			prefix:   "FHDH",
			key:      "635266556A586E5A7234753778214125442A472D4B6150645367566B59703373",
			tags: []string{
				"0010,0020",
				"0002,0016", /* tag containing "CLUNIE1" */
			},
			encryptionType: v1alpha1.Mask_aes,
			ignoreValues:   []string{"CLUNIE1"},

			wantErr: false,
			want:    "testdata/mr_small_mask_aes_ignore_values_out.dcm",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name:           "dicom mask aes after a legacy mask",
			filename:       "testdata/mr_small_mask_legacy_out.dcm",
			prefix:         "FHDH",
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",
			tags:           []string{"0010,0020"},
			encryptionType: v1alpha1.Mask_aes,
			ignoreValues:   []string{},

			wantErr: false,
			want:    "testdata/mr_small_mask_legacy_then_aes_out.dcm",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name:           "dicom mask legacy with implicit transfert syntax",
			filename:       "testdata/implicit.dcm",
			prefix:         "FHDH",
			key:            "48656c6c6f",
			tags:           []string{"0010,0020", "0010,0021"},
			encryptionType: v1alpha1.Mask_legacy,
			ignoreValues:   []string{},

			wantErr: false,
			want:    "testdata/implicit_mask_legacy_out.dcm",
			wantCt: masquage.CorrespondenceTable{
				"FHDH:6b9fec95a222069d19f9e4370da11386c758733e816edde633e031c5cd6889b2": "Jean Dujardin",
				"FHDH:ee78ccead1c49241a14d14393ccd31acf7c8ea8c88de0226f0340f3690f686b5": "Clinique Jean Vilars",
			},
		},
		{
			name:           "dicom mask aes with implicit transfert syntax",
			filename:       "testdata/implicit.dcm",
			prefix:         "pdlr",
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",
			tags:           []string{"0010,0020", "0010,0021"},
			encryptionType: v1alpha1.Mask_aes,
			ignoreValues:   []string{},

			wantErr: false,
			want:    "testdata/implicit_mask_aes_out.dcm",
			wantCt:  masquage.CorrespondenceTable{},
		},
		{
			name:           "dicom mask aes after legacy with implicit transfert syntax",
			filename:       "testdata/implicit_mask_legacy_out.dcm",
			prefix:         "pdlr",
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",
			tags:           []string{"0010,0020", "0010,0021"},
			encryptionType: v1alpha1.Mask_aes,
			ignoreValues:   []string{},

			wantErr: false,
			want:    "testdata/implicit_mask_legacy_then_aes_out.dcm",
			wantCt:  masquage.CorrespondenceTable{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here
			r := newHashReader(t, tt.filename, tt.prefix, tt.key, tt.encryptionType, tt.tags, tt.ignoreValues)

			go func() {
				errP := r.Reader.process()
				if err := r.Writer.CloseWithError(errP); (errP != nil) != tt.wantErr {
					t.Errorf("HashReader.process() error = %v, wantErr %v", err, tt.wantErr)
				}
			}()

			got, err := io.ReadAll(r.Reader)
			if err != nil {
				t.Errorf("Couldn't read the output: %v", err) //nolint
			}

			wantBytes, err := testdata.ReadFile(tt.want)
			if err != nil {
				t.Errorf("Couldn't read the expected result file: %v", err) //nolint
			}

			if !testEq(got, wantBytes) {
				t.Errorf("Dicom output does not match expected file (%s)\n", tt.want)
				// uncomment to debug
				// os.WriteFile("/tmp/dicom_output_test_file", got, fs.ModePerm)
			}

			if !reflect.DeepEqual(r.Reader.ct, tt.wantCt) {
				t.Errorf("CT: process() = \n%s\n -- want CT \n%v\n", r.Reader.ct, tt.wantCt)
			}
		})
	}
}

func testEq(a, b []byte) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func BenchmarkDicomHash(b *testing.B) {

	for i := 0; i < b.N; i++ {
		file, _ := testdata.Open("testdata/mr_small.dcm")
		rOut, wOut := io.Pipe()
		r := &HashReader{
			in:          file,
			dicomWriter: wOut,
			out:         rOut,

			log:            hdhlog.Discard(),
			key:            "1eab92ba5318ec8c3daecce0e4637ec83864b79f0025c1fe38340ef1c596e5f5",
			prefix:         "myprefix",
			tags:           []string{"0010,0020"},
			ignoreValues:   []string{},
			ct:             masquage.CorrespondenceTable{},
			encryptionType: v1alpha1.Mask_aes,
		}
		go io.Copy(io.Discard, r)
		err := r.process()
		if err != nil {
			b.Error(err)
		}
	}
}
