package dicomMask

import (
	"bytes"
	"crypto/md5"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strings"

	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/suyashkumar/dicom"
)

var _ io.Reader = (*PairingReader)(nil)

type PairingReader struct {
	in          io.Reader
	dicomWriter io.Writer // the dicom lib writes here
	out         io.Reader // the caller reads here

	tags           []string
	ignoreValues   []string
	currentline    int
	ct             masquage.CorrespondenceTable
	log            hdhlog.Logger
	encryptionType string
	key            string
}

func NewPairingReader(log hdhlog.Logger, in io.Reader, tags []string, ignoreValues []string, ct masquage.CorrespondenceTable, encryptionType, key string) io.Reader {
	dicomReader := in
	pipeR, pipeW := io.Pipe()

	log = log.With("tags", tags)
	log = log.With("encryptionType", encryptionType)
	log = log.With("ignoreValues", ignoreValues)

	pairingReader := &PairingReader{
		in:             dicomReader,
		dicomWriter:    pipeW,
		out:            pipeR,
		tags:           tags,
		ignoreValues:   ignoreValues,
		currentline:    0,
		ct:             ct,
		log:            log,
		encryptionType: encryptionType,
		key:            key,
	}

	// Put data into pipe waiting for read
	go (func() {
		err := pairingReader.process()
		if err := pipeW.CloseWithError(err); err != nil {
			log.Error(err, "error closing")
		}
		if err != nil {
			log.Error(err, "error hashing")
		}
	})()
	return pairingReader
}

func (r *PairingReader) Read(p []byte) (int, error) {
	return r.out.Read(p)
}

func (r *PairingReader) process() error {

	r.log.Info("Start processing file pairing")

	dataset, err := dicom.ParseUntilEOF(r.in, nil,
		dicom.AllowMismatchPixelDataLength())
	if err != nil {
		return fmt.Errorf("parsing dicom: %w", err)
	}

	hdhImplementorExists, err := implementorTagExists(dataset)
	if err != nil {
		return fmt.Errorf("checking if implementor tag exists: %w", err)
	}
	// if hdh implementor tag exists, fetch newCiphers from the cipher map
	newCiphers := []string{}
	existingCiphers := []string{}
	if hdhImplementorExists {
		cipherMap, err := dataset.FindElementByTag(hdhPrivateTagCipherMap)
		if err != nil {
			return fmt.Errorf("finding element hdhPrivateTagCipherMap: %w", err)
		}
		switch cipherMap.Value.GetValue().(type) {
		case []uint8:

			// Two character codes of the single-byte character sets invoked have
			// special significance in the DICOM Standard. The character SPACE,
			// represented by bit combination 02/00, shall be used for the padding
			// of Data Element Values that are character strings. This is why we
			// use a trim function below.
			// The Graphic Character represented by "\" (BACKSLASH) shall only
			// be used in character strings with Value Representations of UT, ST and LT.
			// Otherwise the character is used as a separator for multi-valued Data Elements.
			// This is why we split.

			splittedVals := bytes.Split(cipherMap.Value.GetValue().([]uint8), []byte("\\"))
			for _, v := range splittedVals {
				existingCiphers = append(existingCiphers, strings.TrimSpace(string(v)))
			}
		case []string:
			existingCiphers = cipherMap.Value.GetValue().([]string)
		}
	}

	tags, err := convertToDicomTags(r.log, r.tags)
	if err != nil {
		return fmt.Errorf("converting tags: %w", err)
	}

	for _, t := range tags {
		elem, err := dataset.FindElementByTag(t)
		if err == dicom.ErrorElementNotFound {
			r.log.Info("cant find tag in file. Ignoring it.", t)
			continue
		}
		if elem.Value.ValueType() != dicom.Strings {
			return errors.New("unsupported dicom type, can only work on string types")
		}
		value := elem.Value.GetValue().([]string)[0]
		if ignoreValue(r.ignoreValues, value) {
			continue
		}

		cipher := value                           // legacy way of masking
		if strings.HasPrefix(value, HDHOrgRoot) { // new way of masking
			cipher = cipherFromMap(existingCiphers, value)
			if cipher == "" {
				return fmt.Errorf("cipher %s not found in cipher map", value)
			}
		}

		var clearText string
		switch r.encryptionType {
		case v1alpha1.Mask_aes:
			cipherNoPrefix := strings.TrimSpace(cipher[strings.Index(cipher, ":")+1:])
			clearText, err = masquage.DecryptAES(cipherNoPrefix, r.key)
			if err != nil {
				return fmt.Errorf("decrypt value with AES: %w", err)
			}
		case v1alpha1.Mask_legacy:
			var ok bool
			clearText, ok = r.ct[cipher]
			if !ok {
				return fmt.Errorf("CT key %s could not be found", cipher)
			}
		}
		if guessIsHDHMask(clearText) {

			// if the decrypted is still a hdh, we save the value in the map
			// and put the uid prefix in the tag value
			hash := md5.Sum([]byte(clearText))
			uid := fmt.Sprintf("%s.%d", HDHOrgRoot, binary.BigEndian.Uint32(hash[:]))

			newCiphers = append(newCiphers, uid+"::"+clearText)
			elem.Value, err = dicom.NewValue([]string{uid})
			if err != nil {
				return fmt.Errorf("creating new tag value from hdh cipher: %w", err)
			}
		} else {
			elem.Value, err = dicom.NewValue([]string{clearText})
			if err != nil {
				return fmt.Errorf("creating new tag value: %w", err)
			}
		}
	}
	// write new ciphers map
	if hdhImplementorExists {
		cipherMap, err := dataset.FindElementByTag(hdhPrivateTagCipherMap)
		if err != nil {
			return fmt.Errorf("finding element hdhPrivateTagCipherMap: %w", err)
		}
		cipherMap.Value, err = dicom.NewValue(newCiphers)
		if err != nil {
			return fmt.Errorf("creating new tag value: %w", err)
		}
	}
	cleanupDataset(&dataset)
	if err := dicom.Write(r.dicomWriter,
		dataset,
		dicom.SkipValueTypeVerification(),
		dicom.SkipVRVerification(),
		dicom.DefaultMissingTransferSyntax(),
	); err != nil {
		return fmt.Errorf("serializing dicom file: %w", err)
	}
	return nil
}

var HDHMaskre = regexp.MustCompile(`[A-Za-z0-9]{3,5}:[0-9a-fA-F]+`)

// this functions helps detecting whether or not a decrypted mask
// is still a HDH cipher. This can happen when we mask twice.
// In this case, we keep data in our custom tag.
// Otherwise we want the uncrypted to be saved as is in the origin tag.
// This was the legacy behavior and we need to support old data.
func guessIsHDHMask(text string) bool {
	return HDHMaskre.MatchString(text)
}
