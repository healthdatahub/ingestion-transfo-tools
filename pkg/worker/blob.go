package worker

import (
	"bufio"
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"os/exec"
	"rdd-pipeline/pkg/crypt/hdhcrypt"
	"rdd-pipeline/pkg/keyvault"
	hdhlog "rdd-pipeline/pkg/log"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blockblob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/lease"
	"golang.org/x/sync/errgroup"
)

var (
	ErrNoVersion    = errors.New("no package version found")
	ErrNoLeaseID    = errors.New("no leaseID")
	ErrBlobNotFound = errors.New("blob not found")
)

const (
	// upload blob size, the SDK keeps 100mb in memory before flushing to Azure and
	// create a block. As the max blob number is 100 000, we can reach blob sizes
	// up to 4.75TiB.
	// For more details
	// https://learn.microsoft.com/en-us/rest/api/storageservices/put-block?tabs=microsoft-entra-id#remarks
	uploadBlobSize = 100 * 1024 * 1024 // 100mb

	leasePeriod = 60 // in seconds
	renewDelay  = 30 * time.Second
)

// Blob is the central structure of this project
type Blob struct {
	name          string
	metadata      map[string]string
	tags          map[string]string // this
	contentLength int64             // equals to zero for a new blob

	blobClient      *blockblob.Client
	containerClient *container.Client

	leaseBlobClient *lease.BlobClient

	stopRenew  chan struct{}
	renewMutex sync.Mutex
}

// NewBlob creates a new blob object whether or not the blob already
// exists in azure. Generally called when needed to create a blob from
// a local file, or any type of reader.
func NewBlob(containerClient *container.Client, blobName string, metadata map[string]string,
	tags map[string]string) *Blob {
	b := &Blob{
		name:     blobName,
		metadata: metadata,
		tags:     tags,
	}
	b.containerClient = containerClient
	b.blobClient = containerClient.NewBlockBlobClient(blobName)
	return b
}

// ExistingBlob builds a blob object from an existing azure blob
// If it does not exist, return error of type BlobNotFound.
// Tags are not fetched here, as the permission might not be granted to execution runtime.
func ExistingBlob(ctx context.Context, containerClient *container.Client, blobName string, cpk *blob.CPKInfo) (*Blob, error) {
	b := &Blob{
		name: blobName,
	}
	b.containerClient = containerClient
	b.blobClient = containerClient.NewBlockBlobClient(blobName)

	props, err := b.blobClient.GetProperties(ctx, &blob.GetPropertiesOptions{
		CPKInfo: cpk,
	})
	if err != nil {
		return nil, err
	}
	metadata := make(map[string]string)
	for k, v := range props.Metadata {
		if v != nil {
			metadata[strings.ToLower(k)] = *v
		}
	}
	b.metadata = metadata
	b.contentLength = *props.ContentLength
	return b, nil
}

// NewBlobFromItem builds a blob object from an container item.
// A container item generally comes from a pager operation (listing blobs).
func NewBlobFromItem(containerClient *container.Client, item *container.BlobItem) *Blob {
	tagMap := make(map[string]string)

	if item.BlobTags != nil {
		for _, tag := range item.BlobTags.BlobTagSet {
			tagMap[*tag.Key] = *tag.Value
		}
	}
	metadata := make(map[string]string)
	for k, v := range item.Metadata {
		if v != nil {
			metadata[k] = *v
		}
	}
	b := &Blob{
		name:          *item.Name,
		metadata:      metadata,
		tags:          tagMap,
		contentLength: *item.Properties.ContentLength,
	}

	b.containerClient = containerClient
	b.blobClient = containerClient.NewBlockBlobClient(*item.Name)

	return b
}

// Name returns the blob name
func (b *Blob) Name() string {
	return b.name
}

// URL returns the blob URL in HTTP format like
// https://pplro175mask.blob.core.windows.net/pdben1/mr_small.dcm
func (b *Blob) URL() string {
	return b.blobClient.URL()
}

// Clone creates a copy of the blob object keeping metadatas and converting
// tags into metadata.
// The blob name and the destination container can be modified.
// This operation does not do any interactions with azure apis.
func (b *Blob) Clone(ctx context.Context, destContainer *container.Client, destBlobName string) *Blob {

	// if the name is empty, we override the file itself
	if destBlobName == "" {
		destBlobName = b.name
	}

	// copy metadata
	// ensure we don't keep the original path since it could eventually contain some data
	// and merge the tags into the metadatas, prefixing it with "tag_<storage_account_suffix>_"
	metadata := make(map[string]string)
	for k, v := range b.metadata {
		metadata[k] = v
	}
	delete(metadata, v1alpha1.MetadataOriginalPath)

	// with this cumbersome code (legacy) we convert url like
	// https://pdben1maskhdh.blob.core.windows.net/
	// to
	// tag_maskhdh_
	parts := strings.Split(b.containerClient.URL(), "https://")
	subparts := strings.Split(parts[1], ".")
	re := regexp.MustCompile("[0-9]+")
	loc := re.FindStringIndex(subparts[0])
	saPrefix := "tag_" + subparts[0][loc[1]:] + "_"

	for k, v := range b.tags {
		metadata[saPrefix+k] = v
	}

	return &Blob{
		name:            destBlobName,
		contentLength:   b.contentLength,
		metadata:        metadata,
		blobClient:      destContainer.NewBlockBlobClient(destBlobName),
		containerClient: destContainer,
	}
}

// RemoteCopy copies current azure blob into a new destination without
// loading any data in memory. It uses azcopy binary and requires some
// environnement configuration like
// AZCOPY_AUTO_LOGIN_TYPE="MSI"
// AZCOPY_MSI_CLIENT_ID=$CLIENT_ID
// It does not support customer provided key (CPK).
func (b *Blob) RemoteCopy(ctx context.Context, dest *Blob) error {
	log := hdhlog.FromContext(ctx)
	cmd := exec.Command(
		"azcopy", "copy", b.blobClient.URL(), dest.blobClient.URL(),
	)
	log.Info(fmt.Sprintf("azcopy command: %s", cmd.String()))
	if err := logStdOutputs(cmd, "azcopy", log); err != nil {
		return err
	}
	start := time.Now()
	if err := cmd.Run(); err != nil {
		if errExit, ok := err.(*exec.ExitError); ok {
			return fmt.Errorf("azcopy execution failed (exit code %d): %w", errExit.ExitCode(), err)
		}
		return fmt.Errorf("azcopy execution failed: %w", err)
	}
	log.Info(fmt.Sprintf("azcopy terminated in %s", time.Since(start)))
	return nil
}

// AcquireLease takes the lease on the blob with no time limit
func (b *Blob) AcquireLease(ctx context.Context) error {
	var err error
	if b.leaseBlobClient == nil {
		b.leaseBlobClient, err = lease.NewBlobClient(b.blobClient, &lease.BlobClientOptions{})
		if err != nil {
			return err
		}
	}
	_, err = b.leaseBlobClient.AcquireLease(ctx, int32(leasePeriod), &lease.BlobAcquireOptions{})
	if err != nil {
		return fmt.Errorf("acquire lease error: %w", err)
	}
	b.enableRenewAutomation(ctx)
	return nil
}

// AcquireLease takes the lease on the blob with no time limit
func (b *Blob) ReleaseLease(ctx context.Context) error {
	defer b.disableRenewAutomation()
	var err error
	if b.leaseBlobClient == nil || b.leaseBlobClient.LeaseID() == nil {
		return ErrNoLeaseID
	}
	_, err = b.leaseBlobClient.ReleaseLease(ctx, nil)
	if bloberror.HasCode(err, bloberror.BlobNotFound) {
		return ErrBlobNotFound
	} else if err != nil {
		return fmt.Errorf("release lease error: %w", err)
	}
	return nil
}

func (b *Blob) leaseID() *string {
	if b.leaseBlobClient == nil {
		return nil
	}
	return b.leaseBlobClient.LeaseID()
}

// PushMetadata updates blob metadata
func (b *Blob) PushMetadata(ctx context.Context, cpk *blob.CPKInfo) error {
	metadata := make(map[string]*string)
	for k, v := range b.metadata {
		metadata[k] = to.Ptr(v)
	}
	_, err := b.blobClient.SetMetadata(ctx,
		metadata,
		&blob.SetMetadataOptions{
			CPKInfo: cpk,
			AccessConditions: &blob.AccessConditions{
				LeaseAccessConditions: &blob.LeaseAccessConditions{
					LeaseID: b.leaseID(),
				},
			},
		})
	return err
}

// PushTags updates blob tags
func (b *Blob) PushTags(ctx context.Context) error {
	_, err := b.blobClient.SetTags(ctx,
		b.tags,
		&blob.SetTagsOptions{
			AccessConditions: &blob.AccessConditions{
				LeaseAccessConditions: &blob.LeaseAccessConditions{
					LeaseID: b.leaseID(),
				},
			},
		})
	return err
}

// Metadata returns blob metadata map
func (b *Blob) Metadata() map[string]string {
	if b.metadata == nil {
		// azure sdk returns a nil map if no elements exist
		// which is not convenient for callers
		b.metadata = make(map[string]string)
	}
	return b.metadata
}

// Tags returns blob tags map
// ⚠️ This list might be empty as the tags are filled only for ACI
// with "readtags" permissions and "--filter-with-tags" flag
// Principle of least privilege.
func (b *Blob) Tags() map[string]string {
	if b.tags == nil {
		// azure sdk returns a nil map if no elements exist
		// which is not convenient for callers
		b.tags = make(map[string]string)
	}
	return b.tags
}

// Size returns blob size in bytes
func (b *Blob) Size() int64 {
	return b.contentLength
}

// Download returns a reader to the blob
// Due to unexpected TCP disconnections from the blob source, we have to handle
// a bit of logic. If a connection fails while reading, we make additional requests
// to reestablish a connection and continue reading.
func (b *Blob) Download(ctx context.Context, cpk *blob.CPKInfo) (io.ReadCloser, int64, error) {
	log := hdhlog.FromContext(ctx)
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		var totalRead int64
		retryDelay := 1 * time.Second
		const maxRetryDelay = 32 * time.Second
		retries := 0

		resp, err := b.blobClient.DownloadStream(ctx, &blob.DownloadStreamOptions{
			CPKInfo: cpk,
			Range: blob.HTTPRange{
				Offset: totalRead,
			},
		})
		if err != nil {
			pw.CloseWithError(fmt.Errorf("fail to establish connection: %w", err))
			return
		}
		for {
			select {
			case <-ctx.Done():
				pw.CloseWithError(ctx.Err())
				return
			default:
				n, err := io.Copy(pw, resp.Body)
				totalRead += n
				if err != nil && err != io.EOF {
					log.Info("the connection was lost, reopen a connection and continue streaming")
					time.Sleep(retryDelay)

					resp, err = b.blobClient.DownloadStream(ctx, &blob.DownloadStreamOptions{
						CPKInfo: cpk,
						Range: blob.HTTPRange{
							Offset: totalRead,
						},
					})
					if err == nil {
						retryDelay = 1 * time.Second
						retries = 0
						continue
					}
					retryDelay *= 2
					if retryDelay > maxRetryDelay {
						retryDelay = maxRetryDelay
					}
					retries++
					if retries > 5 {
						pw.CloseWithError(fmt.Errorf("download failed after 5 retries: %w", err))
						return
					}
				}
				if err == io.EOF || totalRead >= *resp.ContentLength {
					log.Info("reached EOF: closing pipewriter")
					pw.Close()
					return
				}
			}
		}
	}()
	return pr, -1, nil
}

// Delete marks the specified blob or snapshot for deletion.
// The blob is later deleted during garbage collection.
// Deleting a blob also deletes all its snapshots.
func (b *Blob) Delete(ctx context.Context) error {
	_, err := b.blobClient.Delete(ctx, &blob.DeleteOptions{
		AccessConditions: &blob.AccessConditions{
			LeaseAccessConditions: &blob.LeaseAccessConditions{
				LeaseID: b.leaseID(),
			},
		},
	})
	return err
}

// Upload pushes a reader content to its blob destination
func (b *Blob) Upload(ctx context.Context, r io.Reader, cpk *blob.CPKInfo) error {
	metadata := make(map[string]*string)
	for k, v := range b.metadata {
		metadata[k] = to.Ptr(v)
	}

	uploadOpts := blockblob.UploadStreamOptions{
		BlockSize: uploadBlobSize,
		Metadata:  metadata,
		Tags:      b.tags,
		CPKInfo:   cpk,
	}

	// Check if a lease ID is present and set the AccessConditions accordingly
	if leaseID := b.leaseID(); leaseID != nil {
		uploadOpts.AccessConditions = &blob.AccessConditions{
			LeaseAccessConditions: &blob.LeaseAccessConditions{
				LeaseID: leaseID,
			},
		}
	}
	_, err := b.blobClient.UploadStream(ctx, r, &uploadOpts)
	return err
}

// EncryptAndUpload generates a symetric key and encrypt an input blob
// using a new generated key. The key is then wrapped (encrypted) with another
// key defined in the vaultURL/keyVaultKey parameters and attached as
// a metadata of the output blob.
func (b *Blob) EncryptAndUpload(ctx context.Context, reader io.Reader, vaultURL, keyVaultKey, metadataKVKey, metadataWrappedKey string) error {
	var err error

	log := hdhlog.FromContext(ctx)

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(vaultURL)
	if err != nil {
		log.Error(err, "get Key Vault client", "vaultURL", vaultURL)
		return err
	}

	// we generate a symmetrical key that we encrypt with the keyvault key
	// this encrypted key will be copied in the metadata of the output blob.
	key, err := hdhcrypt.GenerateKey()
	if err != nil {
		log.Error(err, "generating key")
		return err
	}

	wrappedKey, err := kvClient.Wrap(ctx, keyVaultKey, key)
	if err != nil {
		log.Error(err, "wrapping key")
		return err
	}

	encryptionKeyVersion, err := kvClient.GetKeyLastVersionID(ctx, keyVaultKey)
	if err != nil {
		log.Error(err, "error while getting the encryption key version")
		return err
	}

	// we alter the metadata of the original blob so that they are copied
	// then in the output blob and we drop the original path because we don't care.
	b.Metadata()[metadataWrappedKey] = base64.StdEncoding.EncodeToString(wrappedKey)
	b.Metadata()[metadataKVKey] = keyVaultKey
	b.Metadata()[v1alpha1.MetadataEncryptionKeyvaultKeyVersion] = encryptionKeyVersion

	pr, pw := io.Pipe()
	defer pr.Close()
	stream, err := hdhcrypt.GetStreamWriter(key, pw)
	if err != nil {
		log.Error(err, "getStreamWriter")
		return err
	}

	// we open a reader and send it to the hdhcrypt encryptor.
	g, gctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		defer stream.Close()
		if _, err := io.Copy(stream, reader); err != nil {
			log.Error(err, "can't copy data")
			return err
		}
		return nil
	})

	// we copy the output of the hdhcrypt into the new blob.
	g.Go(func() error {
		defer pr.Close()
		if err := b.Upload(gctx, pr, &blob.CPKInfo{}); err != nil {
			log.Error(err, "upload blob as stream")
			return fmt.Errorf("upload blob '%s' as stream: %w", b.Name(), err)
		}
		return nil
	})
	return g.Wait()
}

// CopyLeaseBlobClient Copies the leaseClient from the blob passed as parameter to the blob calling it
func (b *Blob) CopyLeaseBlobClient(source *Blob) {
	if source.leaseBlobClient != nil && source.leaseBlobClient.LeaseID() != nil {
		b.leaseBlobClient = source.leaseBlobClient
	}
}

// BuildReaderAt returns a struct implementing ReaderAt interface
// This specificity is required to read zip archive (legacy format)
// using guilhem library (one of our old deps)
// not pretty, we know.
func (b *Blob) BuildReaderAt(cpk *blob.CPKInfo) *blobReaderAt {
	return &blobReaderAt{
		blob: b,
		cpk:  cpk,
	}
}

type blobReaderAt struct {
	blob *Blob
	cpk  *blob.CPKInfo
}

func (b blobReaderAt) ReadAt(p []byte, offset int64) (int, error) {
	resp, err := b.blob.blobClient.DownloadStream(context.Background(), &blob.DownloadStreamOptions{
		Range: blob.HTTPRange{
			Offset: offset,
			Count:  int64(len(p)),
		},
		CPKInfo: b.cpk,
	})
	if err != nil {
		return 0, fmt.Errorf("download failed with: %w", err)
	}
	r := resp.Body
	defer r.Close()
	n, err := io.ReadFull(r, p)
	if err != nil {
		return n, fmt.Errorf("readFull failed: %w", err)
	}
	return n, nil
}

func (b blobReaderAt) Size() int64 {
	return b.blob.Size()
}

// private utils

func logStdOutputs(cmd *exec.Cmd, processName string, log hdhlog.Logger) error {
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return err
	}
	go func() {
		s := bufio.NewScanner(stdout)
		for s.Scan() {
			log.Info(fmt.Sprintf("%s stdout :%s", processName, s.Text()))
		}
		if err := s.Err(); err != nil {
			log.Error(err, "scanner returned an error")
		}
	}()
	stderr, err := cmd.StderrPipe()
	if err != nil {
		return err
	}
	go func() {
		s := bufio.NewScanner(stderr)
		for s.Scan() {
			log.Error(fmt.Errorf("%s stderr :%s", processName, s.Text()), "stderr")
		}
		if err := s.Err(); err != nil {
			log.Error(err, "scanner returned an error")
		}
	}()
	return nil
}

func (b *Blob) enableRenewAutomation(ctx context.Context) {
	log := hdhlog.FromContext(ctx).With("blobName", b.Name())
	b.renewMutex.Lock()
	defer b.renewMutex.Unlock()
	if b.stopRenew != nil {
		return
	}
	go func() {
		for {
			select {
			case <-b.stopRenew:
				b.stopRenew <- struct{}{}
				return
			case <-time.After(renewDelay):
				if _, err := b.leaseBlobClient.RenewLease(ctx, nil); err != nil {
					log.Error(err, "failed to renew lease automatically")
					continue
				}
				log.Info("lease renewed")
			}
		}
	}()
	b.stopRenew = make(chan struct{})
}

func (b *Blob) disableRenewAutomation() {
	b.renewMutex.Lock()
	defer b.renewMutex.Unlock()
	if b.stopRenew != nil {
		b.stopRenew <- struct{}{}
		<-b.stopRenew
		b.stopRenew = nil
	}
}
