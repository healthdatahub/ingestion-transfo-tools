package worker

import (
	"context"
	"fmt"

	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/schema/v1alpha1"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"gopkg.in/yaml.v2"
)

type ConfigContainer struct {
	StorageAccountURL string
	ContainerName     string
	BlobName          string
}

type BlobConfig interface {
	Match(string) bool
}

func (c *ConfigContainer) Download(ctx context.Context) (*v1alpha1.PipelineConfig, error) {
	configContainer, err := azure.NewContainerClient(c.StorageAccountURL, c.ContainerName)
	if err != nil {
		return nil, fmt.Errorf("create Config container client: %w", err)
	}
	client := configContainer.NewBlockBlobClient(c.BlobName)
	resp, err := client.DownloadStream(ctx, &blob.DownloadStreamOptions{})
	if err != nil {
		return nil, fmt.Errorf("download config blob '%s': %w", c.BlobName, err)
	}
	pipelineConfig := new(v1alpha1.PipelineConfig)
	if err := yaml.NewDecoder(resp.Body).Decode(pipelineConfig); err != nil {
		return nil, fmt.Errorf("unmarshall config from blob '%s': %w", c.BlobName, err)
	}
	return pipelineConfig, nil
}
