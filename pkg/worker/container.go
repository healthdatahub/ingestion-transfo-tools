package worker

import (
	"context"
	"fmt"
	"os"
	"runtime"
	"sync/atomic"
	"time"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/lease"

	"github.com/guilhem/gorkers"
)

// Ignored the blob file generate by blobfuse.
// Check the metadata with key => `hdi_isfolder` and `is_symlink`
// show ticket: https://dev.azure.com/health-data-hub/health-data-hub-platform/_boards/board/t/Dev%20Data%20Gouvernance/Stories/?workitem=5717
const (
	folderKeyShouldBeIgnored  = "hdi_isfolder"
	symlinkKeyShouldBeIgnored = "is_symlink"
)

var gMaxWorkers = int64(runtime.NumCPU())

// time interval between two blob checks in seconds
var timeTicker = 30

// autoShutdownDelay is in seconds, 30 min by default if the flag is not set
var autoShutdownDelay = int64(1800)

func SetMaxWorkers(n int) {
	atomic.StoreInt64(&gMaxWorkers, int64(n))
}

func SetAutoShutdownDelay(n int) {
	atomic.StoreInt64(&autoShutdownDelay, int64(n))
}

type Job struct {
	Blob *Blob
}

type ProcessContainer struct {
	storageAccountURL string
	containerName     string
	blobConfig        BlobConfig
	requiredMetadata  map[string]string
	metadataToSkip    map[string]string
	requiredTags      map[string]string
	currentTasks      int
	idleTime          int
}

func NewProcessContainer(storageAccountURL, containerName string, requiredMetadata, metadataToSkip, requiredTags map[string]string) *ProcessContainer {
	if metadataToSkip == nil {
		metadataToSkip = make(map[string]string)
	}

	// Ignored the blob file generate by blobfuse.
	// Check the metadata with `hdi_isfolder` or `is_symlink` keys and "true" value
	// https://github.com/Azure/azure-storage-fuse/blob/main/component/azstorage/utils.go#L291
	metadataToSkip[folderKeyShouldBeIgnored] = "true"
	metadataToSkip[symlinkKeyShouldBeIgnored] = "true"

	return NewProcessContainerWithConfig(storageAccountURL, containerName, nil, requiredMetadata, metadataToSkip, requiredTags, 0, 0)
}

// NewProcessContainerWithConfig returns a processContainer that owns details
// about how to find incoming data
func NewProcessContainerWithConfig(
	storageAccountURL,
	containerName string,
	blobConfig BlobConfig,
	requiredMetadata,
	metadataToSkip,
	requiredTags map[string]string,
	currentTasks int,
	idleTime int,
) *ProcessContainer {
	return &ProcessContainer{
		storageAccountURL: storageAccountURL,
		containerName:     containerName,
		blobConfig:        blobConfig,
		requiredMetadata:  requiredMetadata,
		metadataToSkip:    metadataToSkip,
		requiredTags:      requiredTags,
		currentTasks:      0,
		idleTime:          0,
	}
}

// Run starts a pool of goroutines that
// - wait for inputs by calling the Work
// - apply a task passed in parameter to each incoming item
func (pc *ProcessContainer) Run(ctx context.Context, task gorkers.WorkFunc, runOnce bool) error {
	pc.idleTime = 0
	log := hdhlog.FromContext(ctx).
		With("maxWorkers", gMaxWorkers).
		With("autoShutdown (secs)", autoShutdownDelay)

	ctx, cancel := context.WithCancel(ctx)

	// start watching in container for blob
	watchGroup := gorkers.NewRunner(ctx, pc.Work, 1, 1)
	watchGroup.AfterFunc(func(_ context.Context, _ interface{}, err error) error {
		log.Debug("watch finished")
		if err != nil {
			log.Error(err, "watch worker failed")
			cancel()
		}
		return err
	})

	// start executing on blob sent from watcher
	taskGroup := gorkers.NewRunner(ctx, task, gMaxWorkers, 1).InFrom(watchGroup)
	taskGroup.AfterFunc(func(_ context.Context, _ interface{}, err error) error {
		log.Info("task finished")
		pc.currentTasks--
		if err != nil {
			log.Error(err, "task worker failed")
		} else {
			pc.idleTime = 0
		}
		return nil
	})

	if err := watchGroup.Start(); err != nil {
		return fmt.Errorf("starting watch worker: %w", err)
	}
	if err := taskGroup.Start(); err != nil {
		return fmt.Errorf("starting task worker: %w", err)
	}

	defer func() {
		watchGroup.Wait().Stop()
		taskGroup.Wait().Stop()
		log.Info("defer finished")
	}()

	if err := watchGroup.Send(nil); err != nil {
		return fmt.Errorf("sending first tick to watcher: %w", err)
	}

	if !runOnce {
		ticker := time.NewTicker(time.Duration(timeTicker) * time.Second)
		defer ticker.Stop()
		for {
			if pc.currentTasks != 0 {
				log.Info("A file is still being processed")
			}
			// Autoshutdown check
			if pc.currentTasks == 0 && autoShutdownDelay != 0 {
				pc.idleTime += timeTicker
				if int64(pc.idleTime) == int64(autoShutdownDelay)+int64(timeTicker) {
					log.Info("auto shutdown timer reached, shutting down the ACI")
					os.Exit(0)
				}
			}
			select {
			case <-ctx.Done():
				return nil
			case <-ticker.C:
				if err := watchGroup.Send(nil); err != nil {
					return fmt.Errorf("sending tick to watcher : %w", err)
				}
			}
		}
	}

	return nil
}

func (pc *ProcessContainer) Work(ctx context.Context, _ interface{}, out chan<- interface{}) error {
	log, ctx := hdhlog.FromContext(ctx).
		With(
			"storageAccount", pc.storageAccountURL,
			"containerName", pc.containerName).
		AttachToContext(ctx)
	creds, err := azidentity.NewDefaultAzureCredential(nil)
	if err != nil {
		return err
	}
	containerClient, err := container.NewClient(pc.storageAccountURL+pc.containerName, creds, nil)
	if err != nil {
		return err
	}

	pager := containerClient.NewListBlobsFlatPager(&container.ListBlobsFlatOptions{
		Include: container.ListBlobsInclude{
			Metadata: true,
		},
	})
	for pager.More() {

		var resp azblob.ListBlobsFlatResponse
		var err error
		resp, err = pager.NextPage(ctx)
		if err != nil {
			return fmt.Errorf("loading next page: %w", err)
		}
		if len(resp.Segment.BlobItems) > 0 {
			log.Debug(fmt.Sprintf("found %d blob(s) in container %s", len(resp.Segment.BlobItems), pc.storageAccountURL+pc.containerName))
		}
		for _, blobItem := range resp.Segment.BlobItems {
			if *blobItem.Properties.LeaseStatus == lease.StatusTypeLocked {
				log.Info("blob is locked and will be ignored", "blobName", blobItem.Name)
				continue
			}
			// Skip blob name not matching patterns
			if pc.blobConfig != nil && !pc.blobConfig.Match(*blobItem.Name) {
				log.Info("blob does not match config file pattern and will be ignored", "blobName", blobItem.Name)
				continue
			}
			skipBlob := false

			// Skip blobs not matching tags
			if os.Getenv("USE_TAGS_FILTERS") == "true" {
				log.Info("tags filter feature enabled, fetching the tags")

				client := containerClient.NewBlockBlobClient(*blobItem.Name)

				// requires "ACI Tags reader" custom role
				tagsResp, err := client.GetTags(ctx, &blob.GetTagsOptions{})
				if err != nil {
					return fmt.Errorf("loading blob tags: %w", err)
				}
				blobItem.BlobTags = &tagsResp.BlobTags // enrich the object

				tags := make(map[string]*string)
				for _, v := range tagsResp.BlobTagSet {
					key := ""
					if v.Key != nil {
						key = *v.Key
					}
					tags[key] = v.Value
				}

				log.Info("Required tags list", "tags", pc.requiredTags)

				for key := range pc.requiredTags {

					tagValue, ok := tags[key]

					// Ensure the tag is present
					if !ok {
						log.Info("blob does not have the required tags and will be ignored", "blobName", blobItem.Name, "missing tag key", key, "missing tag value", pc.requiredTags[key])
						skipBlob = true
						break
					}

					// Ensure that the tag value is correct with the expected one
					// "no_value" means that we only care that the tag is present
					if (*tagValue != pc.requiredTags[key]) && (pc.requiredTags[key] != "no_value") {
						log.Info("Blob tag has a value which defers from the expected one", "tag", key, "expected value", pc.requiredTags[key])
						skipBlob = true
						break
					}
				}
				if skipBlob {
					continue
				}
			} else {
				log.Info("tags filter feature not enabled, ignoring existing tags")
			}
			// Skip blobs matching metadata
			for key, element := range pc.metadataToSkip {
				value, ok := blobItem.Metadata[key]

				if ok && *value == element {
					log.Info("blob has matching metadata and will be ignored", "blobName", blobItem.Name, "metadata_key", key)
					skipBlob = true
					break
				}
			}

			if skipBlob {
				continue
			}

			// Skip blobs NOT matching metadata
			for key, element := range pc.requiredMetadata {
				value, ok := blobItem.Metadata[key]

				if !ok || *value != element {
					log.Info("blob does not have the required metadata and will be ignored", "blobName", blobItem.Name, "metadata_key", key)
					skipBlob = true
					break
				}
			}

			if skipBlob {
				continue
			}
			pc.currentTasks++

			out <- Job{Blob: NewBlobFromItem(containerClient, blobItem)}
		}
	}

	return nil
}
