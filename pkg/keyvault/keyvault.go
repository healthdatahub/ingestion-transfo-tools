package keyvault

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"sort"
	"strings"

	kvauth "github.com/Azure/azure-sdk-for-go/services/keyvault/auth"
	azkeyvault "github.com/Azure/azure-sdk-for-go/services/keyvault/v7.1/keyvault"
)

type KeyVaultClient struct {
	Client *azkeyvault.BaseClient
	KvURL  string
}

func NewKeyVaultClientFromEnvironment(url string) (*KeyVaultClient, error) {
	authorizer, err := kvauth.NewAuthorizerFromEnvironment()
	if err != nil {
		return nil, fmt.Errorf("unable to create vault authorizer: %w", err)
	}

	basicClient := azkeyvault.New()
	basicClient.Authorizer = authorizer

	c := &KeyVaultClient{
		Client: &basicClient,
		KvURL:  url,
	}
	return c, nil
}

func NewKeyVaultClientFromCLI(url string) (*KeyVaultClient, error) {
	authorizer, err := kvauth.NewAuthorizerFromCLI()
	if err != nil {
		return nil, fmt.Errorf("unable to create vault authorizer: %w", err)
	}
	basicClient := azkeyvault.New()
	basicClient.Authorizer = authorizer

	c := &KeyVaultClient{
		Client: &basicClient,
		KvURL:  url,
	}
	return c, nil
}

// GetKey fetches an existing key from a keyvault.
// If it does not exists, an error of type "" is returned.
func (c *KeyVaultClient) GetKey(ctx context.Context, keyName string) (azkeyvault.KeyBundle, error) {
	vaultKey, err := c.Client.GetKey(ctx, c.KvURL, keyName, "")
	if err != nil {
		return azkeyvault.KeyBundle{}, fmt.Errorf("unable to get vault key: %w", err)
	}
	return vaultKey, nil
}

// CreateKey generates a key it does not exists.
// The generated key is RSA with a size of 4096.
// The attached allowed operations are wrap and unwrap.
func (c *KeyVaultClient) CreateKey(ctx context.Context, keyName string) error {
	var keySize int32 = 4096
	_, err := c.Client.CreateKey(ctx, c.KvURL, keyName, azkeyvault.KeyCreateParameters{
		KeySize: &keySize,
		Kty:     azkeyvault.RSA,
		KeyOps: &[]azkeyvault.JSONWebKeyOperation{
			azkeyvault.WrapKey,
			azkeyvault.UnwrapKey,
		},
	})
	return err
}

func (c *KeyVaultClient) GetKeyLastVersionID(ctx context.Context, keyName string) (string, error) {
	// Listing the key versions
	var versions []azkeyvault.KeyItem
	for list, err := c.Client.GetKeyVersions(ctx, c.KvURL, keyName, nil); list.NotDone(); err = list.NextWithContext(ctx) {
		if err != nil {
			return "", fmt.Errorf("failed to list key versions: %w", err)
		}
		versions = append(versions, list.Values()...)
	}

	if len(versions) == 0 {
		return "", fmt.Errorf("no versions found for key: %s", keyName)
	}

	// Sort the versions based on their creation date
	sort.Slice(versions, func(i, j int) bool {
		iTimeData, _ := json.Marshal(versions[i].Attributes.Created)
		jTimeData, _ := json.Marshal(versions[j].Attributes.Created)

		var iTime, jTime int64
		json.Unmarshal(iTimeData, &iTime)
		json.Unmarshal(jTimeData, &jTime)

		return iTime < jTime
	})
	// Retrieve the last version from the sorted list
	lastVersionId := *versions[len(versions)-1].Kid

	// Split the ID to get the version part
	segments := strings.Split(lastVersionId, "/")
	keyVersion := segments[len(segments)-1]

	return keyVersion, nil
}

func (c *KeyVaultClient) GetKeyFirstVersionID(ctx context.Context, keyName string) (string, error) {
	// Listing the key versions
	var versions []azkeyvault.KeyItem
	for list, err := c.Client.GetKeyVersions(ctx, c.KvURL, keyName, nil); list.NotDone(); err = list.NextWithContext(ctx) {
		if err != nil {
			return "", fmt.Errorf("failed to list key versions: %w", err)
		}
		versions = append(versions, list.Values()...)
	}

	if len(versions) == 0 {
		return "", fmt.Errorf("no versions found for key: %s", keyName)
	}

	// Sort the versions based on their creation date
	sort.Slice(versions, func(i, j int) bool {
		iTimeData, _ := json.Marshal(versions[i].Attributes.Created)
		jTimeData, _ := json.Marshal(versions[j].Attributes.Created)

		var iTime, jTime int64
		json.Unmarshal(iTimeData, &iTime)
		json.Unmarshal(jTimeData, &jTime)

		return iTime < jTime
	})
	// Retrieve the last version from the sorted list
	lastVersionId := *versions[0].Kid

	// Split the ID to get the version part
	segments := strings.Split(lastVersionId, "/")
	keyVersion := segments[len(segments)-1]

	return keyVersion, nil
}

func (c *KeyVaultClient) GetSecretLastVersionID(ctx context.Context, secretName string) (string, error) {
	// Listing the secret versions
	var versions []azkeyvault.SecretItem
	for list, err := c.Client.GetSecretVersions(ctx, c.KvURL, secretName, nil); list.NotDone(); err = list.NextWithContext(ctx) {
		if err != nil {
			return "", fmt.Errorf("failed to list secret versions: %w", err)
		}
		versions = append(versions, list.Values()...)
	}

	if len(versions) == 0 {
		return "", fmt.Errorf("no versions found for secret: %s", secretName)
	}

	// Sort the versions based on their creation date
	sort.Slice(versions, func(i, j int) bool {
		iTimeData, _ := json.Marshal(versions[i].Attributes.Created)
		jTimeData, _ := json.Marshal(versions[j].Attributes.Created)

		var iTime, jTime int64
		json.Unmarshal(iTimeData, &iTime)
		json.Unmarshal(jTimeData, &jTime)

		return iTime < jTime
	})
	// Retrieve the oldest version from the sorted list
	lastVersionId := *versions[len(versions)-1].ID

	// Split the ID to get the version part
	segments := strings.Split(lastVersionId, "/")
	secretVersion := segments[len(segments)-1]

	return secretVersion, nil
}

func (c *KeyVaultClient) GetSecretFirstVersionID(ctx context.Context, secretName string) (string, error) {
	// Listing the secret versions
	var versions []azkeyvault.SecretItem
	for list, err := c.Client.GetSecretVersions(ctx, c.KvURL, secretName, nil); list.NotDone(); err = list.NextWithContext(ctx) {
		if err != nil {
			return "", fmt.Errorf("failed to list secret versions: %w", err)
		}
		versions = append(versions, list.Values()...)
	}

	if len(versions) == 0 {
		return "", fmt.Errorf("no versions found for secret: %s", secretName)
	}

	// Sort the versions based on their creation date
	sort.Slice(versions, func(i, j int) bool {
		iTimeData, _ := json.Marshal(versions[i].Attributes.Created)
		jTimeData, _ := json.Marshal(versions[j].Attributes.Created)

		var iTime, jTime int64
		json.Unmarshal(iTimeData, &iTime)
		json.Unmarshal(jTimeData, &jTime)

		return iTime < jTime
	})
	// Retrieve the newest version from the sorted list
	lastVersionId := *versions[0].ID

	// Split the ID to get the version part
	segments := strings.Split(lastVersionId, "/")
	secretVersion := segments[len(segments)-1]

	return secretVersion, nil
}

func (c *KeyVaultClient) GetSecret(ctx context.Context, secretName, secretVersion string) ([]byte, error) {
	secretValue, err := c.Client.GetSecret(ctx, c.KvURL, secretName, secretVersion)
	if err != nil {
		return nil, fmt.Errorf("unable to get secret value: %w", err)
	}

	return []byte(*secretValue.Value), nil
}

func (c *KeyVaultClient) SetSecret(ctx context.Context, secretName, secretValue string) error {
	_, err := c.Client.SetSecret(ctx, c.KvURL, secretName, azkeyvault.SecretSetParameters{Value: &secretValue})
	if err != nil {
		return fmt.Errorf("unable to set secret value: %w", err)
	}

	return nil
}

func (c *KeyVaultClient) Wrap(ctx context.Context, keyName string, value []byte) ([]byte, error) {
	v := base64.RawURLEncoding.EncodeToString(value)
	parameters := azkeyvault.KeyOperationsParameters{Value: &v, Algorithm: azkeyvault.RSAOAEP256}
	result, err := c.Client.WrapKey(ctx, c.KvURL, keyName, "", parameters)
	if err != nil {
		return nil, fmt.Errorf("unable to wrap key '%s': %w", keyName, err)
	}

	return base64.RawURLEncoding.DecodeString(*result.Result)
}

func (c *KeyVaultClient) UnWrap(ctx context.Context, keyName, keyVersion string, value []byte) ([]byte, error) {
	v := base64.RawURLEncoding.EncodeToString(value)
	parameters := azkeyvault.KeyOperationsParameters{Value: &v, Algorithm: azkeyvault.RSAOAEP256}
	result, err := c.Client.UnwrapKey(ctx, c.KvURL, keyName, keyVersion, parameters)
	if err != nil {
		return nil, fmt.Errorf("unable to unwrap key '%s': %w", keyName, err)
	}

	return base64.RawURLEncoding.DecodeString(*result.Result)
}

func (c *KeyVaultClient) SecretExists(ctx context.Context, secretName string) (bool, error) {
	_, err := c.GetSecret(ctx, secretName, "")
	if err != nil {
		if strings.Contains(err.Error(), "was not found in this key vault") {
			return false, nil
		}
		return false, err
	}
	return true, nil
}
