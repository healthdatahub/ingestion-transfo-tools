package schema

import (
	"bytes"
	"errors"
	"fmt"
	"regexp"
	"strings"

	"rdd-pipeline/pkg/schema/meta"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/schema/version"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/blang/semver"
	"gopkg.in/yaml.v2"
)

var SchemaVersions = Versions{
	{
		APIVersion: v1alpha1.Version,
		Kind:       v1alpha1.PipelineKind,
		Factory:    v1alpha1.NewPipelineConfig,
	},
}

type Version struct {
	Factory    func() version.VersionedConfig
	APIVersion string
	Kind       string
}

type Versions []Version

// ParseConfig reads a configuration file.
func ParseConfig(buf []byte) (version.VersionedConfig, error) {
	// This is to quickly check that it's possibly a skaffold.yaml,
	// without parsing the whole file.
	if !bytes.Contains(buf, []byte("apiVersion")) {
		return nil, errors.New("missing apiVersion")
	}

	apiVersion := &meta.Config{}
	if err := yaml.Unmarshal(buf, apiVersion); err != nil {
		return nil, fmt.Errorf("parsing api version: %w", err)
	}

	factory, present := SchemaVersions.Find(apiVersion.APIVersion, apiVersion.Kind)
	if !present {
		return nil, fmt.Errorf("unknown api version: %q", apiVersion.APIVersion)
	}

	// Remove all top-level keys starting with `.` so they can be used as YAML anchors
	parsed := make(map[string]interface{})
	if err := yaml.UnmarshalStrict(buf, parsed); err != nil {
		return nil, fmt.Errorf("unable to parse YAML: %w", err)
	}
	for field := range parsed {
		if strings.HasPrefix(field, ".") {
			delete(parsed, field)
		}
	}
	buf, err := yaml.Marshal(parsed)
	if err != nil {
		return nil, fmt.Errorf("unable to re-marshal YAML without dotted keys: %w", err)
	}

	cfg := factory()
	if err := yaml.UnmarshalStrict(buf, cfg); err != nil {
		return nil, fmt.Errorf("unable to parse config: %w", err)
	}

	return cfg, nil
}

// ParseConfigAndUpgrade reads a configuration file and upgrades it to a given version.
func ParseConfigAndUpgrade(log hdhlog.Logger, buf []byte, toVersion string) (version.VersionedConfig, error) {
	cfg, err := ParseConfig(buf)
	if err != nil {
		return nil, err
	}

	// Check that the target version exists
	if _, present := SchemaVersions.Find(toVersion, cfg.GetKind()); !present {
		return nil, fmt.Errorf("unknown api version: %q", toVersion)
	}

	// Check that the config's version is not newer than the target version
	currentVersion, err := parseRDDPipelineVersion(cfg.GetVersion())
	if err != nil {
		return nil, err
	}
	targetVersion, err := parseRDDPipelineVersion(toVersion)
	if err != nil {
		return nil, err
	}

	if currentVersion.EQ(targetVersion) {
		return cfg, nil
	}
	if currentVersion.GT(targetVersion) {
		return nil, fmt.Errorf("config version %q is more recent than target version %q: upgrade Skaffold", cfg.GetVersion(), toVersion)
	}

	log.Info("config version out of date", "version", cfg.GetVersion(), "toVersion", toVersion)

	for cfg.GetVersion() != toVersion {
		cfg, err = cfg.Upgrade()
		if err != nil {
			return nil, fmt.Errorf("transforming skaffold config: %w", err)
		}
	}

	return cfg, nil
}

// Find search the constructor for a given api version.
func (v *Versions) Find(apiVersion string, kind string) (func() version.VersionedConfig, bool) {
	for _, version := range *v {
		if version.APIVersion == apiVersion {
			return version.Factory, true
		}
	}

	return nil, false
}

var re = regexp.MustCompile(`^rdd-pipeline/v(\d)(?:(alpha|beta)([1-9]?[0-9]))?$`)

// ParseRDDPipelineVersion parses a string into a semver.Version.
func parseRDDPipelineVersion(v string) (semver.Version, error) {
	res := re.FindStringSubmatch(v)
	if res == nil {
		return semver.Version{}, fmt.Errorf("%s is an invalid api version", v)
	}
	if res[2] == "" || res[3] == "" {
		return semver.Parse(fmt.Sprintf("%s.0.0", res[1]))
	}
	return semver.Parse(fmt.Sprintf("%s.0.0-%s.%s", res[1], res[2], res[3]))
}
