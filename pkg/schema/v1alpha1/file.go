package v1alpha1

import (
	"rdd-pipeline/pkg/schema/meta"
)

type FileConfig struct {
	meta.Config `yaml:",inline"`
	// Pipeline defines the Build/Test/Deploy phases.
	HashFileConfig `yaml:",inline"`
	Origin         *OriginConfig `yaml:"origin,omitempty"`
}

type HashFileConfig struct {
	CSV   *CSVConfig   `json:"csv,omitempty" yaml:"csv,omitempty"`
	PATH  *PathConfig  `json:"path,omitempty" yaml:"path,omitempty"`
	Json  *JsonConfig  `json:"json,omitempty" yaml:"json,omitempty"`
	Dicom *DicomConfig `json:"dicom,omitempty" yaml:"dicom,omitempty"`

	IgnoreValues []string `json:"ignoreValues,omitempty" yaml:"ignoreValues,omitempty"`
}

func (h *HashFileConfig) Clone() *HashFileConfig {
	clone := *h

	if h.CSV != nil {
		clone.CSV = h.CSV.Clone()
	}
	if h.PATH != nil {
		clone.PATH = h.PATH.Clone()
	}
	if h.Json != nil {
		clone.Json = h.Json.Clone()
	}
	if h.Dicom != nil {
		clone.Dicom = h.Dicom.Clone()
	}

	clone.IgnoreValues = make([]string, len(h.IgnoreValues))
	copy(clone.IgnoreValues, h.IgnoreValues)

	return &clone
}

type DicomConfig struct {
	Tags []string `json:"tags,omitempty" yaml:"tags,omitempty"`
}

func (d *DicomConfig) Clone() *DicomConfig {
	clone := *d
	clone.Tags = make([]string, len(d.Tags))
	copy(clone.Tags, d.Tags)
	return &clone
}

type JsonConfig struct {
	Tags []string `json:"tags,omitempty" yaml:"tags,omitempty"`
}

func (j *JsonConfig) Clone() *JsonConfig {
	clone := *j
	clone.Tags = make([]string, len(j.Tags))
	copy(clone.Tags, j.Tags)
	return &clone
}

type CSVConfig struct {
	Columns     []int    `json:"columns" yaml:"columns" yamltags:"required"`
	ColumnNames []string `json:"columnNames,omitempty" yaml:"columnNames,omitempty"`
	Separator   string   `json:"separator" yaml:"separator" yamltags:"required"`
	SkipLines   []int    `json:"skipLines,omitempty" yaml:"skipLines,omitempty"`
	LocalCt     bool     `json:"localCt,omitempty" yaml:"localCt,omitempty"`
}

func (c *CSVConfig) Clone() *CSVConfig {
	// Create a new CSVConfig instance
	clone := *c

	clone.Columns = make([]int, len(c.Columns))
	copy(clone.Columns, c.Columns)

	clone.ColumnNames = make([]string, len(c.ColumnNames))
	copy(clone.ColumnNames, c.ColumnNames)

	clone.SkipLines = make([]int, len(c.SkipLines))
	copy(clone.SkipLines, c.SkipLines)

	// Non-slice fields (like Separator and LocalCt) are already copied by the initial shallow copy
	return &clone
}

type PathConfig struct {
	DepthLevel []int  `json:"depthLevel" yaml:"depthLevel" yamltags:"required"`
	Separator  string `json:"separator" yaml:"separator" yamltags:"required"`
}

func (p *PathConfig) Clone() *PathConfig {
	clone := *p
	clone.DepthLevel = make([]int, len(p.DepthLevel))
	copy(clone.DepthLevel, p.DepthLevel)
	return &clone
}

type OriginConfig struct {
	File    string `yaml:"file,omitempty"`
	Archive string `yaml:"archive,omitempty"`
}
