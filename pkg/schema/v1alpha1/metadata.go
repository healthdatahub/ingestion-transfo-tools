package v1alpha1

// Metadata keys added to the blos
// keep them lowercase as Azure lowercase all metadata keys
const (
	MetadataOriginalPath                 = "originalpath" // Keep it for now as a reference for old packages to ensure it is removed
	MetadataMatchPattern                 = "match"
	MetadataBlobType                     = "type"
	MetadataVersion                      = "version"
	MetadataWrappedKey                   = "wrapedkey" // the typo here can not be fixed without breaking compat with existing blobs
	MetadataKeyvaultKey                  = "keyvaultkey"
	MetadataHashConfig                   = "hashconfig"
	MetadataMaskType                     = "masktype"
	MetadataMaskKeyvaultKey              = "maskkeyvaultkey"
	MetadataIgnoreValues                 = "ignorevalues"
	MetadataMaskKeyvaultKeyVersion       = "maskkeyvaultkeyversion"
	MetadataEncryptionKeyvaultKeyVersion = "keyvaultencryptionkeyversion"
	MetadataPlatformCompressionType      = "platformcompressiontype"
)
