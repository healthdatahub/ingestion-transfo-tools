package v1alpha1

import (
	"fmt"
	"regexp"

	"rdd-pipeline/pkg/schema/meta"
	"rdd-pipeline/pkg/schema/version"
)

// !!! WARNING !!! This config version is already released, please DO NOT MODIFY the structs in this file.
const (
	Version      string = "rdd-pipeline/v1alpha1"
	PipelineKind string = "Pipeline"

	CompressionTypeGzip string = "gzip"

	CompressionTypeRar  string = "rar"
	CompressionTypeNone string = "none"
)

// NewSkaffoldConfig creates a SkaffoldConfig.
func NewPipelineConfig() version.VersionedConfig {
	return new(PipelineConfig)
}

type PipelineConfig struct {
	meta.Config `yaml:",inline"`
	// Pipeline defines the Build/Test/Deploy phases.
	Pipeline `yaml:",inline"`
}

func (pc *PipelineConfig) Clone() *PipelineConfig {
	clone := *pc
	clone.Config = *pc.Config.Clone()
	clone.Pipeline = *pc.Pipeline.Clone()
	return &clone
}

func (config *PipelineConfig) GetVersion() string {
	return config.APIVersion
}

func (config *PipelineConfig) GetKind() string {
	return config.Kind
}

// Pipeline describes a Skaffold pipeline.
type Pipeline struct {
	// Decrypt describes how files are decrypted.
	Decrypt DecryptConfigs `yaml:"decrypt,omitempty"`

	// deliveryEncrypt describes how files are encrypted for delivery.
	DeliveryEncrypt DeliveryEncryptConfigs `yaml:"deliveryEncrypt,omitempty"`

	// Hash describes how files are hashed.
	Hash HashConfigs `yaml:"mask,omitempty"`

	// Import describes the SFTP config from where to download files.
	Import Import `yaml:"import,omitempty"`

	// DefaultGPG is used when no GPG specific conf is detailed for a blob type
	DefaultGPG GPGConfig `yaml:"defaultgpg,omitempty"`

	// mask_AES can be true/false.
	Mask_AES bool `yaml:"mask_AES,omitempty"`

	// Tags Filtering configuration
	TagsFilter map[string]string `json:"tagsfilter,omitempty" yaml:"tagsfilter,omitempty"`
}

func (p *Pipeline) Clone() *Pipeline {
	clone := *p
	clone.Decrypt = p.Decrypt.Clone()
	clone.Hash = p.Hash.Clone()
	clone.TagsFilter = make(map[string]string, len(p.TagsFilter))
	for k, v := range p.TagsFilter {
		clone.TagsFilter[k] = v
	}
	return &clone
}

type (
	HashConfigs            []*HashConfig
	DecryptConfigs         []*DecryptConfig
	DeliveryEncryptConfigs []*DeliveryEncryptConfig
)

func (hcs HashConfigs) Clone() HashConfigs {
	clone := make(HashConfigs, len(hcs))
	for i, hc := range hcs {
		clone[i] = hc.Clone()
	}
	return clone
}

func (dcs DecryptConfigs) Clone() DecryptConfigs {
	clone := make(DecryptConfigs, len(dcs))
	for i, dc := range dcs {
		clone[i] = dc.Clone()
	}
	return clone
}

func (des DeliveryEncryptConfigs) Clone() DeliveryEncryptConfigs {
	clone := make(DeliveryEncryptConfigs, len(des))
	for i, de := range des {
		clone[i] = de.Clone()
	}
	return clone
}

func (d HashConfigs) GetConfig(blob string) (*HashConfig, error) {
	for _, c := range d {
		matched, err := match(c.Blob, blob)
		if err != nil {
			return nil, fmt.Errorf("failed to use regex %s: %w", c.Blob, err)
		}

		if matched {
			return c, nil
		}
	}

	return nil, nil
}

func (d DecryptConfigs) Match(blob string) bool {
	c, err := d.GetConfig(blob)
	return (c != nil) && (err == nil)
}

func (d DecryptConfigs) GetConfig(blob string) (*DecryptConfig, error) {
	for _, c := range d {
		matched, err := match(c.Blob, blob)
		if err != nil {
			return nil, fmt.Errorf("failed to use regex %s: %w", c.Blob, err)
		}

		if matched {
			return c, nil
		}
	}

	return nil, nil
}

func (d DeliveryEncryptConfigs) Match(blob string) bool {
	c, err := d.GetConfig(blob)
	return (c != nil) && (err == nil)
}

func (d DeliveryEncryptConfigs) GetConfig(blob string) (*DeliveryEncryptConfig, error) {
	for _, c := range d {
		matched, err := match(c.Blob, blob)
		if err != nil {
			return nil, fmt.Errorf("failed to use regex %s: %w", c.Blob, err)
		}

		if matched {
			return c, nil
		}
	}

	return nil, nil
}

func (d HashConfigs) Match(blob string) bool {
	c, err := d.GetConfig(blob)
	return (c != nil) && (err == nil)
}

func match(c, blob string) (bool, error) {
	return regexp.MatchString(c, blob)
}

type DecryptConfig struct {
	BlobConfig `yaml:",inline"`

	Input  InputConfig  `yaml:"input,omitempty"`
	Output OutputConfig `yaml:"output,omitempty"`

	GPG GPGConfig `yaml:"gpg,omitempty"`

	// Legacy field, kept for retrocompat
	Compression CompressionConfig `yaml:"compression,omitempty"`
}

type DeliveryEncryptConfig struct {
	BlobConfig `yaml:",inline"`
	GPG        GPGEncryptConfig `yaml:"gpg,omitempty"`
}

type GPGEncryptConfig struct {
	SignatureKey SecretReference `yaml:"signatureKey" yamltags:"required"`
	EncryptKey   SecretReference `yaml:"encryptKey" yamltags:"required"`
}

type InputConfig struct {
	Compression CompressionConfig `yaml:"compression,omitempty"`
}

type OutputConfig struct {
	Compression CompressionConfig `yaml:"compression,omitempty"`
}

func (dc *DecryptConfig) Clone() *DecryptConfig {
	clone := *dc
	clone.BlobConfig = *dc.BlobConfig.Clone()
	clone.Compression = *dc.Compression.Clone()
	clone.GPG = *dc.GPG.Clone()
	return &clone
}

func (de *DeliveryEncryptConfig) Clone() *DeliveryEncryptConfig {
	clone := *de
	clone.BlobConfig = *de.BlobConfig.Clone()
	clone.GPG = *de.GPG.Clone()
	return &clone
}

type CompressionConfig struct {
	Type string `yaml:"type" yamltags:"required"`

	// Legacy field, kept for retrocompat, now renamed as "type"
	Format string `yaml:"format" yamltags:"required"`
}

func (cc *CompressionConfig) Clone() *CompressionConfig {
	clone := *cc
	return &clone
}

type GPGConfig struct {
	SignatureKey SecretReference `yaml:"signatureKey" yamltags:"required"`
	DecryptKey   SecretReference `yaml:"decryptKey" yamltags:"required"`
}

func (gc *GPGConfig) Clone() *GPGConfig {
	clone := *gc
	clone.SignatureKey = *gc.SignatureKey.Clone()
	clone.DecryptKey = *gc.DecryptKey.Clone()
	return &clone
}

func (gec *GPGEncryptConfig) Clone() *GPGEncryptConfig {
	clone := *gec
	clone.SignatureKey = *gec.SignatureKey.Clone()
	clone.EncryptKey = *gec.EncryptKey.Clone()
	return &clone
}

type HashConfig struct {
	BlobConfig `yaml:",inline"`
	Key        SecretReference `yaml:"key" yamltags:"required"`
	Default    HashFileConfig  `yaml:"default" yamltags:"required"`
}

func (hc *HashConfig) Clone() *HashConfig {
	clone := *hc
	clone.BlobConfig = *hc.BlobConfig.Clone()
	clone.Key = *hc.Key.Clone()
	clone.Default = *hc.Default.Clone()
	return &clone
}

type SecretReference struct {
	Secret           string `yaml:"secret,omitempty"`
	PassphraseSecret string `yaml:"passphraseSecret,omitempty"`
	Password         string `yaml:"password,omitempty"`
	// in some cases, we store key in a blob instead of a keyvault.
	// this can happen for non-critical secrets like customer public keys.
	Blob string `yaml:"blob"`
}

func (sr *SecretReference) Clone() *SecretReference {
	clone := *sr
	return &clone
}

type Import struct {
	Sftp SftpConfig `yaml:"sftp,omitempty"`
}

type SftpConfig struct {
	Server    string   `yaml:"server"`
	User      string   `yaml:"user"`
	Delete    *bool    `yaml:"delete,omitempty"`
	Files     []string `yaml:"files"`
	KeepAlive bool     `yaml:"keepalive,omitempty"`
}
