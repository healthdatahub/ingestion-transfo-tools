package azure

import (
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
)

// NewStorageAccountClient return a storage account client providing
// high level operations like container creation/deletion. It also provides
// blob-level but you have to specify the container name in each call.
func NewStorageAccountClient(storageAccountURL string) (*azblob.Client, error) {
	creds, err := azidentity.NewDefaultAzureCredential(nil)
	if err != nil {
		return nil, err
	}
	return azblob.NewClient(storageAccountURL, creds, nil)
}

// NewContainerClient return an azure client to interact with a specific container.
func NewContainerClient(storageAccountURL, containerName string) (*container.Client, error) {
	creds, err := azidentity.NewDefaultAzureCredential(nil)
	if err != nil {
		return nil, err
	}
	cc, err := azblob.NewClient(storageAccountURL, creds, nil)
	if err != nil {
		return nil, err
	}
	return cc.ServiceClient().NewContainerClient(containerName), nil
}
