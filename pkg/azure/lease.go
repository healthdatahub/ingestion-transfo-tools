package azure

import (
	"context"
	"fmt"
	"time"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/lease"
)

// NewLeaseClient returns a client to manipulate a lease on a blob
func NewLeaseClient(containerClient *container.Client, blobName string) (*lease.BlobClient, error) {
	return lease.NewBlobClient(containerClient.NewBlockBlobClient(blobName), &lease.BlobClientOptions{})
}

// AcquireContainerLeaseWithLoop tries to acquire a lease on a container.
// It the lease is already taken, wat for 10 seconds and retry.
func AcquireContainerLeaseWithLoop(ctx context.Context, c *container.Client) error {
	log := hdhlog.FromContext(ctx)
	lc, err := lease.NewContainerClient(c, &lease.ContainerClientOptions{})
	if err != nil {
		return fmt.Errorf("building container client : %w", err)
	}
	for {
		_, err := lc.AcquireLease(ctx, -1, &lease.ContainerAcquireOptions{})

		if err != nil {
			if bloberror.HasCode(err, bloberror.LeaseAlreadyPresent) {
				time.Sleep(time.Second * 10)
				log.Info("container lease already present, wait for 10 seconds and retry")
				continue
			}
			return fmt.Errorf("acquire lease : %w", err)
		}
		break
	}
	return nil
}

// BreakContainerLease breaks a container lease.
func BreakContainerLease(ctx context.Context, c *container.Client) error {
	lc, err := lease.NewContainerClient(c, &lease.ContainerClientOptions{})
	if err != nil {
		return fmt.Errorf("building container client : %w", err)
	}
	_, err = lc.BreakLease(ctx, &lease.ContainerBreakOptions{})
	return err
}
