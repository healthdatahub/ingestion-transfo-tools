package azure

import (
	"crypto/aes"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"os"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
)

const cpkEnvironmentVariable = "HDH_CPK"

// NewClientProvidedKey returns a CPK from the environment.
func NewClientProvidedKey() (*blob.CPKInfo, error) {
	key, present := os.LookupEnv(cpkEnvironmentVariable)
	if !present {
		return &blob.CPKInfo{
			EncryptionAlgorithm: to.Ptr(blob.EncryptionAlgorithmTypeNone),
		}, nil
	}
	bKey := []byte(key)
	if l := len(key); l != 32 {
		return nil, fmt.Errorf("env variable %s is not an AES 256 key. Size %d, should be 32", cpkEnvironmentVariable, l)
	}
	if _, err := aes.NewCipher(bKey); err != nil {
		return nil, fmt.Errorf("env variable %s is not an AES", cpkEnvironmentVariable)
	}
	sha := sha256.Sum256(bKey)

	return &blob.CPKInfo{
		EncryptionAlgorithm: to.Ptr(blob.EncryptionAlgorithmTypeAES256),
		EncryptionKey:       to.Ptr(base64.StdEncoding.EncodeToString(bKey)),
		EncryptionKeySHA256: to.Ptr(base64.StdEncoding.EncodeToString(sha[:])),
	}, nil

}
