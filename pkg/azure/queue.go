package azure

import (
	"net/url"

	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azqueue"
)

// NewQueueClientFromEnvironment returns a client to manipulate an azure queue.
func NewQueueClientFromEnvironment(accountURL, queueName string) (*azqueue.QueueClient, error) {
	creds, err := azidentity.NewDefaultAzureCredential(nil)
	if err != nil {
		return nil, err
	}
	queueURL, err := url.JoinPath(accountURL, queueName)
	if err != nil {
		return nil, err
	}
	return azqueue.NewQueueClient(queueURL, creds, &azqueue.ClientOptions{})
}
