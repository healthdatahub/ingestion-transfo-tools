package azure

import (
	"github.com/Azure/azure-sdk-for-go/sdk/azcore/arm/policy"
	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/storage/armstorage"
)

// NewARMStorageClient creates a new instance of AccountsClient with the
// specified subscription ID.
// This client provides high level operations to manipulate storage accounts
// - creating/deleting a storage account
// - manipulating keys (regenerating, revoking)
// - list all storage account attached to a subscription
func NewARMStorageClient(subscriptionID string) (*armstorage.AccountsClient, error) {
	creds, err := azidentity.NewDefaultAzureCredential(&azidentity.DefaultAzureCredentialOptions{})
	if err != nil {
		panic(err)
	}
	return armstorage.NewAccountsClient(subscriptionID, creds, &policy.ClientOptions{})
}
