package hdhcrypt

import (
	"crypto/aes"
	"crypto/cipher"
	"fmt"
	"io"
)

func GetStreamWriter(key []byte, out io.Writer) (cipher.StreamWriter, error) {
	stream, err := getStream(key)

	return cipher.StreamWriter{S: stream, W: out}, err
}

func GetStreamReader(key []byte, in io.Reader) (cipher.StreamReader, error) {
	stream, err := getStream(key)

	return cipher.StreamReader{S: stream, R: in}, err
}

func getStream(key []byte) (cipher.Stream, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, fmt.Errorf("cannot generate a new cipher: %w", err)
	}

	var iv [aes.BlockSize]byte
	return cipher.NewOFB(block, iv[:]), nil
}
