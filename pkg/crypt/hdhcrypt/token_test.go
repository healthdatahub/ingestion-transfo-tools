package hdhcrypt_test

import (
	"testing"

	"rdd-pipeline/pkg/crypt/hdhcrypt"
)

func TestGenerateKey(t *testing.T) {
	tests := []struct {
		name    string
		want    int
		wantErr bool
	}{
		{
			name:    "size is 32",
			want:    32,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here
			got, err := hdhcrypt.GenerateKey()
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateKey() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != tt.want {
				t.Errorf("len(GenerateKey()) = %v, want %v", len(got), tt.want)
			}
		})
	}
}
