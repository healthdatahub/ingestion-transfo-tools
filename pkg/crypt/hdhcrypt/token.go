package hdhcrypt

import (
	"crypto/rand"
	"fmt"
)

func GenerateKey() ([]byte, error) {
	key := make([]byte, 32) // 32 bytes to select AES-256
	if _, err := rand.Read(key); err != nil {
		return nil, fmt.Errorf("Error generate token: %w", err)
	}
	return key, nil
}
