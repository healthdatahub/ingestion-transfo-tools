package hdhcrypt

import (
	"bytes"
	"io"
	"strings"
	"testing"
)

func TestGetStreamWriter(t *testing.T) {
	type args struct {
		key []byte
	}
	tests := []struct {
		name    string
		args    args
		wantOut string
		wantErr bool
	}{
		{
			name: "bad_key",
			args: args{
				key: []byte{},
			},
			wantOut: "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			out := &bytes.Buffer{}
			_, err := GetStreamWriter(tt.args.key, out)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetStreamWriter() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			// if !reflect.DeepEqual(got, tt.want) {
			// 	t.Errorf("GetStreamWriter() = %v, want %v", got, tt.want)
			// }
			if gotOut := out.String(); gotOut != tt.wantOut {
				t.Errorf("GetStreamWriter() = %v, want %v", gotOut, tt.wantOut)
			}
		})
	}
}

func TestGetStreamReader(t *testing.T) {
	type args struct {
		key []byte
		in  io.Reader
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "bad_key",
			args: args{
				key: []byte{},
				in:  strings.NewReader(""),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here

		t.Run(tt.name, func(t *testing.T) {
			_, err := GetStreamReader(tt.args.key, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetStreamReader() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
