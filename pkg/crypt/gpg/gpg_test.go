package gpg_test

import (
	"bytes"
	"io"
	"net/http"
	"os"
	"os/exec"
	"rdd-pipeline/pkg/crypt/gpg"
	hdhlog "rdd-pipeline/pkg/log"
	"reflect"
	"strings"
	"testing"
)

const (
	passphrase      = "testhdh"
	dummypassphrase = "dummy"
)

func TestDecrypt(t *testing.T) {
	encyptedFile, _ := os.Open("testdata/42encrypted.pgp")
	encyptedAndSignedFile, _ := os.Open("testdata/42encrypted&signed.pgp")
	encyptedFileAndSignedKeyExpired, _ := os.Open("testdata/42encryptedAndSignedKeyExpired.pgp") // Encrypted and signed with an expired key
	privateKey, _ := os.Open("testdata/private-key.asc")
	publicKey, _ := os.Open("testdata/public-key.asc")
	publicPlatform, _ := os.Open("testdata/platform-public.asc")
	//privateOutside, _ := os.Open("testdata/outside-private.asc") // for further usage, when we'll encrypt
	publicOutside, _ := os.Open("testdata/outside-public.asc")
	privatePlatform, _ := os.Open("testdata/platform-private.asc")
	publicGuilhem, _ := os.Open("testdata/public-guilhem.asc")

	keyErrored, _ := os.Open("testdata/public-errored.asc")
	publicDummy, _ := os.Open("testdata/public-dummy.asc")
	privateNoSign, _ := os.Open("testdata/private-nosign.asc")
	privateDummy, _ := os.Open("testdata/private-dummy.asc")

	p11encrypted, _ := os.Open("testdata/p11_fake_crypted.png")
	p11public, _ := os.Open("testdata/p11_fake_transfer_public_key.rsa")
	p11HDHprivate, _ := os.Open("testdata/p11_fake_hdh_priv.rsa")

	type args struct {
		input      io.Reader
		publicKey  io.Reader
		secretKey  io.Reader
		passphrase string
	}
	tests := []struct {
		name        string
		args        args
		want        []byte
		contentType string
		wantErr     bool
		errContains string
	}{
		{
			name: "KO private key",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicPlatform,
				secretKey:  keyErrored,
				passphrase: passphrase,
			},
			want:        []byte("42"),
			wantErr:     true,
			errContains: "fail reading armored private key",
		},
		{
			name: "KO public key",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  keyErrored,
				secretKey:  privateKey,
				passphrase: passphrase,
			},
			want:        []byte("42"),
			wantErr:     true,
			errContains: "fail creating public key object",
		},
		{
			name: "No sign KO private key",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicPlatform,
				secretKey:  privateNoSign,
				passphrase: passphrase,
			},
			want:        []byte("42"),
			wantErr:     true,
			errContains: "incorrect key",
		},
		{
			name: "OK encrypt and OK signed and no passphrase",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicPlatform,
				secretKey:  privateKey,
				passphrase: "",
			},
			want:        []byte("42"),
			wantErr:     true,
			errContains: "required passphrase not provided",
		},
		{
			name: "OK encrypt and OK signed",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicOutside,
				secretKey:  privatePlatform,
				passphrase: passphrase,
			},
			want:        []byte("42"),
			contentType: "text/plain; charset=utf-8",
			wantErr:     false,
		},
		{
			name: "OK encrypt and KO signed",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicDummy,
				secretKey:  privateKey,
				passphrase: passphrase,
			},
			want:        nil,
			wantErr:     true,
			errContains: "incorrect key",
		},
		{
			name: "KO encrypt and OK signed",
			args: args{
				input:      encyptedAndSignedFile,
				publicKey:  publicPlatform,
				secretKey:  privateDummy,
				passphrase: dummypassphrase,
			},
			want:        nil,
			wantErr:     true,
			errContains: "incorrect key",
		},
		{
			name: "OK encrypt",
			args: args{
				input:      encyptedFile,
				publicKey:  nil,
				secretKey:  privatePlatform,
				passphrase: passphrase,
			},
			want:    []byte("42"),
			wantErr: false,
		},
		{
			name: "KO encrypt",
			args: args{
				input:      encyptedFile,
				publicKey:  nil,
				secretKey:  privateDummy,
				passphrase: dummypassphrase,
			},
			want:        nil,
			wantErr:     true,
			errContains: "incorrect key",
		},
		{
			name: "KO passphrase",
			args: args{
				input:      encyptedFile,
				publicKey:  publicKey,
				secretKey:  privateKey,
				passphrase: "bad-passphrase",
			},
			want:        nil,
			wantErr:     true,
			errContains: "fail unlocking privateKey",
		},
		{
			name: "deepPiste",
			args: args{
				input:      p11encrypted,
				publicKey:  p11public,
				secretKey:  p11HDHprivate,
				passphrase: "123456",
			},
			want:        nil,
			contentType: "image/png",
			wantErr:     false,
		},
		{
			name: "encrypted with expired key",
			args: args{
				input:      encyptedFileAndSignedKeyExpired,
				publicKey:  publicGuilhem,
				secretKey:  privateKey,
				passphrase: passphrase,
			},
			want:        []byte("42"),
			wantErr:     true,
			errContains: "key expired",
		},
	}

	for _, tt := range tests {
		publicKey.Seek(0, 0)
		privateKey.Seek(0, 0)
		encyptedAndSignedFile.Seek(0, 0)
		encyptedFile.Seek(0, 0)
		publicPlatform.Seek(0, 0)
		privatePlatform.Seek(0, 0)
		privateDummy.Seek(0, 0)
		publicDummy.Seek(0, 0)
		publicGuilhem.Seek(0, 0)

		t.Run(tt.name, func(t *testing.T) {
			tt := tt //nolint
			got, err := gpg.Decrypt(hdhlog.Discard(), tt.args.input, tt.args.publicKey, tt.args.secretKey, []byte(tt.args.passphrase))
			var gotB []byte
			if err == nil {
				gotB, err = io.ReadAll(got)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("Decrypt() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil && !strings.Contains(err.Error(), tt.errContains) {
				t.Errorf("Decrypt() error = %v, expected error to contain %v", err.Error(), tt.errContains)
				return
			}
			if err != nil {
				t.Logf("error is: %v", err)
				return
			}
			if tt.want != nil && !reflect.DeepEqual(gotB, tt.want) {
				t.Errorf("Decrypt() = %v, want %v", got, tt.want)
			}
			if tt.contentType != "" {
				ct := http.DetectContentType(gotB)
				if ct != tt.contentType {
					t.Errorf("Decrypt() ContentType = %v, want %v", ct, tt.contentType)
				}
			}
		})
	}
}

func TestEncryptAndSign(t *testing.T) {
	plainText, _ := os.ReadFile("testdata/plaintext.txt")
	publicOutsideData, _ := os.ReadFile("testdata/outside-public.asc")     // Public key data for encryption
	privatePlatformData, _ := os.ReadFile("testdata/platform-private.asc") // Private key data for signing
	privateOutsideData, _ := os.ReadFile("testdata/outside-private.asc")   // Private key data for decryption
	publicPlatformData, _ := os.ReadFile("testdata/platform-public.asc")   // Public key data for signature verification
	expectedContent := "This is a test file content for encryption and signing."

	type args struct {
		input      io.Reader
		publicKey  []byte
		secretKey  []byte
		passphrase string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "OK encrypt with publicOutside and sign with privatePlatform",
			args: args{
				input:      bytes.NewReader(plainText),
				publicKey:  publicOutsideData,
				secretKey:  privatePlatformData,
				passphrase: "testhdh",
			},
			wantErr: false,
		},
		{
			name: "KO encrypt with publicOutside and sign with privatePlatform using wrong passphrase",
			args: args{
				input:      bytes.NewReader(plainText),
				publicKey:  publicOutsideData,
				secretKey:  privatePlatformData,
				passphrase: "wrong-passphrase",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			// Create new readers from the key data for each test run
			publicKeyReader := bytes.NewReader(tt.args.publicKey)
			privateKeyReader := bytes.NewReader(tt.args.secretKey)

			// Encrypt and sign
			var encryptedBuffer bytes.Buffer
			encryptedData, err := gpg.EncryptAndSign(hdhlog.Discard(), tt.args.input, publicKeyReader, privateKeyReader, []byte(tt.args.passphrase))
			if (err != nil) != tt.wantErr {
				t.Errorf("EncryptAndSign() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !tt.wantErr {
				// Write the encrypted data to a buffer
				_, err := io.Copy(&encryptedBuffer, encryptedData)
				if err != nil {
					t.Errorf("Failed to write encrypted data to buffer: %v", err)
					return
				}

				// Save the encrypted data to a file for inspection
				encryptedFilePath := "testdata/encrypted_output.pgp"
				err = os.WriteFile(encryptedFilePath, encryptedBuffer.Bytes(), 0644)
				if err != nil {
					t.Errorf("Failed to create file for encrypted output: %v", err)
					return
				}

				// Ensure the file is deleted after the test completes
				defer func() {
					err := os.Remove(encryptedFilePath)
					if err != nil {
						t.Errorf("Failed to delete the file %s: %v", encryptedFilePath, err)
					}
				}()

				// Run the GPG command to inspect the file, passing the passphrase
				cmd := exec.Command("gpg", "--list-packets", "-vv", "--show-session-key", "--pinentry-mode", "loopback", "--passphrase", "testhdh", encryptedFilePath)
				cmd.Env = append(os.Environ(), "GPG_TTY=/dev/tty") // Needed for gpg to work properly
				var cmdOutput bytes.Buffer
				cmd.Stdout = &cmdOutput
				cmd.Stderr = &cmdOutput
				err = cmd.Run()
				if err != nil {
					t.Errorf("Failed to run gpg command: %v, err output: %s", err, cmdOutput.String())
					return
				}

				// Check for "AES256 encrypted data" in the output
				if !strings.Contains(cmdOutput.String(), "AES256 encrypted data") {
					t.Errorf("GPG output does not indicate AES256 encryption: %v", cmdOutput.String())
					return
				} else {
					t.Logf("GPG output indicates AES256 encryption:\n%s", cmdOutput.String())
				}

				// Reset the reader for decryption by creating a new reader from the buffer content
				publicPlatformReader := bytes.NewReader(publicPlatformData)
				privateOutsideReader := bytes.NewReader(privateOutsideData)

				decryptedReader, err := gpg.Decrypt(hdhlog.Discard(), bytes.NewReader(encryptedBuffer.Bytes()), publicPlatformReader, privateOutsideReader, []byte(tt.args.passphrase))
				if err != nil {
					t.Errorf("Decrypt after EncryptAndSign failed: %v", err)
					return
				}

				decryptedContent, _ := io.ReadAll(decryptedReader)
				t.Logf("Decrypted content length: %d", len(decryptedContent))
				t.Logf("Decrypted content: %s", decryptedContent)
				if !reflect.DeepEqual(string(decryptedContent), expectedContent) {
					t.Errorf("Decrypted content doesn't match original. got = %v, want %v", string(decryptedContent), expectedContent)
				}
			}
		})
	}
}

func TestGenerateGPGKeys(t *testing.T) {
	identity := "test@hdh.fr"
	keyType := "rsa"
	keySize := 4096
	passphrase := "strongpassphrase"

	// Generate the GPG keys, use them to encrypt and decrypt
	privateKey, publicKey, err := gpg.GenerateGPGKeys(identity, keyType, keySize, passphrase)
	if err != nil {
		t.Fatalf("Failed to generate GPG keys: %v", err)
	}

	publicKeyReader := bytes.NewReader([]byte(publicKey))
	privateKeyReader := bytes.NewReader([]byte(privateKey))
	passphraseBytes := []byte(passphrase)

	plainText := "This is a test string for GPG encryption and decryption."

	// Encrypt the plainText
	encryptedData, err := gpg.EncryptAndSign(hdhlog.Discard(), strings.NewReader(plainText), publicKeyReader, privateKeyReader, passphraseBytes)
	if err != nil {
		t.Fatalf("Failed to encrypt data with generated keys: %v", err)
	}

	var encryptedBuffer bytes.Buffer
	_, err = io.Copy(&encryptedBuffer, encryptedData)
	if err != nil {
		t.Fatalf("Failed to read encrypted data: %v", err)
	}

	// Reset the key readers, we use the same ones here than for encryption
	publicKeyReader = bytes.NewReader([]byte(publicKey))
	privateKeyReader = bytes.NewReader([]byte(privateKey))

	// Decrypt the data
	decryptedData, err := gpg.Decrypt(hdhlog.Discard(), bytes.NewReader(encryptedBuffer.Bytes()), publicKeyReader, privateKeyReader, passphraseBytes)
	if err != nil {
		t.Fatalf("Failed to decrypt data with generated keys: %v", err)
	}

	decryptedBytes, err := io.ReadAll(decryptedData)
	if err != nil {
		t.Fatalf("Failed to read decrypted data: %v", err)
	}

	// Check if the decrypted data matches the original plain text
	if string(decryptedBytes) != plainText {
		t.Errorf("Decrypted content does not match original. Got = %v, want = %v", string(decryptedBytes), plainText)
	}
}
