package gpg

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"strings"
	"time"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/ProtonMail/go-crypto/openpgp"
	"github.com/ProtonMail/gopenpgp/v2/crypto"
)

// GetExpirationTime returns the expiration time of a GPG key
func GetExpirationTime(log hdhlog.Logger, key io.Reader) (time.Time, error) {

	// An EntityList contains one or more Entities.
	//
	// An Entity represents the components of an OpenPGP key:
	//
	// a primary public key (which must be a signing key),
	// one or more identities claimed by that key,
	// and zero or more subkeys, which may be encryption keys.
	//
	// See: https://pkg.go.dev/github.com/ProtonMail/crypto/openpgp#Entity
	// Wiki: https://dev.azure.com/health-data-hub/health-data-hub-platform/_wiki/wikis/wikiMaster/878/2023-06-GPG-Decrypt-Key-Pair-on-Producer-Pipeline

	entityList, err := openpgp.ReadArmoredKeyRing(key)
	if err != nil {
		return time.Time{}, fmt.Errorf("Error reading key ring")
	}

	if len(entityList) == 0 {
		return time.Time{}, fmt.Errorf("No key found in the key ring")
	}

	// Assuming there's only one key in the key ring
	entity := entityList[0]
	flagMessage := ""
	var expirationTime time.Time

	// Check if the primary key has an expiration time
	if entity.PrimaryKey != nil {
		var expirationTime time.Time
		for _, ident := range entity.Identities {
			sig := ident.SelfSignature
			if sig != nil && sig.KeyLifetimeSecs != nil {
				expirationTime = entity.PrimaryKey.CreationTime.Add(time.Duration(*sig.KeyLifetimeSecs) * time.Second)
				break
			}
		}

		if expirationTime.IsZero() {
			log.Info("No expiration time found for the primary key")
		}
	} else {
		log.Info("No primary key found")
	}

	// Check if the subkey has an expiration time
	if len(entity.Subkeys) > 0 {
		log.Info("Subkeys listing", "Number of Subkeys", len(entity.Subkeys))
		for _, subKey := range entity.Subkeys {

			if subKey.Sig != nil && subKey.Sig.KeyLifetimeSecs != nil {
				if subKey.Sig.FlagCertify == true {
					flagMessage = "Encryption key expiration time"
				}
				if subKey.Sig.FlagSign == true {
					flagMessage = "Signing key expiration time"
				}
				expirationTime = subKey.Sig.CreationTime.Add(time.Duration(*subKey.Sig.KeyLifetimeSecs) * time.Second)
				log.Info("Reading Subkey Expiration Time", "flagMessage", flagMessage)
			} else {
				log.Info("No expiration time found for the subkey")
			}
		}
	} else {
		log.Info("No subkeys found")
	}

	return expirationTime, nil
}

func Decrypt(log hdhlog.Logger, input, publicKey, secretKey io.Reader, passphrase []byte) (io.Reader, error) {
	var publicKeyRing *crypto.KeyRing

	if publicKey != nil {
		log.Info("Read Public Key")

		publicKeyBytes, err := io.ReadAll(publicKey)
		if err != nil {
			return nil, fmt.Errorf("fail reading public key: %w", err)
		}

		publicKeyObj, err := crypto.NewKeyFromArmored(string(publicKeyBytes))
		if err != nil {
			return nil, fmt.Errorf("fail creating public key object: %w", err)
		}

		publicKeyRing, err = crypto.NewKeyRing(publicKeyObj)
		if err != nil {
			return nil, fmt.Errorf("fail creating public keyring: %w", err)
		}

		// Log key expiration time of Public Key
		publicKeyExpirationTime, err := GetExpirationTime(log, bytes.NewReader(publicKeyBytes))
		if err != nil {
			return nil, fmt.Errorf("failed to get key expiration time of public key: %w", err)
		}

		log.Info("Finished reading public key", "keyid", strings.ToUpper(publicKeyObj.GetHexKeyID()), "fingerprint", strings.ToUpper(publicKeyObj.GetFingerprint()), "Public Key Expiration time:", publicKeyExpirationTime)
	}

	log.Info("Read Private Key")

	privateKeyBytes, err := io.ReadAll(secretKey)
	if err != nil {
		return nil, fmt.Errorf("fail reading private key: %w", err)
	}

	privateKeyObj, err := crypto.NewKeyFromArmored(string(privateKeyBytes))
	if err != nil {
		return nil, fmt.Errorf("fail reading armored private key: %w", err)
	}

	// Log key expiration time of Private Key
	privateKeyExpirationTime, err := GetExpirationTime(log, bytes.NewReader(privateKeyBytes))
	if err != nil {
		return nil, fmt.Errorf("failed to get key expiration time of private key: %w", err)
	}
	log.Info("Finished reading private key Expiration Time", "keyid", strings.ToUpper(privateKeyObj.GetHexKeyID()), "fingerprint", strings.ToUpper(privateKeyObj.GetFingerprint()), "Private Key Expiration time:", privateKeyExpirationTime)

	locked, err := privateKeyObj.IsLocked()
	if err != nil {
		return nil, fmt.Errorf("can't check locked: %w", err)
	}

	if locked {
		if len(passphrase) != 0 {
			log.Info("Unlocking private key using passphrase.")
			privateKeyObj, err = privateKeyObj.Unlock(passphrase)
			if err != nil {
				return nil, fmt.Errorf("fail unlocking privateKey with password: %w", err)
			}
		} else {
			return nil, errors.New("required passphrase not provided")
		}
	} else if len(passphrase) != 0 {
		log.Info("key is unlocked. unrequired passphrase provided!")
	}

	privateKeyRing, err := crypto.NewKeyRing(privateKeyObj)
	if err != nil {
		return nil, fmt.Errorf("fail creating private keyring: %w", err)
	}

	log.Info("Finished reading private key", "keyid", strings.ToUpper(privateKeyObj.GetHexKeyID()), "fingerprint", strings.ToUpper(privateKeyObj.GetFingerprint()))

	pr, pw := io.Pipe()

	go func() {
		defer pw.Close()

		// Decrypt the message. It seems that the presence of publicKeyRing in the DecryptStream function is not enough to trigger the signature check, therefore we have to call
		// VerifySignature afterwards
		messageReader, err := privateKeyRing.DecryptStream(input, publicKeyRing, crypto.GetUnixTime())
		if err != nil {
			pw.CloseWithError(fmt.Errorf("fail decrypt input: %w", err))
			return
		}

		// Stream the decrypted data
		if _, err := io.Copy(pw, messageReader); err != nil {
			pw.CloseWithError(fmt.Errorf("fail streaming decrypted data: %w", err))
			return
		}

		// If a public key was provided, verify the signature after the entire message is read
		if publicKeyRing != nil {
			if err := messageReader.VerifySignature(); err != nil {
				pw.CloseWithError(fmt.Errorf("signature verification failed: %w", err))
				return
			}
			log.Info("Signature verified successfully.")
		}
	}()

	return pr, nil
}

func EncryptAndSign(log hdhlog.Logger, input io.Reader, publicKey, secretKey io.Reader, passphrase []byte) (io.Reader, error) {
	publicKeyBytes, err := io.ReadAll(publicKey)
	if err != nil {
		return nil, fmt.Errorf("fail reading public key: %w", err)
	}
	armoredPublicKey := strings.TrimSpace(string(publicKeyBytes))

	privateKeyBytes, err := io.ReadAll(secretKey)
	if err != nil {
		return nil, fmt.Errorf("fail reading private key: %w", err)
	}
	armoredPrivateKey := strings.TrimSpace(string(privateKeyBytes))

	if !strings.HasPrefix(armoredPublicKey, "-----BEGIN PGP PUBLIC KEY BLOCK-----") {
		return nil, errors.New("invalid public key format")
	}

	if !strings.HasPrefix(armoredPrivateKey, "-----BEGIN PGP PRIVATE KEY BLOCK-----") {
		return nil, errors.New("invalid private key format")
	}

	privateKeyObj, err := crypto.NewKeyFromArmored(armoredPrivateKey)
	if err != nil {
		return nil, fmt.Errorf("fail creating private key object: %w", err)
	}

	// Get the private key, and unlock it if it has a passphrase.
	isLocked, err := privateKeyObj.IsLocked()
	if err != nil {
		return nil, fmt.Errorf("cannot determine if the private key is locked: %w", err)
	}

	if isLocked {
		privateKeyObj, err = privateKeyObj.Unlock(passphrase)
		if err != nil {
			return nil, fmt.Errorf("fail unlocking private key with passphrase: %w", err)
		}
	}

	publicKeyObj, err := crypto.NewKeyFromArmored(armoredPublicKey)
	if err != nil {
		return nil, fmt.Errorf("fail creating public key object: %w", err)
	}

	publicKeyRing, err := crypto.NewKeyRing(publicKeyObj)
	if err != nil {
		return nil, fmt.Errorf("fail creating public keyring: %w", err)
	}

	privateKeyRing, err := crypto.NewKeyRing(privateKeyObj)
	if err != nil {
		return nil, fmt.Errorf("fail creating private keyring: %w", err)
	}

	// Create a pipe for streaming data
	encryptedPipeReader, encryptedPipeWriter := io.Pipe()

	go func() {
		defer encryptedPipeWriter.Close()

		writer, err := publicKeyRing.EncryptStreamWithCompression(encryptedPipeWriter, nil, privateKeyRing)
		if err != nil {
			encryptedPipeWriter.CloseWithError(fmt.Errorf("fail initializing encryption stream: %w", err))
			return
		}
		defer writer.Close()

		// Copy data from input to the writer (EncryptStream)
		if _, err := io.Copy(writer, input); err != nil {
			encryptedPipeWriter.CloseWithError(fmt.Errorf("fail encrypting data: %w", err))
			return
		}
	}()

	return encryptedPipeReader, nil
}

func GenerateGPGKeys(identity string, keyType string, keySize int, passphrase string) (string, string, error) {
	// Generate a new pair of GPG keys
	keyRing, err := crypto.GenerateKey(identity, "", keyType, keySize)
	if err != nil {
		return "", "", fmt.Errorf("error generating GPG keys: %w", err)
	}

	// Protect the private key with the passphrase
	if passphrase != "" {
		keyRing, err = keyRing.Lock([]byte(passphrase))
		if err != nil {
			return "", "", fmt.Errorf("error locking private key with passphrase: %w", err)
		}
	}

	// Export the private key
	privateKey, err := keyRing.Armor()
	if err != nil {
		return "", "", fmt.Errorf("error exporting private key: %w", err)
	}

	// Export the public key
	publicKey, err := keyRing.GetArmoredPublicKey()
	if err != nil {
		return "", "", fmt.Errorf("error exporting public key: %w", err)
	}

	return privateKey, publicKey, nil
}
