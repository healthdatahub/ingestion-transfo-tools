package preview

import (
	"bufio"
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	hdhlog "rdd-pipeline/pkg/log"
	"rdd-pipeline/pkg/worker"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"gopkg.in/yaml.v3"

	"strings"
)

type FilePreview struct {
	BlobName         string `yaml:"original_blob_name"`
	Headers          string `yaml:"headers,omitempty"`
	LineCount        int    `yaml:"line_count,omitempty"`
	FileSize         int64  `yaml:"file_size"`
	HumanFileSize    string `yaml:"human_file_size"`
	SHA256Hash       string `yaml:"sha256_hash"`
	FileFormat       string `yaml:"file_format"`
	BlobCreationTime string `yaml:"blob_creation_time,omitempty"`
	Separator        string `yaml:"separator,omitempty"`
}

func GetThirdExtension(filename string) (string, error) {
	splitName := strings.Split(filename, ".")
	if len(splitName) < 2 {
		return "", fmt.Errorf("no extension found in filename: %s", filename)
	}

	var targetExtension string
	if len(splitName) >= 4 {
		// Get the third extension from the end if there are three or more extensions (normal case: "filename.type.zip.gpg")
		targetExtension = splitName[len(splitName)-3]
	} else {
		// Otherwise, get the first extension (anormal case: "filename.gpg.zip")
		targetExtension = splitName[1]
	}

	return targetExtension, nil
}

func NewPreviewFile(ctx context.Context, outFile *worker.Blob, data io.Reader) (*FilePreview, error) {
	// Convert to lower case for comparison
	blobNameLower := strings.ToLower(outFile.Name())

	// Initialize FilePreview
	preview := &FilePreview{
		BlobName: outFile.Name(),
	}

	if strings.Contains(blobNameLower, ".csv") {
		preview.FileFormat = "csv"
		err := ProcessCSVFile(ctx, data, preview)
		if err != nil {
			return nil, fmt.Errorf("failed to process CSV file: %w", err)
		}
	} else if strings.Contains(blobNameLower, ".dicom") || strings.Contains(blobNameLower, ".dcm") {
		preview.FileFormat = "dicom"
		err := DefaultProcessFile(ctx, data, preview)
		if err != nil {
			return nil, fmt.Errorf("failed to process dicom file: %w", err)
		}
	} else if strings.Contains(blobNameLower, ".jpeg") || strings.Contains(blobNameLower, ".jpg") {
		preview.FileFormat = "image"
		err := DefaultProcessFile(ctx, data, preview)
		if err != nil {
			return nil, fmt.Errorf("failed to process image file: %w", err)
		}
	} else if strings.Contains(blobNameLower, ".json") {
		preview.FileFormat = "json"
		err := DefaultProcessFile(ctx, data, preview)
		if err != nil {
			return nil, fmt.Errorf("failed to process json file: %w", err)
		}
	} else { // last case is if we don't really know. We then just take the third extension (files here are as following: file.csv.zip.gpg)
		format, err := GetThirdExtension(blobNameLower)
		if err != nil {
			return nil, fmt.Errorf("failed to get extension: %w", err)
		}
		preview.FileFormat = format

		err = DefaultProcessFile(ctx, data, preview)
		if err != nil {
			return nil, fmt.Errorf("failed to process file: %w", err)
		}

	}

	// Get creation time from metadata if available
	if creationTime, ok := outFile.Metadata()["creationTime"]; ok {
		preview.BlobCreationTime = creationTime
	}

	return preview, nil
}

// GuessDelimiter automatically tries to guess the delimiter used in a given set of lines in a CSV file.
// It analyzes the frequency and consistency of characters in the first line across
// subsequent lines to determine the most likely delimiter. The function returns the
// guessed delimiter, a boolean indicating if the initial space should be skipped, and
// an error if no valid delimiter is found or if no lines are provided.
// This way to determine the separator was inspired by the Python's equivalent: https://github.com/python/cpython/blob/3.12/Lib/csv.py#L287
//
// Usage:
//
//	lines := []string{"name,age", "Alice,30", "Bob,25"}
//	delimiter, ok, err := GuessDelimiter(lines)
//	if err != nil {
//	    log.Fatal(err)
//	}
//	fmt.Printf("Delimiter: %c, Skip Initial Space: %t\n", delimiter, ok)
//
// Parameters:
//
//	lines []string: An array of strings representing lines in the input data.
//
// Returns:
//
//	rune: The guessed delimiter as a rune. Returns 0 if no valid delimiter is found.
//	bool: A boolean flag indicating whether the initial space should be skipped.
//	error: An error object. Returns an error in case of failure.
func GuessDelimiter(lines []string) (rune, bool, error) {
	lines = filterEmptyLines(lines)
	if len(lines) == 0 {
		return 0, false, errors.New("no lines provided")
	}

	// Predefined set of delimiter characters
	delimiterChars := map[rune]bool{',': true, ';': true, '|': true, '\t': true, ':': true}

	// Gather characters from the first line that are in the predefined set
	firstLineChars := make(map[rune]int)
	for _, char := range lines[0] {
		if _, exists := delimiterChars[char]; exists {
			firstLineChars[char]++
		}
	}

	charFrequency := make(map[rune]map[int]int)
	consistencyScores := make(map[rune]float64)
	chunkLength := min(10, len(lines))
	iteration := 0

	for start := 0; start < len(lines); start += chunkLength {
		end := min(start+chunkLength, len(lines))
		iteration++

		for _, line := range lines[start:end] {
			seenChars := make(map[rune]bool)
			for _, char := range line {
				if firstLineChars[char] > 0 && !seenChars[char] { // Check only characters present in the first line
					freq := strings.Count(line, string(char))
					if freq > 0 {
						if charFrequency[char] == nil {
							charFrequency[char] = make(map[int]int)
						}
						charFrequency[char][freq]++
					}
					seenChars[char] = true
				}
			}
		}
	}

	// Calculate consistency scores
	for char, freqMap := range charFrequency {
		var score float64
		for freq, count := range freqMap {
			if freq >= firstLineChars[char] { // Check frequency against first line
				proportion := float64(count) / float64(iteration)
				score += proportion * proportion
			}
		}
		consistencyScores[char] = score
	}

	highestConsistency := findHighestConsistency(consistencyScores)
	candidates := filterCandidatesByConsistency(consistencyScores, highestConsistency)

	if len(candidates) > 0 {
		validatedDelim, ok, err := validateAndSelectDelimiter(lines, candidates, firstLineChars)
		if err != nil {
			return 0, false, err
		}
		if ok {
			return validatedDelim, checkSkipInitialSpace(lines[0], validatedDelim), nil
		}
	}

	return 0, false, errors.New("no valid delimiter found")
}

func filterEmptyLines(lines []string) []string {
	var filtered []string
	for _, line := range lines {
		if line != "" {
			filtered = append(filtered, line)
		}
	}
	return filtered
}

func findHighestConsistency(consistencyScores map[rune]float64) float64 {
	var highest float64
	for _, score := range consistencyScores {
		if score > highest {
			highest = score
		}
	}
	return highest
}

func filterCandidatesByConsistency(consistencyScores map[rune]float64, highestConsistency float64) []rune {
	var candidates []rune
	for char, score := range consistencyScores {
		if score == highestConsistency {
			candidates = append(candidates, char)
		}
	}
	return candidates
}

func validateAndSelectDelimiter(lines []string, candidates []rune, firstLineCharFreq map[rune]int) (rune, bool, error) {
	if len(candidates) == 0 {
		return 0, false, errors.New("no candidates to validate")
	}

	var validatedCandidates []rune
	for _, candidate := range candidates {
		if validateSeparator(lines, candidate, firstLineCharFreq) {
			validatedCandidates = append(validatedCandidates, candidate)
		}
	}

	if len(validatedCandidates) == 1 {
		// Only one candidate passed validation, return it
		return validatedCandidates[0], true, nil
	} else if len(validatedCandidates) > 1 {
		// More than one candidate passed validation, construct an error message with the candidates
		var errMsg strings.Builder
		errMsg.WriteString("multiple candidates with the same consistency passed validation: ")
		for i, candidate := range validatedCandidates {
			if i > 0 {
				errMsg.WriteString(", ")
			}
			errMsg.WriteString(fmt.Sprintf("'%c'", candidate))
		}
		return 0, false, errors.New(errMsg.String())
	}

	// No candidate passed validation
	return 0, false, errors.New("no valid delimiter found")
}

func checkSkipInitialSpace(line string, delim rune) bool {
	return strings.Count(line, string(delim)) == strings.Count(line, string(delim)+" ")
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func validateSeparator(lines []string, separator rune, firstLineCharFreq map[rune]int) bool {
	// Check if separator frequency in each line is at least equal to its frequency in the first line
	minFreq := firstLineCharFreq[separator]
	for _, line := range lines {
		if strings.Count(line, string(separator)) < minFreq {
			return false
		}
	}

	reader := csv.NewReader(strings.NewReader(strings.Join(lines, "\n")))
	reader.Comma = separator
	reader.LazyQuotes = true

	var prevNumFields int
	isFirstLine := true
	lineCount := 0

	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		}

		if err != nil {
			return false
		}

		lineCount++
		numFields := len(record)
		if isFirstLine {
			prevNumFields = numFields
			isFirstLine = false
		} else if numFields != prevNumFields {
			return false
		}
	}

	// Check if the number of lines read matches the number of input lines
	if lineCount != len(lines) {
		return false
	}

	return true
}

func ProcessCSVFile(ctx context.Context, data io.Reader, preview *FilePreview) error {
	hasher := sha256.New()
	reader := bufio.NewReader(data)
	log := hdhlog.FromContext(ctx)

	var headers, currentLine string
	var fileSize int64
	lineCount, first50LineCount := 0, 0
	isFirstLine := true
	first50Lines := make([]string, 0, 50)

	buf := make([]byte, 4*1024*1024) // reading in MB chunks

	for {
		n, err := reader.Read(buf)
		if err != nil && err != io.EOF {
			log.Info("Error reading chunk:", err)
			return err
		}

		if err == io.EOF {
			if len(currentLine) > 0 {
				// Add the last line if it's not empty
				first50Lines = append(first50Lines, currentLine)
			}
			break
		}

		// Directly write to hasher
		_, err = hasher.Write(buf[:n])
		if err != nil {
			log.Info("Error writing to hasher:", err)
			return err
		}
		for i := 0; i < n; i++ {
			if buf[i] == '\n' || buf[i] == '\r' {
				// If '\r', check if next character is '\n'
				if buf[i] == '\r' && i+1 < n && buf[i+1] == '\n' {
					i++ // Skip the '\n' after '\r'
				}

				lineCount++
				if first50LineCount < 50 {
					first50Lines = append(first50Lines, currentLine)
					first50LineCount++
				}
				currentLine = ""
				if isFirstLine {
					isFirstLine = false
				}
			} else {
				currentLine += string(buf[i])
				if isFirstLine {
					// If it's first line (header), save the byte to headers
					headers += string(buf[i])
				}
			}
		}

		fileSize += int64(n)
	}

	mostConsistentChar, _, err := GuessDelimiter(first50Lines)
	if err != nil {
		// the error is an information, not an issue at this step
		log.Info("Error detecting automatically a delimiter:", err)
	}
	preview.SHA256Hash = fmt.Sprintf("%x", hasher.Sum(nil))
	preview.HumanFileSize = bytesToHumanReadable(fileSize)
	preview.Headers = strings.TrimSuffix(headers, "\r")
	preview.LineCount = lineCount
	preview.FileSize = fileSize

	var proposedSeparator string
	if mostConsistentChar == 0 {
		proposedSeparator = "not_found"
	} else {
		proposedSeparator = string(mostConsistentChar)
	}
	preview.Separator = proposedSeparator

	return nil
}

func DefaultProcessFile(ctx context.Context, data io.Reader, preview *FilePreview) error {
	hash := sha256.New()
	var fileSize int64
	log := hdhlog.FromContext(ctx)

	buf := make([]byte, 1024) // reading in 1KB chunks
	for {
		n, err := data.Read(buf)
		if err != nil && err != io.EOF {
			log.Info("Error reading chunk:", err)
			return err
		}
		if err == io.EOF {
			break
		}

		// update the hash value and file size
		_, err = hash.Write(buf[:n])
		if err != nil {
			log.Info("Error writing to hash:", err)
			return err
		}
		fileSize += int64(n)
	}

	sha256String := fmt.Sprintf("%x", hash.Sum(nil))
	preview.SHA256Hash = sha256String
	preview.FileSize = fileSize
	preview.HumanFileSize = bytesToHumanReadable(fileSize)

	return nil
}

type ByteCounter struct {
	BytesWritten int64
}

func (bc *ByteCounter) Write(p []byte) (int, error) {
	n := len(p)
	bc.BytesWritten += int64(n)
	return n, nil
}

func (fp *FilePreview) UploadFilePreview(ctx context.Context, containerClient *container.Client, blobName string) error {
	log := hdhlog.FromContext(ctx).With("container", containerClient.URL())
	log = log.With("FilePreviewName", blobName) //nolint

	// Wrap the FilePreview struct in a slice so that we will have the output formatted as a list subitem (this will ease the gathering process)
	previews := []*FilePreview{fp}

	// Convert FilePreview to yaml format
	yamlData, err := yaml.Marshal(previews)
	if err != nil {
		return fmt.Errorf("error marshaling FilePreview to YAML: %w", err)
	}
	t := time.Now()
	finalName := fmt.Sprintf("%s_%04d_%02d_%02d_%02d_%02d_%02d_metadata.yaml", blobName, t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())

	blob := worker.NewBlob(containerClient, finalName, nil, nil)

	return blob.Upload(ctx, bytes.NewReader(yamlData), nil)
}

func bytesToHumanReadable(b int64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%dB", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%.1f%cB", float64(b)/float64(div), "KMGTPE"[exp])
}
