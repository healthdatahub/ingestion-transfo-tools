package preview_test

import (
	"context"
	"fmt"
	"os"
	previewpackage "rdd-pipeline/pkg/preview"
	"strings"
	"testing"

	hdhlog "rdd-pipeline/pkg/log"
)

func TestGetThirdExtension(t *testing.T) {
	tests := []struct {
		name    string
		arg     string
		want    string
		wantErr bool
	}{
		{"no extension", "filename", "", true},
		{"one extension", "filename.gpg", "gpg", false},
		{"two extensions", "filename.zip.gpg", "zip", false},
		{"three extensions", "filename.type.zip.gpg", "type", false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := previewpackage.GetThirdExtension(tt.arg)
			if (err != nil) != tt.wantErr {
				t.Errorf("getThirdExtension() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getThirdExtension() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestProcessCSVFile(t *testing.T) {
	_, ctx := hdhlog.New(os.Stdout, 4).AttachToContext(context.Background())

	data := "header1,header2\nrow1data1,row1data2\nrow2data1,row2data2"
	expectedsha256 := "fa9e2500c9f5d67752dfa4319c857e8ea4c2ab3ca2edc527028bc86db3b8255c"
	lines := 2
	fileSize := int64(len(data))
	expectedHumanReadableSize := fmt.Sprintf("%dB", fileSize)

	reader := strings.NewReader(data)
	preview := &previewpackage.FilePreview{}

	err := previewpackage.ProcessCSVFile(ctx, reader, preview)

	if err != nil {
		t.Errorf("Error processing CSV file: %v", err)
	}
	if preview.Headers != "header1,header2" {
		t.Errorf("Incorrect headers: got %v, want header1,header2", preview.Headers)
	}
	if preview.LineCount != lines {
		t.Errorf("Incorrect line count: got %v, want 3", preview.LineCount)
	}
	if preview.SHA256Hash != expectedsha256 {
		t.Errorf("Incorrect SHA256 hash: got %v, want %v", preview.SHA256Hash, expectedsha256)
	}

	if preview.FileSize != fileSize {
		t.Errorf("Incorrect file size: got %v, want %v", preview.FileSize, fileSize)
	}
	humanFileSize := expectedHumanReadableSize
	if preview.HumanFileSize != humanFileSize {
		t.Errorf("Incorrect human-readable file size: got %v, want %v", preview.HumanFileSize, humanFileSize)
	}
}

func TestDefaultProcessFile(t *testing.T) {
	_, ctx := hdhlog.New(os.Stdout, 4).AttachToContext(context.Background())

	data := "header1,header2\nrow1data1,row1data2\nrow2data1,row2data2"
	expectedsha256 := "fa9e2500c9f5d67752dfa4319c857e8ea4c2ab3ca2edc527028bc86db3b8255c"
	expectedDataSize := int64(len(data))
	expectedHumanReadableSize := fmt.Sprintf("%dB", expectedDataSize)

	reader := strings.NewReader(data)
	preview := &previewpackage.FilePreview{}

	err := previewpackage.DefaultProcessFile(ctx, reader, preview)

	if err != nil {
		t.Errorf("Error processing file: %v", err)
	}

	if preview.FileSize != expectedDataSize {
		t.Errorf("Incorrect file size: got %v, want %v", preview.FileSize, len(data))
	}

	if preview.SHA256Hash != expectedsha256 {
		t.Errorf("Incorrect SHA256 hash: got %v, want %v", preview.SHA256Hash, expectedsha256)
	}

	if preview.HumanFileSize != expectedHumanReadableSize {
		t.Errorf("Incorrect human-readable file size: got %v, want %v", preview.HumanFileSize, expectedHumanReadableSize)
	}
}

func TestGuessDelimiter(t *testing.T) {
	tests := []struct {
		name              string
		lines             []string
		expectedDelimiter rune
		expectError       bool
	}{
		{ // Standard case, with a comma as obvious separator
			name: "CommaSeparatedValues",
			lines: []string{
				"field1,field2,field3,field4,field5,field6,field7,field8,field9,field10,field11,field12,field13,field14,field15",
				"apple,banana,cherry,date,elderberry,fig,grape,honeydew,kiwi,lemon,mango,nectarine,orange,papaya,quince",
				"ant,beetle,cat,dog,elephant,fox,giraffe,hippo,iguana,jaguar,kangaroo,lemur,monkey,newt,octopus",
			},
			expectedDelimiter: ',',
			expectError:       false,
		},
		{ // Multiple line break
			name: "LineBreakMadness",
			lines: []string{
				"field1,field2,field3,field4,field5,field6,field7,field8,field9,field10,field11,field12,field13,field14,field15\n\n",
				"apple,banana,cherry,date,elderberry,fig,grape,honeydew,kiwi,lemon,mango,nectarine,orange,papaya,quince\n\n\n",
				"ant,beetle,cat,dog,elephant,fox,giraffe,hippo,iguana,jaguar,kangaroo,lemur,monkey,newt,octopus\n\n\n\n\n\n\n\n\n",
			},
			expectedDelimiter: ',',
			expectError:       false,
		},
		{ // Malformed csv
			name: "CommaSeparatedValuesMalformed",
			lines: []string{
				"id,first,name,last,name,birth,jsaispas,colonneautre,onsenfout,peuimporte,pasmasquer",
				"test,test2,test3,test4,test5,test6,test7,test8,test9",
				"test,test2,test3,test4,test5,test6,test7,test8,test9",
				"test,test2,test3,test4,test5,test6,test7,,,test8,test9",
				"test,test2,test3,test4,test5,test6,test7,test8,test9",
				"test,test2,test3,test4,test5,test6,test7,test8,test9",
			},
			expectedDelimiter: 0,
			expectError:       true,
		},
		{ // Here we have a lot more of 'a', but 'a' is not as consistent as ','
			name: "ConsistencyTest",
			lines: []string{
				"aaaaaaaaaaaaaaaaa,aaaaaaaaa,aa",
				"a,a,a",
				"aa,a,a",
			},
			expectedDelimiter: ',',
			expectError:       false,
		},
		{ // Here we have a lot more of ',', but ',' is not as consistent as ';'
			name: "ConsistencyTestWithMoreConsistent",
			lines: []string{
				",,,,,,,,,,,;,,,,,,;,,,,",
				",;,;,",
				",,;,,,;,",
			},
			expectedDelimiter: ';',
			expectError:       false,
		},
		{ // '|' is the separator
			name: "UnusualChar",
			lines: []string{
				"field1|val1|test2|iops",
				"apple|banana|cherry|kjhg",
				"ant|beetle|cat,dog|nbvc",
				"123|456|789|nbv",
			},
			expectedDelimiter: '|',
			expectError:       false,
		},
		{ // ':' is the separator
			name: "doubleDot",
			lines: []string{
				"id:first_name:last_name:birth:jsaispas:colonneautre:onsenfout:peuimporte:pasmasquer",
				"test:test2:test3:test4:test5:test6:test7:8:9",
				"test:test2:test3:test4:test5:test6:test7:8:9",
				"test:test2:test3:test4:test5:test6:test7:8:9",
				"test:test2:test3:test4:test5:test6:test7:8:9",
				"test:test2:test3:test4:test5:test6:test7:8:9",
				"test:test2:test3:test4:test5:test6:test7:8:9",
			},
			expectedDelimiter: ':',
			expectError:       false,
		},
		{ // No suitable separator
			name: "NoSuitable",
			lines: []string{
				",,,,,,,,,,,a,,,,,,a,,,,",
				",a,a,",
				",,a,,,a,",
			},
			expectedDelimiter: 0,
			expectError:       true,
		},
		{
			name: "SemicolonSeparatedValues",
			lines: []string{
				"field1;field2;field3;field4;field5;field6;field7;field8;field9;field10;field11;field12;field13;field14;field15",
				"red;orange;yellow;green;blue;indigo;violet;pink;brown;grey;black;white;silver;gold;bronze",
				"car;truck;bus;train;plane;boat;bicycle;motorcycle;helicopter;submarine;scooter;skateboard;rollerblades;hovercraft;spaceship",
			},
			expectedDelimiter: ';',
			expectError:       false,
		},
		{ // Test with less entries
			name: "OnlyOneLine",
			lines: []string{
				"field1;field2;field3;field4;field5;field6;field7;field8;field9;field10;field11;field12;field13;field14;field15",
				"aa;bb;cc;dd;ee;ff;hh;gg;ii;1;2;3;99;88;99",
			},
			expectedDelimiter: ';',
			expectError:       false,
		},
		{ // Test with less entries but can't decide (candidates are ',', ';'). They have the same consistency
			name: "OnlyOneLineCantDecide",
			lines: []string{
				"field1;field2;field3;field4;field5;field6;,field7;field8;field9;field10;field11;field12;field13;field14;field15",
				"aa;bb;cc;dd;ee;ff;hh;gg;ii;11;22;33;44;,55;66",
			},
			expectedDelimiter: 0,
			expectError:       true,
		},
		{
			name: "TabSeparatedValues",
			lines: []string{
				"field1\tfield2\tfield3\tfield4\tfield5\tfield6\tfield7\tfield8\tfield9\tfield10\tfield11\tfield12\tfield13\tfield14\tfield15",
				"Monday\tTuesday\tWednesday\tThursday\tFriday\tSaturday\tSunday\tDay\tNight\tDawn\tDusk\tEvening\tNoon\tMidnight\tAfternoon",
				"January\tFebruary\tMarch\tApril\tMay\tJune\tJuly\tAugust\tSeptember\tOctober\tNovember\tDecember\tWinter\tSpring\tSummer",
			},
			expectedDelimiter: '\t',
			expectError:       false,
		},
		{ // Hard to tell what should be the delimiter, we return an error to avoid a mistake.
			name: "NoDelimiterFound",
			lines: []string{
				"field1field2field3field4field5field6;;field7field8field9field10field11field12field13field14field15",
				"abcdefgijklmnopqrst,uvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
				"123456789012345678901234567890123456,789012345678901234567890",
			},
			expectedDelimiter: 0,
			expectError:       true,
		},
		{
			name:              "EmptyData",
			lines:             []string{},
			expectedDelimiter: 0,
			expectError:       true,
		},
		{ // Latin1 test with ',' as separator
			name: "Latin1EncodedContent",
			lines: []string{
				"fïeld1,fïeld2,fïeld3",
				"äpple,bänana,cherry",
				"ñoño,lämä,koålå",
			},
			expectedDelimiter: ',',
			expectError:       false,
		},
		{ // UTF8 test with ',' as separator
			name: "UTF8EncodedContent",
			lines: []string{
				"字段1,字段2,字段3",
				"苹果,香蕉,樱桃",
				"أبجد,هوز,حطي",
			},
			expectedDelimiter: ',',
			expectError:       false,
		},
		{ // case with a non enclosed double quote (non standard CSV)
			name: "comma separated values with non enclosed double quote",
			lines: []string{
				"\\\"field1\\\",fie\"ld2,field3,\\\"\\\",field5,field6,field7,field8,field9,field10,field11,field12,field13,field14,field15",
				"apple,banana,cherry,date,elderberry,fig,grape,honeydew,kiwi,lemon,mango,nectarine,orange,papaya,quince",
				"ant,beetle,cat,dog,elephant,fox,giraffe,hippo,iguana,jaguar,kangaroo,lemur,monkey,newt,octopus",
			},
			expectedDelimiter: ',',
			expectError:       false,
		},
	}

	for _, test := range tests {
		fmt.Println("test:", test.name)
		t.Run(test.name, func(t *testing.T) {
			delim, _, err := previewpackage.GuessDelimiter(test.lines)
			if (err != nil) != test.expectError {
				t.Errorf("Test %s failed: unexpected error status: %v", test.name, err)
			}
			if delim != test.expectedDelimiter {
				t.Errorf("Test %s failed: expected %q got %q", test.name, string(test.expectedDelimiter), string(delim))
			}
		})
	}
}
