package catalog

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"

	"rdd-pipeline/pkg/schema/v1alpha1"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blockblob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
)

type Catalog struct {
	Reader  *io.PipeReader
	wr      *io.PipeWriter
	catalog *csv.Writer
}

func NewCatalog() *Catalog {
	c := new(Catalog)

	re, wr := io.Pipe()
	catalog := csv.NewWriter(wr)

	c.Reader = re
	c.wr = wr
	c.catalog = catalog

	return c
}

func (c *Catalog) WriteAll(lines [][]string) error {
	defer c.wr.Close()
	return c.catalog.WriteAll(lines)
}

func (c *Catalog) CreateStreamCatalog(ctx context.Context, containerClient *container.Client) error {
	log := hdhlog.FromContext(ctx).With("container", containerClient.URL())

	defer log.Info("Stop listing container")
	defer c.wr.Close()
	defer c.catalog.Flush()

	log.Info("Start listing container")

	line := []string{
		"BlobName",
		"BlobType",
		"PackageVersion",
		"SizeInBytes",
		"CreationDate",
	}

	if err := c.catalog.Write(line); err != nil {
		return fmt.Errorf("write listing header: %w", err)
	}

	pager := containerClient.NewListBlobsFlatPager(&container.ListBlobsFlatOptions{
		Include: container.ListBlobsInclude{
			Metadata: true,
		},
	})

	for pager.More() {
		resp, err := pager.NextPage(ctx)
		if err != nil {
			return fmt.Errorf("list segment: %w", err)
		}

		log.Info("Found blobs", "count", len(resp.Segment.BlobItems))

		for _, blob := range resp.Segment.BlobItems {

			var name, blobtype, version string
			if blob.Name != nil {
				name = *blob.Name
			}
			if blob.Metadata != nil && blob.Metadata[v1alpha1.MetadataBlobType] != nil {
				blobtype = *blob.Metadata[v1alpha1.MetadataBlobType]
			}
			if blob.Name != nil && blob.Metadata[v1alpha1.MetadataVersion] != nil {
				version = *blob.Metadata[v1alpha1.MetadataVersion]
			}

			line := []string{
				name,
				blobtype,
				version,
				fmt.Sprintf("%v", *blob.Properties.ContentLength),
				blob.Properties.CreationTime.String(),
			}

			log.Info("Add line to listing", "line", line)

			if err := c.catalog.Write(line); err != nil {
				return fmt.Errorf("write listing line: %w", err)
			}
		}
	}

	return nil
}

func (c *Catalog) UploadCatalog(ctx context.Context, name string, containerClient *container.Client) error {
	log := hdhlog.FromContext(ctx).With("container", containerClient.URL())
	log = log.With("catalogName", name) //nolint
	blobClient := containerClient.NewBlockBlobClient(name)
	_, err := blobClient.UploadStream(ctx, c.Reader, &blockblob.UploadStreamOptions{})
	if err != nil {
		return fmt.Errorf("upload listing data to '%s': %w", name, err)
	}
	return nil
}
