package imageMask

import (
	"bytes"
	"crypto/sha1"
	"encoding/binary"
	"errors"
	"io"
	"log"
	"net/http"

	hdhlog "rdd-pipeline/pkg/log"

	jpegstructure "github.com/dsoprea/go-jpeg-image-structure"
)

var jmp *jpegstructure.JpegMediaParser

var markerLen = map[byte]int{
	0x00: 0,
	0x01: 0,
	0xd0: 0,
	0xd1: 0,
	0xd2: 0,
	0xd3: 0,
	0xd4: 0,
	0xd5: 0,
	0xd6: 0,
	0xd7: 0,
	0xd8: 0,
	0xd9: 0,
	0xda: 0,

	// J2C
	0x30: 0,
	0x31: 0,
	0x32: 0,
	0x33: 0,
	0x34: 0,
	0x35: 0,
	0x36: 0,
	0x37: 0,
	0x38: 0,
	0x39: 0,
	0x3a: 0,
	0x3b: 0,
	0x3c: 0,
	0x3d: 0,
	0x3e: 0,
	0x3f: 0,
	0x4f: 0,
	0x92: 0,
	0x93: 0,

	// J2C extensions
	0x74: 4,
	0x75: 4,
	0x77: 4,
}

func init() {
	jmp = jpegstructure.NewJpegMediaParser()
}

type List struct {
	segments []*jpegstructure.Segment
}

func RemoveMetadatas(log hdhlog.Logger, stream io.Reader) (*bytes.Buffer, error) {
	log.Info("Start RemoveMetada JPG")

	b, err := io.ReadAll(stream)
	if err != nil {
		log.Error(err, "Error: Read Image readerAll bytes")
		return nil, err
	}
	if http.DetectContentType(b) != "image/jpeg" {
		return nil, errors.New("Error: Format image type not supported!")
	}
	jmp := jpegstructure.NewJpegMediaParser()

	ec, err := jmp.ParseBytes(b)
	if err != nil {
		log.Error(err, "Error: Parse with jmp!")
		return nil, err
	}

	sl := ec.(*jpegstructure.SegmentList)

	list := List{sl.Segments()}
	for i, s := range list.segments {
		if s.IsExif() {
			log.Info("IsExif: ", i)
			drop(&list, i)
		} else if s.IsXmp() {
			log.Info("IsXmp: ", i)
			drop(&list, i)
		} else if s.IsIptc() {
			log.Info("IsIptc: ", i)
			drop(&list, i)
		} else {
			log.Info("Segment: ", s.String())
		}
	}

	var buf bytes.Buffer
	err = list.write(&buf)
	if err != nil {
		log.Error(err, "Error: Write buffer bytes!")
		return nil, err
	}
	return &buf, nil
}

func drop(sl *List, i int) {
	sl.segments = append(sl.segments[:i], sl.segments[i+1:]...)
}

// Write writes the segment data to the given `io.Writer`.
func (sl *List) write(w io.Writer) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
		}
	}()

	offset := 0

	for i, s := range sl.segments {
		h := sha1.New()
		if _, err := h.Write(s.Data); err != nil {
			log.Panicln(err)
		}

		// The scan-data will have a marker-ID of (0) because it doesn't have a
		// marker-ID or length.
		if s.MarkerId != 0 {
			if _, err := w.Write([]byte{0xff}); err != nil {
				log.Panicln(err)
			}

			offset++

			if _, err = w.Write([]byte{s.MarkerId}); err != nil {
				log.Panicln(err)
			}

			offset++

			sizeLen, found := markerLen[s.MarkerId]
			if found == false || sizeLen == 2 {
				sizeLen = 2
				l := uint16(len(s.Data) + sizeLen)

				if err = binary.Write(w, binary.BigEndian, &l); err != nil {
					log.Panicln(err)
				}

				offset += 2
			} else if sizeLen == 4 {
				l := uint32(len(s.Data) + sizeLen)

				if err = binary.Write(w, binary.BigEndian, &l); err != nil {
					log.Panicln(err)
				}

				offset += 4
			} else if sizeLen != 0 {
				log.Panicf("not a supported marker-size: SEGMENT-INDEX=(%d) MARKER-ID=(0x%02x) MARKER-SIZE-LEN=(%d)", i, s.MarkerId, sizeLen)
			}
		}

		if _, err := w.Write(s.Data); err != nil {
			log.Panicln(err)
		}

		offset += len(s.Data)
	}

	return nil
}
