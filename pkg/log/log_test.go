package log_test

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"testing"

	hdhlog "rdd-pipeline/pkg/log"
)

func TestLegacyVerbosityConversion(t *testing.T) {
	tests := []struct {
		name            string
		legacyVerbosity int
		expected        []string
	}{
		{
			name:            "legacy error",
			legacyVerbosity: 1,
			expected:        []string{"this is an error"},
		},
		{
			name:            "legacy warn",
			legacyVerbosity: 2,
			expected:        []string{"warn msg", "this is an error"},
		},
		{
			name:            "legacy info",
			legacyVerbosity: 3,
			expected:        []string{"info msg", "warn msg", "this is an error"},
		},
		{
			name:            "legacy debug/trace",
			legacyVerbosity: 4,
			expected:        []string{"debug msg", "info msg", "warn msg", "this is an error"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buf := &bytes.Buffer{}
			l := hdhlog.New(buf, tt.legacyVerbosity)
			l.Debug("debug msg")
			l.Info("info msg")
			l.Warn("warn msg")
			l.Error(fmt.Errorf("this is an error"), "err msg")
			got := buf.String()
			for _, expected := range tt.expected {
				if !strings.Contains(got, expected) {
					t.Errorf("got = %v, want %v", got, tt.expected)
				}
			}
		})
	}
}

func TestLegacyVerbosityAttribute(t *testing.T) {
	tests := []struct {
		name            string
		legacyVerbosity int
		expected        []string
	}{
		{
			name:            "legacy warn",
			legacyVerbosity: 2,
			expected:        []string{"\"v\":0"},
		},
		{
			name:            "legacy info",
			legacyVerbosity: 3,
			expected:        []string{"\"v\":0", "\"v\":1"},
		},
		{
			name:            "legacy debug/trace",
			legacyVerbosity: 4,
			expected:        []string{"\"v\":0", "\"v\":1", "\"v\":2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			buf := &bytes.Buffer{}
			l := hdhlog.New(buf, tt.legacyVerbosity)
			l.Debug("debug msg")
			l.Info("info msg")
			l.Warn("warn msg")
			l.Error(fmt.Errorf("this is an error"), "err msg")
			got := buf.String()
			for _, expected := range tt.expected {
				if !strings.Contains(got, expected) {
					t.Errorf("got = %v, want %v", got, tt.expected)
				}
			}
		})
	}
}

func TestFromContext(t *testing.T) {
	logger, ctx := hdhlog.New(&bytes.Buffer{}, 0).AttachToContext(context.Background())
	loggerFromCtx := hdhlog.FromContext(ctx)
	if logger != loggerFromCtx {
		t.Errorf("logger != loggerFromCtx")
	}
}

func TestFromContextPanic(t *testing.T) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("FromContext should panic")
		}
	}()
	hdhlog.FromContext(context.Background())
}

func TestWith(t *testing.T) {
	buf := &bytes.Buffer{}
	l := hdhlog.New(buf, 0)
	l.With("foo", "bar").Info("msg")
	if !strings.Contains(buf.String(), "\"foo\":\"bar\"") {
		t.Errorf("expected to find foo:bar in %v", buf.String())
	}
}
