package log

import (
	"context"
	"io"

	"runtime"

	"golang.org/x/exp/slog"
)

type contextKey struct{}
type Logger struct {
	*slog.Logger
}

// New builds a logger and attaches it to a context
func New(w io.Writer, legacyVerbosity int) Logger {
	return Logger{
		Logger: slog.New(newHandler(w, legacyVerbosity)),
	}
}

// Discard returns a logger that discards all logs
func Discard() Logger {
	return Logger{
		Logger: slog.New(slog.NewJSONHandler(io.Discard, nil)),
	}
}

// FromContext returns the Logger from the ctx if it exists or throws a panic.
func FromContext(ctx context.Context) Logger {
	if v, ok := ctx.Value(contextKey{}).(Logger); ok {
		return v
	}
	panic("logger have not been initialized")
}

func (l Logger) Debug(msg string, args ...any) {
	l.Logger.Debug(msg, args...)
}

func (l Logger) Info(msg string, args ...any) {
	l.Logger.Info(msg, args...)
}

func (l Logger) Warn(msg string, args ...any) {
	l.Logger.Warn(msg, args...)
}

func (l Logger) Error(err error, msg string, args ...any) {
	l.Logger.Error(msg, append(args, "error", err)...)
}

// AttachToContext attaches the logger the context
// It also returns the logger to make the api fluent
func (l Logger) AttachToContext(ctx context.Context) (Logger, context.Context) {
	return l, context.WithValue(ctx, contextKey{}, l)
}

// With attaches a list of attributes to the logger
func (l Logger) With(args ...any) Logger {
	return Logger{
		Logger: l.Logger.With(args...),
	}
}

// The custom handler aims at keeping the existing legacy "v" attribute
// once the new level system is in place and accepted, we can drop this handler

type handler struct {
	h slog.Handler
}

// Enabled is a wrapper to the default slog handler
func (h *handler) Enabled(ctx context.Context, level slog.Level) bool {
	return h.h.Enabled(ctx, level)
}

// WithAttrs is a wrapper to the default slog handler
func (h *handler) WithAttrs(attrs []slog.Attr) slog.Handler {
	return &handler{h: h.h.WithAttrs(attrs)}
}

// WithGroup is a wrapper to the default slog handler
func (h *handler) WithGroup(name string) slog.Handler {
	return &handler{h: h.h.WithGroup(name)}
}

// this custom handler aims at keeping the existing legacy "v" attribute
// that was set on every log : v=0 for info, v=1 for debug, v=2 for trace.
// error levels was not concerned by the "v" attribute
// Also this handler face a slog package limitation: the unability to set
// the callstack to skip when logging.
// See https://github.com/golang/go/issues/59145 for more details.
func (h *handler) Handle(ctx context.Context, r slog.Record) error {
	r = r.Clone()
	var legacyVerbosity int // zero legacy verbosity (info)
	if r.Level == slog.LevelDebug {
		legacyVerbosity = 2 // legacy trace/debug
	} else if r.Level == slog.LevelInfo {
		legacyVerbosity = 1 // legacy info
	} else {
		legacyVerbosity = 0 // legacy warn
	}
	if r.Level != slog.LevelError {
		// error level is not concerned by the "v" attribute
		levelAttr := slog.Attr{
			Key:   "v",
			Value: slog.IntValue(legacyVerbosity),
		}
		r.AddAttrs(levelAttr)
	}
	var pcs [1]uintptr
	runtime.Callers(5, pcs[:]) // skip [Callers, Infof]
	r.PC = pcs[0]
	h.h.Handle(ctx, r)
	return nil
}

// NewHandler returns a new handler mapping legacy log levels to the new slog ones
// --log-level = 4 => DEBUG
// --log-level = 3 => INFO
// --log-level = 2 => WARN
func newHandler(w io.Writer, legacyVerbosity int) *handler {
	minLevel := slog.LevelInfo
	if legacyVerbosity == 4 {
		minLevel = slog.LevelDebug
	} else if legacyVerbosity == 3 {
		minLevel = slog.LevelInfo
	} else if legacyVerbosity == 2 {
		minLevel = slog.LevelWarn
	}
	return &handler{
		h: slog.NewJSONHandler(w, &slog.HandlerOptions{
			Level:     minLevel,
			AddSource: true,
		}),
	}
}
