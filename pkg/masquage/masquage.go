package masquage

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
)

type CorrespondenceTable map[string]string

func HMAC(ID string, key string) (string, error) {
	hKey, err := hex.DecodeString(key)
	if err != nil {
		return "", err
	}
	hash := hmac.New(sha256.New, hKey)
	hash.Reset()
	if _, err := hash.Write([]byte(ID)); err != nil {
		return "", fmt.Errorf("hash write: %w", err)
	}
	hdhID := hex.EncodeToString(hash.Sum(nil))
	return hdhID, nil
}

func EncryptAES(plaintextStr string, keyStr string) (string, error) {
	plaintext := []byte(plaintextStr)
	key, err := hex.DecodeString(keyStr)
	if err != nil {
		return "", err
	}
	// this IV is inherited from HDH legacy
	// if we change it, all ciphers will change
	iv := make([]byte, 8)
	for i := 0; i < len(iv); i++ {
		iv[i] = '0'
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	aesgcm, err := cipher.NewGCMWithNonceSize(block, len(iv))
	if err != nil {
		return "", err
	}
	// Create a new slice to hold the ciphertext
	ciphertext := aesgcm.Seal(nil, iv, plaintext, nil)
	// tag := hex.EncodeToString(aesgcm.Seal(nil, iv, ciphertext, nil))
	return hex.EncodeToString(ciphertext), nil
}

func DecryptAES(cipherStr string, keyStr string) (string, error) {
	key, err := hex.DecodeString(keyStr)
	if err != nil {
		return "", err
	}
	c, err := hex.DecodeString(cipherStr)
	if err != nil {
		return "", err
	}
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}
	// this IV is inherited from HDH legacy
	// if we change it, all ciphers will change
	iv := make([]byte, 8)
	for i := 0; i < len(iv); i++ {
		iv[i] = '0'
	}
	aesgcm, err := cipher.NewGCMWithNonceSize(block, len(iv))
	if err != nil {
		return "", err
	}
	plaintext, err := aesgcm.Open(nil, iv, c, nil)
	if err != nil {
		return "", err
	}
	return string(plaintext), nil
}
