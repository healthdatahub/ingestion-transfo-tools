package masquage_test

import (
	"testing"

	"rdd-pipeline/pkg/masquage"
)

const key = "a0ad86477f3aca0cdfc8913b20e733e196b46b03bc3f2dc831539f21022b0e8a91900057541e79373d876523c3dd6cbcff163c1ed8b1e3e6785bc4f402d18ac915f0a1a18eb0cb76e53303d36bab59e922425b6b97a98ba1d595c74d88ee895c6ec9f93d87e3dbc156ef4653ac2ee8219a4e468d5c2b4f00165c2bd3e4a31b61fcf682739a85c83a50455d6a5d9298073b3227f65d27ccb0cf4970fa8c9669c5e5de6b3f80e533424dd3fd813236704adf84728e3b6b646046457ce60fb726c450b36491064142046c0677b937704c92f20fbb35b6dddf28805ff7213c61afd1817089945158341ef5bb38b7d482e6a2f328613b720421aedf911d1b3cd20a61"
const aesKey = "4145533235364B65792D33324368617261637465727331323334353637383930"

func TestHash(t *testing.T) {
	type args struct {
		ID  string
		key string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "non_hex_key",
			args: args{
				ID:  "80",
				key: key + "m",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "id_90",
			args: args{
				ID:  "90",
				key: key,
			},
			want:    "cf2cbdc6e5723f125d4035ecff424988f962fb5ef098512510784bef27a2f213",
			wantErr: false,
		},
		{
			name: "id_91",
			args: args{
				ID:  "91",
				key: key,
			},
			want:    "b2e176c8419d0c61530bb60a10d7a33455b0195a2036574912579ecedcf71b4f",
			wantErr: false,
		},
		// echo -n "100" | openssl dgst -sha256 -mac HMAC -macopt "hexkey:a0ad86477f3aca0cdfc8913b20e733e196b46b03bc3f2dc831539f21022b0e8a91900057541e79373d876523c3dd6cbcff163c1ed8b1e3e6785bc4f402d18ac915f0a1a18eb0cb76e53303d36bab59e922425b6b97a98ba1d595c74d88ee895c6ec9f93d87e3dbc156ef4653ac2ee8219a4e468d5c2b4f00165c2bd3e4a31b61fcf682739a85c83a50455d6a5d9298073b3227f65d27ccb0cf4970fa8c9669c5e5de6b3f80e533424dd3fd813236704adf84728e3b6b646046457ce60fb726c450b36491064142046c0677b937704c92f20fbb35b6dddf28805ff7213c61afd1817089945158341ef5bb38b7d482e6a2f328613b720421aedf911d1b3cd20a61"
		{
			name: "id_100",
			args: args{
				ID:  "100",
				key: key,
			},
			want:    "299b0ecda51b1d352066011cc46e35b41d23a3e21ececf4c13b2933575e6d589",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here
			got, err := masquage.HMAC(tt.args.ID, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("HashOpenssl() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("HashOpenssl() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHashOpenssl(t *testing.T) {
	type args struct {
		ID  string
		key string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "non_hex_key",
			args: args{
				ID:  "80",
				key: key + "m",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "id_90",
			args: args{
				ID:  "90",
				key: key,
			},
			want:    "cf2cbdc6e5723f125d4035ecff424988f962fb5ef098512510784bef27a2f213",
			wantErr: false,
		},
		{
			name: "id_91",
			args: args{
				ID:  "91",
				key: key,
			},
			want:    "b2e176c8419d0c61530bb60a10d7a33455b0195a2036574912579ecedcf71b4f",
			wantErr: false,
		},
		// echo -n "100" | openssl dgst -sha256 -mac HMAC -macopt "hexkey:a0ad86477f3aca0cdfc8913b20e733e196b46b03bc3f2dc831539f21022b0e8a91900057541e79373d876523c3dd6cbcff163c1ed8b1e3e6785bc4f402d18ac915f0a1a18eb0cb76e53303d36bab59e922425b6b97a98ba1d595c74d88ee895c6ec9f93d87e3dbc156ef4653ac2ee8219a4e468d5c2b4f00165c2bd3e4a31b61fcf682739a85c83a50455d6a5d9298073b3227f65d27ccb0cf4970fa8c9669c5e5de6b3f80e533424dd3fd813236704adf84728e3b6b646046457ce60fb726c450b36491064142046c0677b937704c92f20fbb35b6dddf28805ff7213c61afd1817089945158341ef5bb38b7d482e6a2f328613b720421aedf911d1b3cd20a61"
		{
			name: "id_100",
			args: args{
				ID:  "100",
				key: key,
			},
			want:    "299b0ecda51b1d352066011cc46e35b41d23a3e21ececf4c13b2933575e6d589",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here
			got, err := masquage.HMAC(tt.args.ID, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Hash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestEncryptAES(t *testing.T) {
	type args struct {
		ID  string
		key string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "aes_non_hex_key",
			args: args{
				ID:  "80",
				key: aesKey + "m",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "aes_id_90",
			args: args{
				ID:  "90",
				key: aesKey,
			},
			want:    "45bbf97f7b910bc12e70359505c0aa31d927",
			wantErr: false,
		},
		{
			name: "aes_id_91",
			args: args{
				ID:  "91",
				key: aesKey,
			},
			want:    "45ba58aece79d3119d7801e3e921d9b8b432",
			wantErr: false,
		},
		{
			name: "aes_id_100",
			args: args{
				ID:  "100",
				key: aesKey,
			},
			want:    "4dbb89e84df648481ec75715640ae1d50adf48",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here
			got, err := masquage.EncryptAES(tt.args.ID, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Hash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDecryptAES(t *testing.T) {
	type args struct {
		ID  string
		key string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "aes_non_hex_key",
			args: args{
				ID:  "80",
				key: aesKey + "m",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "aes_id_90",
			args: args{
				ID:  "45bbf97f7b910bc12e70359505c0aa31d927",
				key: aesKey,
			},
			want:    "90",
			wantErr: false,
		},
		{
			name: "aes_id_91",
			args: args{
				ID:  "45ba58aece79d3119d7801e3e921d9b8b432",
				key: aesKey,
			},
			want:    "91",
			wantErr: false,
		},
		{
			name: "aes_id_100",
			args: args{
				ID:  "4dbb89e84df648481ec75715640ae1d50adf48",
				key: aesKey,
			},
			want:    "100",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here
			got, err := masquage.DecryptAES(tt.args.ID, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Hash() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMaskPath(t *testing.T) {
	type args struct {
		ID  string
		key string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "aes_non_hex_key",
			args: args{
				ID:  "80",
				key: aesKey + "m",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "aes_id_90",
			args: args{
				ID:  "90",
				key: aesKey,
			},
			want:    "45bbf97f7b910bc12e70359505c0aa31d927",
			wantErr: false,
		},
		{
			name: "aes_id_91",
			args: args{
				ID:  "91",
				key: aesKey,
			},
			want:    "45ba58aece79d3119d7801e3e921d9b8b432",
			wantErr: false,
		},
		{
			name: "aes_id_100",
			args: args{
				ID:  "100",
				key: aesKey,
			},
			want:    "4dbb89e84df648481ec75715640ae1d50adf48",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt := tt //bug scopelint https://github.com/kyoh86/scopelint and syntax scopelint:ignore is not supported here
			got, err := masquage.EncryptAES(tt.args.ID, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("Hash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Hash() = %v, want %v", got, tt.want)
			}
		})
	}
}
