#!/usr/bin/make
.DEFAULT_GOAL := help
# SHELL=sh -x

IMAGES := rdd-control rdd-import rdd-pipeline
IMAGE_TAG := $(shell git rev-parse --abbrev-ref HEAD | sed 's/\#//g' | sed 's/[^a-zA-Z0-9_.-]/-/g')
ifndef GLOBAL_ACR_URL
$(error GLOBAL_ACR_URL is not set)
endif

ifndef DEV_ACR_URL
$(error DEV_ACR_URL is not set)
endif

GO_TEST_FLAGS ?= -race -failfast
GO_VERSION :=1.22

DOCKER_BUILD_ARGS=--build-arg GO_VERSION=$(GO_VERSION)

# set pkgs to all packages
PKGS = ./...

help: ## Show help
	@grep -E -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-30s\033[0m %s\n", $$1, $$2}'

test: ## Run unit tests of all packages
	CGO_ENABLED=1 go test $(GO_TEST_FLAGS) $(PKGS)

install:
	go install -ldflags "all=$(GO_LDFLAGS)" $(PKGS)

.PHONY: docker
docker: ## Build docker images locally
	@for image in $(IMAGES); do \
		image_slashed=$$(echo $$image | sed 's/-/\//g'); \
		DOCKER_BUILDKIT=1 docker build $(DOCKER_BUILD_ARGS) -t $$image_slashed -f docker/Dockerfile.$$image . && \
		docker tag $$image_slashed $(GLOBAL_ACR_URL)/$$image_slashed:$(IMAGE_TAG) && \
		docker tag $$image_slashed $(DEV_ACR_URL)/$$image_slashed:$(IMAGE_TAG) ; \
	done

.PHONY: push-to-central
push-to-central: docker ## Build and push images to central ACR
	az acr login -n $(GLOBAL_ACR_URL)
	@for image in $(IMAGES); do \
		docker push $(GLOBAL_ACR_URL)/$$(echo $$image | sed 's/-/\//g'):$(IMAGE_TAG) ; \
	done

.PHONY: push-to-dev
push-to-dev: ## Build and push images to dev ACR (used by ACIs)
	az acr login -n $(DEV_ACR_URL)
	@for image in $(IMAGES); do \
		docker push $(DEV_ACR_URL)/$$(echo $$image | sed 's/-/\//g'):$(IMAGE_TAG) ; \
	done



