# Guidelines

Here are links to general guidelines for Go:
- general : [Effective Go](https://golang.org/doc/effective_go.html)
- collection of common mistakes : [Go Code Review Comments](https://github.com/golang/go/wiki/CodeReviewComments)
- Uber has a really good [code guideline for go](https://github.com/uber-go/guide/blob/master/style.md)

## Logging

The ERROR level (or higher) should only be used for error that requires the intervention of
the infra team. Thus, expected application error should use the INFO level

The WARN level can be used to spot the attention of the infra team in a less urgent way.

## Continuous improvements

There are places for improvement in the code base so let's do it gradually applying the boyscout rule ("leave the campground cleaner than you found it").

And here are some identified points where we especially want to improve the code base:

### Golang naming convention

Let's move toward golang standard naming convention.

This can be achieved installing linter suggestion directly in your editor and following the suggestions.

### Table driven test

Let's use more systematically table driven tests.

Here is a example :

```golang

err := prepareData()
require.NoError(t, err)

tests := []struct {
        // name is a description to improve readability and to display in case of error
        name            string
        // add here the structs or variables which are the inputs and expected outputs :
        inputs          *input
        outputs         *output
        // add here the expected error :
        expectedErrCode string
	}{
        // add here all the cases
        {
            name:            "empty input",
            in :              &input{},
            out:              &output{},
            expectedErrCode:  pkg.ErrBadParamter
        },
        ...
    }

for _, tt := range tests {
    t.Run(tt.name, func(t *testing.T) {
            // define here assert and require variable for the subtest
            //(so that the name of the case will be displayed in case of failure)
            assert := assert.New(t)
            require := require.New(t)

            err, output := funcToTest(input)

            test.RequireErrorCode(t, tt.expectedErrCode, err)
            assert.Equal(tt.output, output)
            // add here others checks if needed
            // use require if you want the subtest to stop if it fails
    })
}
```

### Go doc

Let's improve the go doc, it could be nice to generate and "publish" it at some point.

Each exported function should have a line of comment.
It should be a sentence starting by the function name and ending with a point.

Let's document the packages too.

### Centralized exposed types

All exposed types can be moved to `pkg/types`.

Exposed means that it's used in the main API of a component, for example :
- structures used in the arguments and replies of the binaries.
- structures serialized in JSON and used as input/output of core apps.
- ...

### The law of Demeter

The fundamental notion is that a given object should **assume as little as possible about the structure or properties of anything else**, in accordance with the principle of "information hiding".

It may be viewed as a corollary to the **principle of least privilege**, which dictates that a module possesses only the information and resources necessary for its legitimate purpose.

(shamely stolen from wikipedia)








