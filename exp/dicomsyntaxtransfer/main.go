package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/suyashkumar/dicom"
	"github.com/suyashkumar/dicom/pkg/tag"
	"github.com/suyashkumar/dicom/pkg/uid"
)

// This file creates a minimalist dicom file with only a few tags, saves it, and
// extracts it again to analyze the deserialize step behavior.
//
// This made it possible to highlight a relationship between the transfer syntax type
// and the way files are read from disk.

var (
	hdhPrivateTagImplementor = tag.Tag{Group: 0x7777, Element: 0x0010}
	hdhPrivateTagCipherMap   = tag.Tag{Group: 0x7777, Element: 0x0020}

	transferSyntax = uid.ExplicitVRLittleEndian
)

const (
	HDHOrgRoot     = "1.2.826.0.1.3680043.10.1304" // official HDH org root, do not change.
	hdhImplementor = "Health Data Hub"
)

func main() {

	f1, err := os.CreateTemp("", "")
	if err != nil {
		panic(err)
	}
	// defer os.Remove(f1.Name())

	transferSyntax, err := dicom.NewValue([]string{transferSyntax})
	if err != nil {
		panic(err)
	}
	_ = transferSyntax
	dataset := dicom.Dataset{
		Elements: []*dicom.Element{
			// {
			// 	Tag:   tag.TransferSyntaxUID,
			// 	Value: transferSyntax,
			// },
		},
	}

	newCiphers := []string{"Jean Dujardin"}
	v, err := dicom.NewValue(newCiphers)
	if err != nil {
		panic(err)
	}

	cipherMap := &dicom.Element{
		Tag:                    tag.Tag{Group: 0x0010, Element: 0x0020},
		ValueRepresentation:    tag.VRStringList,
		RawValueRepresentation: "LT",
		Value:                  v,
	}
	dataset.Elements = append(dataset.Elements, cipherMap)

	v, err = dicom.NewValue([]string{"Clinique Jean Vilars"})
	if err != nil {
		panic(err)
	}

	test := &dicom.Element{
		Tag:                    tag.Tag{Group: 0x0010, Element: 0x0021},
		ValueRepresentation:    tag.VRStringList,
		RawValueRepresentation: "LT",
		Value:                  v,
	}
	dataset.Elements = append(dataset.Elements, test)

	if err := dicom.Write(f1, dataset,
		dicom.SkipValueTypeVerification(),
		dicom.SkipVRVerification(),
		dicom.DefaultMissingTransferSyntax(),
	); err != nil {
		panic(err)
	}
	f1.Close()
	fmt.Println("file created at", f1.Name())

	f1, err = os.Open(f1.Name())
	if err != nil {
		panic(err)
	}

	dataset2, err := dicom.ParseUntilEOF(f1, nil,
		dicom.AllowMismatchPixelDataLength(),
		dicom.SkipProcessingPixelDataValue())
	if err != nil {
		panic(err)
	}
	// create the map tag if does not exist
	cipherMap2, err := dataset2.FindElementByTag(hdhPrivateTagCipherMap)
	if err != nil {
		panic(err)
	}

	existingCiphers2 := cipherMap2.Value.GetValue().([]uint8)

	fmt.Println("FOUND:", existingCiphers2)
	fmt.Println("CONVERTED TO STRING AND SPLIT:", strings.Split(string(existingCiphers2), "\\"))

}
