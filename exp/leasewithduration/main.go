package main

import (
	"context"
	"fmt"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azidentity"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/lease"
)

func main() {
	// Assuming you have the containerClient already set up
	creds, err := azidentity.NewDefaultAzureCredential(nil)
	if err != nil {
		panic(err)
	}

	containerClient, err := container.NewClient("https://sasgitdev.blob.core.windows.net/retention", creds, nil)
	if err != nil {
		panic(err)
	}

	pager := containerClient.NewListBlobsFlatPager(&container.ListBlobsFlatOptions{
		Include: container.ListBlobsInclude{
			Metadata: true,
		},
	})
	for pager.More() {

		var resp azblob.ListBlobsFlatResponse
		var err error
		resp, err = pager.NextPage(context.Background())
		if err != nil {
			panic(err)
		}
		for _, blob := range resp.Segment.BlobItems {
			fmt.Println(*blob.Name)
		}

	}

	blobName := "my-github-project-name"
	ctx := context.Background()

	// Create a new blob client for the specific blob
	blobClient, err := lease.NewBlobClient(containerClient.NewBlockBlobClient(blobName), nil)
	if err != nil {
		panic(err)
	}

	// Acquire a lease on the blob with a period of 60 seconds
	leaseDuration := int32(60) // Lease duration in seconds
	leaseResp, err := blobClient.AcquireLease(ctx, leaseDuration, nil)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Lease ID: %s\n", *leaseResp.LeaseID)

	go func() {
		for {
			time.Sleep(3 * time.Second)
			fmt.Println("A hacker tries to take the lease...")
			anotherBlobClient, err := lease.NewBlobClient(containerClient.NewBlockBlobClient(blobName), nil)
			if err != nil {
				panic(err)
			}
			_, err = anotherBlobClient.AcquireLease(ctx, -1, nil)
			if err == nil {
				panic("A HACKER GOT THE LEASE!")
			}
		}
	}()

	for i := 0; i < 1000; i++ {

		// Wait for some time before the lease expires
		time.Sleep(30 * time.Second)

		// Before the lease is released, renew the lease to extend the lease period
		renewResp, err := blobClient.RenewLease(ctx, &lease.BlobRenewOptions{})
		if err != nil {
			panic(err)
		}
		fmt.Printf("Lease ID renewed: %s\n", *renewResp.LeaseID)

	}

	// Optionally, release the lease when done
	_, err = blobClient.ReleaseLease(ctx, nil)
	if err != nil {
		panic(err)
	}
	fmt.Println("Lease released successfully.")
}
