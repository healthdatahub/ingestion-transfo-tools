# Ingestion-Transfo-tools

This repository contain librairies and runtime core for the ingestion and transformation of the Health data.


## Usage of the rdd-pipeline package

Run `go run ./cmd/rdd-pipeline` for information about the command.

## Proxy configuration

The proxy server shall allow outbound communications with `database.xxxxx.net` in order to update the antivirus.

## Usage of the rdd-import package

Run `go run ./cmd/rdd-import` for information about the command.

## Log Level

Log level is customizable with `-V LEVEL`  option.

Level are :

> `-V 0` : Error
> `-V 1` : Warning (No difference with `0` for now)
> `-V 2` : Info
> `-V 3` : Debug
> `-V 4` : Trace

## Log variables

log example :

```json
{"v":0,"logger":"producer decrypt","caller":"/home/user/hdh-go/rdd-pipeline/cmd/rdd-pipeline/producer_decrypt.go:57","time":"2021-09-27T09:04:00Z","message":"Start"}
{"v":2,"accountURL":"<account>","containerName":"name","caller":"/home/user/hdh-go/rdd-pipeline/pkg/blob/container.go:99","time":"2021-09-27T09:04:00Z","message":"log message"}
```

Explanation for some variables :

- `v` : log level
- `logger` : action currently working (sftp, scan, mask, ...)
- `time` : log entry's timestamp
- `message` : info or error message

For some actions there can be other vars :

- `blobName` : blob name currently treated
- `accountUrl` : account's URL currently working in
- `container` : account's container name currently working in
- `separator`, `skipLines`, `columns`, `columnsNames` : config to mask/read CSV files

## Licence

This code is under licence Apache 2.0
The licence file is [LICENSE]

All licences from tiers Golang packages used in this repository are listed in [licences.csv]