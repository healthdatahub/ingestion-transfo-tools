package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"rdd-pipeline/pkg/schema/v1alpha1"
)

const (
	stepNameEnvironmentVariable = "HDH_STEP_NAME"
	areaNameEnvironmentVariable = "HDH_AREA_NAME"

	// common flags.
	outputStorageAccountFlag = "output-storage-account-endpoint"
	outputContainerFlag      = "output-container"
	inputStorageAccountFlag  = "input-storage-account-endpoint"
	inputContainerFlag       = "input-container"

	configAccountFlag   = "config-storage-account-endpoint"
	configContainerFlag = "config-container"
	configBlobNameFlag  = "config-blob-name"

	keyVaultFlag      = "keyvault-endpoint"
	KeySecretNameFlag = "key-secret-name"

	NoCPKForInputFlag  = "no-cpk-for-input"
	NoCPKForOutputFlag = "no-cpk-for-output"

	CTAccountFlag   = "correspondence-table-storage-account-endpoint"
	CTContainerFlag = "correspondence-table-container-name"

	// scan command specific flags.
	QuarantineAccountFlag   = "quarantine-storage-account-endpoint"
	QuarantineContainerFlag = "quarantine-container"

	// catalog command specific flags.
	CatalogStorageAccountFlag = "catalog-storage-account-endpoint"
	CatalogContainerFlag      = "catalog-container"
	CatalogBlobNameFlag       = "catalog-blob-name"

	// move command specific flags.
	FilterMetadataFlag  = "filter-metadata"
	StepNameMetadataKey = "hdhstep"
	AreaNameMetadataKey = "hdharea"

	// mask command specific flags.
	MaskPrefixFlag       = "mask-prefix"
	OutputBlobPrefixFlag = "output-blob-prefix"
	AutoGenerateKeyFlag  = "auto-generate-key"

	// control command specific flags.
	AllowedVerbsFlag              = "allowed-verbs"
	TargetStorageAccountFlag      = "target-storage-account-endpoint"
	ConfigInputQueueNameFlag      = "config-input-queue-name"
	ConfigStorageAccountQueueFlag = "config-storage-account-queue-endpoint"

	// backup command specific flags.
	backupOutputStorageAccountFlag = "backup-output-storage-account-endpoint"
	backupOutputContainerFlag      = "backup-output-container"
	backupKeyvaultFlag             = "backup-keyvault-endpoint"
	backupKeySecretNameFlag        = "backup-key-secret-name"

	// Metadata preview specific flags
	metadataOutputStorageAccountFlag = "metadata-output-storage-account-endpoint"
	metadataOutputContainerFlag      = "metadata-output-container"

	// rotate command specific flags.
	encryptionKeyVaultFlag = "encryption-keyvault-endpoint"

	// For GPG key generation, contains the identity mail
	keyIdentityFlag = "key-identity-mail"
)

func AddEncryptionKeyVaultFlags(cmd *cobra.Command) {
	cmd.Flags().String(encryptionKeyVaultFlag, "", "Keyvault encryption endpoint")
}

func AddTargetStorageAccountFlags(cmd *cobra.Command) {
	cmd.Flags().String(TargetStorageAccountFlag, "", "Target storage account endpoint")
}

func AddConfigStorageAccountQueueFlag(cmd *cobra.Command) {
	cmd.Flags().String(ConfigStorageAccountQueueFlag, "", "Config storage account queue url")
}

func AddKeyVaultFlags(cmd *cobra.Command) {
	cmd.Flags().String(keyVaultFlag, "", "Keyvault endpoint")
	cmd.Flags().String(KeySecretNameFlag, "", "Keyvault secret name")
}

func AddConfigInputQueueNameFlag(cmd *cobra.Command) {
	cmd.Flags().String(ConfigInputQueueNameFlag, "", "Config storage account input queue name")
}

func AddAllowedVerbsFlag(cmd *cobra.Command) {
	cmd.Flags().String(AllowedVerbsFlag, "", "Allowed verbs for the ACI")
}

func AddBackupFlags(cmd *cobra.Command) {
	cmd.Flags().String(backupOutputStorageAccountFlag, "", "Backup storage account endpoint")
	cmd.Flags().String(backupOutputContainerFlag, "", "Backup output container")
	cmd.Flags().String(backupKeyvaultFlag, "", "Backup keyvault endpoint")
	cmd.Flags().String(backupKeySecretNameFlag, "", "Backup keyvault secret name")
}

func AddMetadataFlags(cmd *cobra.Command) {
	cmd.Flags().String(metadataOutputStorageAccountFlag, "", "Metadata storage account endpoint")
	cmd.Flags().String(metadataOutputContainerFlag, "", "Backup output container")
}

func AddMetadataFilterFlag(cmd *cobra.Command) {
	cmd.Flags().Bool(FilterMetadataFlag, false,
		"True to filter blob to copy based on metadata, otherwise False (default to false)")
}

func AddNoCPKFlags(cmd *cobra.Command) {
	cmd.Flags().Bool(NoCPKForInputFlag, false,
		"true if no CPK is used for input blob; otherwise false (default to flase)")
	cmd.Flags().Bool(NoCPKForOutputFlag, false,
		"true if no CPK shall be used for output blob; otherwise false (default to flase)")
}

func AddInputBlobFlags(cmd *cobra.Command) {
	cmd.Flags().String(inputContainerFlag, "data", "Input container to output the processed blob")
	(cobra.MarkFlagRequired(cmd.Flags(), inputContainerFlag))

	cmd.Flags().String(inputStorageAccountFlag, "", "Input Blob storage account")
	(cobra.MarkFlagRequired(cmd.Flags(), inputStorageAccountFlag))
}

func AddOutputBlobFlags(cmd *cobra.Command) {
	cmd.Flags().String(outputContainerFlag, "data", "Output container to output the processed blob")
	(cobra.MarkFlagRequired(cmd.Flags(), outputContainerFlag))

	cmd.Flags().String(outputStorageAccountFlag, "", "Output Blob storage account")
	(cobra.MarkFlagRequired(cmd.Flags(), outputStorageAccountFlag))
}

func AddCatalogBlobFlags(cmd *cobra.Command, required bool) {
	cmd.Flags().String(CatalogContainerFlag, "catalog", "Catalog container to output the processed blob")
	cmd.Flags().String(CatalogStorageAccountFlag, "", "Catalog Blob storage account")
	cmd.Flags().String(CatalogBlobNameFlag, "", "Catalog file name")
	if required {
		(cobra.MarkFlagRequired(cmd.Flags(), CatalogContainerFlag))
		(cobra.MarkFlagRequired(cmd.Flags(), CatalogStorageAccountFlag))
		(cobra.MarkFlagRequired(cmd.Flags(), CatalogBlobNameFlag))
	}
}

func AddPrefixFlags(cmd *cobra.Command) {
	cmd.Flags().String(MaskPrefixFlag, "", "prefix string for each masked value")
	(cobra.MarkFlagRequired(cmd.Flags(), MaskPrefixFlag))

}

func AddCTFlags(cmd *cobra.Command) {
	cmd.Flags().String(CTAccountFlag, "", "correspondence table storage account endpoint")
	cmd.Flags().String(CTContainerFlag, "", "container name where correspondence tables are stored")
}

func AddOutputBlobPrefixFlags(cmd *cobra.Command) {
	cmd.Flags().String(OutputBlobPrefixFlag, "", "prefix output blob")
}

func AddAutoGenerateKeyFlags(cmd *cobra.Command) {
	cmd.Flags().String(AutoGenerateKeyFlag, "", "auto generate key format")
}

func AddConfigFlags(cmd *cobra.Command) {
	cmd.Flags().String(configAccountFlag, "", "Config container for the blob to process")
	(cobra.MarkFlagRequired(cmd.Flags(), configAccountFlag))

	cmd.Flags().String(configContainerFlag, "config", "Config container for the blob to process")

	cmd.Flags().String(configBlobNameFlag, "", "Config blob name for the blob to process")
	(cobra.MarkFlagRequired(cmd.Flags(), configBlobNameFlag))
}

func AddOptionalConfigFlags(cmd *cobra.Command) {
	cmd.Flags().String(configAccountFlag, "", "Config container for the blob to process")
	cmd.Flags().String(configContainerFlag, "config", "Config container for the blob to process")
	cmd.Flags().String(configBlobNameFlag, "", "Config blob name for the blob to process")
}

func AddKeyIdentityFlag(cmd *cobra.Command) {
	cmd.Flags().String(keyIdentityFlag, "", "GPG key identity mail")
}

func GetEncryptionKeyvaultInfo(cmd *cobra.Command) (string, error) {
	keyvaultURL, err := cmd.Flags().GetString(encryptionKeyVaultFlag)
	if err != nil {
		return "", fmt.Errorf("retrieve flag '%s': %w", encryptionKeyVaultFlag, err)
	}
	return keyvaultURL, err
}

func GetMetadataKeys() (string, string, error) {
	return v1alpha1.MetadataKeyvaultKey, v1alpha1.MetadataWrappedKey, nil
}

func GetPipelineConfigInfo(cmd *cobra.Command) (string, string, string, error) {
	configAccountURL, containerName, err := getStorageInfo(cmd, configAccountFlag, configContainerFlag)
	if err != nil {
		return "", "", "", err
	}

	blobName, err := cmd.Flags().GetString(configBlobNameFlag)
	if err != nil {
		return "", "", "", fmt.Errorf("get flag '%s': %w", configBlobNameFlag, err)
	}

	return configAccountURL, containerName, blobName, nil
}

func GetKeyIdentityMail(cmd *cobra.Command) (string, error) {
	return cmd.Flags().GetString(keyIdentityFlag)
}

func GetInputStorageInfo(cmd *cobra.Command) (string, string, error) {
	return getStorageInfo(cmd, inputStorageAccountFlag, inputContainerFlag)
}

func GetQuarantineStorageInfo(cmd *cobra.Command) (string, string, error) {
	return getStorageInfo(cmd, QuarantineAccountFlag, QuarantineContainerFlag)
}

func GetOutputStorageInfo(cmd *cobra.Command) (string, string, error) {
	return getStorageInfo(cmd, outputStorageAccountFlag, outputContainerFlag)
}

func GetMetadataStorageInfo(cmd *cobra.Command) (string, string, error) {
	return getStorageInfo(cmd, metadataOutputStorageAccountFlag, metadataOutputContainerFlag)
}

func GetBackupOutputStorageInfo(cmd *cobra.Command) (string, string, error) {
	return getStorageInfo(cmd, backupOutputStorageAccountFlag, backupOutputContainerFlag)
}

func GetQueueTargetStorageInfo(cmd *cobra.Command) (string, string, string, error) {
	return getStorageQueueInfo(cmd, TargetStorageAccountFlag, ConfigStorageAccountQueueFlag, ConfigInputQueueNameFlag)
}

func GetAllowedVerbs(cmd *cobra.Command) (string, error) {
	allowedVerbs, err := cmd.Flags().GetString(AllowedVerbsFlag)
	if err != nil {
		return "", fmt.Errorf("retrieve flag '%s': %w", AllowedVerbsFlag, err)
	}
	return allowedVerbs, nil
}

func GetCatalogStorageInfo(cmd *cobra.Command) (string, string, error) {
	return getStorageInfo(cmd, CatalogStorageAccountFlag, CatalogContainerFlag)
}

func GetCtStorageInfo(cmd *cobra.Command) (string, string, error) {
	return getStorageInfo(cmd, CTAccountFlag, CTContainerFlag)
}

func GetKeyvaultInfo(cmd *cobra.Command) (string, string, error) {
	keyvaultURL, err := cmd.Flags().GetString(keyVaultFlag)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag '%s': %w", keyVaultFlag, err)
	}
	secretName, err := cmd.Flags().GetString(KeySecretNameFlag)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag '%s': %w", keyVaultFlag, err)
	}
	return keyvaultURL, secretName, err
}

func GetBackupKeyvaultInfo(cmd *cobra.Command) (string, string, error) {
	keyvaultURL, err := cmd.Flags().GetString(backupKeyvaultFlag)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag '%s': %w", backupKeyvaultFlag, err)
	}
	secretName, err := cmd.Flags().GetString(backupKeySecretNameFlag)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag '%s': %w", backupKeySecretNameFlag, err)
	}
	return keyvaultURL, secretName, err
}

func getStorageInfo(cmd *cobra.Command, accountFlag, containerFlag string) (string, string, error) {
	storageAccountURL, err := cmd.Flags().GetString(accountFlag)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag '%s': %w", accountFlag, err)
	}

	containerName, err := cmd.Flags().GetString(containerFlag)
	if err != nil {
		return "", "", fmt.Errorf("retrieve flag '%s': %w", containerFlag, err)
	}
	return storageAccountURL, containerName, nil
}

func getStorageQueueInfo(cmd *cobra.Command, accountFlag,
	queueURLFlag, inputQueueNameFlag string) (string, string, string, error) {
	storageAccountURL, err := cmd.Flags().GetString(accountFlag)
	if err != nil {
		return "", "", "", fmt.Errorf("retrieve flag '%s': %w", accountFlag, err)
	}
	queueURL, err := cmd.Flags().GetString(queueURLFlag)
	if err != nil {
		return "", "", "", fmt.Errorf("retrieve flag '%s': %w", queueURLFlag, err)
	}
	inputQueueName, err := cmd.Flags().GetString(inputQueueNameFlag)
	if err != nil {
		return "", "", "", fmt.Errorf("retrieve flag '%s': %w", inputQueueNameFlag, err)
	}
	return storageAccountURL, queueURL, inputQueueName, nil
}

func GetMetadataFilter(cmd *cobra.Command) (bool, map[string]string, error) {
	filterMedata, _ := cmd.Flags().GetBool(FilterMetadataFlag)

	if filterMedata {
		stepName, stepNamePresent := os.LookupEnv(stepNameEnvironmentVariable)
		areaName, areaNamePresent := os.LookupEnv(areaNameEnvironmentVariable)

		if !stepNamePresent || !areaNamePresent {
			return true,
				map[string]string{},
				fmt.Errorf("when flag '%s' is set, env variables '%s' and '%s' must be set",
					FilterMetadataFlag, stepNameEnvironmentVariable, areaNameEnvironmentVariable)
		}
		metadata := map[string]string{
			AreaNameMetadataKey: areaName,
			StepNameMetadataKey: stepName,
		}
		return true, metadata, nil
	}
	return false, map[string]string{}, nil
}
