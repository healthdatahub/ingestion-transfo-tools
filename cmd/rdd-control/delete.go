package main

import (
	"context"
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"

	"rdd-pipeline/pkg/azure"
	hdhlog "rdd-pipeline/pkg/log"
)

type DeleteResult struct {
	FileName string
	Status   string
	Unleased bool
}

type MessageContent struct {
	Container        string `json:"container"`
	FileName         string `json:"filename,omitempty"` // Empty if using regex
	Regex            string `json:"regex,omitempty"`    // Empty if using filename
	LastModifiedDate string `json:"last_modified_date"`
}

func DeleteFiles(ctx context.Context, log hdhlog.Logger, messageContentJSON, targetStorageAccountURL string) (string, error) {
	var messageContent MessageContent
	err := json.Unmarshal([]byte(messageContentJSON), &messageContent)
	if err != nil {
		return "", fmt.Errorf("Error parsing message content: %w", err)
	}

	targetStorageAccountBlobURL := strings.Replace(targetStorageAccountURL, "queue", "blob", 1)
	accountName := strings.TrimPrefix(targetStorageAccountBlobURL, "https://")
	accountName = strings.Split(accountName, ".")[0]

	deleteResultsSlice := []DeleteResult{}
	unleasedFiles := ""
	var lastModifiedDate time.Time
	var dateOperator string
	if messageContent.LastModifiedDate != "" {
		if len(messageContent.LastModifiedDate) > 5 {
			dateOperator = messageContent.LastModifiedDate[4:5]
			dateStr := messageContent.LastModifiedDate[5:]
			lastModifiedDate, err = time.Parse(inputDateFormat, dateStr)
			if err != nil {
				return "", fmt.Errorf("Error parsing last modified date: %w", err)
			}
		} else {
			return "", fmt.Errorf("Invalid last modified date format")
		}
	}

	if messageContent.FileName != "" {
		log.Debug("looking for a specific file name", "filename", messageContent.FileName)
		unleasedFiles, err = executeUnlease(ctx, log, messageContent.FileName, messageContent.Container, accountName, targetStorageAccountBlobURL, false, lastModifiedDate, dateOperator)
		if err != nil {
			return "", fmt.Errorf("Unleasing: %w", err)
		}
		deleteResultsSlice, err = executeDelete(ctx, log, messageContent.FileName, messageContent.Container, accountName, targetStorageAccountBlobURL, false, lastModifiedDate, dateOperator)
		if err != nil {
			return "", fmt.Errorf("Deleting: %w", err)
		}
	} else if messageContent.Regex != "" {
		log.Debug("looking for a regex", "regex", messageContent.Regex)
		unleasedFiles, err = executeUnlease(ctx, log, messageContent.Regex, messageContent.Container, accountName, targetStorageAccountBlobURL, true, lastModifiedDate, dateOperator)
		if err != nil {
			return "", fmt.Errorf("Unleasing: %w", err)
		}
		deleteResultsSlice, _ = executeDelete(ctx, log, messageContent.Regex, messageContent.Container, accountName, targetStorageAccountBlobURL, true, lastModifiedDate, dateOperator)
	} else {
		return "", fmt.Errorf("Neither FileName nor Regex provided")
	}

	unleasedFilesSlice := strings.Fields(unleasedFiles)
	for _, unleasedFile := range unleasedFilesSlice {
		for i, deleteResult := range deleteResultsSlice {
			if deleteResult.FileName == unleasedFile {
				deleteResultsSlice[i].Unleased = true
			}
		}
	}
	deleteResults := convertDeleteResultsToString(deleteResultsSlice)
	return deleteResults, nil
}

func executeDelete(ctx context.Context, log hdhlog.Logger, pattern, containerName, accountName, targetStorageAccountBlobURL string, isRegex bool, lastModifiedDate time.Time, dateOperator string) ([]DeleteResult, error) {
	var deleteResults []DeleteResult

	// Get the container client
	containerClient, err := azure.NewContainerClient(targetStorageAccountBlobURL+"/", containerName)
	if err != nil {
		return nil, fmt.Errorf("create container client: %w", err)
	}

	var matcher *regexp.Regexp
	if isRegex {
		matcher, err = regexp.Compile(pattern)
		if err != nil {
			return nil, fmt.Errorf("Compile regex: %w", err)
		}
	}
	pager := containerClient.NewListBlobsFlatPager(&container.ListBlobsFlatOptions{})
	for pager.More() {
		resp, err := pager.NextPage(ctx)
		if err != nil {
			return nil, fmt.Errorf("list segment: %w", err)
		}
		for _, item := range resp.Segment.BlobItems {
			// Use matchesLastModifiedDate for date comparison
			if !lastModifiedDate.IsZero() && !matchesLastModifiedDate(item, dateOperator, lastModifiedDate) {
				continue
			}

			if (!isRegex && *item.Name == pattern) || (isRegex && matcher.MatchString(*item.Name)) {

				blobClient := containerClient.NewBlobClient(*item.Name)
				_, err := blobClient.Delete(ctx, &blob.DeleteOptions{
					DeleteSnapshots: to.Ptr(blob.DeleteSnapshotsOptionTypeInclude),
				})
				result := DeleteResult{
					FileName: *item.Name,
				}
				if err != nil {
					log.Error(err, "Error Deleting blob")
					result.Status = "Failed"
				} else {
					result.Status = "Deleted"
				}
				deleteResults = append(deleteResults, result)
			}
		}
	}

	return deleteResults, nil
}

// This function will convert a slice of DeleteResult to a string
func convertDeleteResultsToString(results []DeleteResult) string {
	var resultsStrs []string
	for _, result := range results {
		resultsStrs = append(resultsStrs, fmt.Sprintf("FileName: %s, Status: %s, WasUnleased: %t", result.FileName, result.Status, result.Unleased))
	}
	return strings.Join(resultsStrs, "\\n")
}
