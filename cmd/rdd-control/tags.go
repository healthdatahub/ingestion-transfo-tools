package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"

	"rdd-pipeline/pkg/azure"
	hdhlog "rdd-pipeline/pkg/log"
)

type BlobTagParams struct {
	TagsToUpdate string `json:"tags_to_update"`
	TagsToDelete string `json:"tags_to_delete"`
	Filename     string `json:"filename"`
	Container    string `json:"container"`
	Regex        string `json:"regex"`
	DateFilter   string `json:"last_modified_date"` // date<DD/MM/YYYY or date>DD/MM/YYY or date=DD/MM/YYY
}

// AddOrModifyBlobTags is an pilot handler to manipulate tags on blobs.
func AddOrModifyBlobTags(ctx context.Context, log hdhlog.Logger, messageContent, targetStorageAccountURL string) (string, error) {
	var params BlobTagParams
	if err := json.Unmarshal([]byte(messageContent), &params); err != nil {
		return "", fmt.Errorf("failed to unmarshal JSON: %w", err)
	}

	targetStorageAccountBlobURL := strings.Replace(targetStorageAccountURL, "queue", "blob", 1)
	accountName := strings.TrimPrefix(targetStorageAccountBlobURL, "https://")
	accountName = strings.Split(accountName, ".")[0]

	containerName := params.Container
	fileName := params.Filename
	regexPattern := params.Regex

	var regex bool
	if fileName != "" {
		regex = false
	} else if regexPattern != "" {
		regex = true
	} else {
		return "", errors.New("either filename or regex must be provided")
	}

	var lastModifiedDateFilter time.Time
	var dateOperator string
	var err error
	if params.DateFilter != "" {
		// Extract the operator and the date part
		if len(params.DateFilter) > 5 {
			dateOperator = params.DateFilter[4:5]
			dateStr := params.DateFilter[5:]
			lastModifiedDateFilter, err = time.Parse(inputDateFormat, dateStr)
			if err != nil {
				return "", fmt.Errorf("failed to parse date filter: %w", err)
			}
		} else {
			return "", errors.New("invalid date filter format")
		}
	}
	log.Info("Parsed parameters from messageContent",
		"containerName", containerName,
		"regex", regex,
		"TagsToUpdate", params.TagsToUpdate,
		"TagsToDelete", params.TagsToDelete,
		"fileName", fileName,
		"dateOperator", dateOperator,
		"lastModifiedDateFilter", params.DateFilter)

	return executeBlobTagOperation(ctx, log, fileName, regexPattern, containerName, accountName, targetStorageAccountBlobURL, params.TagsToUpdate, params.TagsToDelete, regex, lastModifiedDateFilter, dateOperator)
}

func executeBlobTagOperation(ctx context.Context, log hdhlog.Logger, fileName, regexPattern, containerName, accountName, targetStorageAccountBlobURL, tagsToUpdate, tagsToDelete string, regex bool, lastModifiedDateFilter time.Time, dateOperator string) (string, error) {
	containerClient, err := azure.NewContainerClient(targetStorageAccountBlobURL, containerName)
	if err != nil {
		return "", fmt.Errorf("create container client: %w", err)
	}
	updateTags := []string{}
	if tagsToUpdate != "" {
		updateTags = strings.Split(tagsToUpdate, ",")
	}

	deleteTags := []string{}
	if tagsToDelete != "" {
		deleteTags = strings.Split(tagsToDelete, ",")
	}

	updatedBlobs := []string{}
	regexCompiled, err := regexp.Compile(regexPattern)
	if err != nil {
		return "", fmt.Errorf("invalid regex pattern: %w", err)
	}

	pager := containerClient.NewListBlobsFlatPager(&container.ListBlobsFlatOptions{
		Include: container.ListBlobsInclude{
			Metadata:  true,
			Snapshots: true,
		},
	})

	failedBlobs := []string{}
	for pager.More() {

		resp, err := pager.NextPage(ctx)
		if err != nil {
			return "", fmt.Errorf("list segment: %w", err)
		}

		for _, blob := range resp.Segment.BlobItems {
			// Check if we're looking for a specific filename and it doesn't match the current blob's name
			if !regex && fileName != "" && *blob.Name != fileName {
				continue
			}

			if matchesFilters(blob, regexCompiled, nil, nil, lastModifiedDateFilter, dateOperator) {

				blobClient := containerClient.NewBlobClient(*blob.Name)

				tagsResponse, err := blobClient.GetTags(ctx, nil)
				if err != nil {
					return "", fmt.Errorf("failed to fetch tags for blob %s: %w", *blob.Name, err)
				}

				originalTagsMap := make(map[string]string)
				for _, tag := range tagsResponse.BlobTagSet {
					originalTagsMap[*tag.Key] = *tag.Value
				}

				modifiedTagsMap := make(map[string]string)
				for k, v := range originalTagsMap {
					modifiedTagsMap[k] = v
				}

				for _, tag := range updateTags {
					splitTag := strings.Split(tag, ":")
					key := splitTag[0]
					var value string
					if len(splitTag) > 1 {
						value = splitTag[1]
					}
					modifiedTagsMap[key] = value
				}

				for _, tag := range deleteTags {
					splitTag := strings.Split(tag, ":")
					key := splitTag[0]
					var value string
					if len(splitTag) > 1 {
						value = splitTag[1]
						if existingValue, ok := modifiedTagsMap[key]; ok && existingValue == value {
							delete(modifiedTagsMap, key)
						}
					} else {
						delete(modifiedTagsMap, key)
					}
				}

				if !reflect.DeepEqual(originalTagsMap, modifiedTagsMap) {
					_, err = blobClient.SetTags(ctx, modifiedTagsMap, nil)
					if err != nil {
						log.Error(err, "failed to set tags", "blobname", *blob.Name)
						failedBlobs = append(failedBlobs, *blob.Name)
						continue // We don't stop the process, we simply continue to the next blob
					}
					updatedBlobs = append(updatedBlobs, *blob.Name)
				}
			}
		}

		if !regex && fileName != "" && len(updatedBlobs) > 0 {
			// If a specific file was processed, no need to continue to the next page
			break
		}
	}

	var resultMessage string
	if len(updatedBlobs) == 0 && len(failedBlobs) == 0 {
		resultMessage = "No blobs were updated."
	}
	if len(updatedBlobs) > 0 {
		resultMessage += fmt.Sprintf("Successfully modified tags for blobs: %s. ", strings.Join(updatedBlobs, ", "))
	}
	if len(failedBlobs) > 0 {
		resultMessage += fmt.Sprintf("Failed to process blobs: %s.", strings.Join(failedBlobs, ", "))
	}

	return resultMessage, nil
}
