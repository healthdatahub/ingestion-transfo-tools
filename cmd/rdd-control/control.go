package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"regexp"
	"strings"
	"time"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/lease"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azqueue"
	"github.com/spf13/cobra"
)

var controlCmd = &cobra.Command{
	Use:   "control",
	Short: "Use control",
	Run:   runControl,
	Example: `/rdd-control control \
		--target-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \,
		--config-storage-account-queue-endpoint https://myConfigStorageAccount.queue.core.windows.net \,
		--config-input-queue-name queuemanagementinput
		--allowed-verbs list,unlease,restore,delete,generate_sas_token`,
}

const inputDateFormat = "02/01/2006"

func init() {
	rootCmd.AddCommand(controlCmd)
	common.AddTargetStorageAccountFlags(controlCmd)
	common.AddConfigStorageAccountQueueFlag(controlCmd)
	common.AddConfigInputQueueNameFlag(controlCmd)
	common.AddAllowedVerbsFlag(controlCmd)

}

var errExit *exec.ExitError

func runControl(cmd *cobra.Command, args []string) {
	idleTime := 0
	autoShutdownTime := 1800
	ctx := cmd.Context()
	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	targetStorageAccountURL, configStorageAccountQueueURL, inputQueueName, err := common.GetQueueTargetStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetQueueTargetStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	allowedVerbs, err := common.GetAllowedVerbs(cmd)
	if err != nil {
		log.Error(err, "GetAllowedVerbs", "cmd", cmd)
		os.Exit(1)
	}

	targetStorageAccountURL = strings.TrimSuffix(targetStorageAccountURL, "/")
	configStorageAccountQueueURL = strings.TrimSuffix(configStorageAccountQueueURL, "/")

	log.Info("Allowed verbs", "allowedVerbs", allowedVerbs)

	// Creating the input queue client
	inputQueueClient, err := azure.NewQueueClientFromEnvironment(configStorageAccountQueueURL, inputQueueName)

	if err != nil && inputQueueClient != nil {
		fmt.Println("[ERROR] Creating the client for the input queue (NewQueueClientFromEnvironment)")
		fmt.Println(err)
		os.Exit(0)
	}

	var poisonMessageDequeueThreshold int64 = 4 // Indicates how many times a message is attempted to be processed before considering it a poison message	// The code below shows the service's infinite loop that dequeues messages and dispatches them in batches for processsing:

	for {
		// Try to dequeue a batch of messages from the queue
		dequeue, err := inputQueueClient.DequeueMessages(ctx, &azqueue.DequeueMessagesOptions{
			NumberOfMessages:  to.Ptr(int32(32)),
			VisibilityTimeout: to.Ptr(int32(300)),
		})
		if err != nil {
			log.Error(err, "Error dequeuing message")
		}
		if len(dequeue.Messages) == 0 {
			// The queue was empty; sleep a bit and try again
			time.Sleep(time.Second * 10)
			idleTime = idleTime + 10
		} else {
			idleTime = 0
			log.Info("Message found, resetting the autoshutdown countdown")

			// Process each message sequentially
			for i := 0; i < len(dequeue.Messages); i++ {
				msg := dequeue.Messages[i]
				msgID := msg.MessageID
				popReceipt := msg.PopReceipt // This message's most-recent pop receipt

				// Check if we attempted to dequeue this message already.
				// If the number of dequeuing for this message exceeds "poisonMessageDequeueThreshold", it is considered poisonned and therefore should be dropped
				if *msg.DequeueCount > poisonMessageDequeueThreshold {
					fmt.Println("Deleting message :", msg.MessageText)
					_, err = inputQueueClient.DeleteMessage(ctx, *msgID, *popReceipt, nil)
					if err != nil {
						log.Error(err, "Error deleting the message")
						os.Exit(1)
					}
					continue // Process a different message
				}

				err = ProcessMessage(ctx, log, msg, targetStorageAccountURL, configStorageAccountQueueURL, inputQueueName, allowedVerbs)
				if err != nil {
					log.Error(err, "Error processing the message")
					os.Exit(1)
				}
				// After processing the message, delete it from the queue
				_, err = inputQueueClient.DeleteMessage(ctx, *msgID, *popReceipt, nil)
				if err != nil {
					log.Error(err, "Error deleting message from the queue")
				}
			}
		}

		if idleTime == autoShutdownTime {
			log.Info("Autoshutdown time reached, this ACI will stop")
			os.Exit(0)
		}
	}
}

type ControlMessage struct {
	Command        string
	Content        string
	Origin         string
	Results_amount int
}

var ErrVerbNotAllowed string = "[ERROR] This verb is not allowed by this ACI"

// Dispatch function depending on the command we ask the ACI to execute
func ProcessMessage(ctx context.Context, log hdhlog.Logger, messageContent *azqueue.DequeuedMessage, targetStorageAccountURL, configStorageAccountQueueURL, inputQueueName, allowedVerbs string) error {
	var message ControlMessage
	var err error
	var result string
	logResult := true
	err = json.Unmarshal([]byte(*messageContent.MessageText), &message)
	if err != nil {
		fmt.Println("Error occured in unmarshal of the message:", err)
		return err
	}

	switch message.Command {
	case "list_files":
		if !strings.Contains(allowedVerbs, "list") {
			result = ErrVerbNotAllowed
		} else {
			result, err = ListFiles(ctx, log, message.Content, message.Results_amount, targetStorageAccountURL)
			if err != nil {
				result = "[ERROR]" + err.Error()
			}
		}
	case "unlease_files":
		if !strings.Contains(allowedVerbs, "unlease") {
			result = ErrVerbNotAllowed
		} else {
			result, err = UnleaseFiles(ctx, log, message.Content, targetStorageAccountURL)
			if err != nil {
				result = "[ERROR]" + err.Error()
			}
		}
	case "generate_sas_token":
		var ipFilterRequired bool
		// Note: the generate_sas_token function has a mandatory ip_filter parameter but only on some SA. The different verbs are flagging this additional parameter.
		// The rest of the function remains the same for both of these cases
		logResult = false
		if strings.Contains(allowedVerbs, "generate_sas_token_with_ip_mandatory") {
			ipFilterRequired = true
		} else if strings.Contains(allowedVerbs, "generate_sas_token") {
			ipFilterRequired = false
		} else {
			result = ErrVerbNotAllowed
			break
		}
		result, err = GenerateSASToken(ctx, log, message.Content, ipFilterRequired, targetStorageAccountURL)
		if err != nil {
			result = "[ERROR]" + err.Error()
		}

	case "delete_files":
		if !strings.Contains(allowedVerbs, "delete") {
			result = ErrVerbNotAllowed
		} else {
			result, err = DeleteFiles(ctx, log, message.Content, targetStorageAccountURL)
			if err != nil {
				result = "[ERROR]" + err.Error()
			}
		}
	case "update_tags":
		if !strings.Contains(allowedVerbs, "update_tags") {
			result = ErrVerbNotAllowed
		} else {
			result, err = AddOrModifyBlobTags(ctx, log, message.Content, targetStorageAccountURL)
			if err != nil {
				result = "[ERROR]" + err.Error()
			}
		}
	default:
		return fmt.Errorf("unsupported command '%s' in this message", message.Command)
	}
	fmt.Println("Message processed, sending response to the output queue")

	if logResult {
		fmt.Printf("MessageID: %s, Result: %s \n", string(*messageContent.MessageID), result)
	}

	formattedResult := result
	if strings.Contains(result, "ERROR") {
		message.Results_amount = 400
		formattedResult = "[ERROR]: " + base64.StdEncoding.EncodeToString([]byte(result))
	}
	err = AnswerMessage(ctx, log, formattedResult, configStorageAccountQueueURL, string(*messageContent.MessageID), inputQueueName, message.Results_amount)
	if err != nil {
		fmt.Println("Error occured while answering the message:", err)
		return err
	}
	return nil
}

// Send the answer in the managementoutput queue of the storage account with the following data:
// request_id: the id of the message that was sent into the managementinput queue, used to link a request to an answer
// content: the actual content of the message, ex: files list...
func AnswerMessage(ctx context.Context, log hdhlog.Logger, messageContent, configStorageAccountQueueURL, messageId, inputQueueName string, results_amount int) error {
	outputQueueName := strings.Replace(inputQueueName, "managementinput", "managementoutput", 1)

	outputQueueClient, err := azure.NewQueueClientFromEnvironment(configStorageAccountQueueURL, outputQueueName)
	if err != nil {
		return err
	}

	messageContent = strings.TrimPrefix(messageContent, " ")
	// Split the message into chunks < 60Ko (max in Azure queue is 64Ko)
	const maxChunkSize = 60000
	chunks := make([]string, 0, len(messageContent)/maxChunkSize+1)
	for len(messageContent) > maxChunkSize {
		chunk, rest := messageContent[:maxChunkSize], messageContent[maxChunkSize:]
		chunks = append(chunks, chunk)
		messageContent = rest
	}
	chunks = append(chunks, messageContent) // append the last chunk

	// Send each chunk, with their position each time and with an indicator with the one which is last
	for i, chunk := range chunks {
		isLast := "false"
		if i == len(chunks)-1 {
			isLast = "true"
		}
		messageValue := fmt.Sprintf("{\"request_id\":\"%s\",\"chunk_id\":%d,\"is_last\":\"%s\",\"content\":\"%s\"}", messageId, i, isLast, chunk)
		_, err = outputQueueClient.EnqueueMessage(ctx, messageValue, &azqueue.EnqueueMessageOptions{
			TimeToLive: to.Ptr(int32(20 * 60))})
		if err != nil {
			return err
		}
	}
	return nil
}

type CommandContent struct {
	Container           string `json:"container"`
	Regex               string `json:"regex"`
	TagFilterOnly       string `json:"tag_filter_only"`
	TagFilterNotPresent string `json:"tag_filter_not_present"`
	FullMode            bool   `json:"full_mode"`
	ShowMetadatas       bool   `json:"show_metadatas"`
	LastModifiedDate    string `json:"last_modified_date"` // date<DD/MM/YYYY or date>DD/MM/YYY or date=DD/MM/YYY
}

func parseCommandContent(jsonStr string, logger hdhlog.Logger) (string, string, map[string]string, map[string]string, bool, bool, string, time.Time) {
	// jsonStr content example: {"regex":"une_regex","tag_filter_only":"tag1:val1,tag2","tag_filter_not_present":"tag3,tag4:val4","last_modified_date":"18/12/2023","full_mode":"false","show_metadatas":"false","container":"data"}

	var content CommandContent

	err := json.Unmarshal([]byte(jsonStr), &content)
	if err != nil {
		logger.Error(err, "Error unmarshalling JSON")
		return "", "", nil, nil, false, false, "", time.Time{}
	}

	if content.Regex == "empty_regex" {
		content.Regex = ""
	}

	tagFilterOnlyMap := stringToMap(content.TagFilterOnly)
	tagFilterNotPresentMap := stringToMap(content.TagFilterNotPresent)

	var lastModifiedDateFilter time.Time
	var dateOperator string

	if content.LastModifiedDate != "" {
		// Extract the operator and the date part
		if len(content.LastModifiedDate) > 5 {
			dateOperator = content.LastModifiedDate[4:5]
			dateStr := content.LastModifiedDate[5:]
			lastModifiedDateFilter, err = time.Parse(inputDateFormat, dateStr)
			if err != nil {
				logger.Error(err, "Error parsing provided last modified date")
				return "", "", nil, nil, false, false, "", time.Time{}
			}
		} else {
			logger.Error(nil, "Invalid last modified date format")
			return "", "", nil, nil, false, false, "", time.Time{}
		}
	}
	return content.Container, content.Regex, tagFilterOnlyMap, tagFilterNotPresentMap, content.FullMode, content.ShowMetadatas, dateOperator, lastModifiedDateFilter
}

// stringToMap converts a string into a map where each key-value pair in the string is separated by a colon (:)
// and each pair is separated by a comma (,). If a key exists without a corresponding value (i.e., there's no colon after the key),
// the function assigns an empty string as the value for that key.
//
// Examples:
//
//	stringToMap("a:1,b:2,c:3") => map[string]string{"a": "1", "b": "2", "c": "3"}
//	stringToMap("a:1,b:") => map[string]string{"a": "1", "b": ""}
//	stringToMap("a,b,c") => map[string]string{"a": "", "b": "", "c": ""}
//
// Parameters:
//
//	s : The input string containing key-value pairs.
//
// Returns:
//
//	A map containing the key-value pairs parsed from the input string.
func stringToMap(s string) map[string]string {
	m := make(map[string]string)

	if s == "" {
		return m
	}

	pairs := strings.Split(s, ",")
	for _, pair := range pairs {
		kv := strings.Split(pair, ":")
		if len(kv) == 2 {
			m[kv[0]] = kv[1]
		} else if len(kv) == 1 {
			m[kv[0]] = ""
		}
	}
	return m
}

func ListFiles(ctx context.Context, log hdhlog.Logger, messageContent string, resultAmount int, targetStorageAccountURL string) (string, error) {

	// Parse message content
	containerName, regexStr, tagFilterOnlyMap, tagFilterNotPresentMap, fullMode, showMetadatas, dateOperator, lastModifiedDateFilter := parseCommandContent(messageContent, log)
	log.Info("Parsed parameters", "containerName", containerName, "regex", regexStr, "tagFilterOnly", tagFilterOnlyMap, "tagFilterNotPresent", tagFilterNotPresentMap, "fullMode", fullMode, "showMetadatas", showMetadatas, "dateOperator", dateOperator, "lastModifiedDate", lastModifiedDateFilter)

	// targetStorageAccountURL is the queue url, not the blob url
	targetStorageAccountBlobURL := strings.Replace(targetStorageAccountURL, "queue", "blob", 1)
	// Get access to the storage account container
	listContainerClient, err := azure.NewContainerClient(targetStorageAccountBlobURL, containerName)
	if err != nil {
		return "", fmt.Errorf("Create container client: %w", err)
	}

	// Prepare variables for result
	var result strings.Builder
	var totalCount, totalSize, returnedMatchingCount, returnedMatchingSize, totalMatchingCount, totalMatchingSize int64
	var regex *regexp.Regexp
	if regexStr != "" {
		regex, err = regexp.Compile(regexStr)
		if err != nil {
			return "", fmt.Errorf("compile regex: %w", err)
		}
	}

	// Set listing options
	pager := listContainerClient.NewListBlobsFlatPager(&container.ListBlobsFlatOptions{
		Include: container.ListBlobsInclude{
			Metadata: true,
			Tags:     true,
		},
	})

	// Iterate over blobs
	for pager.More() {
		resp, err := pager.NextPage(ctx)
		if bloberror.HasCode(err, bloberror.ContainerNotFound) {
			return "", fmt.Errorf("Container not found")
		} else if err != nil {
			return "", fmt.Errorf("list segment: %w", err)
		}
		log.Debug("Found blobs", "count", len(resp.Segment.BlobItems))

		for _, blob := range resp.Segment.BlobItems {
			if fullMode {
				totalSize += int64(*blob.Properties.ContentLength)
				totalCount++
			}

			matches := matchesFilters(blob, regex, tagFilterOnlyMap, tagFilterNotPresentMap, lastModifiedDateFilter, dateOperator)
			if matches {
				totalMatchingCount++
				totalMatchingSize += int64(*blob.Properties.ContentLength)

				if returnedMatchingCount < int64(resultAmount) {
					appendBlobToResult(&result, blob, showMetadatas)
					returnedMatchingCount++
					returnedMatchingSize += int64(*blob.Properties.ContentLength)
				}
			}
		}
	}

	// Build the final result
	var resultString string
	if fullMode {
		resultString = fmt.Sprintf("TotalMatchingSize: %d, TotalMatchingCount: %d, TotalBlobs: %d, TotalSize: %d, ", totalMatchingSize, totalMatchingCount, totalCount, totalSize)
	} else {
		resultString = fmt.Sprintf("ReturnedMatchingSize: %d, ReturnedMatchingCount: %d, ", returnedMatchingSize, returnedMatchingCount)
	}

	resultString += fmt.Sprintf("BlobsFound: %s", result.String())

	return resultString, nil
}

// matchesLastModifiedDate checks if the blob's lastModifiedDate matches the provided time based on the operator
func matchesLastModifiedDate(blob *container.BlobItem, operator string, dateFilter time.Time) bool {
	// Ensure the blob's LastModified date is not zero (unset)
	if blob.Properties.LastModified.IsZero() {
		return false
	}

	switch operator {
	case "=":
		// Check for equality (year, month, and day)
		return blob.Properties.LastModified.Year() == dateFilter.Year() &&
			blob.Properties.LastModified.Month() == dateFilter.Month() &&
			blob.Properties.LastModified.Day() == dateFilter.Day()

	case ">":
		// Check if the blob's last modified date is strictly after the provided date
		return blob.Properties.LastModified.After(dateFilter) &&
			!(blob.Properties.LastModified.Year() == dateFilter.Year() &&
				blob.Properties.LastModified.Month() == dateFilter.Month() &&
				blob.Properties.LastModified.Day() == dateFilter.Day())

	case "<":
		// Check if the blob's last modified date is strictly before the provided date
		return blob.Properties.LastModified.Before(dateFilter) &&
			!(blob.Properties.LastModified.Year() == dateFilter.Year() &&
				blob.Properties.LastModified.Month() == dateFilter.Month() &&
				blob.Properties.LastModified.Day() == dateFilter.Day())

	default:
		// Unsupported operator
		return false
	}
}

// matchesFilters checks if a blob matches the provided filters
func matchesFilters(blob *container.BlobItem, regex *regexp.Regexp, tagFilterOnlyMap, tagFilterNotPresentMap map[string]string, lastModifiedDateFilter time.Time, dateOperator string) bool {
	if regex != nil && !regex.MatchString(*blob.Name) {
		return false
	}

	// Check tag filters
	blobTagMap := make(map[string]string)
	if blob.BlobTags != nil {
		for _, tag := range blob.BlobTags.BlobTagSet {
			blobTagMap[*tag.Key] = *tag.Value
		}
	}

	for key, val := range tagFilterOnlyMap {
		if blobVal, exists := blobTagMap[key]; !exists || (val != "" && val != blobVal) {
			return false
		}
	}

	for key, val := range tagFilterNotPresentMap {
		if blobVal, exists := blobTagMap[key]; exists && (val == "" || val == blobVal) {
			return false
		}
	}

	// lastModifiedDate filter
	if !lastModifiedDateFilter.IsZero() {
		if !matchesLastModifiedDate(blob, dateOperator, lastModifiedDateFilter) {
			return false
		}
	}
	return true
}

func appendBlobToResult(result *strings.Builder, blob *container.BlobItem, showMetadatas bool) {
	result.WriteString(" name:")
	result.WriteString(*blob.Name)
	result.WriteString(",size:")
	result.WriteString(formatSize(float64(*blob.Properties.ContentLength)))
	result.WriteString(",LastModified:")
	result.WriteString(strings.Replace(blob.Properties.LastModified.String(), " ", "_", -1))
	result.WriteString(",leaseStatus:")
	result.WriteString(fmt.Sprintf("%v", *blob.Properties.LeaseStatus))
	if showMetadatas {
		result.WriteString(",metadatas:")
		jsonData, _ := json.Marshal(blob.Metadata)
		base64String := base64.StdEncoding.EncodeToString(jsonData)
		result.WriteString(base64String)
	}
}

func UnleaseFiles(ctx context.Context, log hdhlog.Logger, messageContentJSON, targetStorageAccountURL string) (string, error) {
	var messageContent MessageContent
	err := json.Unmarshal([]byte(messageContentJSON), &messageContent)
	if err != nil {
		return "", fmt.Errorf("Error parsing message content: %w", err)
	}

	targetStorageAccountBlobURL := strings.Replace(targetStorageAccountURL, "queue", "blob", 1)
	accountName := strings.TrimPrefix(targetStorageAccountBlobURL, "https://")
	accountName = strings.Split(accountName, ".")[0]

	var lastModifiedDate time.Time
	var dateOperator string
	if messageContent.LastModifiedDate != "" {
		if len(messageContent.LastModifiedDate) > 5 {
			dateOperator = messageContent.LastModifiedDate[4:5]
			dateStr := messageContent.LastModifiedDate[5:]
			lastModifiedDate, err = time.Parse(inputDateFormat, dateStr)
			if err != nil {
				return "", fmt.Errorf("Error parsing last modified date: %w", err)
			}
		} else {
			return "", fmt.Errorf("Invalid last modified date format")
		}
	}

	var result string
	if messageContent.FileName != "" {
		log.Debug("looking for a specific file name", "filename", messageContent.FileName)
		result, _ = executeUnlease(ctx, log, messageContent.FileName, messageContent.Container, accountName, targetStorageAccountBlobURL, false, lastModifiedDate, dateOperator)
	} else if messageContent.Regex != "" {
		log.Debug("looking for a regex", "regex", messageContent.Regex)
		result, _ = executeUnlease(ctx, log, messageContent.Regex, messageContent.Container, accountName, targetStorageAccountBlobURL, true, lastModifiedDate, dateOperator)
	} else {
		return "", fmt.Errorf("Neither FileName nor Regex provided")
	}

	return result, nil
}

func executeUnlease(ctx context.Context, log hdhlog.Logger, pattern, containerName, accountName, targetStorageAccountBlobURL string, isRegex bool, lastModifiedDate time.Time, dateOperator string) (string, error) {
	var result []string
	var err error

	// Get the container client
	containerClient, err := azure.NewContainerClient(targetStorageAccountBlobURL, containerName)
	if err != nil {
		return "", fmt.Errorf("Create container client: %w", err)
	}

	pager := containerClient.NewListBlobsFlatPager(&container.ListBlobsFlatOptions{
		Include: container.ListBlobsInclude{
			Metadata: true,
			Tags:     true,
		},
	})

	var matcher *regexp.Regexp
	if isRegex {
		matcher, err = regexp.Compile(pattern)
		if err != nil {
			return "", fmt.Errorf("Compile regex: %w", err)
		}
	}

	for pager.More() {
		resp, err := pager.NextPage(ctx)
		if bloberror.HasCode(err, bloberror.ContainerNotFound) {
			return "", fmt.Errorf("Container not found")
		} else if err != nil {
			return "", fmt.Errorf("list segment: %w", err)
		}
		log.Debug("Found blobs", "count", len(resp.Segment.BlobItems))

		for _, blob := range resp.Segment.BlobItems {
			if !lastModifiedDate.IsZero() && !matchesLastModifiedDate(blob, dateOperator, lastModifiedDate) {
				continue
			}

			match := (!isRegex && *blob.Name == pattern) || (isRegex && matcher.MatchString(*blob.Name))
			if match && *blob.Properties.LeaseState == lease.StateTypeLeased {

				log.Info("Unleasing blob", "blob", blob.Name)
				leaseClient, err := azure.NewLeaseClient(containerClient, *blob.Name)
				if err != nil {
					log.Error(err, "Error Breaking lease")
					os.Exit(1)
				}
				_, err = leaseClient.BreakLease(ctx, nil)
				if err != nil {
					log.Error(err, "Error Breaking lease")
					os.Exit(1)
				}

				result = append(result, *blob.Name)
			}
		}
	}
	return strings.Join(result, " "), nil
}

// Helper function to format size
func formatSize(size float64) string {
	const (
		B  = 1.0
		KB = 1024.0 * B
		MB = 1024.0 * KB
		GB = 1024.0 * MB
		TB = 1024.0 * GB
	)

	switch {
	case size < KB:
		return fmt.Sprintf("%.0fB", size)
	case size < MB:
		return fmt.Sprintf("%.2fKB", size/KB)
	case size < GB:
		return fmt.Sprintf("%.2fMB", size/MB)
	case size < TB:
		return fmt.Sprintf("%.2fGB", size/GB)
	default:
		return fmt.Sprintf("%.2fTB", size/TB)
	}
}
