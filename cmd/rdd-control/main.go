package main

import (
	"context"
	"os"
	"os/signal"
	hdhlog "rdd-pipeline/pkg/log"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands.
var rootCmd = &cobra.Command{
	Use:               "rdd-control",
	Short:             "Execute commands on the storage accounts",
	PersistentPreRunE: rootPreRun,
	SilenceUsage:      true,
}

var logLvl int

func init() {
	rootCmd.PersistentFlags().IntVarP(&logLvl, "log-level", "V", 1, "log level")
}

// main adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func main() {
	ctxCancel, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func() {
		select {
		case <-c:
			cancel()
		case <-ctxCancel.Done():
		}
	}()
	if err := rootCmd.ExecuteContext(ctxCancel); err != nil {
		os.Exit(0) // to prevent aci restart
	}
}

func rootPreRun(cmd *cobra.Command, args []string) error {
	if err := cmd.ParseFlags(args); err != nil {
		return err
	}

	// add the command name has a permanent attribute logger
	// and attach a logger to the context
	log, logctx := hdhlog.
		New(os.Stdout, logLvl).
		With("cmd", cmd.CommandPath()).
		AttachToContext(cmd.Context())
	cmd.SetContext(logctx) // make it available to subcommands
	log.Info("starting command", "command-line", os.Args)

	return nil
}
