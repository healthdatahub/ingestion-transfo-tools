package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"rdd-pipeline/pkg/azure"
	hdhlog "rdd-pipeline/pkg/log"
	"strings"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/resourcemanager/storage/armstorage"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"github.com/Azure/azure-sdk-for-go/storage"
)

type Request struct {
	Code                 string
	Mode                 string
	Date                 string
	DaysBeforeExpiration int
	IpFilter             string
}

const MaxValidTokenDurationInDays = 5

// GenerateSASToken generates a SASURL on a given storage account.
// This URL grants permissions like read/write on SA depending on the usecase.
// Typically, this type of URL is given to our customers for them to push their
// data files over HTTPs.
func GenerateSASToken(ctx context.Context, log hdhlog.Logger, messageContent string, ipFilterRequired bool, targetStorageAccountURL string) (string, error) {
	log.Info(fmt.Sprintf("New request: %s", messageContent))

	var request Request
	err := json.Unmarshal([]byte(messageContent), &request)
	if err != nil {
		return "", err
	}

	// Check for IpFilter if ipFilterRequired is true
	if ipFilterRequired && request.IpFilter == "" {
		return "", fmt.Errorf("IpFilter field is missing in the request")
	}

	if request.DaysBeforeExpiration > MaxValidTokenDurationInDays {
		return "", fmt.Errorf("token validity duration cannot exceed %d days", MaxValidTokenDurationInDays)
	}

	sasURL, err := doGenerateSASToken(ctx, request.Code, request.Mode, request.Date, request.DaysBeforeExpiration, request.IpFilter, targetStorageAccountURL)
	if err != nil {
		return "", err
	}

	response := map[string]interface{}{
		"sas_token": sasURL,
		"blobs":     []string{},
	}

	// If the mode is "export", fetch and include the first N blobs
	if strings.ToLower(request.Mode) == "export" {
		blobNames, err := listFirstNBlobs(ctx, log, targetStorageAccountURL, "output", 3)
		if err != nil {
			return "", fmt.Errorf("failed to list blobs: %w", err)
		}
		response["blobs"] = blobNames
	}

	jsonResponse, err := json.Marshal(response)
	if err != nil {
		return "", fmt.Errorf("failed to marshal response: %w", err)
	}

	log.Info("returning response")
	return base64.StdEncoding.EncodeToString(jsonResponse), nil
}

func doGenerateSASToken(
	ctx context.Context,
	code string,
	mode string,
	startAsString string,
	daysBeforeExpiration int,
	ipFilter string,
	targetStorageAccountURL string) (string, error) {

	// Parse the start date
	start, err := time.Parse("2006-01-02", startAsString)
	if err != nil {
		return "", err
	}
	expiry := start.AddDate(0, 0, daysBeforeExpiration)

	// storage account name
	storageAccountName := strings.Split(strings.TrimPrefix(targetStorageAccountURL, "https://"), ".blob")[0]

	// regenerate storage account access key
	storageAccountKey, err := regenerateAccessKey(code, storageAccountName)
	if err != nil {
		return "", err
	}

	var permissions storage.ContainerSASPermissions
	var containerName string
	switch strings.ToLower(mode) {
	case "import":
		containerName = "input"
		// Set permissions
		permissions.Write = true
		permissions.List = true
	case "export":
		containerName = "output"
		// Set permissions
		permissions.Read = true
		permissions.Write = false
		permissions.Delete = false
		permissions.List = true
	case "rd":
		containerName = "data"
		// Set permissions
		permissions.Write = true
		permissions.List = true
	default:
		return "", errors.New("mode not available. 'rd', 'import' or 'export' expected")
	}

	// Create shared key credential
	client, err := storage.NewBasicClient(storageAccountName, storageAccountKey)
	if err != nil {
		return "", err
	}

	blobService := client.GetBlobService()
	container := blobService.GetContainerReference(containerName)

	// Define the SAS options
	sasOptions := storage.ContainerSASOptions{
		ContainerSASPermissions: permissions,
		SASOptions: storage.SASOptions{
			Start:    start,
			Expiry:   expiry,
			UseHTTPS: true,
		},
	}

	// If an IP filter is specified, include it in the SAS options
	if ipFilter != "" {
		sasOptions.SASOptions.IP = ipFilter
	}

	// Generate the SAS URI
	sasURI, err := container.GetSASURI(sasOptions)
	if err != nil {
		return "", err
	}

	return sasURI, nil
}

// when accessing azure with a user of managed identities, we can not get an
// access key without regenerating one. The solution consists in regenerating
// the second storage account named "key2".
// Note that this approach can not work in a concurrent environment.
func regenerateAccessKey(code, storageAccountName string) (key string, err error) {
	armclient, err := azure.NewARMStorageClient(os.Getenv("AZURE_SUBSCRIPTION_ID"))
	// storage account name

	if err != nil {
		return "", err
	}
	resp, err := armclient.RegenerateKey(context.Background(),
		fmt.Sprintf("%s-components", code),
		fmt.Sprintf("%s", storageAccountName),
		armstorage.AccountRegenerateKeyParameters{
			KeyName: to.Ptr("key2"),
		}, &armstorage.AccountsClientRegenerateKeyOptions{})
	if err != nil {
		return "", err
	}
	return *resp.Keys[1].Value, nil
}

func listFirstNBlobs(ctx context.Context, log hdhlog.Logger, targetStorageAccountURL, containerName string, n int) ([]string, error) {
	client, err := azure.NewContainerClient(targetStorageAccountURL, containerName)
	if err != nil {
		return nil, fmt.Errorf("failed to create container client: %w", err)
	}

	pager := client.NewListBlobsFlatPager(&container.ListBlobsFlatOptions{})

	results := make([]string, 0, n)

	for pager.More() {
		page, err := pager.NextPage(ctx)
		if err != nil {
			return nil, fmt.Errorf("failed to list blobs: %w", err)
		}
		for _, blob := range page.Segment.BlobItems {
			if len(results) >= n {
				return results, nil
			}
			results = append(results, *blob.Name)
		}
	}

	return results, nil
}
