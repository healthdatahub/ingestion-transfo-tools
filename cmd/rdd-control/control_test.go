package main

import (
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
)

func TestStringToMap(t *testing.T) {
	tests := []struct {
		input    string
		expected map[string]string
	}{
		{"a:1,b:2,c:3", map[string]string{"a": "1", "b": "2", "c": "3"}},
		{"a:1,b:", map[string]string{"a": "1", "b": ""}},
		{"a,b,c", map[string]string{"a": "", "b": "", "c": ""}},
		{"", map[string]string{}},
	}

	for _, test := range tests {
		t.Run(test.input, func(t *testing.T) {
			got := stringToMap(test.input)
			if !reflect.DeepEqual(got, test.expected) {
				t.Errorf("Expected %v, got %v", test.expected, got)
			}
		})
	}
}

func TestFormatSize(t *testing.T) {
	tests := []struct {
		input    float64
		expected string
	}{
		{500, "500B"},
		{1023, "1023B"},
		{1024, "1.00KB"},
		{1500, "1.46KB"},
		{1048576, "1.00MB"},
		{1572864, "1.50MB"},
		{1073741824, "1.00GB"},
		{1610612736, "1.50GB"},
		{1099511627776, "1.00TB"},
		{1649267441664, "1.50TB"},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("formatSize(%f)", test.input), func(t *testing.T) {
			got := formatSize(test.input)
			if got != test.expected {
				t.Errorf("Expected %s, got %s", test.expected, got)
			}
		})
	}
}

func TestMatchesLastModifiedDate(t *testing.T) {
	// Mock dates for testing
	dateFilter := time.Date(2022, 1, 15, 0, 0, 0, 0, time.UTC)
	dateFilterSameButDifferent := time.Date(2022, 1, 15, 0, 0, 1, 0, time.UTC)
	beforeDate := time.Date(2022, 1, 14, 23, 59, 59, 999, time.UTC)
	afterDate := time.Date(2022, 1, 16, 0, 0, 0, 1, time.UTC)
	zeroDate := time.Time{}

	// Mock BlobItem with different LastModified dates
	blobEqualDate := container.BlobItem{Properties: &container.BlobProperties{LastModified: &dateFilterSameButDifferent}}
	blobBeforeDate := container.BlobItem{Properties: &container.BlobProperties{LastModified: &beforeDate}}
	blobAfterDate := container.BlobItem{Properties: &container.BlobProperties{LastModified: &afterDate}}
	blobZeroDate := container.BlobItem{Properties: &container.BlobProperties{LastModified: &zeroDate}}

	tests := []struct {
		name       string
		blob       *container.BlobItem
		operator   string
		dateFilter time.Time
		want       bool
	}{
		{
			name:       "EqualDates",
			blob:       &blobEqualDate,
			operator:   "=",
			dateFilter: dateFilter,
			want:       true,
		},
		{
			name:       "NotEqualDates",
			blob:       &blobBeforeDate,
			operator:   "=",
			dateFilter: dateFilter,
			want:       false,
		},
		{
			name:       "DateAfter",
			blob:       &blobAfterDate,
			operator:   ">",
			dateFilter: dateFilter,
			want:       true,
		},
		{
			name:       "DateBefore",
			blob:       &blobBeforeDate,
			operator:   "<",
			dateFilter: dateFilter,
			want:       true,
		},
		{
			name:       "InvalidOperator",
			blob:       &blobEqualDate,
			operator:   "!",
			dateFilter: dateFilter,
			want:       false,
		},
		{
			name:       "LastModifiedIsZero",
			blob:       &blobZeroDate,
			operator:   "=",
			dateFilter: dateFilter,
			want:       false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := matchesLastModifiedDate(tt.blob, tt.operator, tt.dateFilter); got != tt.want {
				t.Errorf("matchesLastModifiedDate() = %v, want %v", got, tt.want)
			}
		})
	}
}
