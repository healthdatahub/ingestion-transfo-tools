package main

import (
	"os"
	"reflect"
	"testing"
	"time"
)

type mockWalker struct {
	paths []string
	stats []os.FileInfo
	errs  []error
	i     int
}

func (mw *mockWalker) Step() bool {
	mw.i++
	return mw.i <= len(mw.paths)
}

func (mw *mockWalker) Path() string {
	return mw.paths[mw.i-1]
}

func (mw *mockWalker) Stat() os.FileInfo {
	return mw.stats[mw.i-1]
}

func (mw *mockWalker) Err() error {
	return mw.errs[mw.i-1]
}

type mockFileInfo struct {
	isDir bool
}

func (mfi mockFileInfo) Name() string       { return "" }
func (mfi mockFileInfo) Size() int64        { return 0 }
func (mfi mockFileInfo) Mode() os.FileMode  { return 0 }
func (mfi mockFileInfo) ModTime() time.Time { return time.Time{} }
func (mfi mockFileInfo) IsDir() bool        { return mfi.isDir }
func (mfi mockFileInfo) Sys() interface{}   { return nil }

func TestCreateNonHiddenFilesList(t *testing.T) {
	mw := &mockWalker{
		paths: []string{"/path/to/file1", "/path/to/.hiddenfile", "/path/to/file2", "/path/.hiddendir/file3", "/path/to/folder", "/path/to/file5"},
		stats: []os.FileInfo{
			mockFileInfo{isDir: false},
			mockFileInfo{isDir: false},
			mockFileInfo{isDir: false},
			mockFileInfo{isDir: false},
			mockFileInfo{isDir: true},
			mockFileInfo{isDir: false},
		}, errs: []error{nil, nil, nil, nil, nil, nil},
	}

	nonHiddenFiles, err := CreateNonHiddenFilesList(mw)
	if err != nil {
		t.Errorf("CreateNonHiddenFilesList returned an error: %v", err)
	}

	expected := []string{"/path/to/file1", "/path/to/file2", "/path/to/file5"}
	if !reflect.DeepEqual(nonHiddenFiles, expected) {
		t.Errorf("CreateNonHiddenFilesList returned %v, want %v", nonHiddenFiles, expected)
	}
}
