package main

import (
	"fmt"
	"net"
	"os"
	"path/filepath"
	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/keyvault"
	hdhlog "rdd-pipeline/pkg/log"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"
	"strings"
	"time"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blockblob"
	"github.com/pkg/sftp"
	"github.com/spf13/cobra"
	"golang.org/x/crypto/ssh"
)

// sftpCmd represents the hdh command.
var sftpCmd = &cobra.Command{
	Use:   "sftp",
	Short: "Use sftp protocol",
	Run:   runSftp,
	Example: `/rdd-import sftp \
		--config-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \
		--config-container config \
		--config-blob-name sftp.yaml \
		--keyvault-endpoint https://myVault.vault.azure.net \
		--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
		--ssh-private-key idrsa \
		--delete true|false`,
}

type Walker interface {
	Step() bool
	Path() string
	Stat() os.FileInfo
	Err() error
}

func init() {
	rootCmd.AddCommand(sftpCmd)

	common.AddOutputBlobFlags(sftpCmd)
	common.AddKeyVaultFlags(sftpCmd)
	common.AddConfigFlags(sftpCmd)

	sftpCmd.Flags().String("ssh-private-key", "", "keyvault secret name where ssh-private-key is stored")
	(cobra.MarkFlagRequired(sftpCmd.Flags(), "ssh-private-key"))
	sftpCmd.Flags().Bool("delete", true, "whether or not delete the files after transfer")
}

func runSftp(cmd *cobra.Command, args []string) {
	ctx := cmd.Context()

	log := hdhlog.FromContext(ctx)

	sshPrivateKeyName, err := cmd.Flags().GetString("ssh-private-key")
	if err != nil {
		log.Error(err, "getting flag ssh-private-key")
		os.Exit(0)
	}

	delete, err := cmd.Flags().GetBool("delete")
	if err != nil {
		log.Error(err, "getting flag delete")
		os.Exit(0)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo")
		os.Exit(0)
	}

	keyvaultURL, _, err := common.GetKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetKeyvaultInfo")
		os.Exit(0)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo")
		os.Exit(0)
	}

	wConfig := worker.ConfigContainer{
		StorageAccountURL: configStorageAccountURL,
		ContainerName:     configContainerName,
		BlobName:          configBlobName,
	}
	pipelineConfig, err := wConfig.Download(ctx)
	if err != nil {
		log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
		os.Exit(0)
	}

	outputContainerClient, err := azure.NewContainerClient(outputStorageAccountURL, outputContainerName)
	if err != nil {
		log.Error(err, "NewContainerClientFromEnvironment")
		os.Exit(0)
	}

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(keyvaultURL)
	if err != nil {
		log.Error(err, "NewKeyVaultClientFromEnvironment")
		os.Exit(0)
	}

	sshPrivateKey, err := kvClient.GetSecret(ctx, sshPrivateKeyName, "")
	if err != nil {
		log.Error(err, "GetSecret", "SSHPrivateKeyName", sshPrivateKeyName)
		os.Exit(0)
	}

	sftpConfig := pipelineConfig.Import.Sftp
	if sftpConfig.Delete == nil {
		sftpConfig.Delete = &delete
	}

	sshClient, err := sshClient(sshPrivateKey, sftpConfig.Server, sftpConfig.User)
	if err != nil {
		log.Error(err, "sshClient", "SSHPrivateKeyName", sshPrivateKeyName, "SFTP Config Server", sftpConfig.Server, "SFTP Config User", sftpConfig.User)
		os.Exit(0)
	}
	defer sshClient.Close()

	sftpClient, err := sftp.NewClient(sshClient)
	if err != nil {
		log.Error(err, "SftpNewClient", "SSHPrivateKeyName", sshPrivateKeyName, "SFTP Config Server", sftpConfig.Server, "SFTP Config User", sftpConfig.User)
		os.Exit(0)
	}
	defer sftpClient.Close()

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "NewClientProvidedKeyOptions")
		os.Exit(0)
	}

	keepAlive := sftpConfig.KeepAlive
	if keepAlive {
		log.Info("KeepAlive = true, this ACI will remain active even after all the files are processed")
	} else {
		log.Info("KeepAlive = false, this ACI will stop itself after the last file retrieved")
	}

	// Find files to transfert
	log.Info("Start reading config file", "globs", sftpConfig.Files)

	for {
		for _, fp := range sftpConfig.Files {
			nonHiddenFiles, err := CreateNonHiddenFilesList(sftpClient.Walk(fp))
			if err != nil {
				log.Error(err, "fail to get non hidden files for pattern", "pattern", fp)
				continue
			}
			log.Info("Non-hidden files found in folder", "number", len(nonHiddenFiles), "folder", fp)
			for _, f := range nonHiddenFiles {
				err := func() error {
					log.Info("Processing", "file", f)
					file, err := sftpClient.Open(f)
					if err != nil {
						return fmt.Errorf("fail to open file '%s': %w", f, err)
					}
					defer file.Close()

					metadata := make(map[string]*string)
					metadata[v1alpha1.MetadataMatchPattern] = to.Ptr(fp)

					fstat, err := file.Stat()
					if err != nil {
						return fmt.Errorf("fail to stat file '%s': %w", f, err)
					}
					if !fstat.IsDir() {
						log.Info("Uploading", "file", f)

						blobClient := outputContainerClient.NewBlockBlobClient(strings.TrimPrefix(file.Name(), "/"))
						_, err = blobClient.UploadStream(ctx, file, &blockblob.UploadStreamOptions{
							Metadata: metadata,
							CPKInfo:  cpk,
						})
						if err != nil {
							return fmt.Errorf("fail to upload %s: %w", f, err)
						}
						log.Info("Successfully uploaded", "file", f)
					}

					// Delete Files only if marked as to delete
					if *sftpConfig.Delete {
						if err := sftpClient.Remove(f); err != nil {
							return fmt.Errorf("fail to remove file '%s': %w", f, err)
						}
						log.Info("Successfully removed from SFTP server", "file", f)
					}
					return nil
				}()
				if err != nil {
					log.Error(err, "Remove", "file", f)
				}

			}
		}
		if !keepAlive {
			break
		}
		time.Sleep(30 * time.Second)
	}
}

func sshClient(key []byte, addr string, user string) (*ssh.Client, error) {
	privateKey, err := ssh.ParsePrivateKey(key)
	if err != nil {
		return nil, fmt.Errorf("failed to parse worker private key: %w", err)
	}

	conn, err := net.DialTimeout("tcp", addr, 10*time.Second)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to TSA: %w", err)
	}

	clientConfig := &ssh.ClientConfig{
		User: user,
		/* #nosec */
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),

		Auth: []ssh.AuthMethod{ssh.PublicKeys(privateKey)},
	}

	clientConn, chans, reqs, err := ssh.NewClientConn(conn, addr, clientConfig)
	if err != nil {
		return nil, fmt.Errorf("failed to construct sftpClient connection: %w", err)
	}

	return ssh.NewClient(clientConn, chans, reqs), nil
}

func CreateNonHiddenFilesList(w Walker) ([]string, error) {
	nonHiddenFiles := []string{}
	for w.Step() {
		if w.Err() != nil {
			return nil, fmt.Errorf("error walking directory: %v", w.Err())
		}

		fileInfo := w.Stat()
		filePath := w.Path()
		if isHidden(filePath) {
			// Ignore hidden files
			continue
		}

		if fileInfo.IsDir() {
			// Continue walking
			continue
		}

		// Append non-hidden files to the list
		nonHiddenFiles = append(nonHiddenFiles, filePath)
	}
	return nonHiddenFiles, nil
}

func isHidden(filePath string) bool {
	// Split the path into components
	components := strings.Split(filePath, string(filepath.Separator))

	// Check if any of the components are hidden
	for _, component := range components {
		if len(component) > 0 && component[0] == '.' {
			return true
		}
	}
	return false
}
