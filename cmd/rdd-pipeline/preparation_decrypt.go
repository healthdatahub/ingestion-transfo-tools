package main

import (
	"compress/gzip"
	"context"
	"encoding/base64"
	"fmt"

	"io"
	"os"
	"path/filepath"
	"strings"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/crypt/hdhcrypt"
	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/spf13/cobra"
)

var preparationDecryptCmd = &cobra.Command{
	Use:   "decrypt",
	Short: "Decrypt a blob",
	RunE:  runHdhDecrypt,
	Example: `rdd-pipeline preparation decrypt \
	--config-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \
	--config-container config \
	--config-blob-name decrypt.yaml \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--keyvault-endpoint https://myVault.vault.azure.net`,
}

func init() {
	preparationCmd.AddCommand(preparationDecryptCmd)

	common.AddInputBlobFlags(preparationDecryptCmd)
	common.AddOutputBlobFlags(preparationDecryptCmd)

	common.AddKeyVaultFlags(preparationDecryptCmd)
	common.AddConfigFlags(preparationDecryptCmd)
}

func runHdhDecrypt(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, _, err := common.GetKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetKeyvaultInfo", "cmd", cmd)
		os.Exit(1)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyVaultKeyName, wrappedKeyName, err := common.GetMetadataKeys()
	if err != nil {
		log.Error(err, "retrieve flags")
		os.Exit(1)
	}

	wConfig := worker.ConfigContainer{
		StorageAccountURL: configStorageAccountURL,
		ContainerName:     configContainerName,
		BlobName:          configBlobName,
	}
	pipelineConfig, err := wConfig.Download(ctx)
	if err != nil {
		log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
		os.Exit(1)
	}

	requiredTags := make(map[string]string)
	// Add the optional tagFilters
	if pipelineConfig.Pipeline.TagsFilter != nil {
		log.Info("tagsFilter is not empty, these tags will be considered as required:", "tagsFilter", pipelineConfig.Pipeline.TagsFilter)
		for key, value := range pipelineConfig.Pipeline.TagsFilter {
			if value == "" {
				value = "no_value"
			}
			requiredTags[key] = value
		}
	} else {
		log.Info("pipelineconfig empty, no optional tag to use", "tagFilter", pipelineConfig.Pipeline.TagsFilter)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainerWithConfig(inputStorageAccountURL, inputContainerName, pipelineConfig.Decrypt, nil, nil, requiredTags, 0, 0)

	// start executing on blob sent from watcher
	task := NewPreparationDecryptTask(
		pipelineConfig,
		keyvaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		keyVaultKeyName,
		wrappedKeyName,
	)

	if err := watchWorker.Run(ctx, task.Work, true); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type PreparationDecryptTask struct {
	config                  *v1alpha1.PipelineConfig
	vaultURL                string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
	keyVaultKeyName         string
	wrappedKeyName          string
}

func NewPreparationDecryptTask(
	config *v1alpha1.PipelineConfig,
	vaultURL,
	inputContainerName,
	outputStorageAccountURL,
	outputContainerName,
	keyVaultKeyName,
	wrappedKeyName string,
) *PreparationDecryptTask {
	return &PreparationDecryptTask{
		config,
		vaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		keyVaultKeyName,
		wrappedKeyName,
	}
}

func (t *PreparationDecryptTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob

	log, ctx := hdhlog.FromContext(ctx).With("sourceBlobName", b.Name()).AttachToContext(ctx)

	log.Info("Start")

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "failed to get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}

	outputContainerClient, err := azure.NewContainerClient(t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		log.Error(err, "create Output container client")
		return fmt.Errorf(workTaskErr)
	}

	blobVersion := b.Metadata()["version"]
	if blobVersion == "v1" {
		log.Error(err, "Wrong version: v1 is not allowed")
		return fmt.Errorf(workTaskErr)
	}

	// Get wrapped key from encrypt
	wrappedKey, ok := b.Metadata()[t.wrappedKeyName]
	if !ok {
		log.Error(err, "finding wrapped key in the metadatas", "wrappedKeyName", t.wrappedKeyName)
		return fmt.Errorf(workTaskErr)
	}

	wk, err := base64.StdEncoding.DecodeString(wrappedKey)
	if err != nil {
		log.Error(err, "b64 decoding wrapped-key", "wrappedKeyName", t.wrappedKeyName)
		return fmt.Errorf(workTaskErr)
	}

	// Get vault key used to encrypt
	keyvaultKey, ok := b.Metadata()[t.keyVaultKeyName]
	if !ok {
		log.Error(err, "finding key name from blob's metadatas", "keyVaultKeyName", t.keyVaultKeyName)
		return fmt.Errorf(workTaskErr)
	}
	log = log.With("keyVaultKeyName", t.keyVaultKeyName)

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(t.vaultURL)
	if err != nil {
		log.Error(err, "get Key Vault client", "vaultURL", t.vaultURL)
		return fmt.Errorf(workTaskErr)
	}

	// Get keyvault key version ID from metadatas
	var keyvaultKeyVersionID string
	if b.Metadata()[v1alpha1.MetadataEncryptionKeyvaultKeyVersion] != "" {
		keyvaultKeyVersionID = b.Metadata()[v1alpha1.MetadataEncryptionKeyvaultKeyVersion]
	} else {
		keyvaultKeyVersionID, err = kvClient.GetKeyFirstVersionID(ctx, keyvaultKey)
		if err != nil {
			log.Error(err, "get Key Vault key oldest version", "vaultURL", t.vaultURL)
			return fmt.Errorf(workTaskErr)
		}
	}

	key, err := kvClient.UnWrap(ctx, keyvaultKey, keyvaultKeyVersionID, wk)
	if err != nil {
		log.Error(err, "unwrapping key")
		return fmt.Errorf(workTaskErr)
	}

	log.Info((b.Name()))
	decryptConfig, err := t.config.Pipeline.Decrypt.GetConfig(b.Name())
	if err != nil {
		log.Error(err, "retrieve Decrypt Config for blob")
		return fmt.Errorf(workTaskErr)
	}

	if decryptConfig == nil {
		log.Error(err, "no Decrypt Config matching blobName could be found")
		return fmt.Errorf(workTaskErr)
	}

	content, _, err := b.Download(ctx, cpk)
	if err != nil {
		log.Error(err, "can't download inblob")
		return fmt.Errorf(workTaskErr)
	}

	stream, err := hdhcrypt.GetStreamReader(key, content)
	if err != nil {
		log.Error(err, "get stream reader from blob")
		return fmt.Errorf(workTaskErr)
	}

	outputCompressionType := v1alpha1.CompressionTypeGzip
	inputCompressionType := v1alpha1.CompressionTypeGzip

	if b.Metadata()[v1alpha1.MetadataPlatformCompressionType] != "" {
		inputCompressionType = b.Metadata()[v1alpha1.MetadataPlatformCompressionType]
	}

	if decryptConfig.Input.Compression.Type != "" {
		inputCompressionType = strings.ToLower(decryptConfig.Input.Compression.Type)
	}

	if b.Metadata()[v1alpha1.MetadataPlatformCompressionType] != "" {
		outputCompressionType = b.Metadata()[v1alpha1.MetadataPlatformCompressionType]
	}

	if decryptConfig.Output.Compression.Type != "" {
		outputCompressionType = strings.ToLower(decryptConfig.Output.Compression.Type)
	}

	// Remove the encryption type in the name
	finalBlobName := strings.TrimSuffix(b.Name(), filepath.Ext(b.Name()))

	var processingReader io.Reader
	processingReader = stream
	if b.Metadata()["version"] == "v3" {
		if inputCompressionType == v1alpha1.CompressionTypeGzip && outputCompressionType == v1alpha1.CompressionTypeNone {
			// Remove the compression extension to correct it if required
			if strings.HasSuffix(finalBlobName, ".gz") {
				finalBlobName = strings.TrimSuffix(finalBlobName, filepath.Ext(finalBlobName))
			}
			log.Info("gunzip data")
			processingReader, err = gzip.NewReader(stream)
			if err != nil {
				log.Error(err, "gunzip for the blob")
				return fmt.Errorf(workTaskErr)
			}
		} else if inputCompressionType == v1alpha1.CompressionTypeNone && outputCompressionType == v1alpha1.CompressionTypeGzip {
			// add the correct extension
			if !strings.HasSuffix(finalBlobName, ".gz") {
				finalBlobName += ".gz"
			}
			log.Info("gzip data")
			processingReader, err = gzipIoReader(stream)
			if err != nil {
				log.Error(err, "gzip for the blob")
				return fmt.Errorf(workTaskErr)
			}

		}
		b.Metadata()[v1alpha1.MetadataPlatformCompressionType] = v1alpha1.CompressionTypeGzip
	} else {
		// Required for legacy management purpose, can be removed once we have no more hmac in producer encryption storage accounts.
		// This is used to keep the match between the CT and the blob's name. The CT should never be compressed, however its name should match the blob's name.
		if strings.Contains(t.outputContainerName, "ctable") {
			if inputCompressionType == v1alpha1.CompressionTypeGzip && outputCompressionType == v1alpha1.CompressionTypeNone {
				// Remove the compression extension to correct it if required
				if strings.HasSuffix(finalBlobName, ".gz") {
					finalBlobName = strings.TrimSuffix(finalBlobName, filepath.Ext(finalBlobName))
				}
			} else if inputCompressionType == v1alpha1.CompressionTypeNone && outputCompressionType == v1alpha1.CompressionTypeGzip {
				// add the correct extension
				if !strings.HasSuffix(finalBlobName, ".gz") {
					finalBlobName += ".gz"
				}
			}
		}

	}

	uploadBlob := b.Clone(ctx, outputContainerClient, finalBlobName)

	delete(uploadBlob.Metadata(), t.keyVaultKeyName)
	delete(uploadBlob.Metadata(), t.wrappedKeyName)
	delete(uploadBlob.Metadata(), v1alpha1.MetadataOriginalPath)
	delete(uploadBlob.Metadata(), v1alpha1.MetadataEncryptionKeyvaultKeyVersion)

	if outputCompressionType != "" && !strings.Contains(t.outputContainerName, "ctable") && b.Metadata()["version"] == "v3" {
		uploadBlob.Metadata()[v1alpha1.MetadataPlatformCompressionType] = outputCompressionType
	}
	if err := uploadBlob.Upload(ctx, processingReader, cpk); err != nil {
		log.Error(err, "can't upload uploadBlob")
		return fmt.Errorf(workTaskErr)
	}

	log.Info("successfully processed")
	return nil
}

func gzipIoReader(r io.Reader) (io.Reader, error) {
	pr, pw := io.Pipe()
	go func() {
		gw := gzip.NewWriter(pw)
		_, err := io.Copy(gw, r)
		if err != nil {
			return
		}
		if err := gw.Close(); err != nil {
			return
		}
		pw.Close()
	}()
	return pr, nil
}
