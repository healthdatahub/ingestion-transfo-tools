package main

import (
	"github.com/spf13/cobra"
)

var producerCmd = &cobra.Command{
	Use:   "producer",
	Short: "Base command for all producer related commands",
}

func init() {
	rootCmd.AddCommand(producerCmd)
}
