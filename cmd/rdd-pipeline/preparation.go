package main

import (
	"github.com/spf13/cobra"
)

var preparationCmd = &cobra.Command{
	Use:   "preparation",
	Short: "Base command for all preparation related commands",
}

func init() {
	rootCmd.AddCommand(preparationCmd)
}
