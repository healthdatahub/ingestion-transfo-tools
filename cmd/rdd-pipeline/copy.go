package main

import (
	"github.com/spf13/cobra"
)

// copyCmd represents the copy command.
var copyCmd = &cobra.Command{
	Use:   "copy",
	Short: "Copy a blob",
	RunE:  runCopy,
	Example: `rdd-pipeline copy \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--warn-size 9999`,
}

func init() {
	initMoveOrCopy(copyCmd)
}

func runCopy(cmd *cobra.Command, args []string) error {
	return runMoveOrCopy(cmd, false)
}
