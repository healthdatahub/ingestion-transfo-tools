package main

import (
	"context"
	"fmt"
	"os"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/spf13/cobra"
)

// moveCmd represents the copy command.
var moveCmd = &cobra.Command{
	Use:   "move",
	Short: "Move a blob",
	RunE:  runMove,
	Example: `rdd-pipeline move \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--warn-size 9999`,
}

func init() {
	initMoveOrCopy(moveCmd)
}

func runMove(cmd *cobra.Command, args []string) error {
	return runMoveOrCopy(cmd, true)
}

func initMoveOrCopy(cmd *cobra.Command) {
	rootCmd.AddCommand(cmd)

	common.AddInputBlobFlags(cmd)
	common.AddOutputBlobFlags(cmd)

	cmd.Flags().Int64("warn-size", 0, "warn if file size is over")

	cmd.Flags().Bool("watch", false, "true to run the command as a watcher; otherwise false (default to flase)")

	cmd.Flags().Bool("passthrough-only", false, "true to move or copy ONLY blobs flaged as 'passthrough'; otherwise all blobs will be processed (default to flase)")

	common.AddNoCPKFlags(cmd)

	common.AddMetadataFilterFlag(cmd)
}

func runMoveOrCopy(cmd *cobra.Command, deleteInputBlob bool) error {
	ctx := cmd.Context()
	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	noCPKForInput, _ := cmd.Flags().GetBool(common.NoCPKForInputFlag)
	noCPKForOutput, _ := cmd.Flags().GetBool(common.NoCPKForOutputFlag)
	runAsWatcher, _ := cmd.Flags().GetBool("watch")
	passthroughOnly, _ := cmd.Flags().GetBool("passthrough-only")
	warnSize, _ := cmd.Flags().GetInt64("warn-size")

	setMetadataForInput, metadata, err := common.GetMetadataFilter(cmd)
	if err != nil {
		log.Error(err, "GetMetadataFilter", "cmd", cmd)
		os.Exit(1)
	}

	requiredMetadata := map[string]string{}
	requiredTags := map[string]string{}

	if passthroughOnly {
		requiredMetadata[v1alpha1.MetadataBlobType] = string(v1alpha1.PassThrough)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(inputStorageAccountURL, inputContainerName, requiredMetadata, metadata, requiredTags)

	// start executing on blob send from watcher
	task := NewMoveTask(
		inputStorageAccountURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		noCPKForInput,
		noCPKForOutput,
		runAsWatcher,
		deleteInputBlob,
		setMetadataForInput,
		metadata,
		warnSize,
	)
	if err := watchWorker.Run(ctx, task.Work, !runAsWatcher); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type moveTask struct {
	metadata                map[string]string
	inputStorageAccountURL  string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
	warnSize                int64
	noCPKForOutput          bool
	runAsWatcher            bool
	deleteInputBlob         bool
	setMetadataForInput     bool
	noCPKForInput           bool
}

func NewMoveTask(inputStorageAccountURL, inputContainerName, outputStorageAccountURL, outputContainerName string,
	noCPKForInput, noCPKForOutput, runAsWatcher, deleteInputBlob, setMetadataForInput bool,
	metadata map[string]string, warnSize int64) *moveTask {
	return &moveTask{
		inputStorageAccountURL:  inputStorageAccountURL,
		inputContainerName:      inputContainerName,
		outputStorageAccountURL: outputStorageAccountURL,
		outputContainerName:     outputContainerName,
		noCPKForInput:           noCPKForInput,
		noCPKForOutput:          noCPKForOutput,
		runAsWatcher:            runAsWatcher,
		deleteInputBlob:         deleteInputBlob,
		setMetadataForInput:     setMetadataForInput,
		metadata:                metadata,
		warnSize:                warnSize,
	}
}

func (t *moveTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob

	log, ctx := hdhlog.FromContext(ctx).With("sourceBlobName", b.Name()).AttachToContext(ctx)

	log.Info("Start")

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}

	inputCPK := &blob.CPKInfo{
		EncryptionAlgorithm: to.Ptr(blob.EncryptionAlgorithmTypeNone),
	}
	if !t.noCPKForInput {
		inputCPK = cpk
	}

	outputCPK := &blob.CPKInfo{
		EncryptionAlgorithm: to.Ptr(blob.EncryptionAlgorithmTypeNone),
	}

	if !t.noCPKForOutput {
		outputCPK = cpk
	}

	outputContainerClient, err := azure.NewContainerClient(t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		log.Error(err, "create Output container client")
		return fmt.Errorf(workTaskErr)
	}

	// put lease on blob if metadata must be write or if moving
	if t.setMetadataForInput || t.deleteInputBlob {
		if err := b.AcquireLease(ctx); err != nil {
			log.Error(err, "acquire lease for blob")
			return fmt.Errorf(workTaskErr)
		}
		defer func() {
			err := b.ReleaseLease(context.Background())
			if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
				log.Error(err, "release lease failed")
			}
		}()
	}

	if t.warnSize != 0 && b.Size() > t.warnSize {
		log.Info("size exceed limit", "warnSize", t.warnSize, "size", b.Size())
	}

	content, _, err := b.Download(ctx, inputCPK)
	if err != nil {
		log.Error(err, "can't download inblob")
		return fmt.Errorf(workTaskErr)
	}
	defer content.Close()

	outputBlob := b.Clone(ctx, outputContainerClient, "")

	delete(outputBlob.Metadata(), v1alpha1.MetadataOriginalPath)

	if err := outputBlob.Upload(ctx, content, outputCPK); err != nil {
		log.Error(err, "upload hdh blob", "uploadBlobName", outputBlob.Name())
		return fmt.Errorf(workTaskErr)
	}

	if t.setMetadataForInput {
		for k, v := range t.metadata {
			b.Metadata()[k] = v
		}
		delete(b.Metadata(), v1alpha1.MetadataOriginalPath)
		if err := b.PushMetadata(ctx, inputCPK); err != nil {
			if err != nil && !bloberror.HasCode(err, bloberror.BlobNotFound) {
				log.Error(err, "set metadata on input blob")
			}
		}
	} else if t.deleteInputBlob {
		if err := b.Delete(ctx); err != nil {
			log.Error(err, "deleting")
		} else {
			log.Info("deleted successfully infile")
		}
	}

	log.Info("successfully processed")

	return nil
}
