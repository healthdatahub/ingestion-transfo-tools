package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/csv"
	"rdd-pipeline/pkg/dicomMask"
	"rdd-pipeline/pkg/hdhpackage"
	"rdd-pipeline/pkg/jsonMask"
	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/pairingPath"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/spf13/cobra"
)

var pairingCmd = &cobra.Command{
	Use:   "pairing",
	Short: "Pair data with a producer",
	RunE:  runPairing,
	Example: `rdd-pipeline preparation pairing \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--correspondence-table-storage-account-endpoint https://myCTStorageAccount.core.windows.net \
	--correspondence-table-container-name ctContainer \
	--correspondence-table-container-name ctContainer \
	--keyvault-endpoint https://myVault.vault.azure.net`,
}

func init() {
	preparationCmd.AddCommand(pairingCmd)

	common.AddInputBlobFlags(pairingCmd)
	common.AddOutputBlobFlags(pairingCmd)
	common.AddKeyVaultFlags(pairingCmd)

	pairingCmd.Flags().String(common.CTAccountFlag, "", "Blob storage account Name")
	(cobra.MarkFlagRequired(pairingCmd.Flags(), common.CTAccountFlag))

	pairingCmd.Flags().String(common.CTContainerFlag, "", "container name where files are stored")
	(cobra.MarkFlagRequired(pairingCmd.Flags(), common.CTContainerFlag))
}

func runPairing(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()
	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	ctStorageAccountURL, ctContainerName, err := common.GetCtStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetCtStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}
	keyvaultURL, _, err := common.GetKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetKeyvaultInfo", "cmd", cmd)
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(inputStorageAccountURL, inputContainerName, nil, nil, nil)

	task := NewPreparationPairingTask(
		outputStorageAccountURL,
		outputContainerName,
		inputContainerName,
		ctStorageAccountURL,
		ctContainerName,
		keyvaultURL,
	)

	// start executing on blob sent from watcher
	if err := watchWorker.Run(ctx, task.Work, false); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type PreparationPairingTask struct {
	outputStorageAccountURL string
	outputContainerName     string
	inputContainerName      string
	ctStorageAccountURL     string
	ctContainerName         string
	keyvaultURL             string
}

func NewPreparationPairingTask(
	outputStorageAccountURL,
	outputContainerName,
	inputContainerName,
	ctStorageAccountURL,
	ctContainerName string,
	keyvaultURL string,
) *PreparationPairingTask {
	return &PreparationPairingTask{
		outputStorageAccountURL,
		outputContainerName,
		inputContainerName,
		ctStorageAccountURL,
		ctContainerName,
		keyvaultURL,
	}
}

func (t *PreparationPairingTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob

	log, ctx := hdhlog.FromContext(ctx).With("sourceBlobName", b.Name()).AttachToContext(ctx)

	log.Info("Start")

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "failed to get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}

	inputCPK := cpk
	outputCPK := cpk

	outputContainerClient, err := azure.NewContainerClient(t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		log.Error(err, "create Output container client")
		return fmt.Errorf(workTaskErr)
	}

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(t.keyvaultURL)
	if err != nil {
		log.Error(err, "get Key Vault client", "keyvaultURL", t.keyvaultURL)
		return fmt.Errorf(workTaskErr)
	}
	blobType := b.Metadata()["type"]
	ct := new(masquage.CorrespondenceTable)

	hdhpack, err := hdhpackage.NewPackageFromBlob(ctx, b, inputCPK)
	if err != nil {
		log.Error(err, "creating hdhpackage")
		return fmt.Errorf(workTaskErr)
	}

	var pathConfig *v1alpha1.PathConfig
	config := hdhpack.Config()
	if config != nil && config.PATH != nil {
		pathConfig = config.PATH
	}

	// TODO: if empty error ?
	encryptionType := hdhpack.Metadata()[v1alpha1.MetadataMaskType]
	keySecretName := hdhpack.Metadata()[v1alpha1.MetadataMaskKeyvaultKey]

	var bKey []byte
	var ignoreValues []string

	// legacy format was value1::value2::value3::value4
	// but it was replaced by a json array to maximize compatibility.
	// In a few months, the legacy format handling will be removed.
	ignoreValuesMeta := b.Metadata()[v1alpha1.MetadataIgnoreValues]

	if ignoreValuesMeta != "" {
		if strings.Contains(ignoreValuesMeta, "::") {
			ignoreValues = strings.Split(ignoreValuesMeta, "::")
		} else {
			ignoreValuesDecoded, err := base64.StdEncoding.DecodeString(ignoreValuesMeta)
			if err != nil {
				log.Error(err, "cannot base64 decode ignore values", "ignoreValuesMeta", ignoreValuesMeta)
				return fmt.Errorf(workTaskErr)
			}
			if err := json.Unmarshal(ignoreValuesDecoded, &ignoreValues); err != nil {
				log.Error(err, "cannot unmarshal ignore values", "ignoreValuesMeta", ignoreValuesMeta)
				return fmt.Errorf(workTaskErr)
			}
		}
	}

	if keySecretName != "" {
		// Get keyvault mask secret version ID from metadatas
		// If there is no such metadata, we use the oldest secret in the keyvault
		var keySecretVersion string
		if b.Metadata()[v1alpha1.MetadataMaskKeyvaultKeyVersion] != "" {
			keySecretVersion = b.Metadata()[v1alpha1.MetadataMaskKeyvaultKeyVersion]
		} else {
			keySecretVersion, err = kvClient.GetSecretFirstVersionID(ctx, keySecretName)
			if err != nil {
				log.Error(err, "get Key Vault mask secret oldest version", "vaultURL", t.keyvaultURL)
				return fmt.Errorf(workTaskErr)
			}
		}
		if encryptionType == v1alpha1.Mask_aes {
			bKey, err = kvClient.GetSecret(ctx, keySecretName, keySecretVersion)
			if err != nil && (blobType != string(v1alpha1.Image)) && (blobType != string(v1alpha1.PassThrough)) {
				log.Error(err, "cannot retrieve hash key")
				return fmt.Errorf(workTaskErr)
			}
		}
	} else {
		// This could happen for blobs configured as passthrough or image with no path mask configured.
		log.Info("No hash secret name found for this blob. This may not be an error, depending on the blob hashconfig and type.")
	}

	// CT is required when:
	// We are in legacy mode, except for Image and Passthrough types where we need it only if there is a path mask used
	var ctBlob *worker.Blob
	fetchCt := false
	if encryptionType == v1alpha1.Mask_legacy &&
		((blobType != string(v1alpha1.Image) && blobType != string(v1alpha1.PassThrough)) ||
			((blobType == string(v1alpha1.Image) || blobType == string(v1alpha1.PassThrough)) && (pathConfig != nil && pathConfig.DepthLevel != nil))) {
		fetchCt = true
		ctContainerClient, err := azure.NewContainerClient(t.ctStorageAccountURL, t.ctContainerName)
		if err != nil {
			log.Error(err, "create CT container client")
			return fmt.Errorf(workTaskErr)
		}

		ctBlob, err = worker.ExistingBlob(ctx, ctContainerClient, b.Name(), cpk)
		if err != nil {
			log.Error(err, "retrieving CT")
			return fmt.Errorf(workTaskErr)
		}

		ctReader, _, err := ctBlob.Download(ctx, cpk)
		if err != nil {
			log.Error(err, "download CT")
			return fmt.Errorf(workTaskErr)
		}

		if err := json.NewDecoder(ctReader).Decode(ct); err != nil {
			log.Error(err, "failed to Unmarshall ct", "ctblobName", b.Name())
			return fmt.Errorf(workTaskErr)
		}

	} else {
		log.Info("Mask type doesn't require a CT, it will not be fetched")
	}

	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquire lease")
		return fmt.Errorf(workTaskErr)
	}
	defer func() {
		err := b.ReleaseLease(context.Background())
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	var reader io.Reader
	blobVersion := b.Metadata()["version"]
	if blobVersion == "v1" {
		log.Error(err, "wrong version: v1 is not allowed")
		return fmt.Errorf(workTaskErr)
	}

	switch blobType {
	case string(v1alpha1.CSV):
		log.Info("Run pairing CSV")

		var csvConfig *v1alpha1.CSVConfig

		if config := hdhpack.Config(); config != nil && config.CSV != nil {
			csvConfig = config.CSV
			log.Info("Read config from hdhpackage", "config", config)
		}

		log.Info("Use config for CSV pairing", "config", csvConfig)
		if csvConfig.Separator == "" {
			log.Error(err, "no separator found in config")
			return fmt.Errorf(workTaskErr)
		}

		if len(csvConfig.Columns) != 0 || len(csvConfig.ColumnNames) != 0 {
			reader = csv.NewPairingReader(log, hdhpack, csvConfig.Separator, csvConfig.SkipLines, csvConfig.Columns, csvConfig.ColumnNames, ignoreValues, *ct, encryptionType, string(bKey))
		} else {
			reader = hdhpack
		}
	case string(v1alpha1.PassThrough):
		log.Info("Run Move Task (file type is PassThrough)")
		reader = hdhpack
	case string(v1alpha1.Image):
		log.Info("Run Move Task (file type is Image)")
		reader = hdhpack
	case string(v1alpha1.Json):
		var jsonConfig *v1alpha1.JsonConfig

		if config := hdhpack.Config(); config != nil && config.Json != nil {
			jsonConfig = config.Json
			log.Info("Read config from hdhpackage", "config", config)
		}

		if len(jsonConfig.Tags) != 0 {
			reader = jsonMask.NewPairingReader(log, hdhpack, jsonConfig.Tags, ignoreValues, *ct, encryptionType, string(bKey))
		} else {
			reader = hdhpack
		}
	case string(v1alpha1.Dicom):
		log.Info("Run pairing Dicom")

		var dicomConfig *v1alpha1.DicomConfig

		if config := hdhpack.Config(); config != nil && config.Dicom != nil {
			dicomConfig = config.Dicom
			log.Info("Read config from hdhpackage", "config", config)
		}
		if len(dicomConfig.Tags) != 0 {
			reader = dicomMask.NewPairingReader(log, hdhpack, dicomConfig.Tags, ignoreValues, *ct, encryptionType, string(bKey))
		} else {
			reader = hdhpack
		}
	default:
		log.Error(err, "unsupported blob type", "blobType", blobType)
		return fmt.Errorf(workTaskErr)
	}

	currentBlobName := hdhpack.Name()

	if pathConfig != nil && pathConfig.DepthLevel != nil {
		log.Info("Path masking configuration found in the metadatas, the blob's name will be paired too")

		currentBlobName, err = pairingPath.Process(log, currentBlobName, *ct, pathConfig.Separator, encryptionType, string(bKey))
		if err != nil {
			log.Error(err, "pairing file name")
			return fmt.Errorf(workTaskErr)
		}
	} else {
		log.Info("No Path masking configuration found in the metadatas")
	}

	outFile := hdhpack.Clone(ctx, outputContainerClient, currentBlobName, reader).(hdhpackage.HDHPackage)
	if err := outFile.Upload(ctx, outputCPK); err != nil {
		log.Error(err, "upload hdh blob", "blobName", outFile.Name())
		return fmt.Errorf(workTaskErr)
	}

	if fetchCt {
		if err := ctBlob.Delete(ctx); err != nil {
			log.Error(err, "correspondence table deletion error")
		} else {
			log.Info("Correspondence table deleted successfully")
		}
	}
	if err := b.Delete(ctx); err != nil {
		log.Error(err, "Warning: Could not delete blob")
	} else {
		log.Info("Successfully blob deletion")
	}

	log.Info("successfully processed")

	return nil
}
