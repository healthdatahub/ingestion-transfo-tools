package main

import (
	"bytes"
	"context"
	"os/exec"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/pathMask"

	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"

	"rdd-pipeline/pkg/dicomMask"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/csv"
	"rdd-pipeline/pkg/hdhpackage"
	"rdd-pipeline/pkg/imageMask"
	"rdd-pipeline/pkg/jsonMask"
	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/masquage"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/azcore/to"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/spf13/cobra"
)

var maskCmd = &cobra.Command{
	Use:   "mask",
	Short: "Mask a blob",
	Example: `rdd-pipeline mask \
	--config-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \
	--config-container config \
	--config-blob-name hash.yaml \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myInputContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--correspondence-table-storage-account-endpoint https://myCTStorageAccount.core.windows.net \
	--correspondence-table-container-name myCTContainer \
	--output-blob-prefix HDH \
	--mask-prefix HDH \
	--auto-generate-key hmac512 \
	--keyvault-endpoint https://myVault.vault.azure.net`,
	RunE: runMask,
}

func init() {
	rootCmd.AddCommand(maskCmd)

	common.AddInputBlobFlags(maskCmd)
	common.AddOutputBlobFlags(maskCmd)
	common.AddKeyVaultFlags(maskCmd)
	common.AddConfigFlags(maskCmd)
	common.AddCTFlags(maskCmd)
	common.AddPrefixFlags(maskCmd)
	common.AddOutputBlobPrefixFlags(maskCmd)
	common.AddAutoGenerateKeyFlags(maskCmd)
	common.AddNoCPKFlags(maskCmd)

}

const leaseContainerName = "rddlease"
const hmac512 = "hmac512"

func runMask(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()
	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	ctStorageAccountURL, _ := cmd.Flags().GetString(common.CTAccountFlag)
	ctContainerName, _ := cmd.Flags().GetString(common.CTContainerFlag)
	noCPKForOutput, _ := cmd.Flags().GetBool(common.NoCPKForOutputFlag)
	maskPrefix, _ := cmd.Flags().GetString(common.MaskPrefixFlag)
	outputBlobPrefix, _ := cmd.Flags().GetString(common.OutputBlobPrefixFlag)
	autoGenerateKey, _ := cmd.Flags().GetString(common.AutoGenerateKeyFlag)
	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, _, err := common.GetKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetKeyvaultInfo", "cmd", cmd)
		os.Exit(1)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo", "cmd", cmd)
		os.Exit(1)
	}

	wConfig := worker.ConfigContainer{
		StorageAccountURL: configStorageAccountURL,
		ContainerName:     configContainerName,
		BlobName:          configBlobName,
	}
	pipelineConfig, err := wConfig.Download(ctx)
	if err != nil {
		log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
		os.Exit(1)
	}
	keySize := 32 // 32 bytes, by default

	// Only in when mask_aes is enabled or when we want to explicitely auto_generate the key from the flag
	if autoGenerateKey == hmac512 || pipelineConfig.Mask_AES && ctStorageAccountURL != "" && ctContainerName != "" {
		if autoGenerateKey == hmac512 {
			keySize = 64 // bytes
		}
		log.Info("Mask secret autogeneration enabled, ensuring the mask keys are present. If not, they will be created")
		// Lease the lease container while parsing it to create the missing keys
		// We lease the container because leasing the file would require write access on it.
		// https://learn.microsoft.com/en-us/rest/api/storageservices/authorize-with-azure-active-directory#permissions-for-calling-blob-and-queue-data-operations
		confContainerClient, err := azure.NewContainerClient(configStorageAccountURL, leaseContainerName)
		if err != nil {
			log.Error(err, "creating container client for config file")
			return fmt.Errorf(runMaskErr)
		}

		// We lease this container to ensure that only one thread in only one ACI will do this at a specific moment, therefore avoiding concurrency
		if err := azure.AcquireContainerLeaseWithLoop(ctx, confContainerClient); err != nil {
			log.Error(err, "Acquiring lease", "ConfigStorageAccountUrl", configStorageAccountURL, "leaseContainerName", leaseContainerName)
			return fmt.Errorf(runMaskErr)
		}

		// The defer is here in case we have an error
		defer func() {
			err := azure.BreakContainerLease(ctx, confContainerClient)
			if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
				log.Error(err, "release lease failed")
			}
		}()

		kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(keyvaultURL)
		if err != nil {
			log.Error(err, "get Key Vault client", "keyvaultURL", keyvaultURL)
			return fmt.Errorf(runMaskErr)
		}

		// Get all the keys listed in the mask_producer.yaml file
		// Then for each of them, ensure that it exists, if not, create it.
		for _, h := range pipelineConfig.Hash {
			log.Info("Ensuring that the following secret exists", "key", h.Key.Secret)
			if err := createSecretIfNotExist(ctx, kvClient, h.Key.Secret, keySize); err != nil {
				log.Error(err, "Cannot create secret")
				return fmt.Errorf("createSecretIfNotExist: %w", err)
			}
		}

		// This releaseLease is here in case we do NOT have an error
		if err := azure.BreakContainerLease(ctx, confContainerClient); err != nil {
			log.Error(err, "release lease failed")
		}
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainerWithConfig(
		inputStorageAccountURL,
		inputContainerName,
		pipelineConfig.Hash,
		nil, nil, nil, 0, 0)

	// start masking blob send from watcher
	task := NewMaskTask(
		pipelineConfig,
		keyvaultURL,
		outputStorageAccountURL,
		outputContainerName,
		inputContainerName,
		ctStorageAccountURL,
		ctContainerName,
		maskPrefix,
		noCPKForOutput,
		outputBlobPrefix,
	)

	if err := watchWorker.Run(ctx, task.Work, false); err != nil {
		log.Error(err, "running worker")
		return fmt.Errorf(runMaskErr)
	}
	return nil
}

type MaskTask struct {
	config                  *v1alpha1.PipelineConfig
	vaultURL                string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
	ctStorageAccountURL     string
	ctContainerName         string
	prefix                  string
	noCPKForOutput          bool
	outputBlobPrefix        string
}

func NewMaskTask(
	config *v1alpha1.PipelineConfig,
	vaultURL string,
	outputStorageAccountURL string,
	outputContainerName string,
	inputContainerName string,
	ctStorageAccountURL string,
	ctContainerName string,
	prefix string,
	noCPKForOutput bool,
	outputBlobPrefix string,
) *MaskTask {
	return &MaskTask{
		config:                  config,
		vaultURL:                vaultURL,
		outputStorageAccountURL: outputStorageAccountURL,
		inputContainerName:      inputContainerName,
		outputContainerName:     outputContainerName,
		ctStorageAccountURL:     ctStorageAccountURL,
		ctContainerName:         ctContainerName,
		prefix:                  prefix,
		noCPKForOutput:          noCPKForOutput,
		outputBlobPrefix:        outputBlobPrefix,
	}
}

func (t *MaskTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob

	blobName := b.Name()

	log, ctx := hdhlog.FromContext(ctx).With("sourceBlobName", blobName).AttachToContext(ctx)

	log.Info("Start")

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}

	inputCPK := cpk
	outputCPK := cpk
	if t.noCPKForOutput {
		outputCPK = &blob.CPKInfo{
			EncryptionAlgorithm: to.Ptr(blob.EncryptionAlgorithmTypeNone),
		}
		log.Info("Output blob will NOT be encrypted")
	}

	outputContainerClient, err := azure.NewContainerClient(t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		log.Error(err, "creating output container client")
		return fmt.Errorf(workTaskErr)
	}

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(t.vaultURL)
	if err != nil {
		log.Error(err, "get key vault client", "vaultURL", t.vaultURL)
		return fmt.Errorf(workTaskErr)
	}

	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquiring lease")
		return fmt.Errorf(workTaskErr)
	}
	defer func() {
		err := b.ReleaseLease(ctx)
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	inFile, err := hdhpackage.NewPackageFromBlob(ctx, b, inputCPK)
	if err != nil {
		log.Error(err, "can't build hdh package")
		return fmt.Errorf(workTaskErr)
	}

	// Local copy to avoid interfering with the other blobs being treated in parallel
	localConfig := t.config.Clone()
	hashConfig, err := localConfig.Hash.GetConfig(inFile.Name())

	if err != nil {
		log.Error(err, "retrieve Hash Config")
		return fmt.Errorf(workTaskErr)
	}

	if hashConfig == nil {
		log.Error(err, "no hahs config matching blobname could be found")
		return fmt.Errorf(workTaskErr)
	}

	log, ctx = log.With("hashConfig", hashConfig).AttachToContext(ctx)

	var reader io.Reader

	ct := masquage.CorrespondenceTable{}

	blobVersion := inFile.Metadata()["version"]
	if blobVersion == "v1" {
		log.Error(err, "wrong version: v1 is not allowed")
		return fmt.Errorf(workTaskErr)
	}

	blobType := inFile.Type()
	if blobType == "" {
		blobType = string(hashConfig.BlobType)
	}
	log, ctx = log.With("blobType", blobType).AttachToContext(ctx)

	// Define the type of encryption we want, available types are: legacy (HMAC with "correspondance table" aka CT), or AES
	// Two masks are applied in the producer pipeline
	// - the first one (called mask_hdh) MUST NOT BE of type AES because we don't want original values to be retrievable
	// - the second one (called mask_producer) can be either
	//      - HMAC/legacy called with CT flags (ctStorageAccountURL and ctContainerName)
	//      - AES.
	// This "if" weird condition ensures AES not be callable in the first mask
	// (ie if the value mask_AES is set to true in the configuration file)
	var encryptionType string
	if localConfig.Mask_AES && t.ctStorageAccountURL != "" && t.ctContainerName != "" {
		encryptionType = v1alpha1.Mask_aes
	} else {
		encryptionType = v1alpha1.Mask_legacy
	}

	// File name masking part, applied only if the path part is present in the config file or in the file metadata config, and if there is a separator and depthlevel part into it.
	var pathConfig *v1alpha1.PathConfig
	if config := inFile.Config(); config != nil && config.PATH != nil {
		pathConfig = config.PATH
		log.Info("Read config from hdhpackage", "config", config)
	} else {
		pathConfig = hashConfig.Default.PATH
		log.Info("Read config from config file", "config", hashConfig.Default)
	}

	var bKey []byte
	bKey, err = kvClient.GetSecret(ctx, hashConfig.Key.Secret, "") // context, secretName, secretVersion
	if err != nil && (blobType != string(v1alpha1.PassThrough) || pathConfig != nil) {
		log.Error(err, "cannot retrieve hash key")
		return fmt.Errorf(workTaskErr)
	}

	log.Info("encryption type and secret name", "secretName", hashConfig.Key.Secret, "encryptionType", encryptionType)
	ctFileName := ""

	if encryptionType == v1alpha1.Mask_aes && len(bKey) != 64 {
		log.Error(err, "the key is too small, required: 256 bits")
		return fmt.Errorf(workTaskErr)
	}

	// we first try to load ignore values from blob metadata
	// this occurs when a previous mask operation already happened
	// in this case we want to preserve the list of ignored values
	// legacy format was value1::value2::value3::value4
	// but it was replaced by a json array in base64 to maximize compatibility.
	// In a few months, the legacy format handling will be removed.
	var ignoreValues []string

	ignoreValuesMeta := inFile.Metadata()[v1alpha1.MetadataIgnoreValues]
	if ignoreValuesMeta != "" {
		if strings.Contains(ignoreValuesMeta, "::") {
			ignoreValues = strings.Split(ignoreValuesMeta, "::")
		} else {
			ignoreValuesDecoded, err := base64.StdEncoding.DecodeString(ignoreValuesMeta)
			if err != nil {
				log.Error(err, "cannot base64 decode ignore values", "ignoreValuesMeta", ignoreValuesMeta)
				return fmt.Errorf(workTaskErr)
			}
			if err := json.Unmarshal(ignoreValuesDecoded, &ignoreValues); err != nil {
				log.Error(err, "cannot unmarshal ignore values", "ignoreValuesMeta", ignoreValuesMeta)
				return fmt.Errorf(workTaskErr)
			}
		}
	} else
	// if we don't have ignore values from metadata, we count on standard config
	// either config from archive metadata or global config from config storage account
	{
		if config := inFile.Config(); config != nil && len(config.IgnoreValues) > 0 {
			ignoreValues = config.IgnoreValues
			log.Info("Read ignorevalues config from archive metadata", "config", config)
		} else {
			ignoreValues = hashConfig.Default.IgnoreValues
			log.Info("Read ignorevalues config from config file", "config", hashConfig.Default)
		}
	}
	var csvConfig *v1alpha1.CSVConfig
	var csvConfigSeparator string
	switch blobType {
	case string(v1alpha1.CSV):
		createCT := true
		log.Info("Run Mask CSV")

		if config := inFile.Config(); config != nil && config.CSV != nil {
			csvConfig = config.CSV
			log.Info("Read config from hdhpackage", "config", config)
		} else {
			csvConfig = hashConfig.Default.CSV
			log.Info("Read config from config file", "config", hashConfig.Default)
		}

		log.Info("Use config for CSV Hash", "config", csvConfig)

		if csvConfig == nil {
			log.Info(`[ERROR] No hashconfig found, please check your mask config file. Here is an example (case sensitivity is enabled):
apiversion: rdd-pipeline/v1alpha1
kind: Pipeline
mask_AES: true
mask:
- blob: "<Your regex here>"
  type: "CSV"
  key:
    secret: <secret name here>
  default:
    ignoreValues : ["N/A", "n/a"]
    csv:
      separator: ","
      columnNames: ["patient_id","patient_name"]
      skipLines: [1]
      localCt: true
`)
			log.Error(err, "hashconfig is empty (null)")
			return fmt.Errorf(workTaskErr)
		}

		inFile.Config().CSV = csvConfig

		if t.ctStorageAccountURL == "" && t.ctContainerName == "" {
			createCT = false
		} else {
			if csvConfig.LocalCt {
				ctFile, err := os.CreateTemp("", "ctfile")
				if err != nil {
					log.Error(err, "create temp file")
					return fmt.Errorf(workTaskErr)
				}

				_, err = ctFile.Write([]byte("{"))
				if err != nil {
					log.Error(err, "appending first '{' to the ct file")
					return fmt.Errorf(workTaskErr)
				}
				if err := ctFile.Close(); err != nil {
					log.Error(err, "close tmp file %w")
				}

				defer func() {
					if err := os.Remove(ctFile.Name()); err != nil {
						log.Error(err, "delete tmp file: %w")
					}
				}()

				ctFileName = ctFile.Name()
			}
		}

		if len(csvConfig.Columns) != 0 || len(csvConfig.ColumnNames) != 0 {
			reader, csvConfigSeparator = csv.NewHashReader(log, inFile, csvConfig.Separator, string(bKey), t.prefix, csvConfig.SkipLines, csvConfig.Columns, csvConfig.ColumnNames, ignoreValues, ct, createCT, ctFileName, encryptionType)

			if csvConfigSeparator == "" {
				log.Error(err, "We could not find any suitable automatically detected separator, please specify one")
				return fmt.Errorf(workTaskErr)
			}

			if csvConfigSeparator != csvConfig.Separator {
				csvConfig.Separator = csvConfigSeparator
			}
		} else {
			reader = inFile
		}
	case string(v1alpha1.Image):
		log.Info("Run Mask Image")
		r, err := imageMask.RemoveMetadatas(log, inFile)
		if r == nil && err != nil {
			log.Error(err, "Mask Image not process!")
			return fmt.Errorf(workTaskErr)
		}
		log.Info("Mask image process with success!")
		reader = r
	case string(v1alpha1.PassThrough):
		log.Info("Run Move Task (file type is PassThrough)")
		reader = inFile
	case string(v1alpha1.Json):
		log.Info("Run Mask Json")
		var jsonConfig *v1alpha1.JsonConfig

		if config := inFile.Config(); config != nil && config.Json != nil {
			jsonConfig = config.Json
			log.Info("Read config from hdhpackage", "config", config)
		} else {
			jsonConfig = hashConfig.Default.Json
			log.Info("Read config from config file", "config", hashConfig.Default)
		}

		if jsonConfig == nil {
			log.Info(`[ERROR] No hashconfig found, please check your mask config file. Here is an example (case sensitivity is enabled):
apiversion: rdd-pipeline/v1alpha1
kind: Pipeline
mask:
- blob: "<Your regex here>"
  type: "Json"
  key:
    secret: <secret name here>
  default:
    ignoreValues : ["N/A", "n/a"]
    json:
	  tags: ["$..userId", "$..birthdate"]
`)
			log.Error(err, "no hashconfig found, please check your mask config file")
			return fmt.Errorf(workTaskErr)
		}

		log.Info("Use config for Json Hash", "config", jsonConfig)
		inFile.Config().Json = jsonConfig

		if len(jsonConfig.Tags) != 0 {
			reader = jsonMask.NewHashReader(log, inFile, string(bKey), t.prefix, jsonConfig.Tags, ignoreValues, ct, t.ctContainerName, encryptionType)
		} else {
			reader = inFile
		}
	case string(v1alpha1.Dicom):
		log.Info("Run Mask Dicom")
		var dicomConfig *v1alpha1.DicomConfig

		if config := inFile.Config(); config != nil && config.Dicom != nil {
			dicomConfig = config.Dicom
			log.Info("Read config from hdhpackage", "config", config)
		} else {
			dicomConfig = hashConfig.Default.Dicom
			log.Info("Read config from config file", "config", hashConfig.Default)
		}

		if dicomConfig == nil {
			log.Info(`[ERROR] No hashconfig found, please check your mask config file. Here is an example (case sensitivity is enabled):
apiversion: rdd-pipeline/v1alpha1
kind: Pipeline
mask:
- blob: "<Your regex here>"
  type: "Dicom"
  key:
    secret: <secret name here>
  default:
    ignoreValues : ["N/A", "n/a"]
    dicom:
	  tags: ["0010,0020", "0010,0010"]
`)
			log.Error(err, "no hashconfig found, please check your mask config file")
			return fmt.Errorf(workTaskErr)
		}

		log.Info("Use config for Dicom Hash", "config", dicomConfig)
		inFile.Config().Dicom = dicomConfig

		if len(dicomConfig.Tags) != 0 {
			reader = dicomMask.NewHashReader(log, inFile, string(bKey), t.prefix, dicomConfig.Tags, ignoreValues, ct, encryptionType)
		} else {
			reader = inFile
		}
	default:
		log.Error(err, "unsupported blob type")
		return fmt.Errorf(workTaskErr)
	}

	// File name Masking
	var currentBlobName string
	currentBlobName = inFile.Name()
	if pathConfig != nil && pathConfig.DepthLevel != nil {
		log.Info("Path masking config found, proceeding.")

		if pathConfig.Separator == "" {
			log.Error(err, "no separator found in config")
			return fmt.Errorf(workTaskErr)
		}
		currentBlobName = pathMask.Process(log, currentBlobName, pathConfig.Separator, pathConfig.DepthLevel, ignoreValues, string(bKey), t.prefix, ct, ctFileName, encryptionType)
		inFile.Config().PATH = pathConfig
	} else {
		log.Info("No path masking config found, skipping this step.")
	}
	var outBlobName string
	if t.outputBlobPrefix != "" {
		outBlobName = t.outputBlobPrefix + "_" + currentBlobName
	} else {
		outBlobName = currentBlobName
	}

	outFile := inFile.Clone(ctx, outputContainerClient, outBlobName, reader).(hdhpackage.HDHPackage)
	outFile.Metadata()[v1alpha1.MetadataBlobType] = blobType
	outFile.Metadata()[v1alpha1.MetadataMaskType] = encryptionType

	if encryptionType == v1alpha1.Mask_aes {
		outFile.Metadata()[v1alpha1.MetadataMaskKeyvaultKey] = hashConfig.Key.Secret
		version, err := kvClient.GetSecretLastVersionID(ctx, hashConfig.Key.Secret)
		if err != nil {
			log.Error(err, "error while getting the mask secret version")
			return fmt.Errorf(workTaskErr)
		}
		outFile.Metadata()[v1alpha1.MetadataMaskKeyvaultKeyVersion] = version
	}
	// convert a slice of string into its JSON string representation
	jsonString, err := json.Marshal(ignoreValues)
	if err != nil {
		log.Error(err, "serializing ignore values")
		return fmt.Errorf(workTaskErr)
	}
	outFile.Metadata()[v1alpha1.MetadataIgnoreValues] = base64.StdEncoding.EncodeToString(jsonString)

	log.Info("outFile copy", "config", outFile.Config())

	if err := outFile.Upload(ctx, outputCPK); err != nil {
		log.Error(err, "upload hdh file")
		return fmt.Errorf(workTaskErr)
	}

	if t.ctStorageAccountURL != "" && t.ctContainerName != "" && encryptionType != v1alpha1.Mask_aes {
		ctContainerClient, err := azure.NewContainerClient(t.ctStorageAccountURL, t.ctContainerName)
		if err != nil {
			log.Error(err, "build CT container client from storage account")
			return fmt.Errorf(workTaskErr)
		}

		// ctFileName != "" means that we use a local file as ct. It needs to be modified to fit our needs (no duplicated lines, json format)
		if ctFileName != "" {
			ctFile, err := os.OpenFile(ctFileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
			if err != nil {
				log.Error(err, "opening tmp ct file")
				return fmt.Errorf(workTaskErr)
			}

			_, err = ctFile.Write([]byte("\n}"))
			if err != nil {
				log.Error(err, "appending last '}' to the ct file")
				return fmt.Errorf(workTaskErr)
			}

			if err := ctFile.Close(); err != nil {
				log.Error(err, "close tmp file %w")
				return fmt.Errorf(workTaskErr)
			}

			// Apply Uniq to the file
			cmd := exec.Command(
				"uniq",
				ctFileName)

			ctOutfile, err := os.Create(ctFileName + "_uniq")
			if err != nil {
				log.Error(err, "creating ct file name")
				return fmt.Errorf(workTaskErr)
			}

			defer func() {
				if err := os.Remove(ctOutfile.Name()); err != nil {
					log.Error(err, "delete tmp ct out file: %w")
				}
			}()

			var stderr bytes.Buffer
			cmd.Stdout = ctOutfile
			cmd.Stderr = &stderr
			err = cmd.Run()
			ctOutfile.Close()

			if err != nil {
				log.Error(err, "during uniq call for the ct file")
				return fmt.Errorf(workTaskErr)
			}

			file, err := os.Open(ctOutfile.Name())
			if err != nil {
				log.Error(err, "opening the ct file for transfer")
				return fmt.Errorf(workTaskErr)
			}
			ctBlob := worker.NewBlob(ctContainerClient, outBlobName, nil, nil)
			if err := ctBlob.Upload(ctx, file, outputCPK); err != nil {
				log.Error(err, "upload ct data", "outBlobName", outBlobName)
				return fmt.Errorf(workTaskErr)
			}

		} else {
			var ctEncoder bytes.Buffer
			err = json.NewEncoder(&ctEncoder).Encode(ct)
			if err != nil {
				log.Error(err, "create ct json file")
				return fmt.Errorf(workTaskErr)
			}
			ctBlob := worker.NewBlob(ctContainerClient, outBlobName, nil, nil)
			if err := ctBlob.Upload(ctx, &ctEncoder, outputCPK); err != nil {
				log.Error(err, "upload ct data", "outBlobName", outBlobName)
				return fmt.Errorf(workTaskErr)
			}
			log.Info("ct processed successfully", "entries", len(ct))
		}
	}

	if err := inFile.Delete(ctx); err != nil {
		log.Error(err, "Could not delete")
		return fmt.Errorf(workTaskErr)
	} else {
		log.Info("deleted successfully infile")
	}

	log.Info("successfully processed")
	return nil
}
