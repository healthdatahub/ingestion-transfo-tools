package main

import (
	"github.com/spf13/cobra"
)

var deliveryCmd = &cobra.Command{
	Use:   "delivery",
	Short: "Base command for all delivery related commands",
}

func init() {
	rootCmd.AddCommand(deliveryCmd)
}
