package main

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"fmt"
	"net/http"
	"rdd-pipeline/pkg/keyvault"
	"strings"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/go-autorest/autorest"
)

// createSecretIfNotExist checks if a key exists in a vault
// if not, a new keySize bytes key is created at this place.
func createSecretIfNotExist(ctx context.Context, kvClient *keyvault.KeyVaultClient, key string, keySize int) error {
	log := hdhlog.FromContext(ctx)
	_, err := kvClient.GetSecret(ctx, key, "")
	if err != nil && strings.Contains(err.Error(), "was not found in this key vault") {
		bKey := make([]byte, keySize)
		if _, err := rand.Read(bKey); err != nil {
			log.Error(err, "Creating the key")
			panic(err.Error())
		}
		log.Info("secret not found, creating it", "secretName", key, "vaultUrl", kvClient.KvURL)
		err = kvClient.SetSecret(ctx, key, hex.EncodeToString(bKey))
		if err != nil {
			log.Error(err, "Cannot create secret")
			return fmt.Errorf(runMaskErr)
		}
	} else if err != nil {
		log.Error(err, "Cannot create secret")
		return fmt.Errorf(runMaskErr)
	}
	return nil
}

// createVaultKeyIfNotExist checks if a key exists in a vault
// if not, a new 32 bytes key is created at this place.
func createKeyVaultKeyIfNotExist(ctx context.Context, kvClient *keyvault.KeyVaultClient, key string) error {
	log := hdhlog.FromContext(ctx)

	_, err := kvClient.GetKey(ctx, key)
	if err != nil {
		if azerr, ok := errors.Unwrap(err).(autorest.DetailedError); ok && azerr.Response.StatusCode == http.StatusNotFound {
			log.Info("vault key not found, creating it", "keyname", key, "vaultUrl", kvClient.KvURL)
			if err := kvClient.CreateKey(ctx, key); err != nil {
				log.Error(err, "Cannot create vault key")
				return fmt.Errorf(runMaskErr)
			}
		} else {
			log.Error(err, "Cannot create vault key")
			return fmt.Errorf(runMaskErr)
		}
	}
	return nil
}
