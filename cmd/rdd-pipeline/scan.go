package main

import (
	"context"
	"fmt"
	"os"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/clamav"
	"rdd-pipeline/pkg/worker"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/spf13/cobra"
)

var producerScanCmd = &cobra.Command{
	Use:   "scan",
	Short: "Scan a file with ClamAV",
	RunE:  runScan,
	Example: `rdd-pipeline scan \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container inputContainer \
	--output-storage-account-endpoint https://myRetentionStorageAccount.core.windows.net \
	--output-container retentionContainer \
	--quarantine-storage-account-endpoint https://myQuarantineStorageAccount.core.windows.net \
	--quarantine-container quarantineContainer`,
}

func init() {
	rootCmd.AddCommand(producerScanCmd)

	common.AddInputBlobFlags(producerScanCmd)
	common.AddOutputBlobFlags(producerScanCmd)

	producerScanCmd.Flags().String(common.QuarantineAccountFlag, "", "Quarantine storage account endpoint")
	(cobra.MarkFlagRequired(producerScanCmd.Flags(), common.QuarantineAccountFlag))

	producerScanCmd.Flags().String(common.QuarantineContainerFlag, "", "Quarantine container to output the errored blob")
	(cobra.MarkFlagRequired(producerScanCmd.Flags(), common.QuarantineContainerFlag))

	common.AddNoCPKFlags(producerScanCmd)
}

func runScan(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	noCPKForInput, _ := cmd.Flags().GetBool(common.NoCPKForInputFlag)

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo")
		os.Exit(1)
	}

	retentionStorageAccountURL, retentionContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo")
		os.Exit(1)
	}

	quarantineStorageAccountURL, quarantineContainerName, err := common.GetQuarantineStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetQuarantineStorageInfo")
		os.Exit(1)
	}

	scanner, err := clamav.New(log)
	if err != nil {
		log.Error(err, "initializing clamav")
		os.Exit(1)
	}

	if exitCode, err := scanner.Update(); err != nil {
		log.Error(err, "execute database update freshclam", "exitCode", exitCode)
		os.Exit(1)
	}

	log.Info("ready to start scanner", "scanner", *scanner)

	log.Info("starting clamav daemon")

	if err := scanner.StartClamd(); err != nil {
		log.Error(err, "starting clamd daemon")
		os.Exit(1)
	}

	log.Info("clamav daemon started")

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(
		inputStorageAccountURL,
		inputContainerName,
		nil, nil, nil)

	// start executing on blob sent from watcher
	task := NewScanTask(
		scanner,
		retentionStorageAccountURL,
		retentionContainerName,
		inputContainerName,
		quarantineStorageAccountURL,
		quarantineContainerName,
		noCPKForInput,
	)

	if err := watchWorker.Run(ctx, task.Work, false); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type ScanTask struct {
	scanner                     *clamav.Scanner
	retentionStorageAccountURL  string
	retentionContainerName      string
	inputContainerName          string
	quarantineStorageAccountURL string
	quarantineContainerName     string
	noCPKForInput               bool
}

func NewScanTask(
	scanner *clamav.Scanner,
	retentionStorageAccountURL,
	retentionContainerName,
	inputContainerName,
	quarantineStorageAccountURL,
	quarantineContainerName string,
	noCPKForInput bool,
) *ScanTask {
	return &ScanTask{
		retentionStorageAccountURL:  retentionStorageAccountURL,
		retentionContainerName:      retentionContainerName,
		inputContainerName:          inputContainerName,
		quarantineStorageAccountURL: quarantineStorageAccountURL,
		quarantineContainerName:     quarantineContainerName,
		scanner:                     scanner,
		noCPKForInput:               noCPKForInput,
	}
}

func (t *ScanTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob

	log, ctx := hdhlog.FromContext(ctx).With("blobName", b.Name()).AttachToContext(ctx)

	log.Info("Start")

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}

	retentionContainerClient, err := azure.NewContainerClient(t.retentionStorageAccountURL, t.retentionContainerName)
	if err != nil {
		log.Error(err, "create Retention container client", "retentionStorageAccountURL", t.retentionStorageAccountURL, "retentionContainerName", t.retentionContainerName)
		return fmt.Errorf(workTaskErr)
	}

	quarantineContainerClient, err := azure.NewContainerClient(t.quarantineStorageAccountURL, t.quarantineContainerName)
	if err != nil {
		log.Error(err, "create Quarantine container client", "quarantineStorageAccountURL", t.quarantineStorageAccountURL, "quarantineContainerName", t.quarantineContainerName)
		return fmt.Errorf(workTaskErr)
	}

	var inputCPK *blob.CPKInfo
	if !t.noCPKForInput {
		inputCPK = cpk
	}

	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquiring lease for blob", "b.Name()", b.Name())
		return fmt.Errorf(workTaskErr)
	}
	defer func() {
		err := b.ReleaseLease(context.Background())
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	file, err := os.CreateTemp("", "scanfile")
	if err != nil {
		log.Error(err, "creating scan temp file")
		return fmt.Errorf(workTaskErr)
	}

	log.Info("create file for scan", "filename", file.Name())

	defer func() {
		if err := file.Close(); err != nil {
			log.Error(err, "close tmp file", "file", file.Name())
		}

		if err := os.Remove(file.Name()); err != nil {
			log.Error(err, "delete tmp file", "file", file.Name())
		}
	}()

	if err := file.Chmod(0o644); err != nil {
		log.Error(err, "chmod scan temp file")
		return fmt.Errorf(workTaskErr)
	}

	content, _, err := b.Download(ctx, inputCPK)
	if err != nil {
		log.Error(err, "can't download inblob")
		return fmt.Errorf(workTaskErr)
	}

	if _, err := file.ReadFrom(content); err != nil {
		log.Error(err, "reading from stream file")
		return fmt.Errorf(workTaskErr)
	}

	if err := file.Sync(); err != nil {
		log.Error(err, "syncing file")
		return fmt.Errorf(workTaskErr)
	}

	// reset pointer to BOF
	_, err = file.Seek(0, 0)
	if err != nil {
		log.Error(err, "file seek failed")
		return fmt.Errorf(workTaskErr)
	}

	if exitCode, err := t.scanner.Scan(file.Name()); err != nil {
		switch exitCode {
		case 1:
			log.Error(err, "some virus founds")
			quarantineBlob := b.Clone(ctx, quarantineContainerClient, "") // nolint:contextcheck
			if err := b.RemoteCopy(ctx, quarantineBlob); err != nil {
				log.Error(err, "remote copy blob to quarantine")
				return fmt.Errorf(workTaskErr)
			}
		default:
			log.Error(err, "some errors occurred on blob", "exitCode", exitCode)
			return fmt.Errorf(workTaskErr)
		}
	} else {
		log.Info("Scan succeeded")

		retentionBlob := b.Clone(ctx, retentionContainerClient, "")
		if err := b.RemoteCopy(ctx, retentionBlob); err != nil {
			return fmt.Errorf("remote copy blob '%s' to retention: %w", retentionBlob.Name(), err)
		}
	}

	if err := b.Delete(ctx); err != nil {
		log.Error(err, "Could not delete blob")
	} else {
		log.Info("Successfully blob deletion")
	}

	log.Info("successfully processed")
	return nil
}
