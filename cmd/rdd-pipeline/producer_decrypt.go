package main

import (
	"bytes"
	"compress/gzip"
	"context"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/crypt/gpg"
	v3 "rdd-pipeline/pkg/hdhpackage/v3"
	"rdd-pipeline/pkg/keyvault"
	hdhlog "rdd-pipeline/pkg/log"
	"rdd-pipeline/pkg/preview"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	"github.com/mholt/archiver/v4"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"github.com/spf13/cobra"

	"github.com/ProtonMail/go-crypto/openpgp/armor"
)

var producerDecryptCmd = &cobra.Command{
	Use:   "decrypt",
	Short: "Decrypt data from the SFTP import",
	RunE:  runDecrypt,
	Example: `rdd-pipeline producer decrypt \
	--config-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \
	--config-container config \
	--config-blob-name decryption.yaml \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--keyvault-endpoint https://myVault.vault.azure.net \
	--backup-output-storage-account-endpoint https://myBackupOutputStorageAccount.core.windows.net \
	--backup-output-container myBackupOutputContainer \
	--backup-keyvault-endpoint https://myBackupVault.vault.azure.net \
	--backup-key-secret-name encrypt`,
}

func init() {
	producerCmd.AddCommand(producerDecryptCmd)

	common.AddInputBlobFlags(producerDecryptCmd)
	common.AddOutputBlobFlags(producerDecryptCmd)

	common.AddKeyVaultFlags(producerDecryptCmd)
	common.AddConfigFlags(producerDecryptCmd)

	common.AddBackupFlags(producerDecryptCmd)

	common.AddMetadataFlags(producerDecryptCmd)
}

func runDecrypt(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()
	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	metadataStorageAccountURL, metadataContainerName, err := common.GetMetadataStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetMetadataStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, _, err := common.GetKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetKeyvaultInfo", "cmd", cmd)
		os.Exit(1)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo", "cmd", cmd)
		os.Exit(1)
	}

	wConfig := worker.ConfigContainer{
		StorageAccountURL: configStorageAccountURL,
		ContainerName:     configContainerName,
		BlobName:          configBlobName,
	}
	pipelineConfig, err := wConfig.Download(ctx)
	if err != nil {
		log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainerWithConfig(
		inputStorageAccountURL,
		inputContainerName,
		pipelineConfig.Decrypt,
		nil, nil, nil, 0, 0)

	// fetch information to fill backup storage.
	backupOutputStorageAccountURL, backupOutputContainerName, err := common.GetBackupOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	backupKeyvaultURL, backupKeySecretName, err := common.GetBackupKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetKeyvaultInfo", "cmd", cmd)
		os.Exit(1)
	}

	backupKVClient, err := keyvault.NewKeyVaultClientFromEnvironment(backupKeyvaultURL)
	if err != nil {
		log.Error(err, "get Key backup Vault client", "backupKeyvaultURL", backupKeyvaultURL)
		os.Exit(1)
	}

	if err := createKeyVaultKeyIfNotExist(ctx, backupKVClient, backupKeySecretName); err != nil {
		log.Error(err, "cannot create keyvault key")
		os.Exit(1)
	}

	backupMetadataKVKey, backupMetadataWrappedKey, err := common.GetMetadataKeys()
	if err != nil {
		log.Error(err, "retrieve flags")
		os.Exit(1)
	}

	backupContainerClient, err := azure.NewContainerClient(backupOutputStorageAccountURL, backupOutputContainerName)
	if err != nil {
		log.Error(err, "create backup container client")
		return fmt.Errorf("creating backup container client: %w", err)
	}

	metadataContainerClient, err := azure.NewContainerClient(metadataStorageAccountURL, metadataContainerName)
	if err != nil {
		log.Error(err, "create metadata container client")
		return fmt.Errorf("creating metadata container client: %w", err)
	}

	outputContainerClient, err := azure.NewContainerClient(outputStorageAccountURL, outputContainerName)
	if err != nil {
		log.Error(err, "create output container client")
		return fmt.Errorf("creating output container client: %w", err)
	}

	configContainerClient, err := azure.NewContainerClient(configStorageAccountURL, configContainerName)
	if err != nil {
		log.Error(err, "create config container client")
		return fmt.Errorf("creating config container client: %w", err)
	}

	// start executing on blob sent from watcher
	task := NewProducerDecryptTask(
		pipelineConfig,
		keyvaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		backupOutputStorageAccountURL,
		backupOutputContainerName,
		backupKeyvaultURL,
		backupKeySecretName,
		backupMetadataKVKey,
		backupMetadataWrappedKey,
		metadataStorageAccountURL,
		metadataContainerName,
		backupContainerClient,
		metadataContainerClient,
		outputContainerClient,
		configContainerClient,
	)

	if err := watchWorker.Run(ctx, task.Work, false); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}

	return nil
}

type ProducerDecryptTask struct {
	config                  *v1alpha1.PipelineConfig
	vaultURL                string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string

	backupOutputStorageAccountURL string
	backupOutputContainerName     string
	backupVaultURL                string
	backupVaultSecretName         string
	backupMetadataKVKey           string
	backupMetadataWrappedKey      string

	metadataStorageAccountURL string
	metadataContainerName     string

	backupContainerClient   *container.Client
	metadataContainerClient *container.Client
	outputContainerClient   *container.Client
	configContainerClient   *container.Client
}

func NewProducerDecryptTask(
	pipelineConfig *v1alpha1.PipelineConfig,
	keyvaultURL,
	inputContainerName,
	outputStorageAccountURL,
	outputContainerName,
	backupOutputStorageAccountURL,
	backupOutputContainerName,
	backupVaultURL,
	backupVaultSecretName,
	backupMetadataKVKey,
	backupMetadataWrappedKey,
	metadataStorageAccountURL,
	metadataContainerName string,
	backupContainerClient *container.Client,
	metadataContainerClient *container.Client,
	outputContainerClient *container.Client,
	configContainerClient *container.Client,
) *ProducerDecryptTask {
	return &ProducerDecryptTask{
		pipelineConfig,
		keyvaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		backupOutputStorageAccountURL,
		backupOutputContainerName,
		backupVaultURL,
		backupVaultSecretName,
		backupMetadataKVKey,
		backupMetadataWrappedKey,
		metadataStorageAccountURL,
		metadataContainerName,
		backupContainerClient,
		metadataContainerClient,
		outputContainerClient,
		configContainerClient,
	}
}

func (t *ProducerDecryptTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob
	log, ctx := hdhlog.FromContext(ctx).With("blobName", b.Name()).AttachToContext(ctx)

	log.Info("Start")

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "failed to get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}

	inputCPK := cpk
	outputCPK := cpk

	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquiring lease for")
		return fmt.Errorf(workTaskErr)
	}
	defer func() {
		err := b.ReleaseLease(ctx)
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	decryptConfig, err := t.config.Pipeline.Decrypt.GetConfig(b.Name())
	if err != nil {
		log.Error(err, "retrieve Decrypt Config for blob")
		return fmt.Errorf(workTaskErr)
	}

	if decryptConfig == nil {
		log.Error(err, "no Decrypt Config matching blobName could be found")
		return fmt.Errorf(workTaskErr)
	}

	content, _, err := b.Download(ctx, inputCPK)
	if err != nil {
		log.Error(err, "can't download inblob")
		return fmt.Errorf(workTaskErr)
	}

	// Subcase: The blob section does not have a specific "gpg" part, so we apply the default one
	if decryptConfig.GPG.SignatureKey.Secret == "" && decryptConfig.GPG.DecryptKey.Secret == "" {
		log.Info("No specific GPG configuration found for the blob, applying DefaultGPG")
		decryptConfig.GPG = t.config.Pipeline.DefaultGPG
	} else {
		// Subcase required when the blob regex has been assigned with defaultgpg decryption configuration
		// Without this subcase, we can't detect that the decryptConfig has previously been assigned with the defaultgpg value
		// caveat: if a custom config matches the default config, we will have the "applying DefaultGPG" message
		if decryptConfig.GPG == t.config.Pipeline.DefaultGPG {
			log.Info("No specific GPG configuration found for the blob, applying DefaultGPG")
		}
	}

	log.Info("current GPG configuration for the blob", "decryptConfig.GPG", decryptConfig.GPG)
	var compressedData io.Reader
	if (decryptConfig.GPG.SignatureKey.Secret != "" || decryptConfig.GPG.SignatureKey.Blob != "") &&
		decryptConfig.GPG.DecryptKey.Secret != "" &&
		decryptConfig.GPG.DecryptKey.PassphraseSecret != "" {

		// Check if the header is armored by reading the beginning of the blob
		strArmoredHeader := "-----BEGIN PGP MESSAGE-----"
		bytesToRead := 50
		readBytes, err := io.ReadAll(io.LimitReader(content, int64(bytesToRead)))
		if err != nil {
			log.Error(err, "reading the PGP header")
			return fmt.Errorf(workTaskErr)
		}

		var armored bool
		if strings.Contains(string(readBytes), strArmoredHeader) {
			armored = true
			log.Info("blob is armored")
		} else {
			armored = false
			log.Info("blob is not armored")
		}
		// reset the reader
		content, _, err := b.Download(ctx, inputCPK)
		if err != nil {
			log.Error(err, "can't download inblob (after reading the PGP headers)")
			return fmt.Errorf(workTaskErr)
		}

		kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(t.vaultURL)
		if err != nil {
			log.Error(err, "get Key Vault client")
			return fmt.Errorf(workTaskErr)
		}

		var bSignatureKey []byte
		// the signature key (public key) can be either uploaded as a blob or as a secret in Key Vault.
		// the existence of the blob takes precedence over the secret.
		if decryptConfig.GPG.SignatureKey.Blob != "" {
			resp, err := t.configContainerClient.
				NewBlockBlobClient(decryptConfig.GPG.SignatureKey.Blob).
				DownloadStream(ctx, &blob.DownloadStreamOptions{})
			if err != nil {
				log.Error(err, "download public key")
				return fmt.Errorf(workTaskErr)
			}
			bSignatureKey, err = io.ReadAll(resp.Body)
			if err != nil {
				log.Error(err, "streaming public key")
				return fmt.Errorf(workTaskErr)
			}
		} else {
			bSignatureKey, err = kvClient.GetSecret(ctx, decryptConfig.GPG.SignatureKey.Secret, "")
			if err != nil {
				log.Error(err, "open public key")
				return fmt.Errorf(workTaskErr)
			}
		}

		bPrivateKey, err := kvClient.GetSecret(ctx, decryptConfig.GPG.DecryptKey.Secret, "")
		if err != nil {
			log.Error(err, "open private key")
			return fmt.Errorf(workTaskErr)
		}

		var passphrase []byte
		if decryptConfig.GPG.DecryptKey.Password != "" {
			passphrase = []byte(decryptConfig.GPG.DecryptKey.Password)
		}

		if decryptConfig.GPG.DecryptKey.PassphraseSecret != "" {
			passphrase, err = kvClient.GetSecret(ctx, decryptConfig.GPG.DecryptKey.PassphraseSecret, "")
			if err != nil {
				log.Error(err, "open passphrase secret ")
				return fmt.Errorf(workTaskErr)
			}
		}

		// This does not seem optimal and could benefit from some improvements.
		// This part is added specifically for rar archives because of the behaviour of the new function that checks the signature
		// Without it, the signature is not checked before uploading blobs: since we are in a .rar archive, files are packaged one by one, but the signature check
		// requires to read the whole source file, therefore we add here doing this full-read as a pre-check only to ensure that the signature is correct, only for rar archives for now.
		if decryptConfig.Input.Compression.Type == v1alpha1.CompressionTypeRar {
			// Perform the GPG decryption and signature verification as a pre-check
			decryptedData, err := decryptGPG(ctx, content, bSignatureKey, bPrivateKey, passphrase, armored)
			if err != nil {
				log.Error(err, "decryptGPG failed for the RAR archive")
				return fmt.Errorf(workTaskErr)
			}

			// Force a final read to ensure any errors are captured
			buffer := make([]byte, 1024)
			for {
				_, err := decryptedData.Read(buffer)
				if err != nil && err != io.EOF {
					log.Error(err, "error during pre-check")
					return fmt.Errorf("error reading decrypted data: %w", err)
				}
				if err == io.EOF {
					break
				}
			}

			// Re-create the content stream from the original blob download after the pre-check
			content, _, err = b.Download(ctx, inputCPK)
			if err != nil {
				log.Error(err, "can't re-download inblob after GPG pre-check")
				return fmt.Errorf(workTaskErr)
			}
		}

		compressedData, err = decryptGPG(ctx, content, bSignatureKey, bPrivateKey, passphrase, armored)
		if err != nil {
			log.Error(err, "decryptGPG failed for the blob")
			return fmt.Errorf(workTaskErr)
		}
	} else {
		log.Error(err, "missing correct GPG configuration for the blob, please check your decryption.yaml file")
		return fmt.Errorf(workTaskErr)
	}

	var decompressedData io.Reader
	// Remove the .gpg/.pgp extension
	finalBlobName := b.Name()
	if strings.HasSuffix(finalBlobName, ".gpg") || strings.HasSuffix(finalBlobName, ".pgp") {
		finalBlobName = strings.TrimSuffix(finalBlobName, filepath.Ext(finalBlobName))
	}

	// Extract input compression type
	inputCompressionType := decryptConfig.Input.Compression.Type // new input compression field
	if inputCompressionType == "" {
		inputCompressionType = decryptConfig.Compression.Format
		// legacy input compression field, kept for retrocompatibility
	}

	switch inputCompressionType {
	case v1alpha1.CompressionTypeGzip:
		// Remove the compression extension to correct it if required
		if strings.HasSuffix(finalBlobName, ".gz") {
			finalBlobName = strings.TrimSuffix(finalBlobName, filepath.Ext(finalBlobName))
		}
		log.Info("gunzip data")
		decompressedData, err = gzip.NewReader(compressedData)
		if err != nil {
			log.Error(err, "gunzip for the blob")
			return fmt.Errorf(workTaskErr)
		}
		err = processUploads(ctx, log, finalBlobName, decompressedData, t.outputContainerClient, t.metadataContainerClient, t.backupContainerClient, decryptConfig, outputCPK, *b, t.backupVaultURL, t.backupVaultSecretName, t.backupMetadataKVKey, t.backupMetadataWrappedKey)
		if err != nil {
			return err
		}

	case v1alpha1.CompressionTypeRar:
		err = processRarArchive(ctx, log, compressedData, t.outputContainerClient, t.metadataContainerClient, t.backupContainerClient, decryptConfig, outputCPK, *b, t.backupVaultURL, t.backupVaultSecretName, t.backupMetadataKVKey, t.backupMetadataWrappedKey)
		if err != nil {
			log.Error(err, "processing RAR archive")
			return fmt.Errorf(workTaskErr)
		}

	default:
		err = processUploads(ctx, log, finalBlobName, compressedData, t.outputContainerClient, t.metadataContainerClient, t.backupContainerClient, decryptConfig, outputCPK, *b, t.backupVaultURL, t.backupVaultSecretName, t.backupMetadataKVKey, t.backupMetadataWrappedKey)
		if err != nil {
			return err
		}
	}

	if err := b.Delete(ctx); err != nil {
		log.Error(err, "Warning: Could not delete blob")
	} else {
		log.Info("Successfully blob deletion")
	}

	log.Info("successfully processed")
	return nil
}

func decryptGPG(ctx context.Context, content io.Reader, bSignatureKey, bPrivateKey, passphrase []byte, armored bool) (io.Reader, error) {
	log := hdhlog.FromContext(ctx)
	log.Info("Decrypt with GPG")

	signatureKey := bytes.NewReader(bSignatureKey)
	privateKey := bytes.NewReader(bPrivateKey)

	messageReader := content

	if armored {
		block, err := armor.Decode(content)
		if err != nil {
			return nil, err
		}
		messageReader = block.Body
	}

	data, err := gpg.Decrypt(log, messageReader, signatureKey, privateKey, passphrase)
	if err != nil {
		return nil, fmt.Errorf("decrypt data: %w", err)
	}

	return data, nil
}

func processRarArchive(
	ctx context.Context,
	log hdhlog.Logger,
	compressedData io.Reader,
	outputContainerClient *container.Client,
	metadataContainerClient *container.Client,
	backupContainerClient *container.Client,
	decryptConfig *v1alpha1.DecryptConfig,
	outputCPK *blob.CPKInfo,
	b worker.Blob,
	backupVaultURL string,
	backupVaultSecretName string,
	backupMetadataKVKey string,
	backupMetadataWrappedKey string,
) error {
	rar := archiver.Rar{
		ContinueOnError: false,
		Password:        "", // TODO: this parameter to handle password protected archives.
	}

	// handler function that will process each file in the archive
	handleFile := func(ctx context.Context, file archiver.File) error {
		log.Info("Processing file in archive", "fileName", file.NameInArchive)

		// Skip directories
		if file.IsDir() {
			return nil
		}
		reader, err := file.Open()
		if err != nil {
			log.Error(err, "unable to open file in RAR archive", "fileName", file.NameInArchive)
			return fmt.Errorf(workTaskErr)
		}
		defer reader.Close()

		// Process the file using the processUploads function
		err = processUploads(ctx, log, file.NameInArchive, reader, outputContainerClient, metadataContainerClient, backupContainerClient, decryptConfig, outputCPK, b, backupVaultURL, backupVaultSecretName, backupMetadataKVKey, backupMetadataWrappedKey)
		if err != nil {
			log.Error(err, "error processing file in RAR archive", "fileName", file.NameInArchive)
			return fmt.Errorf(workTaskErr)
		}

		return nil
	}

	// Extract the archive and handle each file using the above function
	err := rar.Extract(ctx, compressedData, nil, handleFile)
	if err != nil {
		log.Error(err, "unable to extract RAR archive")
		return fmt.Errorf(workTaskErr)
	}

	return nil
}

func processUploads(
	ctx context.Context,
	log hdhlog.Logger,
	blobName string,
	decompressedData io.Reader,
	outputContainerClient *container.Client,
	metadataContainerClient *container.Client,
	backupContainerClient *container.Client,
	decryptConfig *v1alpha1.DecryptConfig,
	outputCPK *blob.CPKInfo,
	b worker.Blob,
	backupVaultURL string,
	backupVaultSecretName string,
	backupMetadataKVKey string,
	backupMetadataWrappedKey string,
) error {

	metaReader, metaWriter := io.Pipe()
	teeReader := io.TeeReader(decompressedData, metaWriter)

	// For the goroutine outputs
	metaDataChannel := make(chan *preview.FilePreview)
	errorChannel := make(chan error)

	if decryptConfig.Output.Compression.Type != v1alpha1.CompressionTypeNone &&
		!strings.HasSuffix(blobName, v3.CompressionExtension) {
		blobName += v3.CompressionExtension
	}

	// Goroutine to read from metaReader concurrently
	go func() {
		filePreview, err := preview.NewPreviewFile(ctx, &b, metaReader)
		if err != nil {
			errorChannel <- err
			return
		}
		metaDataChannel <- filePreview
	}()

	// Clone the blob for output
	outBlob := b.Clone(ctx, outputContainerClient, blobName)

	// attach platform compression type to blob
	// for backward compat, we set a default value to gzip
	outBlob.Metadata()[v1alpha1.MetadataPlatformCompressionType] = v1alpha1.CompressionTypeGzip
	if decryptConfig.Output.Compression.Type != "" {
		outBlob.Metadata()[v1alpha1.MetadataPlatformCompressionType] = decryptConfig.Output.Compression.Type
	}

	// Create a new package
	outhdhpack, err := v3.New(ctx, teeReader, outBlob)
	if err != nil {
		log.Error(err, "can't create HDHPackage V3 from file")
		return fmt.Errorf(workTaskErr)
	}

	// Upload the package using the output container client
	if err := outhdhpack.Upload(ctx, outputCPK); err != nil {
		log.Error(err, "upload blob as stream")
		return fmt.Errorf(workTaskErr)
	}

	// Close the metadata writer
	if err := metaWriter.Close(); err != nil {
		log.Error(err, "error closing metaWriter")
		return fmt.Errorf(workTaskErr)
	}

	// Backup the file using the backupContainerClient
	backupBlob := outBlob.Clone(ctx, backupContainerClient, strings.TrimSuffix(blobName, ".gpg")+".gpg")
	reader, _, err := outBlob.Download(ctx, outputCPK)
	if err != nil {
		log.Error(err, "can't download inblob")
		return fmt.Errorf(workTaskErr)
	}
	if err := backupBlob.EncryptAndUpload(ctx, reader, backupVaultURL, backupVaultSecretName, backupMetadataKVKey, backupMetadataWrappedKey); err != nil {
		log.Error(err, "fail to copy blob to backup storage")
		return fmt.Errorf(workTaskErr)
	}

	select {
	case filePreview := <-metaDataChannel:
		err = filePreview.UploadFilePreview(ctx, metadataContainerClient, blobName)
		if err != nil {
			log.Error(err, "can't upload preview file")
			return fmt.Errorf(workTaskErr)
		}

	case err := <-errorChannel:
		log.Error(err, "fail to get metadatas and headers")
		return fmt.Errorf(workTaskErr)
	}

	log.Info("successfully processed", "blobName", blobName)
	return nil
}
