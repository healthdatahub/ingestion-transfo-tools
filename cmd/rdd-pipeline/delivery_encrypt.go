package main

import (
	"bytes"
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"os"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/crypt/gpg"
	"rdd-pipeline/pkg/keyvault"
	hdhlog "rdd-pipeline/pkg/log"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/blob"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"
	"github.com/spf13/cobra"
)

var deliveryEncryptCmd = &cobra.Command{
	Use:   "encrypt",
	Short: "compress encrypt and sign data to delivery",
	RunE:  runEncrypt,
	Example: `rdd-pipeline delivery encrypt \
	--config-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \
	--config-container config \
	--config-blob-name encrypt.yaml \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer \
	--key-identity-mail mail@mail.fr \
	--keyvault-endpoint https://myVault.vault.azure.net`,
}

func init() {
	deliveryCmd.AddCommand(deliveryEncryptCmd)

	common.AddInputBlobFlags(deliveryEncryptCmd)
	common.AddOutputBlobFlags(deliveryEncryptCmd)
	common.AddKeyIdentityFlag(deliveryEncryptCmd)

	common.AddKeyVaultFlags(deliveryEncryptCmd)
	common.AddConfigFlags(deliveryEncryptCmd)
}

func runEncrypt(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()
	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyIdentityMail, err := common.GetKeyIdentityMail(cmd)
	if err != nil {
		log.Error(err, "GetKeyIdentityMail", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, _, err := common.GetKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetKeyvaultInfo", "cmd", cmd)
		os.Exit(1)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo", "cmd", cmd)
		os.Exit(1)
	}

	wConfig := worker.ConfigContainer{
		StorageAccountURL: configStorageAccountURL,
		ContainerName:     configContainerName,
		BlobName:          configBlobName,
	}
	pipelineConfig, err := wConfig.Download(ctx)
	if err != nil {
		log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
		os.Exit(1)
	}

	log.Info("Ensuring the signature private key and passphrase are present. If not, they will be created")
	// Lease the lease container while parsing it to create the missing keys
	// We lease the container because leasing the file would require write access on it.
	// https://learn.microsoft.com/en-us/rest/api/storageservices/authorize-with-azure-active-directory#permissions-for-calling-blob-and-queue-data-operations
	confContainerClient, err := azure.NewContainerClient(configStorageAccountURL, leaseContainerName)
	if err != nil {
		log.Error(err, "creating container client for config file")
		return fmt.Errorf("errored during runEncrypt function")
	}

	// We lease this container to ensure that only one thread in only one ACI will do this at a specific moment, therefore avoiding concurrency
	if err := azure.AcquireContainerLeaseWithLoop(ctx, confContainerClient); err != nil {
		log.Error(err, "Acquiring lease", "ConfigStorageAccountUrl", configStorageAccountURL, "leaseContainerName", leaseContainerName)
		return fmt.Errorf("errored during runEncrypt function")
	}

	// The defer is here in case we have an error
	defer func() {
		err := azure.BreakContainerLease(ctx, confContainerClient)
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(keyvaultURL)
	if err != nil {
		log.Error(err, "get Key Vault client", "keyvaultURL", keyvaultURL)
		return fmt.Errorf("errored during runEncrypt function")
	}

	// Get all the keys listed in the encrypt.yaml file
	// Then for each of them, ensure that it exists alongside their passphrase and public keys, if not, create them.
	for _, de := range pipelineConfig.DeliveryEncrypt {
		log.Info("Ensuring that the following keys exist (public and private)", "key", de.GPG.SignatureKey.Secret)

		identity := keyIdentityMail
		keyType := "rsa"
		keySize := 4096
		privateKeyName := de.GPG.SignatureKey.Secret // the private key is not suffixed by "-private", it's easier that way for users to match with the config file's referenced secret
		publicKeyName := de.GPG.SignatureKey.Secret + "-public"

		// Check if the private key exists
		privateKeyExists, err := kvClient.SecretExists(ctx, privateKeyName)
		if err != nil {
			log.Error(err, "Error checking for private key existence")
			return fmt.Errorf("error checking for private key: %w", err)
		}

		// Check if the public key exists
		publicKeyExists, err := kvClient.SecretExists(ctx, publicKeyName)
		if err != nil {
			log.Error(err, "Error checking for public key existence")
			return fmt.Errorf("error checking for public key: %w", err)
		}

		if de.GPG.SignatureKey.PassphraseSecret == privateKeyName {
			return fmt.Errorf("Passphrase secret name is the same as the private key secret name, please make them different")
		}
		// Check if the passphrase exists
		passphraseExists, err := kvClient.SecretExists(ctx, de.GPG.SignatureKey.PassphraseSecret)
		if err != nil {
			log.Error(err, "Error checking for passphrase existence")
			return fmt.Errorf("error checking for passphrase: %w", err)
		}

		// Check if private key, public key, and passphrase existence are inconsistent (all should either exist or not)
		if privateKeyExists != publicKeyExists || privateKeyExists != passphraseExists || publicKeyExists != passphraseExists {
			log.Error(nil, "Inconsistent key presence: private key, public key, and passphrase should all be present, or none should",
				"privateKeyExists", privateKeyExists,
				"publicKeyExists", publicKeyExists,
				"passphraseExists", passphraseExists)
			return fmt.Errorf("inconsistent key presence in the keyvault")
		}

		// If all the secrets exist, skip key creation
		if privateKeyExists && passphraseExists && publicKeyExists {
			log.Info("All the secrets exist. Skipping key creation.")
			continue
		}

		// Generate a random passphrase
		bytes := make([]byte, 32)
		if _, err := rand.Read(bytes); err != nil {
			return fmt.Errorf("error generating random passphrase: %w", err)
		}

		passphrase := base64.URLEncoding.EncodeToString(bytes)
		// Generate GPG keys
		log.Info("Generating key", "identity", identity, "keyType", "keyType", "secretName", privateKeyName)
		privateKey, publicKey, err := gpg.GenerateGPGKeys(identity, keyType, keySize, passphrase)
		if err != nil {
			log.Error(err, "Cannot generate GPG keys")
			return fmt.Errorf("generateGPGKeys: %w", err)
		}

		// Upload the private key to the key vault
		err = kvClient.SetSecret(ctx, privateKeyName, privateKey)
		if err != nil {
			log.Error(err, "Cannot store private key in key vault")
			return fmt.Errorf("error storing private key: %w", err)
		}

		// Upload the passphrase key to the key vault
		err = kvClient.SetSecret(ctx, de.GPG.SignatureKey.PassphraseSecret, passphrase)
		if err != nil {
			log.Error(err, "Cannot store passphrase in key vault")
			return fmt.Errorf("error storing passphrase: %w", err)
		}

		// Upload the public key to the key vault
		err = kvClient.SetSecret(ctx, publicKeyName, publicKey)
		if err != nil {
			log.Error(err, "Cannot store public key in key vault")
			return fmt.Errorf("error storing public key: %w", err)
		}
	}

	// This releaseLease is here in case we do NOT have an error
	if err := azure.BreakContainerLease(ctx, confContainerClient); err != nil {
		log.Error(err, "release lease failed")
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainerWithConfig(
		inputStorageAccountURL,
		inputContainerName,
		pipelineConfig.DeliveryEncrypt,
		nil, nil, nil, 0, 0)

	outputContainerClient, err := azure.NewContainerClient(outputStorageAccountURL, outputContainerName)
	if err != nil {
		log.Error(err, "create output container client")
		return fmt.Errorf("creating output container client: %w", err)
	}

	configContainerClient, err := azure.NewContainerClient(configStorageAccountURL, configContainerName)
	if err != nil {
		log.Error(err, "create config container client")
		return fmt.Errorf("creating config container client: %w", err)
	}

	// start executing on blob sent from watcher
	task := NewdeliveryEncryptTask(
		pipelineConfig,
		keyvaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		outputContainerClient,
		configContainerClient,
	)

	if err := watchWorker.Run(ctx, task.Work, false); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}

	return nil
}

type deliveryEncryptTask struct {
	config                  *v1alpha1.PipelineConfig
	vaultURL                string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string

	outputContainerClient *container.Client
	configContainerClient *container.Client
}

func NewdeliveryEncryptTask(
	pipelineConfig *v1alpha1.PipelineConfig,
	keyvaultURL string,
	inputContainerName string,
	outputStorageAccountURL string,
	outputContainerName string,
	outputContainerClient *container.Client,
	configContainerClient *container.Client,
) *deliveryEncryptTask {
	return &deliveryEncryptTask{
		pipelineConfig,
		keyvaultURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		outputContainerClient,
		configContainerClient,
	}
}

func (t *deliveryEncryptTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob
	log, ctx := hdhlog.FromContext(ctx).With("blobName", b.Name()).AttachToContext(ctx)

	log.Info("Start")

	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquiring lease for")
		return fmt.Errorf(workTaskErr)
	}
	defer func() {
		err := b.ReleaseLease(ctx)
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	EncryptConfig, err := t.config.Pipeline.DeliveryEncrypt.GetConfig(b.Name())
	if err != nil {
		log.Error(err, "retrieve Encrypt Config for blob")
		return fmt.Errorf(workTaskErr)
	}

	if EncryptConfig == nil {
		log.Error(err, "no Encrypt Config matching blobName could be found")
		return fmt.Errorf(workTaskErr)
	}

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(t.vaultURL)
	if err != nil {
		log.Error(err, "get Key Vault client")
		return fmt.Errorf(workTaskErr)
	}

	var bSignatureKey []byte
	// signature key has two keys in the keyvault: the public and private one (we generate them). We sign with the private one, the public one has to be transmitted to the entity
	// that will receive the data
	privateSignatureKeyName := EncryptConfig.GPG.SignatureKey.Secret
	bSignatureKey, err = kvClient.GetSecret(ctx, privateSignatureKeyName, "")
	if err != nil {
		log.Error(err, "open signature key")
		return fmt.Errorf(workTaskErr)
	}

	// Encrypt key is a public one, it is the one of the entity that will receive the data. We do not generate this one.
	var bEncryptKey []byte
	// the encryption key (public key) can be either uploaded as a blob or as a secret in Key Vault.
	// the existence of the blob takes precedence over the secret.
	if EncryptConfig.GPG.EncryptKey.Blob != "" {
		resp, err := t.configContainerClient.
			NewBlockBlobClient(EncryptConfig.GPG.EncryptKey.Blob).
			DownloadStream(ctx, &blob.DownloadStreamOptions{})
		if err != nil {
			log.Error(err, "download public key")
			return fmt.Errorf(workTaskErr)
		}
		bEncryptKey, err = io.ReadAll(resp.Body)
		if err != nil {
			log.Error(err, "streaming public key")
			return fmt.Errorf(workTaskErr)
		}
	} else {
		bEncryptKey, err = kvClient.GetSecret(ctx, EncryptConfig.GPG.EncryptKey.Secret, "")
		if err != nil {
			log.Error(err, "open public key")
			return fmt.Errorf(workTaskErr)
		}
	}

	var passphrase []byte
	if EncryptConfig.GPG.SignatureKey.Password != "" {
		passphrase = []byte(EncryptConfig.GPG.SignatureKey.Password)
	}

	if EncryptConfig.GPG.SignatureKey.PassphraseSecret != "" {
		passphrase, err = kvClient.GetSecret(ctx, EncryptConfig.GPG.SignatureKey.PassphraseSecret, "")
		if err != nil {
			log.Error(err, "open signature key passphrase secret ")
			return fmt.Errorf(workTaskErr)
		}
	}

	outputBlob := b.Clone(ctx, t.outputContainerClient, b.Name()+".gpg")

	reader, _, err := b.Download(ctx, nil)
	if err != nil {
		log.Error(err, "can't download inblob")
		return fmt.Errorf(workTaskErr)
	}

	encryptKeyReader := bytes.NewReader(bEncryptKey)
	signatureKeyReader := bytes.NewReader(bSignatureKey)
	data, err := gpg.EncryptAndSign(log, reader, encryptKeyReader, signatureKeyReader, passphrase)
	if err != nil {
		return fmt.Errorf("encrypt data: %w", err)
	}

	// Upload the data to the blob
	err = outputBlob.Upload(ctx, data, nil)
	if err != nil {
		log.Error(err, "Failed to upload encrypted and signed blob")
		return fmt.Errorf("upload blob: %w", err)
	}

	if err := b.Delete(ctx); err != nil {
		log.Error(err, "Warning: Could not delete blob")
	} else {
		log.Info("Successful blob deletion")
	}

	log.Info("successfully processed")
	return nil
}
