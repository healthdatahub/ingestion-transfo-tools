package main

import (
	"context"
	"fmt"
	"os"
	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	v3 "rdd-pipeline/pkg/hdhpackage/v3"
	hdhlog "rdd-pipeline/pkg/log"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"
	"strings"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/spf13/cobra"
)

// prepConvertCmd represents the convert command in preparation env.
var prepConvertCmd = &cobra.Command{
	Use:   "convert",
	Short: "Convert a blob to passthrough hdhpackage v2",
	RunE:  runPrepConvert,
	Example: `rdd-pipeline preparation convert \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer`,
}

func init() {
	preparationCmd.AddCommand(prepConvertCmd)

	common.AddInputBlobFlags(prepConvertCmd)
	common.AddOutputBlobFlags(prepConvertCmd)
}

func runPrepConvert(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(inputStorageAccountURL, inputContainerName, nil, nil, nil)

	// start executing on blob sent from watcher
	task := NewPreparationConvertTask(outputStorageAccountURL, outputContainerName, inputContainerName)

	if err := watchWorker.Run(ctx, task.Work, true); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}
	return nil
}

type PreparationConvertTask struct {
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
}

func NewPreparationConvertTask(outputStorageAccountURL, outputContainerName, inputContainerName string) *PreparationConvertTask {
	return &PreparationConvertTask{
		inputContainerName:      inputContainerName,
		outputStorageAccountURL: outputStorageAccountURL,
		outputContainerName:     outputContainerName,
	}
}

func (t *PreparationConvertTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob

	log, ctx := hdhlog.FromContext(ctx).With("sourceBlobName", b.Name()).AttachToContext(ctx)

	log.Info("Start")

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}

	outputContainerClient, err := azure.NewContainerClient(t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		log.Error(err, "create Output container client")
		return fmt.Errorf(workTaskErr)
	}

	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquiring lease")
		return fmt.Errorf(workTaskErr)
	}
	defer func() {
		err := b.ReleaseLease(context.Background())
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	bContent, _, err := b.Download(ctx, cpk)
	if err != nil {
		log.Error(err, "downloading blob")
		return fmt.Errorf(workTaskErr)
	}
	defer bContent.Close()

	outputBlobName := b.Name()
	if !strings.HasSuffix(outputBlobName, v3.CompressionExtension) {
		outputBlobName += v3.CompressionExtension
	}

	outBlob := b.Clone(ctx, outputContainerClient, outputBlobName)
	outFile, err := v3.New(ctx, bContent, outBlob)
	if err != nil {
		log.Error(err, "can't create HDHPackage V3 from file")
		return fmt.Errorf(workTaskErr)
	}

	outFile.Metadata()[v1alpha1.MetadataVersion] = v3.Version

	if err := outFile.Upload(ctx, cpk); err != nil {
		log.Error(err, "upload blob as stream")
		return fmt.Errorf(workTaskErr)
	}

	log.Info("successfully processed")
	return nil
}
