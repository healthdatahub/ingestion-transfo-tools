package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/spf13/cobra"
)

var producerEncryptCmd = &cobra.Command{
	Use:   "encrypt",
	Short: "Encrypt a blob",
	RunE:  runHdhEncrypt,
	Example: `rdd-pipeline producer encrypt \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-container myOutputContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--catalog-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--catalog-container myOutputContainer \
	--catalog-blob-name catalog.csv
	--keyvault-endpoint https://myVault.vault.azure.net \
	--key-secret-name encrypt`,
}

func init() {
	producerCmd.AddCommand(producerEncryptCmd)

	common.AddInputBlobFlags(producerEncryptCmd)
	common.AddOutputBlobFlags(producerEncryptCmd)
	common.AddCatalogBlobFlags(producerEncryptCmd, false)
	common.AddKeyVaultFlags(producerEncryptCmd)
	common.AddNoCPKFlags(producerEncryptCmd)
	common.AddOptionalConfigFlags(producerEncryptCmd)
}

func runHdhEncrypt(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	noCPKForInput, _ := cmd.Flags().GetBool(common.NoCPKForInputFlag)

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, keyVaultKey, err := common.GetKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetKeyvaultInfo", "cmd", cmd)
		os.Exit(1)
	}

	backupKVClient, err := keyvault.NewKeyVaultClientFromEnvironment(keyvaultURL)
	if err != nil {
		log.Error(err, "get Key backup Vault client", "backupKeyvaultURL", keyvaultURL)
		os.Exit(1)
	}

	if err := createKeyVaultKeyIfNotExist(ctx, backupKVClient, keyVaultKey); err != nil {
		log.Error(err, "cannot create keyvault key")
		os.Exit(1)
	}

	metadataKVKey, metadataWrappedKey, err := common.GetMetadataKeys()
	if err != nil {
		log.Error(err, "retrieve metadata keys")
		os.Exit(1)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo", "cmd", cmd)
		os.Exit(1)
	}

	requiredTags := make(map[string]string)
	var pipelineConfig *v1alpha1.PipelineConfig
	if configStorageAccountURL != "" {
		log.Info("Config file provided, this ACI will filter tags with the configuration provided by the config file", "storageAccount", configStorageAccountURL, "containerName", configContainerName, "blobName", configBlobName)
		wConfig := worker.ConfigContainer{
			StorageAccountURL: configStorageAccountURL,
			ContainerName:     configContainerName,
			BlobName:          configBlobName,
		}
		pipelineConfig, err = wConfig.Download(ctx)
		if err != nil {
			log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
			os.Exit(1)
		}

		// Add the optional tagFilters
		if pipelineConfig.Pipeline.TagsFilter != nil {
			log.Info("tagsFilter is not empty, these tags will be considered as required:", "tagsFilter", pipelineConfig.Pipeline.TagsFilter)
			for key, value := range pipelineConfig.Pipeline.TagsFilter {
				if value == "" {
					value = "no_value"
				}
				requiredTags[key] = value
			}
		} else {
			log.Info("pipelineconfig empty, no optional tag to use", "tagFilter", pipelineConfig.Pipeline.TagsFilter)
		}
		// "rd":"validated" is a mandatory tag
		requiredTags["rd"] = "validated"
	} else {
		log.Info("No config file was provided, this ACI will NOT use the tag filtering to process blobs.")
		requiredTags = nil
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainer(
		inputStorageAccountURL,
		inputContainerName,
		nil, nil, requiredTags)

	// start executing on blob send from watcher
	task := NewProducerEncryptTask(
		keyvaultURL,
		keyVaultKey,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		metadataKVKey,
		metadataWrappedKey,
		noCPKForInput,
	)

	if err := watchWorker.Run(ctx, task.Work, true); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}

	// execute listing if needed
	if catalogBlobName, err := cmd.Flags().GetString(common.CatalogBlobNameFlag); err == nil && catalogBlobName != "" {
		if err := runCatalog(cmd, outputStorageAccountURL, outputContainerName); err != nil {
			return fmt.Errorf("build catalog failed: %w", err)
		}
	}

	return nil
}

type ProducerEncryptTask struct {
	vaultURL                string
	keyVaultKey             string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
	metadataKVKey           string
	metadataWrappedKey      string
	noCPKForInput           bool
}

func NewProducerEncryptTask(
	vaultURL,
	keyVaultKey,
	inputContainerName,
	outputStorageAccountURL,
	outputContainerName,
	metadataKVKey,
	metadataWrappedKey string,
	noCPKForInput bool,
) *ProducerEncryptTask {
	return &ProducerEncryptTask{
		vaultURL,
		keyVaultKey,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
		metadataKVKey,
		metadataWrappedKey,
		noCPKForInput,
	}
}

func (t *ProducerEncryptTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob
	log := hdhlog.FromContext(ctx).With("sourceBlobName", b.Name())

	log.Info("Start")
	var err error

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}

	inputCPK := cpk

	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquire lease for blob", "blob", b.Name())
		return fmt.Errorf(workTaskErr)
	} else {
		log.Info("Lease acquired", "blobName", b.Name())
	}
	defer func() {
		err := b.ReleaseLease(context.Background())
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	outputContainerClient, err := azure.NewContainerClient(t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		log.Error(err, "create output container client")
		return fmt.Errorf(workTaskErr)
	}
	outputBlob := b.Clone(ctx, outputContainerClient, strings.TrimSuffix(b.Name(), ".gpg")+".gpg")

	reader, _, err := b.Download(ctx, inputCPK)
	if err != nil {
		log.Error(err, "can't download inblob")
		return fmt.Errorf(workTaskErr)
	}

	if err := outputBlob.EncryptAndUpload(ctx, reader, t.vaultURL, t.keyVaultKey, t.metadataKVKey, t.metadataWrappedKey); err != nil {
		log.Error(err, "fail to copy blob to encryption storage")
		return fmt.Errorf(workTaskErr)
	}

	if err := b.Delete(ctx); err != nil {
		log.Error(err, "DeleteBlobWithLease")
	} else {
		log.Info("Blob deleted successfully")
	}

	log.Info("successfully processed")
	return nil
}
