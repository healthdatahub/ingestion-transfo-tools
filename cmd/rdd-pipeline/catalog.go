package main

import (
	"fmt"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/catalog"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/spf13/cobra"
	"golang.org/x/sync/errgroup"
)

var catalogCmd = &cobra.Command{
	Use:   "catalog",
	Short: "Make a catalog in CSV from a container",
	Example: `rdd-pipeline catalog \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myInputContainer \
	--catalog-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--catalog-container myOutputContainer \
	--catalog-blob-name catalog.csv`,
	RunE: runCatalogCmd,
}

func init() {
	rootCmd.AddCommand(catalogCmd)

	common.AddInputBlobFlags(catalogCmd)
	common.AddCatalogBlobFlags(catalogCmd, true)
}

func runCatalogCmd(cmd *cobra.Command, args []string) error {
	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		return fmt.Errorf("GetInputStorageInfo: %w", err)
	}

	return runCatalog(cmd, inputStorageAccountURL, inputContainerName)
}

func runCatalog(cmd *cobra.Command, inputStorageAccountURL, inputContainerName string) error {
	ctx := cmd.Context()
	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	catalogStorageAccountURL, catalogContainerName, err := common.GetCatalogStorageInfo(cmd)
	if err != nil {
		return fmt.Errorf("GetCatalogStorageInfo: %w", err)
	}

	catalogBlobName, err := cmd.Flags().GetString(common.CatalogBlobNameFlag)
	if err != nil {
		return fmt.Errorf("GetCatalogBlobName: %w", err)
	}

	catalogContainerClient, err := azure.NewContainerClient(catalogStorageAccountURL, catalogContainerName)
	if err != nil {
		return fmt.Errorf("create output catalog container client: %w", err)
	}

	inputContainerClient, err := azure.NewContainerClient(inputStorageAccountURL, inputContainerName)
	if err != nil {
		return fmt.Errorf("create input catalog container client: %w", err)
	}

	c := catalog.NewCatalog()
	g, gctx := errgroup.WithContext(ctx)

	g.Go(func() error {
		if err := c.CreateStreamCatalog(gctx, inputContainerClient); err != nil {
			return fmt.Errorf("create catalog failed: %w", err)
		}
		return nil
	})

	g.Go(func() error {
		if err := c.UploadCatalog(gctx, catalogBlobName, catalogContainerClient); err != nil {
			return fmt.Errorf("upload create failed: %w", err)
		}
		return nil
	})

	if err := g.Wait(); err != nil {
		return err
	}

	log.Info("processed successfully")

	return nil
}
