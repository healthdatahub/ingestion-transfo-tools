package main

import (
	"context"
	"os"
	"os/signal"
	"strconv"

	hdhlog "rdd-pipeline/pkg/log"
	"rdd-pipeline/pkg/worker"

	"github.com/spf13/cobra"
)

const (
	workTaskErr = "errored during work function"
	runMaskErr  = "errored during runMask function"
)

// rootCmd represents the base command when called without any subcommands.
var rootCmd = &cobra.Command{
	Use:               "rdd-pipeline",
	Short:             "A simple CLI for Health Data Hub",
	PersistentPreRunE: rootPreRun,
	SilenceUsage:      true,
}

var (
	logLvl            int
	maxWorkers        int
	autoShutdownDelay int
	filterTags        bool
)

func init() {
	rootCmd.PersistentFlags().IntVarP(&logLvl, "log-level", "V", 1, "log level")
	rootCmd.PersistentFlags().BoolVar(&filterTags, "filter-with-tags", false, "enable the tags filter feature")
	rootCmd.PersistentFlags().IntVar(&maxWorkers, "max-workers", 0, "Number of worker to start (default to NumCPU)")
	rootCmd.PersistentFlags().IntVar(&autoShutdownDelay, "auto-shutdown", 1800, "Number of seconds idle before shutting down the ACI")
}

// main adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func main() {
	ctxCancel, cancel := context.WithCancel(context.Background())

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	go func() {
		select {
		case <-c:
			cancel()
		case <-ctxCancel.Done():
		}
	}()
	if err := rootCmd.ExecuteContext(ctxCancel); err != nil {
		os.Exit(1)
	}
}

func rootPreRun(cmd *cobra.Command, args []string) error {
	if err := cmd.ParseFlags(args); err != nil {
		return err
	}
	// add the command name has a permanent attribute logger
	// and attach a logger to the context
	log, logctx := hdhlog.
		New(os.Stdout, logLvl).
		With("cmd", cmd.CommandPath()).
		AttachToContext(cmd.Context())
	cmd.SetContext(logctx) // make it available to subcommands
	log.Info("starting command", "command-line", os.Args)

	// set global runner count
	if maxWorkers > 0 {
		worker.SetMaxWorkers(maxWorkers)
	}
	worker.SetAutoShutdownDelay(autoShutdownDelay)
	err := os.Setenv("USE_TAGS_FILTERS", strconv.FormatBool(filterTags))
	if err != nil {
		return err
	}
	return nil
}
