package main

import (
	"compress/gzip"
	"context"
	"crypto/cipher"
	"encoding/base64"
	"encoding/json"

	"fmt"
	"io"
	"io/ioutil"
	"os"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/hdhpackage"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"rdd-pipeline/pkg/worker"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/crypt/hdhcrypt"
	"rdd-pipeline/pkg/csv"
	"rdd-pipeline/pkg/dicomMask"
	v3 "rdd-pipeline/pkg/hdhpackage/v3"
	"rdd-pipeline/pkg/imageMask"
	hdhlog "rdd-pipeline/pkg/log"
	"rdd-pipeline/pkg/pairingPath"
	"rdd-pipeline/pkg/pathMask"

	"rdd-pipeline/pkg/jsonMask"
	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/masquage"
	"strings"

	"github.com/spf13/cobra"
)

var producerRotateCmd = &cobra.Command{
	Use:   "rotate",
	Short: "Rotates a blob's mask",
	RunE:  runHdhRotate,
	Example: `rdd-pipeline producer rotate \
	--config-storage-account-endpoint https://myConfigStorageAccount.core.windows.net \
	--config-container config \
	--config-blob-name hash.yaml \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--input-ct-storage-account-endpoint https://myCtInputStorageAccount.core.windows.net \
	--input-ct-container myContainer \
	--output-container myOutputContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--catalog-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--catalog-container myOutputContainer \
	--catalog-blob-name catalog.csv \
	--keyvault-endpoint https://myVault.vault.azure.net \
	--mask-prefix HDH`,
}

func init() {
	producerCmd.AddCommand(producerRotateCmd)

	common.AddInputBlobFlags(producerRotateCmd)
	common.AddOutputBlobFlags(producerRotateCmd)
	common.AddCTFlags(producerRotateCmd)
	common.AddKeyVaultFlags(producerRotateCmd)
	common.AddEncryptionKeyVaultFlags(producerRotateCmd)
	common.AddConfigFlags(producerRotateCmd)
	common.AddPrefixFlags(producerRotateCmd)
	common.AddCatalogBlobFlags(producerRotateCmd, false)

}

func runHdhRotate(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	maskPrefix, err := cmd.Flags().GetString(common.MaskPrefixFlag)
	if err != nil {
		log.Error(err, "Get mask prefix", "cmd", cmd)
		os.Exit(1)
	}

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	ctStorageAccountURL, ctContainerName, err := common.GetCtStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetCtStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	ctContainerClient, err := azure.NewContainerClient(ctStorageAccountURL, ctContainerName)
	if err != nil {
		log.Error(err, "create CT container client")
		return fmt.Errorf(workTaskErr)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	encryptionKeyvaultURL, err := common.GetEncryptionKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetEncryptionKeyvaultInfo", "cmd", cmd)
		os.Exit(1)
	}

	keyvaultURL, _, err := common.GetKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetKeyvaultInfo", "cmd", cmd)
		os.Exit(1)
	}

	metadataKVKey, metadataWrappedKey, err := common.GetMetadataKeys()
	if err != nil {
		log.Error(err, "retrieve metadata keys")
		os.Exit(1)
	}

	configStorageAccountURL, configContainerName, configBlobName, err := common.GetPipelineConfigInfo(cmd)
	if err != nil {
		log.Error(err, "GetPipelineConfigInfo", "cmd", cmd)
		os.Exit(1)
	}

	// Secret creation if they do not exist
	log.Info("Rotation, ensuring the AES keys are present. If not, they will be created")
	// Lease the lease container while parsing it to create the missing keys
	// We lease the container because leasing the file would require write access on it.
	// https://learn.microsoft.com/en-us/rest/api/storageservices/authorize-with-azure-active-directory#permissions-for-calling-blob-and-queue-data-operations
	confContainerClient, err := azure.NewContainerClient(configStorageAccountURL, leaseContainerName)
	if err != nil {
		log.Error(err, "creating container client for config file")
		return fmt.Errorf(runMaskErr)
	}

	// We lease this container to ensure that only one thread in only one ACI will do this at a specific moment, therefore avoiding concurrency
	if err := azure.AcquireContainerLeaseWithLoop(ctx, confContainerClient); err != nil {
		log.Error(err, "Acquiring lease", "ConfigStorageAccountUrl", configStorageAccountURL, "leaseContainerName", leaseContainerName)
		return fmt.Errorf(runMaskErr)
	}

	// The defer is here in case we have an error
	defer func() {
		err := azure.BreakContainerLease(ctx, confContainerClient)
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(keyvaultURL)
	if err != nil {
		log.Error(err, "get Key Vault client", "keyvaultURL", keyvaultURL)
		return fmt.Errorf(runMaskErr)
	}

	wConfig := worker.ConfigContainer{
		StorageAccountURL: configStorageAccountURL,
		ContainerName:     configContainerName,
		BlobName:          configBlobName,
	}
	pipelineConfig, err := wConfig.Download(ctx)
	if err != nil {
		log.Error(err, "DownloadConfig", "configStorageAccountURL", configStorageAccountURL, "configContainerName", configContainerName, "configBlobName", configBlobName)
		os.Exit(1)
	}

	// Get all the keys listed in the mask_producer.yaml file
	// Then for each of them, ensure that it exists, if not, create it.
	for _, h := range pipelineConfig.Hash {
		log.Info("Ensuring that the following secret exists", "key", h.Key.Secret)
		if err := createSecretIfNotExist(ctx, kvClient, h.Key.Secret, 32); err != nil {
			log.Error(err, "Cannot create secret")
			return fmt.Errorf("createSecretIfNotExist: %w", err)
		}
	}

	// This releaseLease is here in case we do NOT have an error
	if err := azure.BreakContainerLease(ctx, confContainerClient); err != nil {
		log.Error(err, "release lease failed")
	}

	// start watching in container for blob
	watchWorker := worker.NewProcessContainerWithConfig(
		inputStorageAccountURL,
		inputContainerName,
		pipelineConfig.Hash,
		nil, nil, nil, 0, 0)

	// start executing on blob sent from watcher
	task := NewProducerRotateTask(
		pipelineConfig,
		encryptionKeyvaultURL,
		keyvaultURL,
		inputContainerName,
		inputStorageAccountURL,
		ctStorageAccountURL,
		ctContainerName,
		outputStorageAccountURL,
		outputContainerName,
		metadataKVKey,
		metadataWrappedKey,
		maskPrefix,
		ctContainerClient,
	)

	if err := watchWorker.Run(ctx, task.Work, true); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}

	// execute listing if needed
	if catalogBlobName, err := cmd.Flags().GetString(common.CatalogBlobNameFlag); err == nil && catalogBlobName != "" {
		if err := runCatalog(cmd, inputStorageAccountURL, inputContainerName); err != nil {
			return fmt.Errorf("build catalog failed: %w", err)
		}
	}

	return nil
}

type ProducerRotateTask struct {
	config                  *v1alpha1.PipelineConfig
	encryptionVaultURL      string
	keyvaultURL             string
	inputContainerName      string
	inputStorageAccountURL  string
	ctStorageAccountURL     string
	ctContainerName         string
	outputStorageAccountURL string
	outputContainerName     string
	metadataKVKey           string
	metadataWrappedKey      string
	maskPrefix              string
	ctContainerClient       *container.Client
}

func NewProducerRotateTask(
	config *v1alpha1.PipelineConfig,
	encryptionVaultURL,
	keyvaultURL,
	inputContainerName,
	inputStorageAccountURL,
	ctStorageAccountURL,
	ctContainerName,
	outputStorageAccountURL,
	outputContainerName,
	metadataKVKey,
	metadataWrappedKey,
	maskPrefix string,
	ctContainerClient *container.Client,
) *ProducerRotateTask {
	return &ProducerRotateTask{
		config,
		encryptionVaultURL,
		keyvaultURL,
		inputContainerName,
		inputStorageAccountURL,
		ctStorageAccountURL,
		ctContainerName,
		outputStorageAccountURL,
		outputContainerName,
		metadataKVKey,
		metadataWrappedKey,
		maskPrefix,
		ctContainerClient,
	}
}

func (t *ProducerRotateTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	// Rotation includes:
	// - Decrypt data file
	// - Decrypt Ct file if present
	// - Execute pairing
	// - Execute mask aes

	// Cases for rotation:
	// hashkey version differs from the current one in the config file used by the maskprdc ACI
	// masktype is legacy
	// no hashkey version is defined

	///////////// 1. Data decryption /////////////
	b := in.(worker.Job).Blob
	log, ctx := hdhlog.FromContext(ctx).With("sourceBlobName", b.Name()).AttachToContext(ctx)
	log.Info("Start producer rotate")

	blobName := b.Name()

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "failed to get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}

	blobVersion := b.Metadata()["version"]
	if blobVersion != "v3" {
		log.Info("Wrong version: only v3 is allowed, this blob will not be processed")
		return nil
	}

	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquiring lease")
		return fmt.Errorf(workTaskErr)
	}
	defer func() {
		err := b.ReleaseLease(ctx)
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	// Check if a rotation is required (key in metadatas)
	keySecretName := b.Metadata()[v1alpha1.MetadataMaskKeyvaultKey]

	hashConfig, err := t.config.Hash.GetConfig(b.Name())
	if err != nil {
		log.Error(err, "retrieve Hash Config")
		return fmt.Errorf(workTaskErr)
	}

	// We operate a rotation if the key in the metadata differs with the one in the mask_producer.yaml file, or if we are in HMAC
	if keySecretName == "" || keySecretName != hashConfig.Key.Secret || b.Metadata()[v1alpha1.MetadataMaskType] == v1alpha1.Mask_legacy {
		log.Info("The hash key in the blob metadatas and the key in the config file are different, a rotation will be done.", "metadatas key", keySecretName, "config file key", hashConfig.Key.Secret)

		// Get wrapped key from encrypt
		wrappedKey, ok := b.Metadata()[t.metadataWrappedKey]
		if !ok {
			log.Error(err, "finding wrapped key in the metadatas", "metadataWrappedKey", t.metadataWrappedKey)
			return fmt.Errorf(workTaskErr)
		}

		wk, err := base64.StdEncoding.DecodeString(wrappedKey)
		if err != nil {
			log.Error(err, "b64 decoding wrapped-key", "metadataWrappedKey", t.metadataWrappedKey)
			return fmt.Errorf(workTaskErr)
		}

		// Get vault key used to encrypt
		keyvaultKey, ok := b.Metadata()[t.metadataKVKey]
		if !ok {
			log.Error(err, "finding key name from blob's metadatas", "metadataKVKey", t.metadataKVKey)
			return fmt.Errorf(workTaskErr)
		}
		log = log.With("metadataKVKey", t.metadataKVKey)

		kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(t.encryptionVaultURL)
		if err != nil {
			log.Error(err, "get Key Vault client", "encryptionVaultURL", t.encryptionVaultURL)
			return fmt.Errorf(workTaskErr)
		}

		// Get keyvault key version ID from metadatas
		var keyvaultKeyVersionID string
		if b.Metadata()[v1alpha1.MetadataEncryptionKeyvaultKeyVersion] != "" {
			keyvaultKeyVersionID = b.Metadata()[v1alpha1.MetadataEncryptionKeyvaultKeyVersion]
		} else {
			keyvaultKeyVersionID, err = kvClient.GetKeyFirstVersionID(ctx, keyvaultKey)
			if err != nil {
				log.Error(err, "get Key Vault key oldest version", "encryptionVaultURL", t.encryptionVaultURL)
				return fmt.Errorf(workTaskErr)
			}
		}

		outputContainerClient, err := azure.NewContainerClient(t.outputStorageAccountURL, t.outputContainerName)
		if err != nil {
			log.Error(err, "creating output container client")
			return fmt.Errorf(workTaskErr)
		}

		key, err := kvClient.UnWrap(ctx, keyvaultKey, keyvaultKeyVersionID, wk)
		if err != nil {
			log.Error(err, "unwrapping key")
			return fmt.Errorf(workTaskErr)
		}

		content, _, err := b.Download(ctx, cpk)
		if err != nil {
			log.Error(err, "can't download inblob")
			return fmt.Errorf(workTaskErr)
		}

		streamzipped, err := hdhcrypt.GetStreamReader(key, content)
		if err != nil {
			log.Error(err, "get stream reader from blob")
			return fmt.Errorf(workTaskErr)
		}

		var gzipReader io.Reader
		var inFile hdhpackage.HDHPackage

		if b.Metadata()[v1alpha1.MetadataPlatformCompressionType] == "gzip" || b.Metadata()[v1alpha1.MetadataPlatformCompressionType] == "" {
			gzipReader, err = gzipReaderStreamReader(streamzipped)
			if err != nil {
				log.Error(err, "gFailed to create gzip reader")
				return fmt.Errorf(workTaskErr)
			}
			inFile, err = v3.New(ctx, gzipReader, b)

		} else {
			inFile, err = v3.New(ctx, streamzipped, b)
		}

		if err != nil {
			log.Error(err, "can't create HDHPackage V3 from file")
			return fmt.Errorf(workTaskErr)
		}

		blobType := inFile.Metadata()["type"]

		var pathConfig *v1alpha1.PathConfig
		config := inFile.Config()
		if config != nil && config.PATH != nil {
			pathConfig = config.PATH
		}

		encryptionType, ok := inFile.Metadata()[v1alpha1.MetadataMaskType]
		if !ok {
			log.Info("No mask type metadata was found, legacy is then used") // The blobs lacking this metadata should only be legacy masked blobs
			encryptionType = v1alpha1.Mask_legacy
		}

		var ignoreValues []string

		// legacy format was value1::value2::value3::value4
		// but it was replaced by a json array in base64 to maximize compatibility.
		// In a few months, the legacy format handling will be removed.
		ignoreValuesMeta := b.Metadata()[v1alpha1.MetadataIgnoreValues]

		if ignoreValuesMeta != "" {
			if strings.Contains(ignoreValuesMeta, "::") {
				ignoreValues = strings.Split(ignoreValuesMeta, "::")
			} else {
				ignoreValuesDecoded, err := base64.StdEncoding.DecodeString(ignoreValuesMeta)
				if err != nil {
					log.Error(err, "cannot base64 decode ignore values", "ignoreValuesMeta", ignoreValuesMeta)
					return fmt.Errorf(workTaskErr)
				}
				if err := json.Unmarshal(ignoreValuesDecoded, &ignoreValues); err != nil {
					log.Error(err, "cannot unmarshal ignore values", "ignoreValuesMeta", ignoreValuesMeta)
					return fmt.Errorf(workTaskErr)
				}
			}
		}

		///////////// 2. CT decryption /////////////

		// Check if we have a CT
		fetchCt := false
		ct := new(masquage.CorrespondenceTable)
		var ctInBlob *worker.Blob
		// CT is required when:
		// We are in legacy mode, except for Image and Passthrough types where we need it only if there is a path mask used
		if encryptionType == v1alpha1.Mask_legacy &&
			((blobType != string(v1alpha1.Image) && blobType != string(v1alpha1.PassThrough)) ||
				((blobType == string(v1alpha1.Image) || blobType == string(v1alpha1.PassThrough)) && (pathConfig != nil && pathConfig.DepthLevel != nil))) {
			fetchCt = true

			ctInBlob, err = worker.ExistingBlob(ctx, t.ctContainerClient, b.Name(), cpk)
			if err != nil {
				log.Error(err, "retrieving CT")
				return fmt.Errorf(workTaskErr)
			}

			if err := ctInBlob.AcquireLease(ctx); err != nil {
				log.Error(err, "acquiring lease on CT")
				return fmt.Errorf(workTaskErr)
			}
			defer func() {
				err := ctInBlob.ReleaseLease(ctx)
				if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
					log.Error(err, "release lease on CT failed")
				}
			}()
			// Get wrapped key from encrypt
			ctWrappedKey, ok := ctInBlob.Metadata()[t.metadataWrappedKey]
			if !ok {
				log.Error(err, "finding ct wrapped key in the metadatas", "metadataWrappedKey", t.metadataWrappedKey)
				return fmt.Errorf(workTaskErr)
			}

			ctWk, err := base64.StdEncoding.DecodeString(ctWrappedKey)
			if err != nil {
				log.Error(err, "b64 decoding ct wrapped-key", "metadataWrappedKey", t.metadataWrappedKey)
				return fmt.Errorf(workTaskErr)
			}

			// Get vault key used to encrypt
			ctKeyvaultKey, ok := ctInBlob.Metadata()[t.metadataKVKey]
			if !ok {
				log.Error(err, "finding ct key name from blob's metadatas", "metadataKVKey", t.metadataKVKey)
				return fmt.Errorf(workTaskErr)
			}
			log = log.With("metadataKVKey", t.metadataKVKey)

			// Get keyvault key version ID from metadatas
			keyvaultCtKeyVersionID := ctInBlob.Metadata()[v1alpha1.MetadataEncryptionKeyvaultKeyVersion]
			if keyvaultCtKeyVersionID == "" {
				keyvaultCtKeyVersionID, err = kvClient.GetKeyFirstVersionID(ctx, keyvaultKey)
				if err != nil {
					log.Error(err, "get Key Vault key oldest version", "vaultURL", t.encryptionVaultURL)
					return fmt.Errorf(workTaskErr)
				}
			}

			ctKey, err := kvClient.UnWrap(ctx, ctKeyvaultKey, keyvaultCtKeyVersionID, ctWk)
			if err != nil {
				log.Error(err, "unwrapping key")
				return fmt.Errorf(workTaskErr)
			}

			ctContent, _, err := ctInBlob.Download(ctx, cpk)
			if err != nil {
				log.Error(err, "can't download ctInblob")
				return fmt.Errorf(workTaskErr)
			}

			ctStream, err := hdhcrypt.GetStreamReader(ctKey, ctContent)
			if err != nil {
				log.Error(err, "get ct stream reader from blob")
				return fmt.Errorf(workTaskErr)
			}

			ctData, err := ioutil.ReadAll(ctStream)
			if err != nil {
				fmt.Printf("Error reading ct data: %s", err)
				return fmt.Errorf(workTaskErr)
			}

			err = json.Unmarshal(ctData, ct)
			if err != nil {
				log.Error(err, "failed to Unmarshall ct", "ctblobName", blobName)
				return fmt.Errorf(workTaskErr)
			}

		} else {
			log.Info("no ct here")
		}

		///////////// 3. Execute Pairing /////////////
		var bKey []byte

		kvClientMaskprdc, err := keyvault.NewKeyVaultClientFromEnvironment(t.keyvaultURL)
		if err != nil {
			log.Error(err, "get Key Vault client", "keyvaultURL", t.keyvaultURL)
			return fmt.Errorf(workTaskErr)
		}

		// Get keyvault mask secret version ID from metadatas
		// If there is no such metadata, we use the oldest secret in the keyvault
		if encryptionType == v1alpha1.Mask_aes {

			keySecretVersion := inFile.Metadata()[v1alpha1.MetadataMaskKeyvaultKeyVersion]
			if keySecretVersion == "" {
				keySecretVersion, err = kvClientMaskprdc.GetSecretFirstVersionID(ctx, keySecretName)
				if err != nil {
					log.Error(err, "get Key Vault mask secret oldest version", "vaultURL", t.keyvaultURL)
					return fmt.Errorf(workTaskErr)
				}
			}

			bKey, err = kvClientMaskprdc.GetSecret(ctx, b.Metadata()[v1alpha1.MetadataMaskKeyvaultKey], keySecretVersion)
			if err != nil && (blobType != string(v1alpha1.Image)) && (blobType != string(v1alpha1.PassThrough)) {
				log.Error(err, "cannot retrieve hash key")
				return fmt.Errorf(workTaskErr)
			}
		}

		var reader io.Reader
		bKeyPairing := bKey
		originalEncryptionType := encryptionType
		switch blobType {
		case string(v1alpha1.CSV):
			log.Info("Run pairing CSV")

			var csvConfig *v1alpha1.CSVConfig

			if config := inFile.Config(); config != nil && config.CSV != nil {
				csvConfig = config.CSV
				log.Info("Read config from hdhpackage", "config", config)
			}

			log.Info("Use config for CSV pairing", "config", csvConfig)
			if csvConfig.Separator == "" {
				log.Error(err, "no separator found in config")
				return fmt.Errorf(workTaskErr)
			}

			if len(csvConfig.Columns) != 0 || len(csvConfig.ColumnNames) != 0 {
				reader = csv.NewPairingReader(log, inFile, csvConfig.Separator, csvConfig.SkipLines, csvConfig.Columns, csvConfig.ColumnNames, ignoreValues, *ct, encryptionType, string(bKey))
			} else {
				reader = inFile
			}
		case string(v1alpha1.PassThrough):
			log.Info("Run Move Task (file type is PassThrough)")
			reader = inFile
		case string(v1alpha1.Image):
			log.Info("Run Move Task (file type is Image)")
			reader = inFile
		case string(v1alpha1.Json):
			var jsonConfig *v1alpha1.JsonConfig

			if config := inFile.Config(); config != nil && config.Json != nil {
				jsonConfig = config.Json
				log.Info("Read config from hdhpackage", "config", config)
			}

			if len(jsonConfig.Tags) != 0 {
				reader = jsonMask.NewPairingReader(log, inFile, jsonConfig.Tags, ignoreValues, *ct, encryptionType, string(bKey))
			} else {
				reader = inFile
			}
		case string(v1alpha1.Dicom):
			log.Info("Run pairing Dicom")

			var dicomConfig *v1alpha1.DicomConfig

			if config := inFile.Config(); config != nil && config.Dicom != nil {
				dicomConfig = config.Dicom
				log.Info("Read config from hdhpackage", "config", config)
			}
			if len(dicomConfig.Tags) != 0 {
				reader = dicomMask.NewPairingReader(log, inFile, dicomConfig.Tags, ignoreValues, *ct, encryptionType, string(bKey))
			} else {
				reader = inFile
			}
		default:
			log.Error(err, "unsupported blob type", "blobType", blobType)
			return fmt.Errorf(workTaskErr)
		}

		currentBlobName := inFile.Name()

		delete(inFile.Metadata(), v1alpha1.MetadataKeyvaultKey)
		delete(inFile.Metadata(), v1alpha1.MetadataWrappedKey)
		delete(inFile.Metadata(), v1alpha1.MetadataMaskKeyvaultKey)
		inFile.Metadata()[v1alpha1.MetadataMaskType] = v1alpha1.Mask_aes

		// Execute mask (always aes) ////
		ctFileName := ""
		var readerMask io.Reader
		encryptionType = v1alpha1.Mask_aes

		// We now want the masking key. It should already exist as the initialisation of this ACI is creating it.
		bKey, err = kvClientMaskprdc.GetSecret(ctx, hashConfig.Key.Secret, "")
		if err != nil && (blobType != string(v1alpha1.PassThrough) || pathConfig != nil) {
			log.Error(err, "cannot retrieve hash key")
			return fmt.Errorf(workTaskErr)
		}

		switch blobType {
		case string(v1alpha1.CSV):
			createCT := true
			log.Info("Run Mask CSV")
			var csvConfig *v1alpha1.CSVConfig

			if config := inFile.Config(); config != nil && config.CSV != nil {
				csvConfig = config.CSV
				log.Info("Read config from hdhpackage", "config", config)
			}

			log.Info("Use config for CSV Hash", "config", csvConfig)

			if csvConfig == nil {
				log.Error(err, "hashconfig is empty (null)")
				return fmt.Errorf(workTaskErr)
			}

			if csvConfig.Separator == "" {
				log.Error(err, "no separator found in config")
				return fmt.Errorf(workTaskErr)
			}
			inFile.Config().CSV = csvConfig

			createCT = false

			if len(csvConfig.Columns) != 0 || len(csvConfig.ColumnNames) != 0 {
				readerMask, _ = csv.NewHashReader(log, reader, csvConfig.Separator, string(bKey), t.maskPrefix, csvConfig.SkipLines, csvConfig.Columns, csvConfig.ColumnNames, ignoreValues, *ct, createCT, ctFileName, encryptionType)
			} else {
				readerMask = reader
			}
		case string(v1alpha1.Image):
			log.Info("Run Mask Image")
			r, err := imageMask.RemoveMetadatas(log, inFile)
			if r == nil && err != nil {
				log.Error(err, "Mask Image not process!")
				return fmt.Errorf(workTaskErr)
			}
			log.Info("Mask image process with success!")
			readerMask = r
		case string(v1alpha1.PassThrough):
			log.Info("Run Move Task (file type is PassThrough)")
			readerMask = reader
		case string(v1alpha1.Json):
			log.Info("Run Mask Json")
			var jsonConfig *v1alpha1.JsonConfig

			if config := inFile.Config(); config != nil && config.Json != nil {
				jsonConfig = config.Json
				log.Info("Read config from hdhpackage", "config", config)
			}

			if jsonConfig == nil {
				log.Error(err, "no hashconfig found, please check your mask config file")
				return fmt.Errorf(workTaskErr)
			}

			log.Info("Use config for Json Hash", "config", jsonConfig)
			inFile.Config().Json = jsonConfig

			if len(jsonConfig.Tags) != 0 {
				readerMask = jsonMask.NewHashReader(log, reader, string(bKey), t.maskPrefix, jsonConfig.Tags, ignoreValues, *ct, t.ctContainerName, encryptionType)
			} else {
				readerMask = reader
			}
		case string(v1alpha1.Dicom):
			log.Info("Run Mask Dicom")
			var dicomConfig *v1alpha1.DicomConfig

			if config := inFile.Config(); config != nil && config.Dicom != nil {
				dicomConfig = config.Dicom
				log.Info("Read config from hdhpackage", "config", config)
			}

			if dicomConfig == nil {
				log.Error(err, "no hashconfig found, please check your mask config file")
				return fmt.Errorf(workTaskErr)
			}

			log.Info("Use config for Dicom Hash", "config", dicomConfig)
			inFile.Config().Dicom = dicomConfig

			if len(dicomConfig.Tags) != 0 {
				readerMask = dicomMask.NewHashReader(log, reader, string(bKey), t.maskPrefix, dicomConfig.Tags, ignoreValues, *ct, encryptionType)
			} else {
				readerMask = reader
			}
		default:
			log.Error(err, "unsupported blob type", "blobType", blobType, "blobName", blobName)
			return fmt.Errorf(workTaskErr)
		}

		// File name pairing and masking
		if pathConfig != nil && pathConfig.DepthLevel != nil {
			log.Info("Path masking config found, proceeding.")

			if pathConfig.Separator == "" {
				log.Error(err, "no separator found in config")
				return fmt.Errorf(workTaskErr)
			}

			currentBlobName, err = pairingPath.Process(log, currentBlobName, *ct, pathConfig.Separator, originalEncryptionType, string(bKeyPairing))
			if err != nil {
				log.Error(err, "pairing file name")
				return fmt.Errorf(workTaskErr)
			}

			currentBlobName = pathMask.Process(log, currentBlobName, pathConfig.Separator, pathConfig.DepthLevel, ignoreValues, string(bKey), t.maskPrefix, *ct, ctFileName, encryptionType)
			inFile.Config().PATH = pathConfig
		} else {
			log.Info("No path masking config found, skipping this step.")
		}

		// gzip //////////////
		var outputCompressionReader io.Reader

		if b.Metadata()[v1alpha1.MetadataPlatformCompressionType] == "gzip" || b.Metadata()[v1alpha1.MetadataPlatformCompressionType] == "" {
			outputCompressionReader, err = CompressReader(readerMask)
			if err != nil {
				log.Error(err, "can't gzip from rotated masked data")
				return fmt.Errorf(workTaskErr)
			}
		} else {
			outputCompressionReader = readerMask
		}

		/*var lease string
		// If we have a path mask/pairing, it means that the new blob name will differ from the current one, therefore we don't need to use the lease
		if currentBlobName != inFile.Name() {
			lease = ""
		} else {
			lease = *b.LeaseBlobClient().LeaseID()
		}*/
		b.Metadata()[v1alpha1.MetadataMaskKeyvaultKey] = hashConfig.Key.Secret

		// convert a slice of string into its JSON string representation
		jsonString, err := json.Marshal(ignoreValues)
		if err != nil {
			log.Error(err, "serializing ignore values")
			return fmt.Errorf(workTaskErr)
		}
		b.Metadata()[v1alpha1.MetadataIgnoreValues] = base64.StdEncoding.EncodeToString(jsonString)

		// Get the hash secret version and write it into the metadatas
		version, err := kvClientMaskprdc.GetSecretLastVersionID(ctx, hashConfig.Key.Secret)
		if err != nil {
			log.Error(err, "error while getting the mask secret version")
			return fmt.Errorf(workTaskErr)
		}
		b.Metadata()[v1alpha1.MetadataMaskKeyvaultKeyVersion] = string(version)

		outBlob := b.Clone(ctx, outputContainerClient, currentBlobName)
		// Clone leaseBlobClient only if a lease is present and on the same blob's name as the source
		if currentBlobName == inFile.Name() {
			outBlob.CopyLeaseBlobClient(b)
		}

		if err := outBlob.EncryptAndUpload(ctx, outputCompressionReader, t.encryptionVaultURL, keyvaultKey, t.metadataKVKey, t.metadataWrappedKey); err != nil {
			log.Error(err, "encryptAndStoreBlobFromReader")
			return fmt.Errorf(workTaskErr)
		}

		if fetchCt {
			if err := ctInBlob.Delete(ctx); err != nil {
				log.Error(err, "correspondence table deletion error")
			} else {
				log.Info("Correspondence table deleted successfully")
			}
		}
		if currentBlobName != inFile.Name() {
			if err := b.Delete(ctx); err != nil {
				log.Error(err, "Warning: Could not delete blob")
			} else {
				log.Info("Successful blob deletion")
			}
		}
	} else {
		log.Info("No rotation required for this blob.")

	}
	log.Info("successfully processed")

	return nil
}

func gzipReaderStreamReader(input cipher.StreamReader) (io.Reader, error) {
	gr, err := gzip.NewReader(&input)
	if err != nil {
		return nil, err
	}
	return gr, nil
}

func CompressReader(r io.Reader) (io.Reader, error) {
	pr, pw := io.Pipe()
	go func() {
		gw := gzip.NewWriter(pw)
		_, err := io.Copy(gw, r)
		if err != nil {
			return
		}
		if err := gw.Close(); err != nil {
			return
		}
		pw.Close()
	}()
	return pr, nil
}
