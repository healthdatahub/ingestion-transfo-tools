package main

import (
	"context"
	"fmt"
	"os"
	"regexp"
	"strings"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/worker"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"
	"github.com/spf13/cobra"
)

const (
	PreparationValidatedTagKeyCopy      = "prepa_copy"
	PreparationValidatedTagKeyOverwrite = "prepa_overwrite"
	PreparationValidatedTagKeyRename    = "prepa_rename"

	PreparationValidatedTagValueReady     = "ready"
	PreparationValidatedTagValueDone      = "done"
	PreparationValidatedTagValueOWFailure = "overwrite_failure"
)

var preparationValidatedCmd = &cobra.Command{
	Use:   "copy-validated",
	Short: "move files tagged as ready into a project",
	RunE:  runHdhPreparationValidated,
	Example: `rdd-pipeline preparation copy-validated
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--output-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--output-container myOutputContainer
	--filter-with-tags`,
}

func init() {
	preparationCmd.AddCommand(preparationValidatedCmd)

	common.AddInputBlobFlags(preparationValidatedCmd)
	common.AddOutputBlobFlags(preparationValidatedCmd)
}

func runHdhPreparationValidated(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	outputStorageAccountURL, outputContainerName, err := common.GetOutputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetOutputStorageInfo", "cmd", cmd)
		os.Exit(1)
	}

	// Log the past overwritten failures in the ACI logs so that ops projet
	// keep in mind those files are still not copied.
	requiredTags := map[string]string{
		PreparationValidatedTagKeyCopy: PreparationValidatedTagValueOWFailure,
	}
	watchWorker := worker.NewProcessContainer(
		inputStorageAccountURL,
		inputContainerName,
		nil, nil, requiredTags)

	if err := watchWorker.Run(ctx, func(ctx context.Context, in interface{}, out chan<- interface{}) error {
		b := in.(worker.Job).Blob
		log := hdhlog.FromContext(ctx)
		log = log.With("sourceBlobName", b.Name())
		log.Info("previous copy failed for blob because of an overwrite issue")
		return nil
	}, true); err != nil {
		return fmt.Errorf("running overwrite_failure worker : %w", err)
	}

	// Then do the copy for the ready files.
	requiredTags = map[string]string{
		PreparationValidatedTagKeyCopy: PreparationValidatedTagValueReady,
	}
	watchWorker = worker.NewProcessContainer(
		inputStorageAccountURL,
		inputContainerName,
		nil, nil, requiredTags)
	task := NewPreparationValidatedTask(
		inputStorageAccountURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
	)
	if err := watchWorker.Run(ctx, task.Work, true); err != nil {
		return fmt.Errorf("running ready worker : %w", err)
	}
	return nil
}

type PreparationValidatedTask struct {
	inputStorageAccountURL  string
	inputContainerName      string
	outputStorageAccountURL string
	outputContainerName     string
}

func NewPreparationValidatedTask(
	inputStorageAccountURL,
	inputContainerName,
	outputStorageAccountURL,
	outputContainerName string,
) *PreparationValidatedTask {
	return &PreparationValidatedTask{
		inputStorageAccountURL,
		inputContainerName,
		outputStorageAccountURL,
		outputContainerName,
	}
}

// Work copies a file from the input container to the output container.
// We first check if the destination blobname already exists.
// - If it doesn't: Copy the file; Change the "prepa_copy:ready" tag in "prepa_copy:done".
// - If it does, depending on the presence of "prepa_overwrite:true" tag either:
//   - Overwrite the file; Change the "prepa_copy:ready" tag in "prepa_copy:done"
//   - Do not overwrite the file ; change "prepa_copy:ready" tag in "prepa_copy:overwrite_failure" ;
//   - Log the failure in the ACI logs.
//
// The destination blobname can be enforced by the presence of a tag "prepa_rename:{...}"
func (t *PreparationValidatedTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {
	b := in.(worker.Job).Blob

	log, ctx := hdhlog.FromContext(ctx).With("sourceBlobName", b.Name()).AttachToContext(ctx)

	log.Info("Start")

	var canOverride = false
	if b.Tags()[PreparationValidatedTagKeyOverwrite] == "true" {
		canOverride = true
	}
	var destinationName = b.Name()
	if b.Tags()[PreparationValidatedTagKeyRename] != "" {
		destinationName = b.Tags()[PreparationValidatedTagKeyRename]
	}

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "failed to get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}
	inputCPK := cpk
	outputCPK := cpk

	outputContainerClient, err := azure.NewContainerClient(t.outputStorageAccountURL, t.outputContainerName)
	if err != nil {
		log.Error(err, "create Output container client")
		return fmt.Errorf(workTaskErr)
	}
	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquiring lease for blob", "b.Name()", b.Name())
		return fmt.Errorf(workTaskErr)
	}
	log.Info("lease acquired")
	defer func() {
		err := b.ReleaseLease(context.Background())
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	for k, v := range b.Tags() {
		if k == PreparationValidatedTagKeyOverwrite && v == "true" {
		}
		if k == PreparationValidatedTagKeyRename {
			destinationName = v
		}
	}

	var destinationAlreadyExists = true
	_, err = worker.ExistingBlob(ctx, outputContainerClient, destinationName, inputCPK)
	if bloberror.HasCode(err, bloberror.BlobNotFound) {
		destinationAlreadyExists = false
		log.Info("destination does not exit yet")
	} else if err != nil {
		log.Error(err, "can't getProperties from target blob")
		return fmt.Errorf(workTaskErr)
	}

	destBlob := b.Clone(ctx, outputContainerClient, destinationName)

	log.Info("copy parameters", "destinationAlreadyExists", destinationAlreadyExists, "canOverride", canOverride, "destinationName", destinationName)

	// If destination blob exists and can't be overwritten, set tag to overwrite_failure
	if destinationAlreadyExists && !canOverride {
		b.Tags()[PreparationValidatedTagKeyCopy] = PreparationValidatedTagValueOWFailure
		if err = b.PushTags(ctx); err != nil {
			log.Error(err, "can't setTags")
			return fmt.Errorf(workTaskErr)
		}
		log.Info("copy failed because destination blob exists and can't be overwritten")
		return nil
	}

	outBlob := b.Clone(ctx, outputContainerClient, destinationName)

	// we do a remote copy to avoid transfering data here
	if err := b.RemoteCopy(ctx, outBlob); err != nil {
		log.Error(err, "can't copy blob")
		return fmt.Errorf(workTaskErr)
	}

	// Add metadata to destination blob
	// - 'Tag_<SAPREFIX>_prepa_origin_blobname:{blobname}'
	// - 'Tag_<SAPREFIX>_prepa_origin_containername:{containername}'
	parts := strings.Split(b.URL(), "https://")
	subparts := strings.Split(parts[1], ".")
	re := regexp.MustCompile("[0-9]+")
	loc := re.FindStringIndex(subparts[0])
	saPrefix := "tag_" + subparts[0][loc[1]:] + "_"

	outBlob.Metadata()[saPrefix+"prepa_origin_blobname"] = b.Name()
	outBlob.Metadata()[saPrefix+"prepa_origin_containername"] = t.inputContainerName
	log.Info("metadata ready for destination blob", "metadata", outBlob.Metadata())
	if err := destBlob.PushMetadata(ctx, outputCPK); err != nil {
		log.Error(err, "can't setMetadata")
		return fmt.Errorf(workTaskErr)
	}
	log.Info("metadata set on dest blob", "metadata", outBlob.Metadata())
	b.Tags()[PreparationValidatedTagKeyCopy] = PreparationValidatedTagValueDone

	if err := b.PushTags(ctx); err != nil {
		log.Error(err, "can't setTags")
	}
	log.Info("tags set on input blob", "tags", b.Tags())
	log.Info("successfully processed")

	return nil
}
