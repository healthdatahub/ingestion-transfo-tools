package main

import (
	"archive/zip"
	"compress/gzip"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"

	common "rdd-pipeline/cmd"
	"rdd-pipeline/pkg/azure"
	"rdd-pipeline/pkg/crypt/hdhcrypt"
	"rdd-pipeline/pkg/schema/v1alpha1"
	"strings"
	"time"

	yaml "gopkg.in/yaml.v3"

	hdhlog "rdd-pipeline/pkg/log"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/bloberror"

	"github.com/Azure/azure-sdk-for-go/sdk/storage/azblob/container"

	"rdd-pipeline/pkg/keyvault"
	"rdd-pipeline/pkg/worker"

	"github.com/spf13/cobra"
)

var producerMigratePackageToV3Cmd = &cobra.Command{
	Use:   "migratev3",
	Short: "Migrate a hdhpackage to v3",
	RunE:  runHdhMigrateToV3,
	Example: `rdd-pipeline producer migratev3 \
	--input-storage-account-endpoint https://myInputStorageAccount.core.windows.net \
	--input-container myContainer \
	--catalog-storage-account-endpoint https://myOutputStorageAccount.core.windows.net \
	--catalog-container myOutputContainer \
	--catalog-blob-name catalog.csv \
	--keyvault-endpoint https://myVault.vault.azure.net `,
}

func init() {
	producerCmd.AddCommand(producerMigratePackageToV3Cmd)

	common.AddInputBlobFlags(producerMigratePackageToV3Cmd)
	common.AddKeyVaultFlags(producerMigratePackageToV3Cmd)
	producerMigratePackageToV3Cmd.Flags().String(common.CTAccountFlag, "", "Blob storage account Name")
	(cobra.MarkFlagRequired(producerMigratePackageToV3Cmd.Flags(), common.CTAccountFlag))

	producerMigratePackageToV3Cmd.Flags().String(common.CTContainerFlag, "", "container name where files are stored")
	(cobra.MarkFlagRequired(producerMigratePackageToV3Cmd.Flags(), common.CTContainerFlag))
	common.AddCatalogBlobFlags(producerMigratePackageToV3Cmd, false)

}

func runHdhMigrateToV3(cmd *cobra.Command, args []string) error {
	ctx := cmd.Context()

	log := hdhlog.FromContext(ctx)

	log.Info("Start")

	inputStorageAccountURL, inputContainerName, err := common.GetInputStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetInputStorageInfo")
		os.Exit(1)
	}

	ctStorageAccountURL, ctContainerName, err := common.GetCtStorageInfo(cmd)
	if err != nil {
		log.Error(err, "GetCtStorageInfo")
		os.Exit(1)
	}

	ctContainerClient, err := azure.NewContainerClient(ctStorageAccountURL, ctContainerName)
	if err != nil {
		log.Error(err, "create input container client")
		os.Exit(1)
	}

	encryptionKeyvaultURL, _, err := common.GetKeyvaultInfo(cmd)
	if err != nil {
		log.Error(err, "GetEncryptionKeyvaultInfo")
		os.Exit(1)
	}

	metadataKVKey, metadataWrappedKey, err := common.GetMetadataKeys()
	if err != nil {
		log.Error(err, "retrieve metadata keys")
		os.Exit(1)
	}

	kvClient, err := keyvault.NewKeyVaultClientFromEnvironment(encryptionKeyvaultURL)
	if err != nil {
		log.Error(err, "get Key Vault client", "keyvaultURL", encryptionKeyvaultURL)
		os.Exit(1)
	}

	// List the blobs in V1, V2 and V3 in the storage account to establish an advancement of the migration
	inputContainerClient, err := azure.NewContainerClient(inputStorageAccountURL, inputContainerName)
	if err != nil {
		log.Error(err, "create input container client")
		os.Exit(1)
	}

	ticker := time.NewTicker(1 * time.Hour)
	listBlobsVersions(ctx, inputContainerClient)

	go func() {
		for range ticker.C {
			listBlobsVersions(ctx, inputContainerClient)
		}
	}()
	///////////// End blobs version listing ///////////////

	// start watching in container for blob
	watchWorker := worker.NewProcessContainerWithConfig(
		inputStorageAccountURL,
		inputContainerName,
		nil,
		nil, nil, nil, 0, 0)

	// start executing on blob sent from watcher
	task := NewProducerMigrateTask(
		encryptionKeyvaultURL,
		inputContainerName,
		inputStorageAccountURL,
		ctContainerClient,
		inputContainerClient,
		inputContainerClient,
		metadataKVKey,
		metadataWrappedKey,
		kvClient,
	)

	if err := watchWorker.Run(ctx, task.Work, true); err != nil {
		return fmt.Errorf("running worker : %w", err)
	}

	// execute listing if needed
	if catalogBlobName, err := cmd.Flags().GetString(common.CatalogBlobNameFlag); err == nil && catalogBlobName != "" {
		fmt.Println("running catalog")
		if err := runCatalog(cmd, inputStorageAccountURL, inputContainerName); err != nil {
			return fmt.Errorf("build catalog failed: %w", err)
		}
	}

	return nil
}

type ProducerMigrateTask struct {
	encryptionVaultURL     string
	inputContainerName     string
	inputStorageAccountURL string
	ctContainerClient      *container.Client
	inputContainerClient   *container.Client
	outputContainerClient  *container.Client
	metadataKVKey          string
	metadataWrappedKey     string
	kvClient               *keyvault.KeyVaultClient
}

func NewProducerMigrateTask(
	encryptionVaultURL string,
	inputContainerName string,
	inputStorageAccountURL string,
	ctContainerClient *container.Client,
	inputContainerClient *container.Client,
	outputContainerClient *container.Client,
	metadataKVKey string,
	metadataWrappedKey string,
	kvClient *keyvault.KeyVaultClient,
) *ProducerMigrateTask {
	return &ProducerMigrateTask{
		encryptionVaultURL,
		inputContainerName,
		inputStorageAccountURL,
		ctContainerClient,
		inputContainerClient,
		outputContainerClient,
		metadataKVKey,
		metadataWrappedKey,
		kvClient,
	}
}

func (t *ProducerMigrateTask) Work(ctx context.Context, in interface{}, out chan<- interface{}) error {

	b := in.(worker.Job).Blob

	log := hdhlog.FromContext(ctx).With("sourceBlobName", b.Name())

	log.Info("Start")

	cpk, err := azure.NewClientProvidedKey()
	if err != nil {
		log.Error(err, "failed to get Client Provided Key")
		return fmt.Errorf(workTaskErr)
	}
	inputCPK := cpk
	outputCPK := cpk

	if err := b.AcquireLease(ctx); err != nil {
		log.Error(err, "acquiring lease for blob", "b.Name()", b.Name())
		return fmt.Errorf(workTaskErr)
	}
	defer func() {
		err := b.ReleaseLease(context.Background())
		if err != nil && bloberror.HasCode(err, bloberror.BlobNotFound) {
			log.Error(err, "release lease failed")
		}
	}()

	var blobVersion string
	if b.Metadata()["version"] == "" {
		blobVersion = "v1"
	} else {
		blobVersion = b.Metadata()["version"]
	}

	var blobNameGz string
	if blobVersion != "v1" && blobVersion != "v2" {
		log.Info("[WARNING] Blob version isn't v1 or v2, it won't be processed")
		return nil
	}

	// Get wrapped key from encrypt
	wrappedKey, ok := b.Metadata()[t.metadataWrappedKey]
	if !ok {
		log.Error(err, "finding wrapped key in the metadatas", "metadataWrappedKey", t.metadataWrappedKey)
		return fmt.Errorf(workTaskErr)
	}

	wk, err := base64.StdEncoding.DecodeString(wrappedKey)
	if err != nil {
		log.Error(err, "b64 decoding wrapped-key", "metadataWrappedKey", t.metadataWrappedKey)
		return fmt.Errorf(workTaskErr)
	}

	// Get vault key used to encrypt
	keyvaultKey, ok := b.Metadata()[t.metadataKVKey]
	if !ok {
		log.Error(err, "finding key name from blob's metadatas", "metadataKVKey", t.metadataKVKey)
		return fmt.Errorf(workTaskErr)
	}
	log = log.With("metadataKVKey", t.metadataKVKey)
	// Get keyvault key version ID from metadatas
	var keyvaultKeyVersionID string
	if b.Metadata()[v1alpha1.MetadataEncryptionKeyvaultKeyVersion] != "" {
		keyvaultKeyVersionID = b.Metadata()[v1alpha1.MetadataEncryptionKeyvaultKeyVersion]
	} else {
		keyvaultKeyVersionID, err = t.kvClient.GetKeyFirstVersionID(ctx, keyvaultKey)
		if err != nil {
			log.Error(err, "get Key Vault key oldest version", "vaultURL", t.encryptionVaultURL)
			return fmt.Errorf(workTaskErr)
		}
	}
	key, err := t.kvClient.UnWrap(ctx, keyvaultKey, keyvaultKeyVersionID, wk)

	if err != nil {
		log.Error(err, "unwrapping key")
		return fmt.Errorf(workTaskErr)
	}

	content, _, err := b.Download(ctx, inputCPK)
	if err != nil {
		log.Error(err, "can't download inblob")
		return fmt.Errorf(workTaskErr)
	}
	streamzipped, err := hdhcrypt.GetStreamReader(key, content) //cipher.StreamReader
	if err != nil {
		log.Error(err, "get stream reader from blob")
		return fmt.Errorf(workTaskErr)
	}

	log.Info("Downloading locally zip file")
	tempFile, err := os.CreateTemp("", "zipped")
	if err != nil {
		log.Error(err, "creating temp file")
	}
	defer os.Remove(tempFile.Name()) // clean up

	if _, err := io.Copy(tempFile, streamzipped); err != nil {
		log.Error(err, "copying to temp file")
		return fmt.Errorf("workTaskErr")
	}

	r, err := zip.OpenReader(tempFile.Name())
	if err != nil {
		log.Error(err, "opening zip reader")
		return fmt.Errorf("workTaskErr")
	}
	defer r.Close()

	if len(r.File) == 0 {
		log.Error(err, "no files in zip")
		return fmt.Errorf("workTaskErr")
	}

	var streamData, streamMetadata io.ReadCloser

	for _, f := range r.File {
		if blobVersion == "v1" && f.Name == "metadata" {
			streamMetadata, err = f.Open()
			if err != nil {
				log.Error(err, "Opening metadata file")
				return fmt.Errorf("workTaskErr")
			}
			defer streamMetadata.Close()
		}

		if f.Name == "data" {
			streamData, err = f.Open()
			if err != nil {
				log.Error(err, "Opening data file")
				return fmt.Errorf("workTaskErr")
			}
			defer streamData.Close()
		}
	}

	// v1 and v2 should both have a "data" file
	if streamData == nil {
		log.Error(errors.New("data file not found in zip"), "error with data file in blob")
		return fmt.Errorf("workTaskErr")
	}

	// V1 should also have a "metadata" file
	if blobVersion == "v1" && streamMetadata == nil {
		log.Error(errors.New("metadata file not found in zip"), "error with v1 metadata file in blob")
		return fmt.Errorf("workTaskErr")
	}

	if streamMetadata != nil {
		// Metadatas are in yaml in v1
		var data map[interface{}]interface{}
		dec := yaml.NewDecoder(streamMetadata)
		if err := dec.Decode(&data); err != nil {
			log.Error(err, "Decoding YAML")
			return fmt.Errorf("workTaskErr")
		}

		// Convert map to map[string]interface{} type
		convertedData := convertMapIface(data)

		// Convert map to JSON
		jsonData, err := json.Marshal(convertedData)
		if err != nil {
			log.Error(err, "Encoding JSON")
			return fmt.Errorf("workTaskErr")
		}
		jsonHashconfigMetadata := string(jsonData)
		fmt.Println("jsonHashconfigMetadata")
		b.Metadata()[v1alpha1.MetadataHashConfig] = jsonHashconfigMetadata
	}

	log.Info("Zip file downloaded")

	// We change the archive name to reflect the new compression type and upload it
	blobNameGz = strings.ReplaceAll(b.Name(), ".zip", ".gz")

	outBlob := b.Clone(ctx, t.inputContainerClient, blobNameGz)

	// source and target blobs should not have the same name, but if it happens, we need the lease from the original blob to upload on the target
	if blobNameGz == b.Name() {
		outBlob.CopyLeaseBlobClient(b)
	}

	outBlob.Metadata()[v1alpha1.MetadataVersion] = "v3"

	if err := outBlob.EncryptAndUpload(ctx, gzipReader(streamData), t.encryptionVaultURL, keyvaultKey, t.metadataKVKey, t.metadataWrappedKey); err != nil {
		log.Error(err, "encryptAndStoreBlobFromReader")
		return fmt.Errorf(workTaskErr)
	}

	encryptionType := b.Metadata()[v1alpha1.MetadataMaskType]
	if encryptionType == v1alpha1.Mask_aes {
		log.Info("Encryption type is aes, there is no ct to migrate")
	} else if blobNameGz == b.Name() {
		log.Info("Source and target blob have the same name, we do not rename the CT")
	} else {
		log.Info("Encryption type is legacy, there is a ct to migrate")

		inCT := worker.NewBlob(t.ctContainerClient, b.Name(), nil, nil)

		r, _, err := inCT.Download(ctx, inputCPK)
		if err != nil {
			log.Error(err, "can't read ct")
			return fmt.Errorf(workTaskErr)
		}
		outCT := inCT.Clone(ctx, t.ctContainerClient, blobNameGz)
		if err = outCT.Upload(ctx, r, outputCPK); err != nil {
			log.Error(err, "can't upload blob")
			return fmt.Errorf(workTaskErr)
		}
	}

	if blobNameGz != b.Name() {
		if err := b.Delete(ctx); err != nil {
			log.Error(err, "DeleteBlobWithLease")
		} else {
			log.Info("Blob deleted successfully")
		}
	}
	log.Info("successfully processed")

	return nil
}

func gzipReader(input io.ReadCloser) io.Reader {
	pr, pw := io.Pipe()
	go func() {
		defer input.Close()
		gw := gzip.NewWriter(pw)
		_, err := io.Copy(gw, input)
		if err != nil {
			pw.CloseWithError(err)
			return
		}
		gw.Close()
		pw.Close()
	}()
	return pr
}

func listBlobsVersions(ctx context.Context, inputContainerClient *container.Client) {
	log := hdhlog.FromContext(ctx)

	pager := inputContainerClient.NewListBlobsFlatPager(&container.ListBlobsFlatOptions{
		Include: container.ListBlobsInclude{
			Metadata: true,
			Versions: false,
		},
	})

	go func() {

		var amountV1, amountV2, amountV3, amountUnknown, totalSizeV1, totalSizeV2, totalSizeV3, totalSizeUnknown int64
		var blobMetadatas map[string]*string
		var blobVersion string

		for pager.More() {
			resp, err := pager.NextPage(ctx)
			if err != nil {

				if strings.Contains(err.Error(), "ContainerNotFound") {
					// Subcase to make the error message easier to read
					log.Error(err, "Container not found")
					os.Exit(1)

				}
				log.Error(err, "list segment")
				os.Exit(1)
			}

			log.Info("Found blobs", "count", len(resp.Segment.BlobItems))
			for _, blob := range resp.Segment.BlobItems {
				blobMetadatas = blob.Metadata
				if version, ok := blobMetadatas["version"]; ok && version != nil {
					blobVersion = *version
				} else {
					log.Info("blob has no version metadata, it will be ignored.", "BlobName", blob.Name)
					blobVersion = "unknown"
				}

				switch blobVersion {
				case "v1":
					amountV1++
					totalSizeV1 += *blob.Properties.ContentLength
				case "v2":
					amountV2++
					totalSizeV2 += *blob.Properties.ContentLength
				case "v3":
					amountV3++
					totalSizeV3 += *blob.Properties.ContentLength
				default:
					amountUnknown++
					totalSizeUnknown += *blob.Properties.ContentLength
				}
			}
		}

		// Calculate the total amount of blobs and the total size (except unknown).
		totalAmount := amountV1 + amountV2 + amountV3
		totalSize := totalSizeV1 + totalSizeV2 + totalSizeV3

		// Calculate the percentages.
		percentV1 := fmt.Sprintf("%.6f", float64(amountV1)/float64(totalAmount)*100)
		percentSizeV1 := fmt.Sprintf("%.6f", float64(totalSizeV1)/float64(totalSize)*100)

		percentV2 := fmt.Sprintf("%.6f", float64(amountV2)/float64(totalAmount)*100)
		percentSizeV2 := fmt.Sprintf("%.6f", float64(totalSizeV2)/float64(totalSize)*100)

		percentV3 := fmt.Sprintf("%.6f", float64(amountV3)/float64(totalAmount)*100)
		percentSizeV3 := fmt.Sprintf("%.6f", float64(totalSizeV3)/float64(totalSize)*100)

		log.Info("Blobs listed",
			"V1 amount", amountV1,
			"V1 amount percent", percentV1,
			"v1 totalSize", totalSizeV1,
			"V1 percent of total size", percentSizeV1,

			"V2 amount", amountV2,
			"V2 amount percent", percentV2,
			"v2 totalSize", totalSizeV2,
			"V2 percent of total size", percentSizeV2,

			"V3 amount", amountV3,
			"V3 amount percent", percentV3,
			"v3 totalSize", totalSizeV3,
			"V3 percent of total size", percentSizeV3,

			"Unknown type amount", amountUnknown,
			"Unknown type totalSize", totalSizeUnknown)
	}()

}

func convertMapIface(input map[interface{}]interface{}) map[string]interface{} {
	result := make(map[string]interface{})

	for key, value := range input {
		strKey, ok := key.(string)
		if !ok {
			panic("key is not a string")
		}

		switch typedValue := value.(type) {
		case map[interface{}]interface{}:
			result[strKey] = convertMapIface(typedValue)
		default:
			result[strKey] = value
		}
	}

	return result
}
